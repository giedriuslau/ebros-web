<?php get_header(); ?>

<?php the_post(); ?>
<?php fildisi_eutf_print_header_title( 'portfolio' ); ?>
<?php fildisi_eutf_print_header_breadcrumbs( 'portfolio' ); ?>
<?php fildisi_eutf_print_anchor_menu( 'portfolio' ); ?>

<?php
	$eut_disable_portfolio_recent = fildisi_eutf_post_meta( '_fildisi_eutf_disable_recent_entries' );
	$eut_disable_comments = fildisi_eutf_post_meta( '_fildisi_eutf_disable_comments' );
	$eut_portfolio_media = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_media_selection' );
	$portfolio_media_fullwidth = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_media_fullwidth', 'no' );
	$eut_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'portfolio_layout', 'none' ) );
	$eut_sidebar_extra_content = fildisi_eutf_check_portfolio_details();
	$eut_portfolio_details_sidebar = false;
	if( $eut_sidebar_extra_content && 'none' == $eut_sidebar_layout ) {
		$eut_portfolio_details_sidebar = true;
	}


	$portfolio_media_classes = array( 'eut-portfolio-media' );
	if( 'yes' == $portfolio_media_fullwidth ){
		array_push( $portfolio_media_classes, 'eut-section', 'eut-fullwidth');
	}
	if ( $eut_portfolio_details_sidebar ) {
		array_push( $portfolio_media_classes, 'eut-without-sidebar' );
	} else {
		array_push( $portfolio_media_classes, 'eut-with-sidebar' );
	}

	$portfolio_media_class_string = implode( ' ', $portfolio_media_classes );

	$eut_social_bar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_social_bar_layout', fildisi_eutf_option( 'portfolio_social_bar_layout', 'layout-1' ) );
?>

<div class="eut-single-wrapper">
	<?php
		if ( $eut_portfolio_details_sidebar && 'none' != $eut_portfolio_media ) {
	?>
		<div id="eut-single-media" class="<?php echo esc_attr( $portfolio_media_class_string ); ?>">
			<div class="eut-container">
				<?php fildisi_eutf_print_portfolio_media(); ?>
			</div>
		</div>
	<?php
		}
	?>
	<!-- CONTENT -->
	<div id="eut-content" class="clearfix <?php echo fildisi_eutf_sidebar_class( 'portfolio' ); ?>">
		<div class="eut-content-wrapper">
			<!-- MAIN CONTENT -->
			<div id="eut-main-content">
				<div class="eut-main-content-wrapper clearfix">

					<article id="post-<?php the_ID(); ?>" <?php post_class('eut-single-porfolio'); ?>>
						<?php
							if ( !$eut_portfolio_details_sidebar && 'none' != $eut_portfolio_media ) {
						?>
							<div id="eut-single-media" class="<?php echo esc_attr( $portfolio_media_class_string ); ?>">
								<div class="eut-container">
									<?php fildisi_eutf_print_portfolio_media(); ?>
								</div>
							</div>
						<?php
							}
						?>
						<div id="eut-post-content">
							<?php the_content(); ?>
						</div>
					</article>

				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<?php
				if ( $eut_portfolio_details_sidebar ) {
			?>
				<aside id="eut-sidebar">
					<?php fildisi_eutf_print_portfolio_details(); ?>
				</aside>
			<?php
				} else {
					fildisi_eutf_set_current_view( 'portfolio' );
					get_sidebar();
				}
			?>
		</div>

	</div>
	<!-- End CONTENT -->


	<?php if ( 'layout-1' == $eut_social_bar_layout && fildisi_eutf_social_bar ( 'portfolio', 'check' ) ) { ?>
	<div id="eut-socials-section" class="eut-align-center clearfix">
		<div class="eut-container">
		<?php fildisi_eutf_social_bar ( 'portfolio' ); ?>
		</div>
	</div>
	<?php } ?>

	<?php if ( fildisi_eutf_visibility( 'portfolio_comments_visibility' ) && 'yes' != $eut_disable_comments ) { ?>
	<div id="eut-comments-section">
		<?php comments_template(); ?>
	</div>
	<?php } ?>

	<?php
	//Recent Items
	$eut_disable_portfolio_recent = fildisi_eutf_post_meta( '_fildisi_eutf_disable_recent_entries' );
	if ( fildisi_eutf_visibility( 'portfolio_recents_visibility' ) && 'yes' != $eut_disable_portfolio_recent ) {
		fildisi_eutf_print_recent_portfolio_items_classic();
	}
	?>

	<?php
	//Navigation Bar
	fildisi_eutf_nav_bar( 'portfolio' );
	?>

</div>
<?php get_footer();

//Omit closing PHP tag to avoid accidental whitespace output errors.
