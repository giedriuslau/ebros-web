<?php
/**
 *  Dynamic css style for WooCommerce
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
 */

$css = "";


/* Container Size
============================================================================= */
$css .= "

.eut-woo-error,
.eut-woo-info,
.eut-woo-message,
.eut-woo-tabs #tab-reviews.panel,
.eut-woo-tabs #tab-additional_information.panel {
	max-width: " . fildisi_eutf_option( 'container_size', 1170 ) . "px;
}

";

/* Default Header Shopping Cart
============================================================================= */
$eut_header_mode = fildisi_eutf_option( 'header_mode', 'default' );
if ( 'default' == $eut_header_mode ) {

	$css .= "
	#eut-header .eut-shoppin-cart-content {
		background-color: " . fildisi_eutf_option( 'default_header_submenu_bg_color' ) . ";
	}

	#eut-header .eut-shoppin-cart-content ul li,
	#eut-header .eut-shoppin-cart-content ul li a,
	#eut-header .eut-shoppin-cart-content .total {
		color: " . fildisi_eutf_option( 'default_header_submenu_text_color' ) . ";
	}

	#eut-header .eut-shoppin-cart-content ul li a:hover {
		color: " . fildisi_eutf_option( 'default_header_submenu_text_hover_color' ) . ";
	}

	#eut-header .eut-shoppin-cart-content ul li {
		border-color: " . fildisi_eutf_option( 'default_header_submenu_border_color' ) . ";
	}

	";

/* Logo On Top Header Shopping Cart
============================================================================= */
} else if ( 'logo-top' == $eut_header_mode ) {

	$css .= "
	#eut-header .eut-shoppin-cart-content {
		background-color: " . fildisi_eutf_option( 'logo_top_header_submenu_bg_color' ) . ";
	}

	#eut-header .eut-shoppin-cart-content ul li,
	#eut-header .eut-shoppin-cart-content ul li a,
	#eut-header .eut-shoppin-cart-content .total {
		color: " . fildisi_eutf_option( 'logo_top_header_submenu_text_color' ) . ";
	}

	#eut-header .eut-shoppin-cart-content ul li a:hover {
		color: " . fildisi_eutf_option( 'logo_top_header_submenu_text_bg_hover_color' ) . ";
	}

	#eut-header .eut-shoppin-cart-content ul li {
		border-color: " . fildisi_eutf_option( 'logo_top_header_submenu_border_color' ) . ";
	}

	";

}


/* Cart Area Colors
============================================================================= */
$eut_sliding_area_overflow_background_color = fildisi_eutf_option( 'sliding_area_overflow_background_color', '#000000' );
$css .= "
#eut-cart-area {
	background-color: " . fildisi_eutf_option( 'sliding_area_background_color' ) . ";
	color: " . fildisi_eutf_option( 'sliding_area_text_color' ) . ";
}

.eut-cart-total {
	color: " . fildisi_eutf_option( 'sliding_area_title_color' ) . ";
}

#eut-cart-area .cart-item-content a,
#eut-cart-area .eut-empty-cart .eut-h6 {
	color: " . fildisi_eutf_option( 'sliding_area_title_color' ) . ";
}

#eut-cart-area .eut-empty-cart a {
	color: " . fildisi_eutf_option( 'sliding_area_link_color' ) . ";
}

#eut-cart-area .cart-item-content a:hover,
#eut-cart-area .eut-empty-cart a:hover {
	color: " . fildisi_eutf_option( 'sliding_area_link_hover_color' ) . ";
}

#eut-cart-area .eut-close-btn:after,
#eut-cart-area .eut-close-btn:before,
#eut-cart-area .eut-close-btn span {
	background-color: " . fildisi_eutf_option( 'sliding_area_close_btn_color' ) . ";
}

#eut-cart-area .eut-border {
	border-color: " . fildisi_eutf_option( 'sliding_area_border_color' ) . ";
}

#eut-cart-area-overlay {
	background-color: rgba(" . fildisi_eutf_hex2rgb( $eut_sliding_area_overflow_background_color ) . "," . fildisi_eutf_option( 'sliding_area_overflow_background_color_opacity', '0.9') . ");
}

";


/* Primary Background */
$css .= "

.woocommerce .widget_price_filter .ui-slider .ui-slider-range,
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
.eut-product-item span.onsale {
	background-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	color: #ffffff;
}

";


/* Primary Color */
$css .= "
.woocommerce-grouped-product-list-item label a:hover,
.woocommerce nav.woocommerce-pagination ul li span.current,
nav.woocommerce-pagination ul li a:hover,
.woocommerce-MyAccount-navigation ul li a:hover,
.woocommerce .widget_layered_nav ul li.chosen a:before,
.woocommerce .widget_layered_nav_filters ul li a:before {
	color: " . fildisi_eutf_option( 'body_primary_1_color' ) . "!important;
}

";


/* Content Color
============================================================================= */
$css .= "
nav.woocommerce-pagination ul li a {
	color: " . fildisi_eutf_option( 'body_text_color' ) . ";
}

";


/* Headers Color
============================================================================= */
$css .= "

.woocommerce #reviews #comments ol.commentlist li .comment-text p.meta,
.eut-product-item .eut-add-to-cart-btn a.add_to_cart_button:before,
.woocommerce form .form-row label {
	color: " . fildisi_eutf_option( 'body_heading_color' ) . ";
}

";

// Product Anchor Size
$css .= "

#eut-product-anchor {
	height: " . intval( fildisi_eutf_option( 'product_anchor_menu_height', 120 ) + 2 ) . "px;
}

#eut-product-anchor .eut-anchor-wrapper {
	line-height: " . fildisi_eutf_option( 'product_anchor_menu_height' ) . "px;
}

#eut-product-anchor.eut-anchor-menu .eut-anchor-btn {
	width: " . fildisi_eutf_option( 'product_anchor_menu_height' ) . "px;
}

";

/* Borders
============================================================================= */
$css .= "

.woocommerce-grouped-product-list-item,
.woocommerce-tabs,
.woocommerce #reviews #review_form_wrapper,
.woocommerce-page #reviews #review_form_wrapper,
.woocommerce-MyAccount-navigation ul li,
#eut-theme-wrapper .widget.woocommerce li,
#eut-theme-wrapper .woocommerce table,
#eut-theme-wrapper .woocommerce table tr,
#eut-theme-wrapper .woocommerce table th,
#eut-theme-wrapper .woocommerce table td,
.woocommerce table.shop_attributes,
.woocommerce table.shop_attributes tr,
.woocommerce table.shop_attributes th,
.woocommerce table.shop_attributes td {
	border-color: " . fildisi_eutf_option( 'body_border_color' ) . ";
}

";

/* H6 */
$css .= "

.woocommerce #reviews #comments ol.commentlist li .comment-text p.meta {
	font-family: " . fildisi_eutf_option( 'h6_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'h6_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'h6_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'h6_font', '56px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'h6_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'h6_font', '60px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'h6_font', '0px', 'letter-spacing'  ) . "
}

";


/* Subtitle Text */
$css .= "
.woocommerce-grouped-product-list-item label a {
	font-family: " . fildisi_eutf_option( 'subtitle_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'subtitle_text', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'subtitle_text', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'subtitle_text', '34px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'subtitle_text', 'none', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'subtitle_text', '36px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'subtitle_text', '0px', 'letter-spacing'  ) . "
}
";


/* Small Text */
$css .= "

.woocommerce span.onsale,
.widget.woocommerce .chosen,
.widget.woocommerce .price_label,
.eut-add-cart-wrapper a,
.woocommerce-terms-and-conditions-wrapper label {
	font-family: " . fildisi_eutf_option( 'small_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'small_text', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'small_text', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'small_text', '34px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'small_text', 'none', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'small_text', '0px', 'letter-spacing'  ) . "
}


";

/* Link Text */
$css .= "

.woocommerce-pagination,
.woocommerce form .eut-billing-content .form-row label,
.eut-woo-error a.button,
.eut-woo-info a.button,
.eut-woo-message a.button,
.woocommerce-review-link,
.woocommerce #eut-theme-wrapper #respond input#submit,
.woocommerce #eut-theme-wrapper a.button,
.woocommerce #eut-theme-wrapper button.button,
.woocommerce #eut-theme-wrapper input.button,
.woocommerce-MyAccount-content a.button,
.woocommerce .woocommerce-error a.button,
.woocommerce .woocommerce-info a.button,
.woocommerce .woocommerce-message a.button {
	font-family: " . fildisi_eutf_option( 'link_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'link_text', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'link_text', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'link_text', '13px', 'font-size'  ) . " !important;
	text-transform: " . fildisi_eutf_option( 'link_text', 'uppercase', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'link_text', '0px', 'letter-spacing'  ) . "
}

";


/* Product Area Colors
============================================================================= */

function fildisi_eutf_print_product_area_css() {

$css = '';

	$fildisi_eutf_colors = fildisi_eutf_get_color_array();

	$mode = 'product';
	$fildisi_eutf_area_colors = array(
		'bg_color' => fildisi_eutf_option( $mode . '_area_bg_color', '#eeeeee' ),
		'headings_color' => fildisi_eutf_option( $mode . '_area_headings_color', '#000000' ),
		'font_color' => fildisi_eutf_option( $mode . '_area_font_color', '#999999' ),
		'link_color' => fildisi_eutf_option( $mode . '_area_link_color', '#FF7D88' ),
		'hover_color' => fildisi_eutf_option( $mode . '_area_hover_color', '#000000' ),
		'border_color' => fildisi_eutf_option( $mode . '_area_border_color', '#e0e0e0' ),
		'button_color' => fildisi_eutf_option( $mode . '_area_button_color', 'primary-1' ),
		'button_hover_color' => fildisi_eutf_option( $mode . '_area_button_hover_color', 'black' ),
	);

	$fildisi_eutf_single_area_colors = fildisi_eutf_post_meta( '_fildisi_eutf_area_colors' );
	$fildisi_eutf_single_area_colors_custom = fildisi_eutf_array_value( $fildisi_eutf_single_area_colors, 'custom' );

	if ( 'custom' == $fildisi_eutf_single_area_colors_custom ) {
		$fildisi_eutf_area_colors = $fildisi_eutf_single_area_colors;
	}


$css .= "

.eut-product-area-wrapper {
	background-color: " . fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'bg_color' ) . ";
	color: " . fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'font_color' ) . ";
	border-color: " . fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'border_color' ) . ";
}

#eut-theme-wrapper .eut-product-area-wrapper .eut-border,
#eut-theme-wrapper .eut-product-area-wrapper form,
#eut-theme-wrapper .eut-product-area-wrapper .quantity,
.eut-product-area-wrapper .eut-product-form,
#eut-entry-summary,
#eut-theme-wrapper .summary input,
#eut-theme-wrapper .summary select {
	border-color: " . fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'border_color' ) . ";
}

.eut-product-area-wrapper a {
	color: " . fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'link_color' ) . ";
}

.eut-product-area-wrapper a:hover {
	color: " . fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'hover_color' ) . ";
}

.eut-product-area-wrapper h1,
.eut-product-area-wrapper h2,
.eut-product-area-wrapper h3,
.eut-product-area-wrapper h4,
.eut-product-area-wrapper h5,
.eut-product-area-wrapper h6,
.eut-product-area-wrapper .eut-h1,
.eut-product-area-wrapper .eut-h2,
.eut-product-area-wrapper .eut-h3,
.eut-product-area-wrapper .eut-h4,
.eut-product-area-wrapper .eut-h5,
.eut-product-area-wrapper .eut-h6,
.eut-product-area-wrapper .eut-heading-color {
    color: " . fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'headings_color' ) . ";
}

";

$default_button_color = fildisi_eutf_option( 'body_primary_1_color' );
$area_button_color = fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'button_color' );
$button_color = fildisi_eutf_array_value( $fildisi_eutf_colors, $area_button_color, $default_button_color);
$area_button_hover_color = fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'button_hover_color' );
$button_hover_color = fildisi_eutf_array_value( $fildisi_eutf_colors, $area_button_hover_color, '#000000');

$fildisi_button_css = "";
$fildisi_button_css .= "#eut-theme-wrapper .eut-product-area-wrapper #eut-entry-summary button.single_add_to_cart_button {";
$fildisi_button_css .= "background-color: " . esc_attr( $button_color ) . ";";
$fildisi_button_css .= "border: none;";
if ( 'white' == $area_button_color ) {
	$fildisi_button_css .= "color: #bababa;";
} else {
	$fildisi_button_css .= "color: #ffffff;";
}
$fildisi_button_css .= "}";

$fildisi_button_css .= "#eut-theme-wrapper .eut-product-area-wrapper #eut-entry-summary button.single_add_to_cart_button:hover {";
$fildisi_button_css .= "background-color: " . esc_attr( $button_hover_color ) . ";";
if ( 'white' == $area_button_hover_color ) {
	$fildisi_button_css .= "color: #bababa;";
} else {
	$fildisi_button_css .= "color: #ffffff;";
}
$fildisi_button_css .= "}";

$css .= $fildisi_button_css;

return $css;

}

if ( is_product() ) {
	$css .= fildisi_eutf_print_product_area_css();
}


/* Product Navigation Bar
============================================================================= */

if ( 'layout-1' == fildisi_eutf_option( 'product_nav_bar_layout', 'layout-1' ) ) {
	$css .= "
	#eut-product-bar .eut-post-bar-item:not(.eut-post-navigation),
	#eut-product-bar .eut-post-bar-item .eut-nav-item {
		padding-top: " . fildisi_eutf_option( 'product_nav_spacing', '', 'padding-top' ) . ";
		padding-bottom: " . fildisi_eutf_option( 'product_nav_spacing', '', 'padding-bottom'  ) . ";
	}
	";
}

$css .= "
#eut-product-bar,
#eut-product-bar.eut-layout-3 .eut-post-bar-item .eut-item-icon,
#eut-product-bar.eut-layout-3 .eut-post-bar-item {
	background-color: " . fildisi_eutf_option( 'product_bar_background_color' ) . ";
	border-color: " . fildisi_eutf_option( 'product_bar_border_color' ) . ";
}

#eut-product-bar .eut-post-bar-item,
#eut-product-bar.eut-layout-1 .eut-post-bar-item .eut-nav-item,
#eut-product-bar.eut-layout-2:not(.eut-nav-columns-1) .eut-post-bar-item .eut-next,
#eut-product-bar.eut-layout-2.eut-nav-columns-1 .eut-post-bar-item .eut-prev + .eut-next  {
	border-color: " . fildisi_eutf_option( 'product_bar_border_color' ) . ";
}

#eut-product-bar .eut-nav-item .eut-title {
	color: " . fildisi_eutf_option( 'product_bar_nav_title_color' ) . ";
}

#eut-product-bar .eut-bar-socials li {
	border-color: " . fildisi_eutf_option( 'product_bar_border_color' ) . ";
}

#eut-product-bar .eut-arrow,
#eut-product-bar.eut-layout-3 .eut-post-bar-item .eut-item-icon {
	color: " . fildisi_eutf_option( 'product_bar_arrow_color' ) . ";
}

#eut-product-bar .eut-backlink-icon {
	background-color: " . fildisi_eutf_option( 'product_bar_nav_title_color' ) . ";
	box-shadow: .5em 0 " . fildisi_eutf_option( 'product_bar_nav_title_color' ) . ", 0 .5em " . fildisi_eutf_option( 'product_bar_nav_title_color' ) . ", .5em .5em " . fildisi_eutf_option( 'product_bar_nav_title_color' ) . ";
}

#eut-product-bar .eut-backlink:hover .eut-backlink-icon {
	box-shadow: .8em 0 " . fildisi_eutf_option( 'product_bar_nav_title_color' ) . ", 0 .8em " . fildisi_eutf_option( 'product_bar_nav_title_color' ) . ", .8em .8em " . fildisi_eutf_option( 'product_bar_nav_title_color' ) . ";
}

";

/* Single Product Content Width
============================================================================= */
if ( is_singular( 'product' ) ) {
	$fildisi_eutf_post_content_width = fildisi_eutf_post_meta( '_fildisi_eutf_post_content_width', fildisi_eutf_option( 'product_content_width', 970 ) );

	if ( !is_numeric( $fildisi_eutf_post_content_width ) ) {
		$fildisi_eutf_post_content_width = fildisi_eutf_option( 'container_size', 1170 );
	}

$css .= "

.single-product #eut-content:not(.eut-right-sidebar):not(.eut-left-sidebar) .eut-container {
	max-width: " . esc_attr( $fildisi_eutf_post_content_width ) . "px;
}

";

}

wp_add_inline_style( 'fildisi-eutf-custom-style', fildisi_eutf_compress_css( $css ) );

//Omit closing PHP tag to avoid accidental whitespace output errors.
