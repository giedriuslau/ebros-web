<?php
/**
 *  Add Dynamic css to header
 *  @version	1.0
 *  @author		Euthemians Team
 *  @URI		http://euthemians.com
 */


if ( !function_exists( 'fildisi_eutf_load_dynamic_css' ) ) {

	function fildisi_eutf_load_dynamic_css() {
		include_once get_template_directory() . '/includes/eut-dynamic-typography-css.php';
		include_once get_template_directory() . '/includes/eut-dynamic-css.php';
		if ( fildisi_eutf_woocommerce_enabled() ) {
			include_once get_template_directory() . '/includes/eut-dynamic-woo-css.php';
		}
		if ( fildisi_eutf_events_calendar_enabled() ) {
			include_once get_template_directory() . '/includes/eut-dynamic-event-css.php';
		}
		if ( fildisi_eutf_bbpress_enabled() ) {
			include_once get_template_directory() . '/includes/eut-dynamic-bbpress-css.php';
		}

		$custom_css_code = fildisi_eutf_option( 'css_code' );
		if ( !empty( $custom_css_code ) ) {
			wp_add_inline_style( 'fildisi-eutf-custom-style', fildisi_eutf_compress_css( $custom_css_code ) );
		}

		fildisi_eutf_add_custom_page_css();
		fildisi_eutf_get_global_button_style();
		fildisi_eutf_get_global_shape_style();
	}
}

function fildisi_eutf_add_custom_page_css( $id = null ) {

	$fildisi_eutf_custom_css = '';
	$fildisi_eutf_woo_shop = fildisi_eutf_is_woo_shop();

	if ( is_front_page() && is_home() ) {
		// Default homepage
		$mode = 'blog';
	} else if ( is_front_page() ) {
		// static homepage
		$mode = 'page';
	} else if ( is_home() ) {
		// blog page
		$mode = 'blog';
	} else if ( is_search() ) {
		$mode = 'search_page';
	} else if ( fildisi_eutf_is_bbpress() ) {
		$mode = 'forum';
	} else if ( is_singular() || $fildisi_eutf_woo_shop ) {
		if ( is_singular( 'post' ) ) {
			$mode = 'post';
		} else if ( is_singular( 'portfolio' ) ) {
			$mode = 'portfolio';
		} else if ( is_singular( 'product' ) ) {
			$mode = 'product';
		} else if ( is_singular( 'tribe_events' ) ) {
			$mode = 'event';
		} else if ( is_singular( 'tribe_organizer' ) || is_singular( 'tribe_venue' ) ) {
			$mode = 'event_tax';
		} else {
			$mode = 'page';
		}
	} else if ( is_archive() ) {
		if( fildisi_eutf_is_woo_tax() ) {
			$mode = 'product_tax';
		} else if ( fildisi_eutf_events_calendar_is_overview() || is_post_type_archive( 'tribe_events' ) ) {
			$mode = 'event_tax';
		} else {
			$mode = 'blog';
		}

	} else {
		$mode = 'page';
	}

	$fildisi_eutf_page_title = array(
		'bg_color' => fildisi_eutf_option( $mode . '_title_bg_color', 'dark' ),
		'bg_color_custom' => fildisi_eutf_option( $mode . '_title_bg_color_custom', '#000000' ),
		'content_bg_color' => fildisi_eutf_option( $mode . '_title_content_bg_color', 'none' ),
		'content_bg_color_custom' => fildisi_eutf_option( $mode . '_title_content_bg_color_custom', '#ffffff' ),
		'title_color' => fildisi_eutf_option( $mode . '_title_color', 'light' ),
		'title_color_custom' => fildisi_eutf_option( $mode . '_title_color_custom', '#ffffff' ),
		'caption_color' => fildisi_eutf_option( $mode . '_description_color', 'light' ),
		'caption_color_custom' => fildisi_eutf_option( $mode . '_description_color_custom', '#ffffff' ),
	);

	if ( is_tag() || is_category() || fildisi_eutf_is_woo_category() || fildisi_eutf_is_woo_tag() ) {
		$category_id = get_queried_object_id();
		$fildisi_eutf_custom_title_options = fildisi_eutf_get_term_meta( $category_id, '_fildisi_eutf_custom_title_options' );
		$fildisi_eutf_page_title_custom = fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'custom' );
		if ( 'custom' == $fildisi_eutf_page_title_custom ) {
			$fildisi_eutf_page_title = $fildisi_eutf_custom_title_options;
		}
	}


	if ( is_singular() || $fildisi_eutf_woo_shop ) {

		if ( ! $id ) {
			if ( $fildisi_eutf_woo_shop ) {
				$id = wc_get_page_id( 'shop' );
			} else {
				$id = get_the_ID();
			}
		}
		if ( $id ) {

			//Custom Title
			$fildisi_eutf_custom_title_options = get_post_meta( $id, '_fildisi_eutf_custom_title_options', true );
			$fildisi_eutf_page_title_custom = fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'custom' );
			if ( !empty( $fildisi_eutf_page_title_custom ) ) {
				$fildisi_eutf_page_title = $fildisi_eutf_custom_title_options;
			}

			//Feature Section
			$feature_section = get_post_meta( $id, '_fildisi_eutf_feature_section', true );
			$feature_settings = fildisi_eutf_array_value( $feature_section, 'feature_settings' );
			$feature_element = fildisi_eutf_array_value( $feature_settings, 'element' );

			if ( !empty( $feature_element ) ) {

				switch( $feature_element ) {

					case 'title':
					case 'image':
					case 'video':
						$single_item = fildisi_eutf_array_value( $feature_section, 'single_item' );
						if ( !empty( $single_item ) ) {
							$fildisi_eutf_custom_css .= fildisi_eutf_get_feature_title_css( $single_item );
						}
						break;
					case 'slider':
						$slider_items = fildisi_eutf_array_value( $feature_section, 'slider_items' );
						if ( !empty( $slider_items ) ) {
							foreach ( $slider_items as $item ) {
								$fildisi_eutf_custom_css .= fildisi_eutf_get_feature_title_css( $item, 'slider' );
							}
						}
						break;
					default:
						break;

				}
			}
		}
	}

	$fildisi_eutf_custom_css .= fildisi_eutf_get_title_css( $fildisi_eutf_page_title );

	if ( ! empty( $fildisi_eutf_custom_css ) ) {
		wp_add_inline_style( 'fildisi-eutf-custom-style', fildisi_eutf_compress_css( $fildisi_eutf_custom_css ) );
	}
}

function fildisi_eutf_get_feature_title_css( $item, $type = 'single' ) {

	$fildisi_eutf_custom_css = '';
	$custom_class = '';

	if( 'slider' == $type ) {
		$id = fildisi_eutf_array_value( $item, 'id' );
		if ( !empty( $id ) ) {
			$custom_class = ' .eut-slider-item-id-' . $id ;
		}
	}

	$content_bg_color = fildisi_eutf_array_value( $item, 'content_bg_color', 'none' );
	if ( 'custom' == $content_bg_color ) {
		$content_bg_color_custom = fildisi_eutf_array_value( $item, 'content_bg_color_custom', '#ffffff' );
		$fildisi_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-title-content-wrapper {';
		$fildisi_eutf_custom_css .= fildisi_eutf_get_css_color( 'background-color', $content_bg_color_custom );
		$fildisi_eutf_custom_css .= '}';
	}

	$subheading_color = fildisi_eutf_array_value( $item, 'subheading_color', 'light' );
	if ( 'custom' == $subheading_color ) {
		$subheading_color_custom = fildisi_eutf_array_value( $item, 'subheading_color_custom', '#ffffff' );
		$fildisi_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-subheading, #eut-feature-section' . esc_attr( $custom_class ) . ' .eut-title-meta {';
		$fildisi_eutf_custom_css .= fildisi_eutf_get_css_color( 'color', $subheading_color_custom );
		$fildisi_eutf_custom_css .= '}';
	}

	$title_color = fildisi_eutf_array_value( $item, 'title_color', 'light' );
	if ( 'custom' == $title_color ) {
		$title_color_custom = fildisi_eutf_array_value( $item, 'title_color_custom', '#ffffff' );
		$fildisi_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-title {';
		$fildisi_eutf_custom_css .= fildisi_eutf_get_css_color( 'color', $title_color_custom );
		$fildisi_eutf_custom_css .= '}';
	}

	$caption_color = fildisi_eutf_array_value( $item, 'caption_color', 'light' );
	if ( 'custom' == $caption_color ) {
		$caption_color_custom = fildisi_eutf_array_value( $item, 'caption_color_custom', '#ffffff' );
		$fildisi_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-description {';
		$fildisi_eutf_custom_css .= fildisi_eutf_get_css_color( 'color', $caption_color_custom );
		$fildisi_eutf_custom_css .= '}';
	}

	$media_id = fildisi_eutf_array_value( $item, 'content_image_id', '0' );
	$media_max_height = fildisi_eutf_array_value( $item, 'content_image_max_height', '150' );
	$media_responsive_max_height = fildisi_eutf_array_value( $item, 'content_image_responsive_max_height', '50' );

	if( '0' != $media_id ) {
		$fildisi_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-content .eut-graphic img  {';
		$fildisi_eutf_custom_css .= 'max-height:' . esc_attr( $media_max_height ) .'px';
		$fildisi_eutf_custom_css .= '}';

		$fildisi_eutf_custom_css .= '@media only screen and (max-width: 768px) {';
		$fildisi_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-content .eut-graphic img  {';
		$fildisi_eutf_custom_css .= 'max-height:' . esc_attr( $media_responsive_max_height ) .'px';
		$fildisi_eutf_custom_css .= '}';
		$fildisi_eutf_custom_css .= '}';
	}

	return $fildisi_eutf_custom_css;

}

function fildisi_eutf_get_title_css( $title ) {
	$fildisi_eutf_custom_css = '';

	$bg_color = fildisi_eutf_array_value( $title, 'bg_color', 'dark' );
	if ( 'custom' == $bg_color ) {
		$bg_color_custom = fildisi_eutf_array_value( $title, 'bg_color_custom', '#000000' );
		$fildisi_eutf_custom_css .= '.eut-page-title {';
		$fildisi_eutf_custom_css .= fildisi_eutf_get_css_color( 'background-color', $bg_color_custom );
		$fildisi_eutf_custom_css .= '}';
	}

	$content_bg_color = fildisi_eutf_array_value( $title, 'content_bg_color', 'none' );
	if ( 'custom' == $content_bg_color ) {
		$content_bg_color_custom = fildisi_eutf_array_value( $title, 'content_bg_color_custom', '#ffffff' );
		$fildisi_eutf_custom_css .= '.eut-page-title .eut-title-content-wrapper {';
		$fildisi_eutf_custom_css .= fildisi_eutf_get_css_color( 'background-color', $content_bg_color_custom );
		$fildisi_eutf_custom_css .= '}';
	}

	$subheading_color = fildisi_eutf_array_value( $title, 'subheading_color', 'light' );
	if ( 'custom' == $subheading_color ) {
		$subheading_color_custom = fildisi_eutf_array_value( $title, 'subheading_color_custom', '#ffffff' );
		$fildisi_eutf_custom_css .= '.eut-page-title .eut-title-categories, .eut-page-title .eut-title-meta {';
		$fildisi_eutf_custom_css .= fildisi_eutf_get_css_color( 'color', $subheading_color_custom );
		$fildisi_eutf_custom_css .= '}';
	}

	$title_color = fildisi_eutf_array_value( $title, 'title_color', 'light' );
	if ( 'custom' == $title_color ) {
		$title_color_custom = fildisi_eutf_array_value( $title, 'title_color_custom', '#ffffff' );
		$fildisi_eutf_custom_css .= '.eut-page-title .eut-title, .eut-page-title .eut-title-meta {';
		$fildisi_eutf_custom_css .= fildisi_eutf_get_css_color( 'color', $title_color_custom );
		$fildisi_eutf_custom_css .= '}';
	}

	$caption_color = fildisi_eutf_array_value( $title, 'caption_color', 'light' );
	if ( 'custom' == $caption_color ) {
		$caption_color_custom = fildisi_eutf_array_value( $title, 'caption_color_custom', '#ffffff' );
		$fildisi_eutf_custom_css .= '.eut-page-title .eut-description {';
		$fildisi_eutf_custom_css .= fildisi_eutf_get_css_color( 'color', $caption_color_custom );
		$fildisi_eutf_custom_css .= '}';
	}

	return $fildisi_eutf_custom_css;
}

function fildisi_eutf_get_global_button_style() {

	$fildisi_eutf_custom_css = "";

	$button_type = fildisi_eutf_option( 'button_type', 'simple' );
	$button_shape = fildisi_eutf_option( 'button_shape', 'square' );
	$button_color = fildisi_eutf_option( 'button_color', 'primary-1' );
	$button_hover_color = fildisi_eutf_option( 'button_hover_color', 'black' );

	$fildisi_eutf_colors = fildisi_eutf_get_color_array();

		$fildisi_eutf_custom_css .= ".eut-modal input[type='submit']:not(.eut-custom-btn), #eut-theme-wrapper input[type='submit']:not(.eut-custom-btn), #eut-theme-wrapper input[type='reset']:not(.eut-custom-btn), #eut-theme-wrapper input[type='button']:not(.eut-custom-btn), #eut-theme-wrapper button:not(.eut-custom-btn):not(.vc_general), .eut-portfolio-details-btn.eut-btn:not(.eut-custom-btn) {";
			switch( $button_shape ) {
				case "round":
					$fildisi_eutf_custom_css .= "-webkit-border-radius: 3px;";
					$fildisi_eutf_custom_css .= "border-radius: 3px;";
				break;
				case "extra-round":
					$fildisi_eutf_custom_css .= "-webkit-border-radius: 50px;";
					$fildisi_eutf_custom_css .= "border-radius: 50px;";
				break;
				case "square":
				default:
				break;
			}

			$default_color = fildisi_eutf_option( 'body_primary_1_color' );
			$color = fildisi_eutf_array_value( $fildisi_eutf_colors, $button_color, $default_color );

			if ( "outline" == $button_type ) {

				$fildisi_eutf_custom_css .= "border: 2px solid;";
				$fildisi_eutf_custom_css .= "background-color: transparent;";
				//$fildisi_eutf_custom_css .= "background-image: none;";
				$fildisi_eutf_custom_css .= "border-color: " . esc_attr( $color ) . ";";
				$fildisi_eutf_custom_css .= "color: " . esc_attr( $color ) . ";";

			} else {
				$fildisi_eutf_custom_css .= "background-color: " . esc_attr( $color ) . ";";
				if ( 'white' == $button_color ) {
					$fildisi_eutf_custom_css .= "color: #bababa;";
				} else {
					$fildisi_eutf_custom_css .= "color: #ffffff;";
				}
			}


		$fildisi_eutf_custom_css .= "}";

		$fildisi_eutf_custom_css .= ".eut-modal input[type='submit']:not(.eut-custom-btn):hover, #eut-theme-wrapper input[type='submit']:not(.eut-custom-btn):hover, #eut-theme-wrapper input[type='reset']:not(.eut-custom-btn):hover, #eut-theme-wrapper input[type='button']:not(.eut-custom-btn):hover, #eut-theme-wrapper button:not(.eut-custom-btn):not(.vc_general):hover,.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover, .woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover, .eut-portfolio-details-btn.eut-btn:not(.eut-custom-btn):hover {";

		$hover_color = fildisi_eutf_array_value( $fildisi_eutf_colors, $button_hover_color, "#bababa" );

		if ( "outline" == $button_type ) {

			$fildisi_eutf_custom_css .= "background-color: " . esc_attr( $hover_color ) . ";";
			$fildisi_eutf_custom_css .= "border-color: " . esc_attr( $hover_color ) . ";";
			if ( 'white' == $button_hover_color ) {
				$fildisi_eutf_custom_css .= "color: #bababa;";
			} else {
				$fildisi_eutf_custom_css .= "color: #ffffff;";
			}

		} else {
			$fildisi_eutf_custom_css .= "background-color: " . esc_attr( $hover_color ) . ";";
			if ( 'white' == $button_hover_color ) {
				$fildisi_eutf_custom_css .= "color: #bababa;";
			} else {
				$fildisi_eutf_custom_css .= "color: #ffffff;";
			}
		}

		$fildisi_eutf_custom_css .= "}";

	wp_add_inline_style( 'fildisi-eutf-custom-style', fildisi_eutf_compress_css( $fildisi_eutf_custom_css ) );
}


function fildisi_eutf_get_global_shape_style() {
	$fildisi_eutf_custom_css = "";

	$global_shape = fildisi_eutf_option( 'button_shape', 'square' );

	$fildisi_eutf_custom_css .= "#eut-related-post .eut-related-title, .eut-nav-btn a, .eut-bar-socials li a, #eut-single-post-tags .eut-tags li a, #eut-single-post-categories .eut-categories li a, .widget.widget_tag_cloud a, .eut-body #eut-theme-wrapper .eut-newsletter input[type='email'], #eut-theme-wrapper .eut-search:not(.eut-search-modal) input[type='text'], #eut-socials-modal .eut-social li a, .eut-pagination ul li, .eut-dropcap span.eut-style-2 {";
	switch( $global_shape ) {
		case "round":
			$fildisi_eutf_custom_css .= "-webkit-border-radius: 3px !important;";
			$fildisi_eutf_custom_css .= "border-radius: 3px !important;";
		break;
		case "extra-round":
			$fildisi_eutf_custom_css .= "-webkit-border-radius: 50px !important;";
			$fildisi_eutf_custom_css .= "border-radius: 50px !important;";
		break;
		case "square":
		default:
		break;
	}
	$fildisi_eutf_custom_css .= "}";

	wp_add_inline_style( 'fildisi-eutf-custom-style', fildisi_eutf_compress_css( $fildisi_eutf_custom_css ) );
}

 /**
 * Get color array used in theme from theme options and predefined colors
 */
function fildisi_eutf_get_color_array() {
	return array(
		'primary-1' => fildisi_eutf_option( 'body_primary_1_color' ),
		'primary-2' => fildisi_eutf_option( 'body_primary_2_color' ),
		'primary-3' => fildisi_eutf_option( 'body_primary_3_color' ),
		'primary-4' => fildisi_eutf_option( 'body_primary_4_color' ),
		'primary-5' => fildisi_eutf_option( 'body_primary_5_color' ),
		'primary-6' => fildisi_eutf_option( 'body_primary_6_color' ),
		'light' => '#ffffff',
		'white' => '#ffffff',
		'dark' => '#000000',
		'black' => '#000000',
		'green' => '#6ECA09',
		'red' => '#D0021B',
		'orange' => '#FAB901',
		'aqua' => '#28d2dc',
		'blue' => '#15c7ff',
		'purple' => '#7639e2',
		'grey' => '#e2e2e2',
	);
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
