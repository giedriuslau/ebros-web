<?php
/*
*	Admin Page Import
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( class_exists( 'Fildisi_Importer' ) ) {
	$import_url = admin_url( 'admin.php?import=fildisi-demo-importer' );
} else {
	$import_url = admin_url( 'admin.php?page=fildisi-tgmpa-install-plugins' );
}

?>
	<div id="eut-import-wrap" class="wrap">
		<h2><?php esc_html_e( "Import Demos", 'fildisi' ); ?></h2>
		<?php fildisi_eutf_print_admin_links('import'); ?>
		<div id="eut-import-panel" class="eut-admin-panel">
			<div class="eut-admin-panel-content">
				<h2><?php esc_html_e( "The Easiest Ways to Import Fildisi Demo Content", 'fildisi' ); ?></h2>
				<p class="about-description"><?php esc_html_e( "Fildisi offers you two options to import the content of our theme. With the first one, the Import on Demand, you can import specific pages, posts, portfolios. With the second one, the Full Import Demo, you can import any of the available demos. It's up to you!", 'fildisi' ); ?></p>
				<?php if ( class_exists( 'Fildisi_Importer' ) ) { ?>
				<a href="<?php echo esc_url( $import_url ); ?>" class="eut-admin-panel-btn"><?php esc_html_e( "Import Demos", 'fildisi' ); ?></a>
				<?php } else { ?>
				<a href="<?php echo esc_url( $import_url ); ?>" class="eut-admin-panel-btn"><?php esc_html_e( "Install/Activate Demo Importer", 'fildisi' ); ?></a>
				<?php } ?>
				<div class="eut-admin-panel-container">
					<div class="eut-admin-panel-row">
						<div class="eut-admin-panel-column eut-admin-panel-column-1-2">
							<div class="eut-admin-panel-column-content">
								<iframe width="100%" height="290" src="https://www.youtube-nocookie.com/embed/U9nzJ7yOx7Y" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
								<h3><?php esc_html_e( "Import on Demand", 'fildisi' ); ?></h3>
								<p><?php esc_html_e( "Do you just need specific pages or portfolios, posts, products of our demo content to create your site? Select the ones you prefer via the available multi selectors under Fildisi Demos.", 'fildisi' ); ?></p>
							</div>
						</div>
						<div class="eut-admin-panel-column eut-admin-panel-column-1-2">
							<div class="eut-admin-panel-column-content">
								<iframe width="100%" height="290" src="https://www.youtube-nocookie.com/embed/sYSdBDJ-SCI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
								<h3><?php esc_html_e( "Full Import", 'fildisi' ); ?></h3>
								<p><?php esc_html_e( "Of course, you can still import the whole dummy content. With Fildisi you have the possibility to import any of the demos with just ONE click.", 'fildisi' ); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php

//Omit closing PHP tag to avoid accidental whitespace output errors.
