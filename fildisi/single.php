<?php get_header(); ?>

<?php the_post(); ?>

<?php fildisi_eutf_print_header_title( 'post' ); ?>
<?php fildisi_eutf_print_header_breadcrumbs( 'post' ); ?>
<?php fildisi_eutf_print_anchor_menu( 'post' ); ?>

<div class="eut-single-wrapper">
	<!-- CONTENT -->
	<div id="eut-content" class="clearfix <?php echo fildisi_eutf_sidebar_class(); ?>">
		<div class="eut-content-wrapper">
			<!-- MAIN CONTENT -->
			<div id="eut-main-content">
				<div class="eut-main-content-wrapper clearfix">
					<?php
						get_template_part( 'content', get_post_format() );
						//Post Pagination
						wp_link_pages();
					?>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<?php fildisi_eutf_set_current_view( 'post' ); ?>
			<?php get_sidebar(); ?>
		</div>
	</div>
	<!-- END CONTENT -->

	<?php if ( fildisi_eutf_visibility( 'post_tag_visibility', '1' ) ) { ?>
	<div id="eut-tags-section">
		<div class="eut-container">
		<?php
		// Print Tags & Categories
		fildisi_eutf_print_post_tags();
		?>
		</div>
	</div>
	<?php } ?>

	<?php if ( fildisi_eutf_social_bar ( 'post', 'check' ) ) { ?>
	<div id="eut-socials-section" class="eut-align-center clearfix">
		<div class="eut-container">
		<?php fildisi_eutf_social_bar ( 'post' ); ?>
		</div>
	</div>
	<?php } ?>

	<?php if ( fildisi_eutf_visibility( 'post_comments_visibility' ) ) { ?>
	<div id="eut-comments-section">
		<?php comments_template(); ?>
	</div>
	<?php } ?>

	<?php
	// Print About Author section
	fildisi_eutf_print_post_about_author();
	?>

	<?php
	//Print Related Posts
	if ( fildisi_eutf_visibility( 'post_related_visibility' ) ) {
		$related_query = fildisi_eutf_get_related_posts();
		if ( !empty( $related_query ) ) {
	?>
	<div id="eut-related-section">
		<div class="eut-container">
			<?php fildisi_eutf_print_related_posts( $related_query ); ?>
		</div>
	</div>
	<?php
		}
	}
	?>

	<?php
		//Posts Bar
		fildisi_eutf_nav_bar( 'post' );
	?>
</div>

<?php get_footer();

//Omit closing PHP tag to avoid accidental whitespace output errors.
