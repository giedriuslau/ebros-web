<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.5
 */

defined( 'ABSPATH' ) || exit;

global $product;

$attribute_keys = array_keys( $attributes );
$product_get_id = method_exists( $product, 'get_id' ) ? $product->get_id() : $product->id;
do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<div class="eut-product-form eut-border">
	<form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product_get_id ); ?>" data-product_variations="<?php echo htmlspecialchars( json_encode( $available_variations ) ) ?>">
		<?php do_action( 'woocommerce_before_variations_form' ); ?>

		<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
			<div class="stock out-of-stock"><?php esc_html_e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></div>
		<?php else : ?>
			<ul class="eut-variations variations">
				<?php foreach ( $attributes as $attribute_name => $options ) : ?>
				<li>
					<div class="eut-var-label eut-link-text label"><label for="<?php echo sanitize_title( $attribute_name ); ?>"><?php echo wc_attribute_label( $attribute_name ); ?></label></div>
					<div class="eut-var-content value">
						<?php
								wc_dropdown_variation_attribute_options( array(
									'options'   => $options,
									'attribute' => $attribute_name,
									'product'   => $product,
								) );
						?>
					</div>
				</li>
				<?php echo end( $attribute_keys ) === $attribute_name ? '<li><a class="eut-reset-var eut-link-text eut-text-grey eut-text-hover-black reset_variations" href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a></li>' : ''; ?>
				 <?php endforeach;?>
			</ul>

			<div class="single_variation_wrap" style="display:none;">
				<?php
					/**
					 * woocommerce_before_single_variation Hook
					 */
					do_action( 'woocommerce_before_single_variation' );

					/**
					 * Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
					 *
					 * @since 2.4.0
					 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
					 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
					 */
					do_action( 'woocommerce_single_variation' );

					/**
					 * Hook: woocommerce_after_single_variation.
					 */
					do_action( 'woocommerce_after_single_variation' );
				?>
			</div>
		<?php endif; ?>

		<?php do_action( 'woocommerce_after_variations_form' ); ?>
	</form>
</div>

<?php do_action( 'woocommerce_after_add_to_cart_form' );

//Omit closing PHP tag to avoid accidental whitespace output errors.
