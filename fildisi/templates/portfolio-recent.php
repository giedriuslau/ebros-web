<?php
/*
*	Template Portfolio Recent
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/
?>

	<div class="eut-column eut-column-1-3">
		<div class="eut-column-wrapper">
			<article id="eut-portfolio-recent-<?php the_ID(); ?><?php echo uniqid('-'); ?>" class="eut-element eut-hover-item eut-hover-style-2">
				<figure class="eut-image-hover eut-media eut-zoom-none">
					<a class="eut-item-url" href="<?php echo esc_url( get_permalink() ); ?>"></a>
					<div class="eut-bg-dark eut-hover-overlay eut-opacity-40"></div>
					<?php if ( has_post_thumbnail() ) { ?>
						<?php $image_size = 'fildisi-eutf-small-rect-horizontal'; ?>
						<?php the_post_thumbnail( $image_size ); ?>
					<?php } else { ?>
						<?php $fildisi_eutf_empty_image_url = get_template_directory_uri() . '/images/empty/fildisi-eutf-small-rect-horizontal.jpg'; ?>
						<img width="560" height="420" src="<?php echo esc_url( $fildisi_eutf_empty_image_url ); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>">
					<?php } ?>
					<figcaption class="eut-content eut-align-center">
						<h3 class="eut-title eut-h6 eut-text-light"><?php the_title(); ?></h3>
					</figcaption>
				</figure>
			</article>
		</div>
	</div>