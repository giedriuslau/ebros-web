<?php
/*
*	Euthemians Portfolio Items
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

	add_action( 'save_post', 'fildisi_eutf_portfolio_options_save_postdata', 10, 2 );

	$fildisi_eutf_portfolio_options = array (
		//Media
		array(
			'name' => 'Media Selection',
			'id' => '_fildisi_eutf_portfolio_media_selection',
		),
		array(
			'name' => 'Media Fullwidth',
			'id' => '_fildisi_eutf_portfolio_media_fullwidth',
		),
		array(
			'name' => 'Media Margin Bottom',
			'id' => '_fildisi_eutf_portfolio_media_margin_bottom',
		),
		array(
			'name' => 'Media Image Mode',
			'id' => '_fildisi_eutf_portfolio_media_image_mode',
		),
		array(
			'name' => 'Media Image Link Mode',
			'id' => '_fildisi_eutf_portfolio_media_image_link_mode',
		),
		array(
			'name' => 'Masonry Size',
			'id' => '_fildisi_eutf_portfolio_media_masonry_size',
		),
		array(
			'name' => 'Video webm format',
			'id' => '_fildisi_eutf_portfolio_video_webm',
		),
		array(
			'name' => 'Video mp4 format',
			'id' => '_fildisi_eutf_portfolio_video_mp4',
		),
		array(
			'name' => 'Video ogv format',
			'id' => '_fildisi_eutf_portfolio_video_ogv',
		),
		array(
			'name' => 'Video Poster',
			'id' => '_fildisi_eutf_portfolio_video_poster',
		),
		array(
			'name' => 'Video embed Vimeo/Youtube',
			'id' => '_fildisi_eutf_portfolio_video_embed',
		),
		array(
			'name' => 'Video code',
			'id' => '_fildisi_eutf_portfolio_video_code',
			'html' => true,
		),

		//Link Mode
		array(
			'name' => 'Link Mode',
			'id' => '_fildisi_eutf_portfolio_link_mode',
		),
		array(
			'name' => 'Link URL',
			'id' => '_fildisi_eutf_portfolio_link_url',
		),
		array(
			'name' => 'Open Link in a new window',
			'id' => '_fildisi_eutf_portfolio_link_new_window',
		),
		array(
			'name' => 'Link Extra Class',
			'id' => '_fildisi_eutf_portfolio_link_extra_class',
		),
		//Overview Mode
		array(
			'name' => 'Custom Overview Mode',
			'id' => '_fildisi_eutf_portfolio_overview_mode',
		),
		array(
			'name' => 'Overview Color',
			'id' => '_fildisi_eutf_portfolio_overview_color',
		),
		array(
			'name' => 'Overview Background Color',
			'id' => '_fildisi_eutf_portfolio_overview_bg_color',
		),
		array(
			'name' => 'Overview custom text',
			'id' => '_fildisi_eutf_portfolio_overview_text',
			'html' => true,
		),
		array(
			'name' => 'Overview custom text size',
			'id' => '_fildisi_eutf_portfolio_overview_text_heading',
		),
		array(
			'name' => 'Second Featured Image',
			'id' => '_fildisi_eutf_second_featured_image',
		),

	);

	function fildisi_eutf_second_featured_image_section_box( $post ) {

		$second_featured_image = get_post_meta( $post->ID, '_fildisi_eutf_second_featured_image', true );

	?>

		<div id="eut-second-featured-image-wrapper">
	<?php

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-image',
				'name' => '_fildisi_eutf_second_featured_image',
				'value' => $second_featured_image,
				'label' => array(
					"desc" => esc_html__( 'Set Second Fetured Image', 'fildisi' ),
				),
				'width' => 'fullwidth',
				'wrap_class' => 'eut-metabox-side',
			)
		);
	?>
		</div>
	<?php
	}

	function fildisi_eutf_portfolio_link_mode_box( $post ) {

		$link_mode = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_link_mode', true );
		$link_url = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_link_url', true );
		$new_window = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_link_new_window', true );
		$link_class = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_link_extra_class', true );

		wp_nonce_field( 'fildisi_eutf_nonce_portfolio_save', '_fildisi_eutf_nonce_portfolio_save' );

	?>
		<table class="form-table eut-metabox">
			<tbody>
				<tr>
					<td colspan="2">
						<p class="howto"><?php esc_html_e( 'Select link mode for Portfolio Overview (Used in Portfolio Element Link Type: Custom Link).', 'fildisi' ); ?></p>
					</td>
				</tr>
			</tbody>
		</table>

		<div id="eut-portfolio-custom-overview">
	<?php

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_fildisi_eutf_portfolio_link_mode',
				'id' => 'eut-portfolio-link-mode',
				'options' => array(
					'' => esc_html__( 'Portfolio Item', 'fildisi' ),
					'link' => esc_html__( 'Custom Link', 'fildisi' ),
					'none' => esc_html__( 'None', 'fildisi' ),
				),
				'value' => $link_mode,
				'default_value' => '',
				'label' => array(
					'title' => esc_html__( 'Link Mode', 'fildisi' ),
					'desc' => esc_html__( 'Select Link Mode', 'fildisi' ),
				),
				'group_id' => 'eut-portfolio-custom-overview',
				'highlight' => 'highlight',
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'textfield',
				'name' => '_fildisi_eutf_portfolio_link_url',
				'value' => $link_url,
				'label' => array(
					'title' => esc_html__( 'Link URL', 'fildisi' ),
					'desc' => esc_html__( 'Enter the full URL of your link.', 'fildisi' ),
				),
				'width' => 'fullwidth',
				'dependency' =>
				'[
					{ "id" : "eut-portfolio-link-mode", "values" : ["link"] }
				]',
			)
		);
		fildisi_eutf_print_admin_option(
			array(
				'type' => 'checkbox',
				'name' => '_fildisi_eutf_portfolio_link_new_window',
				'value' => $new_window ,
				'label' => array(
					'title' => esc_html__( 'Open Link in new window', 'fildisi' ),
					'desc' => esc_html__( 'If selected, link will open in a new window.', 'fildisi' ),
				),
				'dependency' =>
				'[
					{ "id" : "eut-portfolio-link-mode", "values" : ["link"] }
				]',
			)
		);
		fildisi_eutf_print_admin_option(
			array(
				'type' => 'textfield',
				'name' => '_fildisi_eutf_portfolio_link_extra_class',
				'value' => $link_class,
				'label' => array(
					'title' => esc_html__( 'Link extra class name', 'fildisi' ),
				),
				'dependency' =>
				'[
					{ "id" : "eut-portfolio-link-mode", "values" : ["link"] }
				]',
			)
		);
	?>
		</div>
	<?php
	}

	function fildisi_eutf_portfolio_overview_mode_box( $post ) {

		$overview_mode = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_overview_mode', true );
		$overview_color = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_overview_color', true );
		$overview_bg_color = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_overview_bg_color', true );
		$overview_text = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_overview_text', true );
		$overview_text_heading = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_overview_text_heading', true );


		wp_nonce_field( 'fildisi_eutf_nonce_portfolio_save', '_fildisi_eutf_nonce_portfolio_save' );

	?>
		<table class="form-table eut-metabox">
			<tbody>
				<tr>
					<td colspan="2">
						<p class="howto"><?php esc_html_e( 'Select overview mode for Portfolio Overview (Used in Portfolio Element Overview Type: Custom Overview).', 'fildisi' ); ?></p>
					</td>
				</tr>
			</tbody>
		</table>

		<div id="eut-portfolio-custom-overview">
	<?php
		global $fildisi_eutf_button_color_selection;

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_fildisi_eutf_portfolio_overview_mode',
				'id' => 'eut-portfolio-overview-mode',
				'options' => array(
					'' => esc_html__( 'Featured Image', 'fildisi' ),
					'color' => esc_html__( 'Color', 'fildisi' ),
				),
				'value' => $overview_mode,
				'default_value' => '',
				'label' => esc_html__( 'Overview Mode', 'fildisi' ),
				'group_id' => 'eut-portfolio-custom-overview',
				'highlight' => 'highlight',
			)
		);
		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_fildisi_eutf_portfolio_overview_bg_color',
				'options' => $fildisi_eutf_button_color_selection,
				'value' => $overview_bg_color,
				'default_value' => 'primary-1',
				'label' => esc_html__( 'Background color', 'fildisi' ),
				'dependency' =>
				'[
					{ "id" : "eut-portfolio-overview-mode", "values" : ["color"] }
				]',
			)
		);
		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_fildisi_eutf_portfolio_overview_color',
				'options' => $fildisi_eutf_button_color_selection,
				'value' => $overview_color,
				'default_value' => 'black',
				'label' => esc_html__( 'Text Color', 'fildisi' ),
				'dependency' =>
				'[
					{ "id" : "eut-portfolio-overview-mode", "values" : ["color"] }
				]',
			)
		);
		fildisi_eutf_print_admin_option(
			array(
				'type' => 'textarea',
				'name' => '_fildisi_eutf_portfolio_overview_text',
				'value' => $overview_text,
				'label' => array(
					'title' => esc_html__( 'Custom Text', 'fildisi' ),
					'desc' => esc_html__( 'If entered, this text will replace default title and description.', 'fildisi' ),
				),
				'width' => 'fullwidth',
				'dependency' =>
				'[
					{ "id" : "eut-portfolio-overview-mode", "values" : ["color"] }
				]',
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_fildisi_eutf_portfolio_overview_text_heading',
				'options' => array(
					'h2'  => 'h2',
					'h3'  => 'h3',
					'h4'  => 'h4',
					'h5'  => 'h5',
					'h6'  => 'h6',
					'leader-text' => esc_html__( 'Leader Text', 'fildisi' ),
					'subtitle-text' => esc_html__( 'Subtitle Text', 'fildisi' ),
					'small-text' => esc_html__( 'Small Text', 'fildisi' ),
					'link-text' => esc_html__( 'Link Text', 'fildisi' ),
				),
				'value' => $overview_text_heading,
				'default_value' => 'h3',
				'label' => array(
					'title' => esc_html__( 'Custom Text size', 'fildisi' ),
					'desc' => esc_html__( 'Custom Text size and typography', 'fildisi' ),
				),
				'dependency' =>
				'[
					{ "id" : "eut-portfolio-overview-mode", "values" : ["color"] }
				]',
			)
		);
	?>
		</div>
	<?php
	}

	function fildisi_eutf_portfolio_media_section_box( $post ) {

		wp_nonce_field( 'fildisi_eutf_nonce_portfolio_save', 'eut_portfolio_media_save_nonce' );

		$portfolio_masonry_size = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_media_masonry_size', true );
		$portfolio_media = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_media_selection', true );
		$portfolio_media_fullwidth = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_media_fullwidth', true );
		$portfolio_media_margin_bottom = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_media_margin_bottom', true );

		$portfolio_image_mode = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_media_image_mode', true );
		$portfolio_image_link_mode = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_media_image_link_mode', true );

		$eut_portfolio_video_webm = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_video_webm', true );
		$eut_portfolio_video_mp4 = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_video_mp4', true );
		$eut_portfolio_video_ogv = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_video_ogv', true );
		$eut_portfolio_video_poster = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_video_poster', true );
		$eut_portfolio_video_embed = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_video_embed', true );
		$eut_portfolio_video_code = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_video_code', true );

		$media_slider_items = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_slider_items', true );
		$media_slider_settings = get_post_meta( $post->ID, '_fildisi_eutf_portfolio_slider_settings', true );
		$media_slider_speed = fildisi_eutf_array_value( $media_slider_settings, 'slideshow_speed', '3500' );
		$media_slider_dir_nav = fildisi_eutf_array_value( $media_slider_settings, 'direction_nav', '1' );
		$media_slider_dir_nav_color = fildisi_eutf_array_value( $media_slider_settings, 'direction_nav_color', 'dark' );

	?>
			<table class="form-table eut-metabox">
				<tbody>
					<tr>
						<th>
							<label for="eut-portfolio-media-masonry-size">
								<strong><?php esc_html_e( 'Masonry Size', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Select your masonry image size.', 'fildisi' ); ?>
									<br/>
									<strong><?php esc_html_e( 'Used in Portfolio Element with style Masonry.', 'fildisi' ); ?></strong>
								</span>
							</label>
						</th>
						<td>
							<select id="eut-portfolio-media-masonry-size" name="_fildisi_eutf_portfolio_media_masonry_size">
								<option value="square" <?php selected( 'square', $portfolio_masonry_size ); ?>><?php esc_html_e( 'Square', 'fildisi' ); ?></option>
								<option value="landscape" <?php selected( 'landscape', $portfolio_masonry_size ); ?>><?php esc_html_e( 'Landscape', 'fildisi' ); ?></option>
								<option value="portrait" <?php selected( 'portrait', $portfolio_masonry_size ); ?>><?php esc_html_e( 'Portrait', 'fildisi' ); ?></option>
							</select>
						</td>
					</tr>
					<tr>
						<th>
							<label for="eut-portfolio-media-selection">
								<strong><?php esc_html_e( 'Media Selection', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Choose your portfolio media.', 'fildisi' ); ?>
									<br/>
									<strong><?php esc_html_e( 'In overview only Featured Image is displayed.', 'fildisi' ); ?></strong>
								</span>
							</label>
						</th>
						<td>
							<select id="eut-portfolio-media-selection" name="_fildisi_eutf_portfolio_media_selection">
								<option value="" <?php selected( '', $portfolio_media ); ?>><?php esc_html_e( 'Featured Image', 'fildisi' ); ?></option>
								<option value="second-image" <?php selected( 'second-image', $portfolio_media ); ?>><?php esc_html_e( 'Second Featured Image', 'fildisi' ); ?></option>
								<option value="gallery" <?php selected( 'gallery', $portfolio_media ); ?>><?php esc_html_e( 'Classic Gallery', 'fildisi' ); ?></option>
								<option value="gallery-vertical" <?php selected( 'gallery-vertical', $portfolio_media ); ?>><?php esc_html_e( 'Vertical Gallery', 'fildisi' ); ?></option>
								<option value="slider" <?php selected( 'slider', $portfolio_media ); ?>><?php esc_html_e( 'Slider', 'fildisi' ); ?></option>
								<option value="video" <?php selected( 'video', $portfolio_media ); ?>><?php esc_html_e( 'YouTube/Vimeo Video', 'fildisi' ); ?></option>
								<option value="video-html5" <?php selected( 'video-html5', $portfolio_media ); ?>><?php esc_html_e( 'HMTL5 Video', 'fildisi' ); ?></option>
								<option value="video-code" <?php selected( 'video-code', $portfolio_media ); ?>><?php esc_html_e( 'Embed Video', 'fildisi' ); ?></option>
								<option value="none" <?php selected( 'none', $portfolio_media ); ?>><?php esc_html_e( 'None', 'fildisi' ); ?></option>
							</select>
						</td>
					</tr>
					<tr class="eut-portfolio-media-item eut-portfolio-media-fullwidth"<?php if ( "none" == $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-portfolio-media-fullwidth">
								<strong><?php esc_html_e( 'Media Fullwidth', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Select if you want fullwidth media.', 'fildisi' ); ?>
								</span>
							</label>
						</th>
						<td>
							<select id="eut-portfolio-media-fullwidth" name="_fildisi_eutf_portfolio_media_fullwidth">
								<option value="no" <?php selected( 'no', $portfolio_media_fullwidth ); ?>><?php esc_html_e( 'No', 'fildisi' ); ?></option>
								<option value="yes" <?php selected( 'yes', $portfolio_media_fullwidth ); ?>><?php esc_html_e( 'Yes', 'fildisi' ); ?></option>
							</select>
						</td>
					</tr>
					<tr class="eut-portfolio-media-item eut-portfolio-media-margin-bottom"<?php if ( "none" == $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-portfolio-media-margin-bottom">
								<strong><?php esc_html_e( 'Margin Bottom', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Define the space below the portfolio media.', 'fildisi' ); ?> <?php esc_html_e( 'You can use px, em, %, etc. or enter just number and it will use pixels.', 'fildisi' ); ?>
								</span>
							</label>
						</th>
						<td>
							<input type="text" id="eut-portfolio-media-margin-bottom" name="_fildisi_eutf_portfolio_media_margin_bottom" value="<?php echo esc_attr( $portfolio_media_margin_bottom ); ?>" />
						</td>
					</tr>
					<tr class="eut-portfolio-media-item eut-portfolio-video-html5"<?php if ( "video-html5" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-portfolio-video-webm">
								<strong><?php esc_html_e( 'WebM File URL', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Upload the .webm video file.', 'fildisi' ); ?>
									<br/>
									<strong><?php esc_html_e( 'This Format must be included for HTML5 Video.', 'fildisi' ); ?></strong>
								</span>
							</label>
						</th>
						<td>
							<input type="text" id="eut-portfolio-video-webm" class="eut-upload-simple-media-field eut-meta-text" name="_fildisi_eutf_portfolio_video_webm" value="<?php echo esc_attr( $eut_portfolio_video_webm ); ?>"/>
							<input type="button" data-media-type="video" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'fildisi' ); ?>"/>
							<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'fildisi' ); ?>"/>
						</td>
					</tr>
					<tr class="eut-portfolio-media-item eut-portfolio-video-html5"<?php if ( "video-html5" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-portfolio-video-mp4">
								<strong><?php esc_html_e( 'MP4 File URL', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Upload the .mp4 video file.', 'fildisi' ); ?>
									<br/>
									<strong><?php esc_html_e( 'This Format must be included for HTML5 Video.', 'fildisi' ); ?></strong>
								</span>
							</label>
						</th>
						<td>
							<input type="text" id="eut-portfolio-video-mp4" class="eut-upload-simple-media-field eut-meta-text" name="_fildisi_eutf_portfolio_video_mp4" value="<?php echo esc_attr( $eut_portfolio_video_mp4 ); ?>"/>
							<input type="button" data-media-type="video" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'fildisi' ); ?>"/>
							<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'fildisi' ); ?>"/>
						</td>
					</tr>
					<tr class="eut-portfolio-media-item eut-portfolio-video-html5"<?php if ( "video-html5" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-portfolio-video-ogv">
								<strong><?php esc_html_e( 'OGV File URL', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Upload the .ogv video file (optional).', 'fildisi' ); ?>
								</span>
							</label>
						</th>
						<td>
							<input type="text" id="eut-portfolio-video-ogv" class="eut-upload-simple-media-field eut-meta-text" name="_fildisi_eutf_portfolio_video_ogv" value="<?php echo esc_attr( $eut_portfolio_video_ogv ); ?>"/>
							<input type="button" data-media-type="video" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'fildisi' ); ?>"/>
							<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'fildisi' ); ?>"/>
						</td>
					</tr>
					<tr class="eut-portfolio-media-item eut-portfolio-video-html5"<?php if ( "video-html5" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-portfolio-video-poster">
								<strong><?php esc_html_e( 'Poster Image', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Use same resolution as video.', 'fildisi' ); ?>
								</span>
							</label>
						</th>
						<td>
							<input type="text" id="eut-portfolio-video-poster" class="eut-upload-simple-media-field eut-meta-text" name="_fildisi_eutf_portfolio_video_poster" value="<?php echo esc_attr( $eut_portfolio_video_poster ); ?>"/>
							<input type="button" data-media-type="image" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'fildisi' ); ?>"/>
							<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'fildisi' ); ?>"/>
						</td>
					</tr>
					<tr class="eut-portfolio-media-item eut-portfolio-video-embed"<?php if ( "video" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-portfolio-video-embed">
								<strong><?php esc_html_e( 'Vimeo/YouTube URL', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Enter the full URL of your video from Vimeo or YouTube.', 'fildisi' ); ?>
								</span>
							</label>
						</th>
						<td>
							<input type="text" id="eut-portfolio-video-embed" class="eut-meta-text" name="_fildisi_eutf_portfolio_video_embed" value="<?php echo esc_attr( $eut_portfolio_video_embed ); ?>"/>
						</td>
					</tr>
					<tr class="eut-portfolio-media-item eut-portfolio-video-code"<?php if ( "video-code" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-portfolio-video-code">
								<strong><?php esc_html_e( 'Video Embed', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Enter the embed code of your video.', 'fildisi' ); ?>
								</span>
							</label>
						</th>
						<td>
							<textarea id="eut-portfolio-video-code" name="_fildisi_eutf_portfolio_video_code" cols="40" rows="5"><?php echo esc_textarea( $eut_portfolio_video_code ); ?></textarea>
						</td>
					</tr>
					<tr class="eut-portfolio-media-item eut-portfolio-media-image-mode"<?php if ( "slider" != $portfolio_media || "gallery-vertical" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-portfolio-media-image-mode">
								<strong><?php esc_html_e( 'Image Mode', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Select image mode.', 'fildisi' ); ?>
								</span>
							</label>
						</th>
						<td>
							<select id="eut-portfolio-media-image-mode" name="_fildisi_eutf_portfolio_media_image_mode">
								<option value="" <?php selected( '', $portfolio_image_mode ); ?>><?php esc_html_e( 'Auto Crop', 'fildisi' ); ?></option>
								<option value="resize" <?php selected( 'resize', $portfolio_image_mode ); ?>><?php esc_html_e( 'Resize', 'fildisi' ); ?></option>
							</select>
						</td>
					</tr>
					<tr class="eut-portfolio-media-item eut-portfolio-media-image-link-mode"<?php if ( "gallery" != $portfolio_media || "gallery-vertical" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-portfolio-media-image-link-mode">
								<strong><?php esc_html_e( 'Image Link Mode', 'fildisi' ); ?></strong>
								<span>
									<?php esc_html_e( 'Select image link mode.', 'fildisi' ); ?>
								</span>
							</label>
						</th>
						<td>
							<select id="eut-portfolio-media-image-link-mode" name="_fildisi_eutf_portfolio_media_image_link_mode">
								<option value="" <?php selected( '', $portfolio_image_link_mode ); ?>><?php esc_html_e( 'Popup', 'fildisi' ); ?></option>
								<option value="none" <?php selected( 'none', $portfolio_image_link_mode ); ?>><?php esc_html_e( 'None', 'fildisi' ); ?></option>
							</select>
						</td>
					</tr>
					<tr id="eut-portfolio-media-slider-speed" class="eut-portfolio-media-item" <?php if ( "slider" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-page-slider-speed">
								<strong><?php esc_html_e( 'Slideshow Speed', 'fildisi' ); ?></strong>
							</label>
						</th>
						<td>
							<input type="text" id="eut-page-slider-speed" name="_fildisi_eutf_portfolio_slider_settings_speed" value="<?php echo esc_attr( $media_slider_speed ); ?>" /> ms
						</td>
					</tr>
					<tr id="eut-portfolio-media-slider-direction-nav" class="eut-portfolio-media-item" <?php if ( "slider" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-page-slider-direction-nav">
								<strong><?php esc_html_e( 'Navigation Buttons', 'fildisi' ); ?></strong>
							</label>
						</th>
						<td>
							<select id="eut-page-slider-direction-nav" name="_fildisi_eutf_portfolio_slider_settings_direction_nav">
								<option value="1" <?php selected( "1", $media_slider_dir_nav ); ?>><?php esc_html_e( 'Style 1', 'fildisi' ); ?></option>
								<option value="0" <?php selected( "0", $media_slider_dir_nav ); ?>><?php esc_html_e( 'No Navigation', 'fildisi' ); ?></option>
							</select>
						</td>
					</tr>
					<tr id="eut-portfolio-media-slider-direction-nav-color" class="eut-portfolio-media-item" <?php if ( "slider" != $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-page-slider-direction-nav-color">
								<strong><?php esc_html_e( 'Navigation Buttons Color', 'fildisi' ); ?></strong>
							</label>
						</th>
						<td>
							<select id="eut-page-slider-direction-nav-color" name="_fildisi_eutf_portfolio_slider_settings_direction_nav_color">
								<option value="dark" <?php selected( "dark", $media_slider_dir_nav_color ); ?>><?php esc_html_e( 'Dark', 'fildisi' ); ?></option>
								<option value="light" <?php selected( "light", $media_slider_dir_nav_color ); ?>><?php esc_html_e( 'Light', 'fildisi' ); ?></option>
							</select>
						</td>
					</tr>
					<tr id="eut-portfolio-media-slider" class="eut-portfolio-media-item" <?php if ( "" == $portfolio_media || "none" == $portfolio_media ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label><?php esc_html_e( 'Media Items', 'fildisi' ); ?></label>
						</th>
						<td>
							<input type="button" class="eut-upload-slider-button button-primary" value="<?php esc_attr_e( 'Insert Images', 'fildisi' ); ?>"/>
							<span id="eut-upload-slider-button-spinner" class="eut-action-spinner"></span>
						</td>
					</tr>
				</tbody>
			</table>
			<div id="eut-slider-container" data-mode="minimal" class="eut-portfolio-media-item" <?php if ( "" == $portfolio_media || "none" == $portfolio_media ) { ?> style="display:none;" <?php } ?>>
				<?php
					if( !empty( $media_slider_items ) ) {
						fildisi_eutf_print_admin_media_slider_items( $media_slider_items );
					}
				?>
			</div>


	<?php
	}

	function fildisi_eutf_portfolio_options_save_postdata( $post_id , $post ) {
		global $fildisi_eutf_portfolio_options;

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! isset( $_POST['_fildisi_eutf_nonce_portfolio_save'] ) || !wp_verify_nonce( $_POST['_fildisi_eutf_nonce_portfolio_save'], 'fildisi_eutf_nonce_portfolio_save' ) ) {
			return;
		}

		// Check permissions
		if ( 'portfolio' == $_POST['post_type'] )
		{
			if ( !current_user_can( 'edit_page', $post_id ) ) {
				return;
			}
		}

		foreach ( $fildisi_eutf_portfolio_options as $value ) {
			$allow_html = ( isset( $value['html'] ) ? $value['html'] : false );
			if( $allow_html ) {
				$new_meta_value = ( isset( $_POST[$value['id']] ) ? wp_filter_post_kses( $_POST[$value['id']] ) : '' );
			} else {
				$new_meta_value = ( isset( $_POST[$value['id']] ) ? sanitize_text_field( $_POST[$value['id']] ) : '' );
			}
			$meta_key = $value['id'];


			$meta_value = get_post_meta( $post_id, $meta_key, true );

			if ( $new_meta_value && '' == $meta_value ) {
				if ( !add_post_meta( $post_id, $meta_key, $new_meta_value, true ) ) {
					update_post_meta( $post_id, $meta_key, $new_meta_value );
				}
			} elseif ( $new_meta_value && $new_meta_value != $meta_value ) {
				update_post_meta( $post_id, $meta_key, $new_meta_value );
			} elseif ( '' == $new_meta_value && $meta_value ) {
				delete_post_meta( $post_id, $meta_key, $meta_value );
			}
		}

		if ( isset( $_POST['eut_portfolio_media_save_nonce'] ) && wp_verify_nonce( $_POST['eut_portfolio_media_save_nonce'], 'fildisi_eutf_nonce_portfolio_save' ) ) {


			//Media Slider Items
			$media_slider_items = array();
			if ( isset( $_POST['_fildisi_eutf_media_slider_item_id'] ) ) {

				$num_of_images = sizeof( $_POST['_fildisi_eutf_media_slider_item_id'] );
				for ( $i=0; $i < $num_of_images; $i++ ) {

					$this_image = array (
						'id' => sanitize_text_field( $_POST['_fildisi_eutf_media_slider_item_id'][ $i ] ),
					);
					array_push( $media_slider_items, $this_image );
				}

			}

			if( empty( $media_slider_items ) ) {
				delete_post_meta( $post->ID, '_fildisi_eutf_portfolio_slider_items' );
				delete_post_meta( $post->ID, '_fildisi_eutf_portfolio_slider_settings' );
			} else{
				update_post_meta( $post->ID, '_fildisi_eutf_portfolio_slider_items', $media_slider_items );

				$media_slider_speed = 3500;
				$media_slider_direction_nav = '1';
				$media_slider_direction_nav_color = 'dark';
				if ( isset( $_POST['_fildisi_eutf_portfolio_slider_settings_speed'] ) ) {
					$media_slider_speed = sanitize_text_field( $_POST['_fildisi_eutf_portfolio_slider_settings_speed'] );
				}
				if ( isset( $_POST['_fildisi_eutf_portfolio_slider_settings_direction_nav'] ) ) {
					$media_slider_direction_nav = sanitize_text_field( $_POST['_fildisi_eutf_portfolio_slider_settings_direction_nav'] );
				}
				if ( isset( $_POST['_fildisi_eutf_portfolio_slider_settings_direction_nav_color'] ) ) {
					$media_slider_direction_nav_color = sanitize_text_field( $_POST['_fildisi_eutf_portfolio_slider_settings_direction_nav_color'] );
				}

				$media_slider_settings = array (
					'slideshow_speed' => $media_slider_speed,
					'direction_nav' => $media_slider_direction_nav,
					'direction_nav_color' => $media_slider_direction_nav_color,
				);
				update_post_meta( $post->ID, '_fildisi_eutf_portfolio_slider_settings', $media_slider_settings );
			}

		}

	}

//Omit closing PHP tag to avoid accidental whitespace output errors.
