<?php
/*
*	Collection of functions for admin feature section
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

function fildisi_eutf_post_select_lookup() {

	check_ajax_referer( 'fildisi-eutf-post-select-lookup', '_eutf_nonce' );

	global $wpdb;

	$result = array();
	$search = $wpdb->esc_like( $_REQUEST['q'] );

	$args = array(
		'posts_per_page' => -1,
		'post_status' => array( 'publish' ),
		'post_type' => 'post',
		'order' => 'ASC',
		'orderby' => 'title',
		'suppress_filters' => false,
		's' => $search,
	);

	$query = new WP_Query( $args );

	while ( $query->have_posts() ) : $query->the_post();
		$post_title = get_the_title();
		$id = get_the_ID();

		$result[] = array(
			'id' => $id,
			'title' => $post_title,
		);
	endwhile;

	wp_reset_postdata();

	echo json_encode($result);

	die();
}
add_action('wp_ajax_fildisi_eutf_post_select_lookup', 'fildisi_eutf_post_select_lookup');

function fildisi_eutf_get_post_titles() {

	check_ajax_referer( 'fildisi-eutf-get-post-titles', '_eutf_nonce' );

	$result = array();

	if (isset($_REQUEST['post_ids'])) {
		$post_ids = sanitize_text_field( $_REQUEST['post_ids'] );
		if (strpos($post_ids, ',') === false) {
			$post_ids = array( $post_ids );
		} else {
			$post_ids = explode(',', $post_ids);
		}
	} else {
		$post_ids = array();
	}

	if (is_array($post_ids) && ! empty($post_ids)) {

		$args = array(
			'posts_per_page' => -1,
			'post_status' => array('publish'),
			'post__in' => $post_ids,
			'post_type' => 'post',
		);

		$query = new WP_Query( $args );

		while ( $query->have_posts() ) : $query->the_post();
			$post_title = get_the_title();
			$id = get_the_ID();

			$result[] = array(
				'id' => $id,
				'title' => $post_title,
			);
		endwhile;

		wp_reset_postdata();
	}

	echo json_encode($result);

	die;
}
add_action('wp_ajax_fildisi_eutf_get_post_titles', 'fildisi_eutf_get_post_titles');

/**
 * Get Replaced Image with ajax
 */
function fildisi_eutf_get_replaced_image() {

	check_ajax_referer( 'fildisi-eutf-get-replaced-image', '_eutf_nonce' );

	if( isset( $_POST['attachment_id'] ) ) {

		if ( isset( $_POST['attachment_mode'] ) && !empty( $_POST['attachment_mode'] ) ) {
			$mode = sanitize_text_field( $_POST['attachment_mode'] );
			switch( $mode ) {
				case 'image':
					$input_name = '_fildisi_eutf_single_item_bg_image_id';
				break;
				case 'custom-image':
					if ( isset( $_POST['field_name'] ) && !empty( $_POST['field_name'] ) ) {
						$input_name = sanitize_text_field( $_POST['field_name'] );
					}
				break;
				case 'full-slider':
				default:
					$input_name = '_fildisi_eutf_slider_item_bg_image_id[]';
				break;
			}
		} else {
			$input_name = '_fildisi_eutf_slider_item_bg_image_id[]';
		}

		$media_id  = sanitize_text_field( $_POST['attachment_id'] );
		$thumb_src = wp_get_attachment_image_src( $media_id, 'thumbnail' );
		$thumbnail_url = $thumb_src[0];
		$alt = get_post_meta( $media_id, '_wp_attachment_image_alt', true );
?>
		<input type="hidden" class="eut-upload-media-id" value="<?php echo esc_attr( $media_id ); ?>" name="<?php echo esc_attr( $input_name ); ?>">
		<?php echo '<img class="eut-thumb" src="' . esc_url( $thumbnail_url ) . '" alt="' . esc_attr( $alt ) . '" width="120" height="120"/>'; ?>
		<a class="eut-upload-remove-image eut-item-new" href="#"></a>
<?php

	}

	if( isset( $_POST['attachment_id'] ) ) { die(); }
}
add_action( 'wp_ajax_fildisi_eutf_get_replaced_image', 'fildisi_eutf_get_replaced_image' );

/**
 * Get Single Feature Slider Media with ajax
 */
function fildisi_eutf_get_admin_feature_slider_media() {

	check_ajax_referer( 'fildisi-eutf-get-feature-slider-media', '_eutf_nonce' );

	if( isset( $_POST['attachment_ids'] ) ) {

		$attachment_ids = sanitize_text_field( $_POST['attachment_ids'] );

		if( !empty( $attachment_ids ) ) {

			$media_ids = explode(",", $attachment_ids);

			foreach ( $media_ids as $media_id ) {
				$slider_item = array (
					'bg_image_id' => $media_id,
				);

				fildisi_eutf_print_admin_feature_slider_item( $slider_item, "new" );
			}
		}
	}

	if( isset( $_POST['post_ids'] ) ) {

		$post_ids = sanitize_text_field( $_POST['post_ids'] );
		if( !empty( $post_ids ) ) {

			$all_post_ids = explode(",", $post_ids);

			foreach ( $all_post_ids as $post_id ) {
				$slider_item = array (
					'type' => 'post',
					'post_id' => $post_id,
				);
				fildisi_eutf_print_admin_feature_slider_item( $slider_item, "new" );
			}
		} else {
			$slider_item = array (
				'type' => 'post',
				'post_id' => '0',
			);
			fildisi_eutf_print_admin_feature_slider_item( $slider_item, "new" );
		}
	}

	if( isset( $_POST['attachment_ids'] ) || isset( $_POST['post_ids'] )  ) { die(); }
}
add_action( 'wp_ajax_fildisi_eutf_get_admin_feature_slider_media', 'fildisi_eutf_get_admin_feature_slider_media' );

/**
 * Get Single Feature Map Point with ajax
 */
function fildisi_eutf_get_map_point() {

	check_ajax_referer( 'fildisi-eutf-get-map-point', '_eutf_nonce' );

	if( isset( $_POST['map_mode'] ) ) {
		$mode = sanitize_text_field( $_POST['map_mode'] );
		fildisi_eutf_print_admin_feature_map_point( array(), $mode );
	}
	if( isset( $_POST['map_mode'] ) ) { die(); }
}
add_action( 'wp_ajax_fildisi_eutf_get_map_point', 'fildisi_eutf_get_map_point' );

/**
 * Prints Feature Map Points
 */
function fildisi_eutf_print_admin_feature_map_items( $map_items ) {

	if( !empty($map_items) ) {
		foreach ( $map_items as $map_item ) {
			fildisi_eutf_print_admin_feature_map_point( $map_item );
		}
	}

}

/**
 * Prints Feature Single Map Point
 */
function fildisi_eutf_print_admin_feature_map_point( $map_item, $mode = '' ) {


	$map_item_id = uniqid('_fildisi_eutf_map_point_');
	$map_uniqid = uniqid('-');
	$map_id = fildisi_eutf_array_value( $map_item, 'id', $map_item_id );

	$map_lat = fildisi_eutf_array_value( $map_item, 'lat', '51.516221' );
	$map_lng = fildisi_eutf_array_value( $map_item, 'lng', '-0.136986' );
	$map_marker = fildisi_eutf_array_value( $map_item, 'marker' );

	$map_title = fildisi_eutf_array_value( $map_item, 'title' );
	$map_infotext = fildisi_eutf_array_value( $map_item, 'info_text','' );
	$map_infotext_open = fildisi_eutf_array_value( $map_item, 'info_text_open','no' );

	$button_text = fildisi_eutf_array_value( $map_item, 'button_text' );
	$button_url = fildisi_eutf_array_value( $map_item, 'button_url' );
	$button_target = fildisi_eutf_array_value( $map_item, 'button_target', '_self' );
	$button_class = fildisi_eutf_array_value( $map_item, 'button_class' );
	$fildisi_eutf_closed_class = 'closed';
	$fildisi_eutf_item_new = '';
	if( "new" == $mode ) {
		$fildisi_eutf_item_new = " eut-item-new";
		$fildisi_eutf_closed_class = "eut-item-new eut-toggle-new";
	}
?>
	<div class="eut-map-item postbox <?php echo esc_attr( $fildisi_eutf_closed_class ); ?>">
		<button class="handlediv button-link" type="button">
			<span class="screen-reader-text"><?php esc_attr_e( 'Toggle panel: Feature Section Map Point', 'fildisi' ); ?></span>
			<span class="toggle-indicator"></span>
		</button>
		<input class="eut-map-item-delete-button button<?php echo esc_attr( $fildisi_eutf_item_new ); ?>" type="button" value="<?php esc_attr_e( 'Delete', 'fildisi' ); ?>" />
		<span class="eut-button-spacer">&nbsp;</span>
		<span class="eut-modal-spinner"></span>
		<h3 class="eut-title eut-item-title">
			<span><?php esc_html_e( 'Map Point', 'fildisi' ); ?>: </span><span id="<?php echo esc_attr( $map_id ); ?>_title_admin_label"><?php if ( !empty ($map_title) ) { echo esc_html( $map_title ); } ?></span>
		</h3>
		<div class="inside">

			<!--  METABOXES -->
			<div class="eut-metabox-content">

				<!-- TABS -->
				<div class="eut-tabs<?php echo esc_attr( $fildisi_eutf_item_new ); ?>">

					<ul class="eut-tab-links">
						<li class="active"><a href="#eut-feature-single-map-tab-marker<?php echo esc_attr( $map_uniqid ); ?>"><?php esc_html_e( 'Marker', 'fildisi' ); ?></a></li>
						<li><a href="#eut-feature-single-map-tab-infobox<?php echo esc_attr( $map_uniqid ); ?>"><?php esc_html_e( 'Info Box', 'fildisi' ); ?></a></li>
						<li><a href="#eut-feature-single-map-tab-button<?php echo esc_attr( $map_uniqid ); ?>"><?php esc_html_e( 'Link', 'fildisi' ); ?></a></li>
					</ul>

					<div class="eut-tab-content">

						<div id="eut-feature-single-map-tab-marker<?php echo esc_attr( $map_uniqid ); ?>" class="eut-tab-item active">
							<input type="hidden" name="_fildisi_eutf_map_item_point_id[]" value="<?php echo esc_attr( $map_id ); ?>"/>
							<div class="eut-fields-wrapper">
								<div class="eut-label">
									<label for="eut-page-feature-element">
										<span class="eut-title"><?php esc_html_e( 'Marker', 'fildisi' ); ?></span>
									</label>
								</div>
								<div class="eut-field-items-wrapper">
									<div class="eut-field-item eut-field-item-fullwidth">
										<input type="text" name="_fildisi_eutf_map_item_point_marker[]" class="eut-upload-simple-media-field" value="<?php echo esc_attr( $map_marker ); ?>"/>
										<label></label>
										<input type="button" data-media-type="image" class="eut-upload-simple-media-button button-primary<?php echo esc_attr( $fildisi_eutf_item_new ); ?>" value="<?php esc_attr_e( 'Insert Marker', 'fildisi' ); ?>"/>
										<input type="button" class="eut-remove-simple-media-button button<?php echo esc_attr( $fildisi_eutf_item_new ); ?>" value="<?php esc_attr_e( 'Remove', 'fildisi' ); ?>"/>
									</div>
								</div>
							</div>
							<?php
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_fildisi_eutf_map_item_point_lat[]',
										'value' => $map_lat,
										'label' => array(
											"title" => esc_html__( 'Latitude', 'fildisi' ),
										),
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_fildisi_eutf_map_item_point_lng[]',
										'value' => $map_lng,
										'label' => array(
											"title" => esc_html__( 'Longitude', 'fildisi' ),
										),
									)
								);
							?>

						</div>
						<div id="eut-feature-single-map-tab-infobox<?php echo esc_attr( $map_uniqid ); ?>" class="eut-tab-item">
							<?php
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'label',
										'label' => array(
											"title" => esc_html__( 'Title / Info Text', 'fildisi' ),
										),
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_fildisi_eutf_map_item_point_title[]',
										'value' => $map_title,
										'label' => array(
											"title" => esc_html__( 'Title', 'fildisi' ),
										),
										'id' => $map_id . '_title',
										'extra_class' => 'eut-admin-label-update',
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_fildisi_eutf_map_item_point_infotext[]',
										'value' => $map_infotext,
										'label' => array(
											"title" => esc_html__( 'Info Text', 'fildisi' ),
										),
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'select-boolean',
										'name' => '_fildisi_eutf_map_item_point_infotext_open[]',
										'value' => $map_infotext_open,
										'label' => esc_html__( 'Open Info Text Onload', 'fildisi' ),
									)
								);
							?>
						</div>
						<div id="eut-feature-single-map-tab-button<?php echo esc_attr( $map_uniqid ); ?>" class="eut-tab-item">
							<?php
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'label',
										'label' => array(
											"title" => esc_html__( 'Link', 'fildisi' ),
										),
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_fildisi_eutf_map_item_point_button_text[]',
										'value' => $button_text,
										'label' => array(
											"title" => esc_html__( 'Link Text', 'fildisi' ),
										),
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_fildisi_eutf_map_item_point_button_url[]',
										'value' => $button_url,
										'label' => array(
											"title" => esc_html__( 'Link URL', 'fildisi' ),
										),
										'width' => 'fullwidth',
									)
								);

								fildisi_eutf_print_admin_option(
									array(
										'type' => 'select-button-target',
										'name' => '_fildisi_eutf_map_item_point_button_target[]',
										'value' => $button_target,
										'label' => array(
											"title" => esc_html__( 'Link Target', 'fildisi' ),
										),
									)
								);

								fildisi_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_fildisi_eutf_map_item_point_button_class[]',
										'value' => $button_class,
										'label' => array(
											"title" => esc_html__( 'Link Class', 'fildisi' ),
										),
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}

/**
 * Prints Section Slider items
 */
function fildisi_eutf_print_admin_feature_slider_items( $slider_items ) {

	foreach ( $slider_items as $slider_item ) {
		fildisi_eutf_print_admin_feature_slider_item( $slider_item, '' );
	}

}

/**
* Prints Single Feature Slider Item
*/
function fildisi_eutf_print_admin_feature_slider_item( $slider_item, $new = "" ) {

	$slide_id = fildisi_eutf_array_value( $slider_item, 'id', uniqid() );
	$slider_item['id'] = $slide_id;
	$slide_type = fildisi_eutf_array_value( $slider_item, 'type' );
	$slide_post_id = fildisi_eutf_array_value( $slider_item, 'post_id' );

	$bg_image_id = fildisi_eutf_array_value( $slider_item, 'bg_image_id' );
	$bg_image_size = fildisi_eutf_array_value( $slider_item, 'bg_image_size' );
	$bg_position = fildisi_eutf_array_value( $slider_item, 'bg_position', 'center-center' );
	$bg_tablet_sm_position = fildisi_eutf_array_value( $slider_item, 'bg_tablet_sm_position' );


	$header_style = fildisi_eutf_array_value( $slider_item, 'header_style', 'default' );
	$title = fildisi_eutf_array_value( $slider_item, 'title' );

	$slider_item_button = fildisi_eutf_array_value( $slider_item, 'button' );
	$slider_item_button2 = fildisi_eutf_array_value( $slider_item, 'button2' );

	$fildisi_eutf_button_class = "eut-feature-slider-item-delete-button";
	$fildisi_eutf_replace_image_class = "eut-upload-replace-image";
	$fildisi_eutf_open_modal_class = "eut-open-slider-modal";
	$fildisi_eutf_closed_class = 'closed';
	$fildisi_eutf_new_class = '';

	if( "new" == $new ) {
		$fildisi_eutf_button_class = "eut-feature-slider-item-delete-button eut-item-new";
		$fildisi_eutf_replace_image_class = "eut-upload-replace-image eut-item-new";
		$fildisi_eutf_open_modal_class = "eut-open-slider-modal eut-item-new";
		$fildisi_eutf_closed_class = 'eut-item-new eut-toggle-new';
		$fildisi_eutf_new_class = ' eut-item-new';
	}

	$slide_uniqid = '-' . $slide_id;

	$slide_type_string = esc_html__( 'Slide', 'fildisi' );
	if ( 'post' == $slide_type ) {
		$slide_type_string = esc_html__( 'Post Slide', 'fildisi' );
		if( !empty( $slide_post_id ) ) {
			$title = get_the_title ( $slide_post_id  );
		}
	}

?>

	<div class="eut-slider-item postbox <?php echo esc_attr( $fildisi_eutf_closed_class ); ?>">
		<button class="handlediv button-link" type="button">
			<span class="screen-reader-text"><?php esc_attr_e( 'Toggle panel: Feature Section Slide', 'fildisi' ); ?></span>
			<span class="toggle-indicator"></span>
		</button>
		<input class="<?php echo esc_attr( $fildisi_eutf_button_class ); ?> button" type="button" value="<?php esc_attr_e( 'Delete', 'fildisi' ); ?>">
		<span class="eut-button-spacer">&nbsp;</span>
		<span class="eut-modal-spinner"></span>
		<h3 class="eut-title eut-item-title eut-movable">
			<span><?php echo esc_html( $slide_type_string ); ?>: </span><span id="_fildisi_eutf_slider_item_title<?php echo esc_attr( $slide_id ); ?>_admin_label"><?php if ( !empty ($title) ) { echo esc_html( $title ); } ?></span>
		</h3>
		<div class="inside">
			<!--  METABOXES -->
			<div class="eut-metabox-content">

				<!-- TABS -->
				<div class="eut-tabs<?php echo esc_attr( $fildisi_eutf_new_class ); ?>">

					<ul class="eut-tab-links">
						<li class="active"><a href="#eut-feature-single-tab-bg<?php echo esc_attr( $slide_uniqid ); ?>"><?php esc_html_e( 'Background / Header', 'fildisi' ); ?></a></li>
						<li><a href="#eut-feature-single-tab-content<?php echo esc_attr( $slide_uniqid ); ?>"><?php esc_html_e( 'Content', 'fildisi' ); ?></a></li>
						<?php if ( 'post' != $slide_type ) { ?>
						<li><a href="#eut-feature-single-tab-button<?php echo esc_attr( $slide_uniqid ); ?>"><?php esc_html_e( 'First Button', 'fildisi' ); ?></a></li>
						<li><a href="#eut-feature-single-tab-button2<?php echo esc_attr( $slide_uniqid ); ?>"><?php esc_html_e( 'Second Button', 'fildisi' ); ?></a></li>
						<?php } ?>
						<li><a href="#eut-feature-single-tab-extra<?php echo esc_attr( $slide_uniqid ); ?>"><?php esc_html_e( 'Extra', 'fildisi' ); ?></a></li>
					</ul>

					<div class="eut-tab-content">

						<div id="eut-feature-single-tab-bg<?php echo esc_attr( $slide_uniqid ); ?>" class="eut-tab-item active">
							<?php

								fildisi_eutf_print_admin_option(
									array(
										'type' => 'hidden',
										'name' => '_fildisi_eutf_slider_item_id[]',
										'value' => $slide_id,
									)
								);

								fildisi_eutf_print_admin_option(
									array(
										'type' => 'hidden',
										'name' => '_fildisi_eutf_slider_item_type[]',
										'value' => $slide_type,
									)
								);
								if ( 'post' == $slide_type ) {
									fildisi_eutf_print_admin_option(
										array(
											'type' => 'hiddenfield',
											'name' => '_fildisi_eutf_slider_item_post_id[]',
											'value' => $slide_post_id,
											'label' => array(
												"title" => esc_html__( 'Post ID', 'fildisi' ),
												"desc" => esc_html__( 'Background Image can be still used instead of the Feature Image', 'fildisi' ),
											),
										)
									);
								} else {
									fildisi_eutf_print_admin_option(
										array(
											'type' => 'hidden',
											'name' => '_fildisi_eutf_slider_item_post_id[]',
											'value' => $slide_post_id,
										)
									);
								}

								fildisi_eutf_print_admin_option(
									array(
										'type' => 'select-image',
										'name' => '_fildisi_eutf_slider_item_bg_image_id[]',
										'value' => $bg_image_id,
										'label' => array(
											"title" => esc_html__( 'Background Image', 'fildisi' ),
										),
										'width' => 'fullwidth',
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'select',
										'name' => '_fildisi_eutf_slider_item_bg_image_size[]',
										'value' => $bg_image_size,
										'options' => array(
											'' => esc_html__( '--Inherit--', 'fildisi' ),
											'responsive' => esc_html__( 'Responsive', 'fildisi' ),
											'extra-extra-large' => esc_html__( 'Extra Extra Large', 'fildisi' ),
											'full' => esc_html__( 'Full', 'fildisi' ),
										),
										'label' => array(
											"title" => esc_html__( 'Background Image Size', 'fildisi' ),
											"desc" => esc_html__( 'Inherit : Theme Options - Media Sizes.', 'fildisi' ),
										),
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'label',
										'label' => array(
											"title" => esc_html__( 'Header / Background Position', 'fildisi' ),
										),
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'select-bg-position',
										'name' => '_fildisi_eutf_slider_item_bg_position[]',
										'value' => $bg_position,
										'label' => array(
											"title" => esc_html__( 'Background Position', 'fildisi' ),
										),
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'select-bg-position-inherit',
										'name' => '_fildisi_eutf_slider_item_bg_tablet_sm_position[]',
										'value' => $bg_tablet_sm_position,
										'label' => array(
											"title" => esc_html__( 'Background Position ( Tablet Portrait )', 'fildisi' ),
											"desc" => esc_html__( 'Tablet devices with portrait orientation and below.', 'fildisi' ),
										),
									)
								);
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'select-header-style',
										'name' => '_fildisi_eutf_slider_item_header_style[]',
										'value' => $header_style,
										'label' => array(
											"title" => esc_html__( 'Header Style', 'fildisi' ),
										),
									)
								);

								fildisi_eutf_print_admin_feature_item_overlay_options( $slider_item, '_fildisi_eutf_slider_item_', 'multi' );
							?>
						</div>
						<div id="eut-feature-single-tab-content<?php echo esc_attr( $slide_uniqid ); ?>" class="eut-tab-item">
							<?php fildisi_eutf_print_admin_feature_item_content_options( $slider_item, '_fildisi_eutf_slider_item_', 'multi' ); ?>
						</div>
						<div id="eut-feature-single-tab-button<?php echo esc_attr( $slide_uniqid ); ?>" class="eut-tab-item">
							<?php fildisi_eutf_print_admin_feature_item_button_options( $slider_item_button, '_fildisi_eutf_slider_item_button_', 'multi' ); ?>
						</div>
						<div id="eut-feature-single-tab-button2<?php echo esc_attr( $slide_uniqid ); ?>" class="eut-tab-item">
							<?php fildisi_eutf_print_admin_feature_item_button_options( $slider_item_button2, '_fildisi_eutf_slider_item_button2_', 'multi' ); ?>
						</div>
						<div id="eut-feature-single-tab-extra<?php echo esc_attr( $slide_uniqid ); ?>" class="eut-tab-item">
							<?php fildisi_eutf_print_admin_feature_item_extra_options( $slider_item, '_fildisi_eutf_slider_item_', 'multi' ); ?>
						</div>
					</div>

				</div>
				<!-- END TABS -->

			</div>
			<!-- END  METABOXES -->
		</div>

	</div>
<?php

}

/**
* Get Revolution Sliders
*/
function fildisi_eutf_get_revolution_selection() {

	$revsliders = array(
		"" => esc_html__( "None", 'fildisi' ),
	);

	if ( class_exists( 'RevSlider' ) ) {
		$slider = new RevSlider();
		$arrSliders = $slider->getArrSliders();

		if ( $arrSliders ) {
			foreach ( $arrSliders as $slider ) {
				$revsliders[ $slider->getAlias() ] = $slider->getTitle();
			}
		}
	}

	return $revsliders;
}

/**
* Prints Item Button Options
*/
function fildisi_eutf_print_admin_feature_item_button_options( $item, $prefix = '_fildisi_eutf_single_item_button_', $mode = '' ) {


	$button_id = fildisi_eutf_array_value( $item, 'id', uniqid() );
	$group_id = $prefix . $button_id;

	$button_text = fildisi_eutf_array_value( $item, 'text' );
	$button_url = fildisi_eutf_array_value( $item, 'url' );
	$button_type = fildisi_eutf_array_value( $item, 'type', '' );
	$button_size = fildisi_eutf_array_value( $item, 'size', 'medium' );
	$button_color = fildisi_eutf_array_value( $item, 'color', 'primary-1' );
	$button_hover_color = fildisi_eutf_array_value( $item, 'hover_color', 'black' );
	$button_shape = fildisi_eutf_array_value( $item, 'shape', 'square' );
	$button_target = fildisi_eutf_array_value( $item, 'target', '_self' );
	$button_class = fildisi_eutf_array_value( $item, 'class' );

	$sufix  = '';
	if ( 'multi' == $mode ) {
		$sufix = '[]';
	};
	echo '<div id="' . esc_attr( $group_id ) . '">';


	fildisi_eutf_print_admin_option(
		array(
			'type' => 'hidden',
			'name' => $prefix . 'id' . $sufix,
			'value' => $button_id,
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => $prefix . 'text' . $sufix,
			'id' => $prefix . 'text' . '_' . $button_id,
			'value' => $button_text,
			'label' => esc_html__( 'Button Text', 'fildisi' ),
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => $prefix . 'url' . $sufix,
			'id' => $prefix . 'url' . '_' . $button_id,
			'value' => $button_url,
			'label' => esc_html__( 'Button URL', 'fildisi' ),
			'width' => 'fullwidth',
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-button-target',
			'name' => $prefix . 'target' . $sufix,
			'id' => $prefix . 'target' . '_' . $button_id,
			'value' => $button_target,
			'label' => esc_html__( 'Button Target', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-button-type',
			'name' => $prefix . 'type' . $sufix,
			'id' => $prefix . 'type' . '_' . $button_id,
			'value' => $button_type,
			'group_id' => $group_id,
			'label' => esc_html__( 'Button Type', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-button-color',
			'name' => $prefix . 'color' . $sufix,
			'id' => $prefix . 'color' . '_' . $button_id,
			'value' => $button_color,
			'group_id' => $group_id,
			'label' => esc_html__( 'Button Color', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-button-color',
			'name' => $prefix . 'hover_color' . $sufix,
			'id' => $prefix . 'hover_color' . '_' . $button_id,
			'value' => $button_hover_color,
			'group_id' => $group_id,
			'label' => esc_html__( 'Button Hover Color', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-button-size',
			'name' => $prefix . 'size' . $sufix,
			'value' => $button_size,
			'label' => esc_html__( 'Button Size', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-button-shape',
			'name' => $prefix . 'shape' . $sufix,
			'value' => $button_shape,
			'label' => esc_html__( 'Button Shape', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => $prefix . 'class' . $sufix,
			'id' => $prefix . 'class' . '_' . $button_id,
			'value' => $button_class,
			'label' => esc_html__( 'Button Class', 'fildisi' ),
		)
	);

	echo '</div>';

}


/**
* Prints Item Overlay Options
*/
function fildisi_eutf_print_admin_feature_item_overlay_options( $item, $prefix = '_fildisi_eutf_single_item_', $mode = '' ) {

	$overlay_id = fildisi_eutf_array_value( $item, 'id', uniqid() );
	$group_id = $prefix . 'overlay_container' . $overlay_id;

	$pattern_overlay = fildisi_eutf_array_value( $item, 'pattern_overlay' );
	$color_overlay = fildisi_eutf_array_value( $item, 'color_overlay', 'dark' );
	$color_overlay_custom  = fildisi_eutf_array_value( $item, 'color_overlay_custom', '#000000' );
	$opacity_overlay = fildisi_eutf_array_value( $item, 'opacity_overlay', '0' );
	$gradient_overlay_custom_1 = fildisi_eutf_array_value( $item, 'gradient_overlay_custom_1', '#034e90' );
	$gradient_overlay_custom_1_opacity = fildisi_eutf_array_value( $item, 'gradient_overlay_custom_1_opacity', '0.90' );
	$gradient_overlay_custom_2 = fildisi_eutf_array_value( $item, 'gradient_overlay_custom_2', '#19b4d7' );
	$gradient_overlay_custom_2_opacity = fildisi_eutf_array_value( $item, 'gradient_overlay_custom_2_opacity', '0.90' );
	$gradient_overlay_direction  = fildisi_eutf_array_value( $item, 'gradient_overlay_direction', '90' );


	$sufix  = '';
	if ( 'multi' == $mode ) {
		$sufix = '[]';
	};

	echo '<div id="' . esc_attr( $group_id ) . '">';

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-pattern-overlay',
			'name' => $prefix . 'pattern_overlay' . $sufix,
			'value' => $pattern_overlay,
			'label' => esc_html__( 'Pattern Overlay', 'fildisi' ),
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-colorpicker',
			'name' => $prefix . 'color_overlay' . $sufix,
			'id' => $prefix . 'color_overlay' . $overlay_id,
			'value' => $color_overlay,
			'value2' => $color_overlay_custom,
			'label' => esc_html__( 'Color Overlay', 'fildisi' ),
			'multiple' => 'multi',
			'type_usage' => 'feature-bg',
			'group_id' => $group_id ,
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-opacity',
			'name' => $prefix . 'opacity_overlay' . $sufix,
			'value' => $opacity_overlay,
			'label' => esc_html__( 'Opacity Overlay', 'fildisi' ),
			'dependency' =>
			'[
				{ "id" : "' . $prefix . 'color_overlay' . $overlay_id . '", "value_not_equal_to" : ["gradient"] }
			]',
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'colorpicker',
			'name' => $prefix . 'gradient_overlay_custom_1' . $sufix,
			'value' => $gradient_overlay_custom_1,
			'label' => esc_html__( 'Custom Color 1', 'fildisi' ),
			'dependency' =>
			'[
				{ "id" : "' . $prefix . 'color_overlay' . $overlay_id . '", "values" : ["gradient"] }
			]',
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-opacity',
			'name' => $prefix . 'gradient_overlay_custom_1_opacity' . $sufix,
			'value' => $gradient_overlay_custom_1_opacity,
			'label' => esc_html__( 'Custom Color 1 Opacity', 'fildisi' ),
			'dependency' =>
			'[
				{ "id" : "' . $prefix . 'color_overlay' . $overlay_id . '", "values" : ["gradient"] }
			]',
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'colorpicker',
			'name' => $prefix . 'gradient_overlay_custom_2' . $sufix,
			'value' => $gradient_overlay_custom_2,
			'label' => esc_html__( 'Custom Color 2', 'fildisi' ),
			'dependency' =>
			'[
				{ "id" : "' . $prefix . 'color_overlay' . $overlay_id . '", "values" : ["gradient"] }
			]',
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-opacity',
			'name' => $prefix . 'gradient_overlay_custom_2_opacity' . $sufix,
			'value' => $gradient_overlay_custom_2_opacity,
			'label' => esc_html__( 'Custom Color 2 Opacity', 'fildisi' ),
			'dependency' =>
			'[
				{ "id" : "' . $prefix . 'color_overlay' . $overlay_id . '", "values" : ["gradient"] }
			]',
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select',
			'name' => $prefix . 'gradient_overlay_direction' . $sufix,
			'value' => $gradient_overlay_direction,
			'options' => array(
				'90' => esc_html__( "Left to Right", 'fildisi' ),
				'135' => esc_html__( "Left Top to Right Bottom", 'fildisi' ),
				'45' => esc_html__( "Left Bottom to Right Top", 'fildisi' ),
				'180' => esc_html__( "Bottom to Top", 'fildisi' ),
			),
			'label' => array(
				"title" => esc_html__( 'Gradient Direction', 'fildisi' ),
			),
			'dependency' =>
			'[
				{ "id" : "' . $prefix . 'color_overlay' . $overlay_id . '", "values" : ["gradient"] }
			]',
		)
	);

	echo '</div>';

}

/**
* Prints Item Content Options
*/
function fildisi_eutf_print_admin_feature_item_content_options( $item, $prefix = '_fildisi_eutf_single_item_', $mode = '' ) {

	$item_id = fildisi_eutf_array_value( $item, 'id' );
	$title = fildisi_eutf_array_value( $item, 'title' );
	$content_bg_color = fildisi_eutf_array_value( $item, 'content_bg_color', 'none' );
	$content_bg_color_custom = fildisi_eutf_array_value( $item, 'content_bg_color_custom', '#ffffff' );
	$title_color = fildisi_eutf_array_value( $item, 'title_color', 'light' );
	$title_color_custom = fildisi_eutf_array_value( $item, 'title_color_custom', '#ffffff' );
	$title_tag = fildisi_eutf_array_value( $item, 'title_tag', 'div' );
	$caption = fildisi_eutf_array_value( $item, 'caption' );
	$caption_color = fildisi_eutf_array_value( $item, 'caption_color', 'light' );
	$caption_color_custom = fildisi_eutf_array_value( $item, 'caption_color_custom', '#ffffff' );
	$caption_tag = fildisi_eutf_array_value( $item, 'caption_tag', 'div' );
	$subheading = fildisi_eutf_array_value( $item, 'subheading' );
	$subheading_color = fildisi_eutf_array_value( $item, 'subheading_color', 'light' );
	$subheading_color_custom = fildisi_eutf_array_value( $item, 'subheading_color_custom', '#ffffff' );
	$subheading_tag = fildisi_eutf_array_value( $item, 'subheading_tag', 'div' );

	$subheading_family = fildisi_eutf_array_value( $item, 'subheading_family' );
	$title_family = fildisi_eutf_array_value( $item, 'title_family' );
	$caption_family = fildisi_eutf_array_value( $item, 'caption_family' );

	$content_size = fildisi_eutf_array_value( $item, 'content_size', 'large' );
	$content_align = fildisi_eutf_array_value( $item, 'content_align', 'center' );
	$content_position = fildisi_eutf_array_value( $item, 'content_position', 'center-center' );
	$content_animation = fildisi_eutf_array_value( $item, 'content_animation', 'fade-in' );
	$content_image_id = fildisi_eutf_array_value( $item, 'content_image_id', '0' );
	$content_image_size = fildisi_eutf_array_value( $item, 'content_image_size' );
	$content_image_max_height = fildisi_eutf_array_value( $item, 'content_image_max_height', '150' );
	$content_image_responsive_max_height = fildisi_eutf_array_value( $item, 'content_image_responsive_max_height', '50' );

	$container_size = fildisi_eutf_array_value( $item, 'container_size' );



	$sufix  = '';
	if ( 'multi' == $mode ) {
		$sufix = '[]';
	};

	$type = fildisi_eutf_array_value( $item, 'type' );

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select',
			'name' => $prefix . 'container_size' . $sufix,
			'options' => array(
				'' => esc_html__( 'Default', 'fildisi' ),
				'large' => esc_html__( 'Large', 'fildisi' ),
			),
			'value' => $container_size,
			'label' => esc_html__( 'Container Size', 'fildisi' ),
		)
	);

	if ( 'post' == $type ) {
?>
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'content_image_id' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'content_image_size' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'content_image_max_height' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'content_image_responsive_max_height' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'subheading' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'title' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'caption' . $sufix ); ?>" value="" />
<?php

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'title_content_bg_color' . $sufix,
				'value' => $content_bg_color,
				'value2' => $content_bg_color_custom,
				'label' => array(
					"title" => esc_html__( 'Content Background Color', 'fildisi' ),
				),
				'multiple' => 'multi',
				'type_usage' => 'title-content-bg',
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'subheading_color' . $sufix,
				'value' => $subheading_color,
				'value2' => $subheading_color_custom,
				'label' => esc_html__( 'Post Meta Color', 'fildisi' ),
				'multiple' => 'multi',
			)
		);
		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'title_color' . $sufix,
				'value' => $title_color,
				'value2' => $title_color_custom,
				'label' => esc_html__( 'Post Title Color', 'fildisi' ),
				'multiple' => 'multi',
			)
		);
		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'caption_color' . $sufix,
				'value' => $caption_color,
				'value2' => $caption_color_custom,
				'label' => esc_html__( 'Post Description Color', 'fildisi' ),
				'multiple' => 'multi',
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'subheading_tag' . $sufix,
				'value' => $subheading_tag,
				'label' => esc_html__( 'Post Meta Tag', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'title_tag' . $sufix,
				'value' => $title_tag,
				'label' => esc_html__( 'Post Title Tag', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'caption_tag' . $sufix,
				'value' => $caption_tag,
				'label' => esc_html__( 'Post Description Tag', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-custom-font-family',
				'name' => $prefix . 'subheading_family' . $sufix,
				'value' => $subheading_family,
				'label' => esc_html__( 'Post Meta Font Family', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-custom-font-family',
				'name' => $prefix . 'title_family' . $sufix,
				'value' => $title_family,
				'label' => esc_html__( 'Post Title Font Family', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-custom-font-family',
				'name' => $prefix . 'caption_family' . $sufix,
				'value' => $caption_family,
				'label' => esc_html__( 'Post Description Font Family', 'fildisi' ),
			)
		);

	} else {
		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-image',
				'name' => $prefix . 'content_image_id' . $sufix,
				'value' => $content_image_id,
				'label' => array(
					"title" => esc_html__( 'Graphic Image', 'fildisi' ),
				),
				'width' => 'fullwidth',
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => $prefix . 'content_image_size' . $sufix,
				'options' => array(
					'' => esc_html__( 'Resize ( Medium )', 'fildisi' ),
					'full' => esc_html__( 'Full size', 'fildisi' ),
				),
				'value' => $content_image_size,
				'label' => esc_html__( 'Graphic Image Size', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'textfield',
				'name' => $prefix . 'content_image_max_height' . $sufix,
				'value' => $content_image_max_height,
				'label' => esc_html__( 'Graphic Image Max Height', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'textfield',
				'name' => $prefix . 'content_image_responsive_max_height' . $sufix,
				'value' => $content_image_responsive_max_height,
				'label' => esc_html__( 'Graphic Image responsive Max Height', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'textarea',
				'name' => $prefix . 'subheading' . $sufix,
				'value' => $subheading,
				'label' => esc_html__( 'Sub Heading', 'fildisi' ),
				'width' => 'fullwidth',
				'rows' => 2,
			)
		);


		fildisi_eutf_print_admin_option(
			array(
				'type' => 'textarea',
				'name' => $prefix . 'title' . $sufix,
				'value' => $title,
				'label' => esc_html__( 'Title', 'fildisi' ),
				'extra_class' =>  'eut-admin-label-update',
				'id' => $prefix . 'title' . $item_id,
				'width' => 'fullwidth',
				'rows' => 2,
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'textarea',
				'name' => $prefix . 'caption' . $sufix,
				'value' => $caption,
				'label' => esc_html__( 'Description', 'fildisi' ),
				'width' => 'fullwidth',
				'rows' => 2,
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'title_content_bg_color' . $sufix,
				'value' => $content_bg_color,
				'value2' => $content_bg_color_custom,
				'label' => array(
					"title" => esc_html__( 'Content Background Color', 'fildisi' ),
				),
				'multiple' => 'multi',
				'type_usage' => 'title-content-bg',
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'subheading_color' . $sufix,
				'value' => $subheading_color,
				'value2' => $subheading_color_custom,
				'label' => esc_html__( 'Sub Heading Color', 'fildisi' ),
				'multiple' => 'multi',
			)
		);
		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'title_color' . $sufix,
				'value' => $title_color,
				'value2' => $title_color_custom,
				'label' => esc_html__( 'Title Color', 'fildisi' ),
				'multiple' => 'multi',
			)
		);
		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'caption_color' . $sufix,
				'value' => $caption_color,
				'value2' => $caption_color_custom,
				'label' => esc_html__( 'Description Color', 'fildisi' ),
				'multiple' => 'multi',
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'subheading_tag' . $sufix,
				'value' => $subheading_tag,
				'label' => esc_html__( 'Sub Heading Tag', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'title_tag' . $sufix,
				'value' => $title_tag,
				'label' => esc_html__( 'Title Tag', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'caption_tag' . $sufix,
				'value' => $caption_tag,
				'label' => esc_html__( 'Description Tag', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-custom-font-family',
				'name' => $prefix . 'subheading_family' . $sufix,
				'value' => $subheading_family,
				'label' => esc_html__( 'Sub Heading Font Family', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-custom-font-family',
				'name' => $prefix . 'title_family' . $sufix,
				'value' => $title_family,
				'label' => esc_html__( 'Title Font Family', 'fildisi' ),
			)
		);

		fildisi_eutf_print_admin_option(
			array(
				'type' => 'select-custom-font-family',
				'name' => $prefix . 'caption_family' . $sufix,
				'value' => $caption_family,
				'label' => esc_html__( 'Description Font Family', 'fildisi' ),
			)
		);

	}

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select',
			'name' => $prefix . 'content_size' . $sufix,
			'options' => array(
				'large' => esc_html__( 'Large', 'fildisi' ),
				'medium' => esc_html__( 'Medium', 'fildisi' ),
				'small' => esc_html__( 'Small', 'fildisi' ),
			),
			'value' => $content_size,
			'label' => esc_html__( 'Content Size', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-align',
			'name' => $prefix . 'content_align' . $sufix,
			'value' => $content_align,
			'label' => esc_html__( 'Content Alignment', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-bg-position',
			'name' => $prefix . 'content_position' . $sufix,
			'value' => $content_position,
			'label' => esc_html__( 'Content Position', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-text-animation',
			'name' => $prefix . 'content_animation' . $sufix,
			'value' => $content_animation,
			'label' => esc_html__( 'Content Animation', 'fildisi' ),
		)
	);

}

/**
* Prints Item Extra Options
*/
function fildisi_eutf_print_admin_feature_item_extra_options( $item, $prefix = '_fildisi_eutf_single_item_', $mode = '' ) {

	$sufix  = '';
	if ( 'multi' == $mode ) {
		$sufix = '[]';
	};

	$el_class = fildisi_eutf_array_value( $item, 'el_class' );
	$arrow_enabled = fildisi_eutf_array_value( $item, 'arrow_enabled', 'no' );
	$arrow_color = fildisi_eutf_array_value( $item, 'arrow_color', 'light' );
	$arrow_color_custom = fildisi_eutf_array_value( $item, 'arrow_color_custom', '#ffffff' );

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-boolean',
			'name' => $prefix . 'arrow_enabled' . $sufix,
			'value' => $arrow_enabled,
			'label' => esc_html__( 'Enable Bottom Arrow', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-colorpicker',
			'name' => $prefix . 'arrow_color' . $sufix,
			'value' => $arrow_color,
			'value2' => $arrow_color_custom,
			'label' => esc_html__( 'Arrow Color', 'fildisi' ),
			'multiple' => 'multi',
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => $prefix . 'el_class' . $sufix,
			'value' => $el_class,
			'label' => esc_html__( 'Extra Class', 'fildisi' ),
		)
	);

}

/**
 * Prints Item Background Image Options
 */
function fildisi_eutf_print_admin_feature_item_background_options( $item ) {

	$bg_image_id = fildisi_eutf_array_value( $item, 'bg_image_id', '0' );
	$bg_image_size = fildisi_eutf_array_value( $item, 'bg_image_size' );
	$bg_position = fildisi_eutf_array_value( $item, 'bg_position', 'center-center' );
	$bg_tablet_sm_position = fildisi_eutf_array_value( $item, 'bg_tablet_sm_position' );
	$image_effect = fildisi_eutf_array_value( $item, 'image_effect' );


	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-image',
			'name' => '_fildisi_eutf_single_item_bg_image_id',
			'value' => $bg_image_id,
			'label' => array(
				"title" => esc_html__( 'Background Image', 'fildisi' ),
			),
			'width' => 'fullwidth',
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select',
			'name' => '_fildisi_eutf_single_item_bg_image_size',
			'value' => $bg_image_size,
			'options' => array(
				'' => esc_html__( '--Inherit--', 'fildisi' ),
				'responsive' => esc_html__( 'Responsive', 'fildisi' ),
				'extra-extra-large' => esc_html__( 'Extra Extra Large', 'fildisi' ),
				'full' => esc_html__( 'Full', 'fildisi' ),
			),
			'label' => array(
				"title" => esc_html__( 'Background Image Size', 'fildisi' ),
				"desc" => esc_html__( 'Inherit : Theme Options - Media Sizes.', 'fildisi' ),
			),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-bg-position',
			'name' => '_fildisi_eutf_single_item_bg_position',
			'value' => $bg_position,
			'label' => esc_html__( 'Background Position', 'fildisi' ),
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-bg-position-inherit',
			'name' => '_fildisi_eutf_single_item_bg_tablet_sm_position',
			'value' => $bg_tablet_sm_position,
			'label' => array(
				"title" => esc_html__( 'Background Position ( Tablet Portrait )', 'fildisi' ),
				"desc" => esc_html__( 'Tablet devices with portrait orientation and below.', 'fildisi' ),
			),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select',
			'name' => '_fildisi_eutf_single_item_image_effect',
			'options' => array(
				'' => esc_html__( 'None', 'fildisi' ),
				'animated' => esc_html__( 'Animated', 'fildisi' ),
				'parallax' => esc_html__( 'Classic Parallax', 'fildisi' ),
				'advanced-parallax' => esc_html__( 'Advanced Parallax', 'fildisi' ),
				'fixed-section' => esc_html__( 'Fixed Section', 'fildisi' ),
			),
			'value' => $image_effect,
			'label' => esc_html__( 'Background Effect', 'fildisi' ),
			'wrap_class' => 'eut-feature-required eut-item-feature-image-settings',
		)
	);

}

/**
 * Prints Item Background Video Options
 */
function fildisi_eutf_print_admin_feature_item_video_options( $item ) {

	$video_webm = fildisi_eutf_array_value( $item, 'video_webm' );
	$video_mp4 = fildisi_eutf_array_value( $item, 'video_mp4' );
	$video_ogv = fildisi_eutf_array_value( $item, 'video_ogv' );
	$video_poster = fildisi_eutf_array_value( $item, 'video_poster', 'no' );
	$video_loop = fildisi_eutf_array_value( $item, 'video_loop', 'yes' );
	$video_device = fildisi_eutf_array_value( $item, 'video_device', 'no' );
	$video_muted = fildisi_eutf_array_value( $item, 'video_muted', 'yes' );
	$video_effect = fildisi_eutf_array_value( $item, 'video_effect' );

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'label',
			'label' => esc_html__( 'HTML5 Video', 'fildisi' ),
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-bg-video',
			'name' => '_fildisi_eutf_single_item_video_webm',
			'value' => $video_webm,
			'label' => esc_html__( 'WebM File URL', 'fildisi' ),
			'width' => 'fullwidth',
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-bg-video',
			'name' => '_fildisi_eutf_single_item_video_mp4',
			'value' => $video_mp4,
			'label' => esc_html__( 'MP4 File URL', 'fildisi' ),
			'width' => 'fullwidth',
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-bg-video',
			'name' => '_fildisi_eutf_single_item_video_ogv',
			'value' => $video_ogv,
			'label' => esc_html__( 'OGV File URL', 'fildisi' ),
			'width' => 'fullwidth',
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-boolean',
			'name' => '_fildisi_eutf_single_item_video_poster',
			'value' => $video_poster,
			'label' => esc_html__( 'Use Fallback Image as Poster', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-boolean',
			'name' => '_fildisi_eutf_single_item_video_device',
			'value' => $video_device,
			'label' => esc_html__( 'Show video on devices', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-boolean',
			'name' => '_fildisi_eutf_single_item_video_loop',
			'value' => $video_loop,
			'label' => esc_html__( 'Loop', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select-boolean',
			'name' => '_fildisi_eutf_single_item_video_muted',
			'value' => $video_muted,
			'label' => array(
				"title" => esc_html__( 'Muted', 'fildisi' ),
				"info" => esc_html__( 'Note: Due to new browser/device restrictions, videos with sound are no longer allowed to autoplay in Chrome, Safari and mobile devices. In these cases videos will be automatically muted.', 'fildisi' ),
			),
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'select',
			'name' => '_fildisi_eutf_single_item_video_effect',
			'options' => array(
				'' => esc_html__( 'None', 'fildisi' ),
				'animated' => esc_html__( 'Animated', 'fildisi' ),
			),
			'value' => $video_effect,
			'label' => esc_html__( 'Video Effect', 'fildisi' ),
		)
	);


}

/**
 * Prints Item Background Video Options
 */
function fildisi_eutf_print_admin_feature_item_youtube_options( $item ) {

	$video_url = fildisi_eutf_array_value( $item, 'video_url' );
	$video_start = fildisi_eutf_array_value( $item, 'video_start' );
	$video_end = fildisi_eutf_array_value( $item, 'video_end' );
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'label',
			'label' => esc_html__( 'YouTube Video', 'fildisi' ),
		)
	);

	fildisi_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => '_fildisi_eutf_single_item_video_url',
			'value' => $video_url,
			'label' => array(
				"title" => esc_html__( 'YouTube link', 'fildisi' ),
				"desc" => esc_html__( 'e.g: https://www.youtube.com/watch?v=C9tsrkRSw-Q', 'fildisi' ),
			),
			'width' => 'fullwidth',
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => '_fildisi_eutf_single_item_video_start',
			'value' => $video_start,
			'label' => array(
				"title" => esc_html__( 'Start at:', 'fildisi' ),
				"desc" => esc_html__( 'Value in seconds', 'fildisi' ),
			),
		)
	);
	fildisi_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => '_fildisi_eutf_single_item_video_end',
			'value' => $video_end,
			'label' => array(
				"title" => esc_html__( 'End at:', 'fildisi' ),
				"desc" => esc_html__( 'Value in seconds', 'fildisi' ),
			),
		)
	);
}


function fildisi_eutf_admin_get_feature_section( $post_id ) {

	$post_type = get_post_type( $post_id );

	//Feature Section
	$feature_section = get_post_meta( $post_id, '_fildisi_eutf_feature_section', true );

	//Feature Settings
	$feature_settings = fildisi_eutf_array_value( $feature_section, 'feature_settings' );

	$feature_element = fildisi_eutf_array_value( $feature_settings, 'element' );
	$feature_size = fildisi_eutf_array_value( $feature_settings, 'size' );
	$feature_height = fildisi_eutf_array_value( $feature_settings, 'height', '60' );
	$feature_min_height = fildisi_eutf_array_value( $feature_settings, 'min_height', '200' );
	$feature_bg_color  = fildisi_eutf_array_value( $feature_settings, 'bg_color', 'dark' );
	$feature_bg_color_custom  = fildisi_eutf_array_value( $feature_settings, 'bg_color_custom', '#eef1f6' );
	$feature_bg_gradient_color_1  = fildisi_eutf_array_value( $feature_settings, 'bg_gradient_color_1', '#034e90' );
	$feature_bg_gradient_color_2  = fildisi_eutf_array_value( $feature_settings, 'bg_gradient_color_2', '#19b4d7' );
	$feature_bg_gradient_direction  = fildisi_eutf_array_value( $feature_settings, 'bg_gradient_direction', '90' );
	$feature_header_position = fildisi_eutf_array_value( $feature_settings, 'header_position', 'above' );
	$feature_separator_bottom  = fildisi_eutf_array_value( $feature_settings, 'separator_bottom' );
	$feature_separator_bottom_size  = fildisi_eutf_array_value( $feature_settings, 'separator_bottom_size', '90px' );
	$feature_separator_bottom_color  = fildisi_eutf_array_value( $feature_settings, 'separator_bottom_color', '#ffffff' );


	//Feature Single Item
	$feature_single_item = fildisi_eutf_array_value( $feature_section, 'single_item' );
	$feature_single_item_button = fildisi_eutf_array_value( $feature_single_item, 'button' );
	$feature_single_item_button2 = fildisi_eutf_array_value( $feature_single_item, 'button2' );


	//Slider Item
	$slider_items = fildisi_eutf_array_value( $feature_section, 'slider_items' );
	$slider_settings = fildisi_eutf_array_value( $feature_section, 'slider_settings' );

	$slider_speed = fildisi_eutf_array_value( $slider_settings, 'slideshow_speed', '3500' );
	$slider_pause = fildisi_eutf_array_value( $slider_settings, 'slider_pause', 'no' );
	$slider_dir_nav = fildisi_eutf_array_value( $slider_settings, 'direction_nav', '1' );
	$slider_dir_nav_color = fildisi_eutf_array_value( $slider_settings, 'direction_nav_color', 'light' );
	$slider_transition = fildisi_eutf_array_value( $slider_settings, 'transition', 'slide' );
	$slider_effect = fildisi_eutf_array_value( $slider_settings, 'slider_effect' );
	$slider_pagination = fildisi_eutf_array_value( $slider_settings, 'pagination', 'yes' );

	//Map Item
	$map_items = fildisi_eutf_array_value( $feature_section, 'map_items' );
	$map_settings = fildisi_eutf_array_value( $feature_section, 'map_settings' );

	$map_zoom = fildisi_eutf_array_value( $map_settings, 'zoom', 14 );
	$map_marker = fildisi_eutf_array_value( $map_settings, 'marker' );
	$map_disable_style = fildisi_eutf_array_value( $map_settings, 'disable_style', 'no' );

	//Revolution slider
	$revslider_alias = fildisi_eutf_array_value( $feature_section, 'revslider_alias' );

	global $fildisi_eutf_area_height;

?>

		<div class="eut-fields-wrapper eut-highlight">
			<div class="eut-label">
				<label for="eut-page-feature-element">
					<span class="eut-title"><?php esc_html_e( 'Feature Element', 'fildisi' ); ?></span>
					<span class="eut-description"><?php esc_html_e( 'Select feature section element', 'fildisi' ); ?></span>
				</label>
			</div>
			<div class="eut-field-items-wrapper">
				<div class="eut-field-item">
					<select id="eut-page-feature-element" name="_fildisi_eutf_page_feature_element">
						<option value="" <?php selected( "", $feature_element ); ?>><?php esc_html_e( 'None', 'fildisi' ); ?></option>
						<option value="title" <?php selected( "title", $feature_element ); ?>><?php esc_html_e( 'Title', 'fildisi' ); ?></option>
						<option value="image" <?php selected( "image", $feature_element ); ?>><?php esc_html_e( 'Image', 'fildisi' ); ?></option>
						<option value="video" <?php selected( "video", $feature_element ); ?>><?php esc_html_e( 'Video', 'fildisi' ); ?></option>
						<option value="youtube" <?php selected( "youtube", $feature_element ); ?>><?php esc_html_e( 'YouTube', 'fildisi' ); ?></option>
						<option value="slider" <?php selected( "slider", $feature_element ); ?>><?php esc_html_e( 'Slider', 'fildisi' ); ?></option>
						<option value="revslider" <?php selected( "revslider", $feature_element ); ?>><?php esc_html_e( 'Revolution Slider', 'fildisi' ); ?></option>
						<option value="map" <?php selected( "map", $feature_element ); ?>><?php esc_html_e( 'Map', 'fildisi' ); ?></option>
					</select>
				</div>
			</div>
		</div>

		<div id="eut-feature-section-options" class="eut-feature-section-item postbox" <?php if ( "" != $feature_element ) { ?> style="display:none;" <?php } ?>>

			<div class="eut-fields-wrapper eut-feature-options-wrapper">
				<div class="eut-label">
					<label for="eut-page-feature-element">
						<span class="eut-title"><?php esc_html_e( 'Feature Size', 'fildisi' ); ?></span>
						<span class="eut-description"><?php esc_html_e( 'With Custom Size option you can select the feature height.', 'fildisi' ); ?></span>
					</label>
				</div>
				<div class="eut-field-items-wrapper">
					<div class="eut-field-item">
						<select name="_fildisi_eutf_page_feature_size" id="eut-page-feature-size">
							<option value="" <?php selected( "", $feature_size ); ?>><?php esc_html_e( 'Full Screen', 'fildisi' ); ?></option>
							<option value="custom" <?php selected( "custom", $feature_size ); ?>><?php esc_html_e( 'Custom Size', 'fildisi' ); ?></option>
						</select>
					</div>
					<div class="eut-field-item">
						<span id="eut-feature-section-height" <?php if ( "" == $feature_size ) { ?> style="display:none;" <?php } ?>>
							<select name="_fildisi_eutf_page_feature_height">
								<?php fildisi_eutf_print_select_options( $fildisi_eutf_area_height, $feature_height ); ?>
							</select>
							<span class="eut-sub-title"><?php esc_html_e( 'Height', 'fildisi' ); ?></span>
							<input type="text" name="_fildisi_eutf_page_feature_min_height" value="<?php echo esc_attr( $feature_min_height ); ?>"/>
							<span class="eut-sub-title"><?php esc_html_e( 'Minimum Height in px', 'fildisi' ); ?></span>
						</span>
					</div>
				</div>
			</div>
			<?php
				fildisi_eutf_print_admin_option(
					array(
						'type' => 'select',
						'options' => array(
							'above' => esc_html__( 'Header above Feature', 'fildisi' ),
							'below' => esc_html__( 'Header below Feature', 'fildisi' ),
						),
						'name' => '_fildisi_eutf_page_feature_header_position',
						'value' => $feature_header_position,
						'label' => array(
							'title' => esc_html__( 'Feature/Header Position', 'fildisi' ),
							'desc' => esc_html__( 'With this option header will be shown above or below feature section.', 'fildisi' ),
						),
					)
				);
			?>
			<div id="eut-feature-options-wrapper" class="eut-feature-options-wrapper">
			<?php

				fildisi_eutf_print_admin_option(
					array(
						'type' => 'select-colorpicker',
						'name' => '_fildisi_eutf_page_feature_bg_color',
						'id' => '_fildisi_eutf_page_feature_bg_color',
						'value' => $feature_bg_color,
						'value2' => $feature_bg_color_custom,
						'label' => esc_html__( 'Background Color', 'fildisi' ),
						'multiple' => 'multi',
						'type_usage' => 'feature-bg',
						'group_id' => 'eut-feature-options-wrapper',
					)
				);
				fildisi_eutf_print_admin_option(
					array(
						'type' => 'colorpicker',
						'name' => '_fildisi_eutf_page_feature_bg_gradient_color_1',
						'value' => $feature_bg_gradient_color_1,
						'label' => esc_html__( 'Custom Color 1', 'fildisi' ),
						'dependency' =>
						'[
							{ "id" : "_fildisi_eutf_page_feature_bg_color", "values" : ["gradient"] }
						]',
					)
				);
				fildisi_eutf_print_admin_option(
					array(
						'type' => 'colorpicker',
						'name' => '_fildisi_eutf_page_feature_bg_gradient_color_2',
						'value' => $feature_bg_gradient_color_2,
						'label' => esc_html__( 'Custom Color 2', 'fildisi' ),
						'dependency' =>
						'[
							{ "id" : "_fildisi_eutf_page_feature_bg_color", "values" : ["gradient"] }
						]',
					)
				);
				fildisi_eutf_print_admin_option(
					array(
						'type' => 'select',
						'name' => '_fildisi_eutf_page_feature_bg_gradient_direction',
						'value' => $feature_bg_gradient_direction,
						'options' => array(
							'90' => esc_html__( "Left to Right", 'fildisi' ),
							'135' => esc_html__( "Left Top to Right Bottom", 'fildisi' ),
							'45' => esc_html__( "Left Bottom to Right Top", 'fildisi' ),
							'180' => esc_html__( "Bottom to Top", 'fildisi' ),
						),
						'label' => array(
							"title" => esc_html__( 'Gradient Direction', 'fildisi' ),
						),
						'dependency' =>
						'[
							{ "id" : "_fildisi_eutf_page_feature_bg_color", "values" : ["gradient"] }
						]',
					)
				);

				$fildisi_eutf_feature_separator_list = array(
					'' => esc_html__( "None", 'fildisi' ),
					'triangle-separator' => esc_html__( "Triangle", 'fildisi' ),
					'large-triangle-separator' => esc_html__( "Large Triangle", 'fildisi' ),
					'curve-separator' => esc_html__( "Curve", 'fildisi' ),
					'curve-left-separator' => esc_html__( "Curve Left", 'fildisi' ),
					'curve-right-separator' => esc_html__( "Curve Right", 'fildisi' ),
					'tilt-left-separator' => esc_html__( "Tilt Left", 'fildisi' ),
					'tilt-right-separator' => esc_html__( "Tilt Right", 'fildisi' ),
					'round-split-separator' => esc_html__( "Round Split", 'fildisi' ),
					'torn-paper-separator' => esc_html__( "Torn Paper", 'fildisi' ),
				);

				$fildisi_eutf_feature_separator_size_list = array(
					'30px' => esc_html__( "Small", 'fildisi' ),
					'60px' => esc_html__( "Medium", 'fildisi' ),
					'90px' => esc_html__( "Large", 'fildisi' ),
					'120px'=> esc_html__( "Extra Large", 'fildisi' ),
					'100%' => esc_html__( "Section Height", 'fildisi' ),
				);
				fildisi_eutf_print_admin_option(
					array(
						'type' => 'select',
						'name' => '_fildisi_eutf_page_feature_separator_bottom',
						'id' => '_fildisi_eutf_page_feature_separator_bottom',
						'value' => $feature_separator_bottom,
						'options' => $fildisi_eutf_feature_separator_list,
						'label' => array(
							"title" => esc_html__( 'Bottom Separator', 'fildisi' ),
						),
						'group_id' => 'eut-feature-options-wrapper',
					)
				);
				fildisi_eutf_print_admin_option(
					array(
						'type' => 'colorpicker',
						'name' => '_fildisi_eutf_page_feature_separator_bottom_color',
						'value' => $feature_separator_bottom_color,
						'label' => esc_html__( 'Bottom Separator Color', 'fildisi' ),
						'dependency' =>
						'[
							{ "id" : "_fildisi_eutf_page_feature_separator_bottom", "value_not_equal_to" : [""] }
						]',
					)
				);
				fildisi_eutf_print_admin_option(
					array(
						'type' => 'select',
						'name' => '_fildisi_eutf_page_feature_separator_bottom_size',
						'value' => $feature_separator_bottom_size,
						'options' => $fildisi_eutf_feature_separator_size_list,
						'label' => array(
							"title" => esc_html__( 'Bottom Separator Size', 'fildisi' ),
						),
						'dependency' =>
						'[
							{ "id" : "_fildisi_eutf_page_feature_separator_bottom", "value_not_equal_to" : [""] }
						]',
					)
				);
			?>
			</div>

		</div>



		<div id="eut-feature-section-slider" class="eut-feature-section-item" <?php if ( "slider" != $feature_element ) { ?> style="display:none;" <?php } ?>>

			<div class="postbox">
				<h3 class="eut-title">
					<span><?php esc_html_e( 'Slider Settings', 'fildisi' ); ?></span>
				</h3>
				<div class="inside">

					<?php
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'textfield',
								'name' => '_fildisi_eutf_page_slider_settings_speed',
								'value' => $slider_speed,
								'label' => esc_html__( 'Slideshow Speed', 'fildisi' ),
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_page_slider_settings_pause',
								'options' => array(
									'no' => esc_html__( 'No', 'fildisi' ),
									'yes' => esc_html__( 'Yes', 'fildisi' ),
								),
								'value' => $slider_pause,
								'label' => esc_html__( 'Pause on Hover', 'fildisi' ),
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'options' => array(
									'yes' => esc_html__( 'Yes', 'fildisi' ),
									'no' => esc_html__( 'No', 'fildisi' ),
								),
								'name' => '_fildisi_eutf_page_slider_settings_pagination',
								'value' => $slider_pagination,
								'label' => array(
									'title' => esc_html__( 'Pagination', 'fildisi' ),
								),
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'options' => array(
									'1' => esc_html__( 'Style 1', 'fildisi' ),
									'2' => esc_html__( 'Style 2', 'fildisi' ),
									'3' => esc_html__( 'Style 3', 'fildisi' ),
									'0' => esc_html__( 'No Navigation', 'fildisi' ),
								),
								'name' => '_fildisi_eutf_page_slider_settings_direction_nav',
								'value' => $slider_dir_nav,
								'label' => array(
									'title' => esc_html__( 'Navigation Buttons', 'fildisi' ),
								),
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'options' => array(
									'slide' => esc_html__( 'Slide', 'fildisi' ),
									'fade' => esc_html__( 'Fade', 'fildisi' ),
									'backSlide' => esc_html__( 'Back Slide', 'fildisi' ),
									'goDown' => esc_html__( 'Go Down', 'fildisi' ),
								),
								'name' => '_fildisi_eutf_page_slider_settings_transition',
								'value' => $slider_transition,
								'label' => array(
									'title' => esc_html__( 'Transition', 'fildisi' ),
								),
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'options' => array(
									'' => esc_html__( 'None', 'fildisi' ),
									'animated' => esc_html__( 'Animated', 'fildisi' ),
									'parallax' => esc_html__( 'Classic Parallax', 'fildisi' ),
									'advanced-parallax' => esc_html__( 'Advanced Parallax', 'fildisi' ),
									'fixed-section' => esc_html__( 'Fixed Section', 'fildisi' ),
								),
								'name' => '_fildisi_eutf_page_slider_settings_effect',
								'value' => $slider_effect,
								'label' => array(
									'title' => esc_html__( 'Slider Effect', 'fildisi' ),
								),
							)
						);
					?>

					<div class="eut-fields-wrapper">
						<div class="eut-label">
							<label for="eut-page-feature-element">
								<span class="eut-title"><?php esc_html_e( 'Add Slides', 'fildisi' ); ?></span>
							</label>
						</div>
						<div class="eut-field-items-wrapper">
							<div class="eut-field-item">
								<input type="button" class="eut-upload-feature-slider-button button-primary" value="<?php esc_attr_e( 'Insert Images to Slider', 'fildisi' ); ?>"/>
								<span id="eut-upload-feature-slider-button-spinner" class="eut-action-spinner"></span>
							</div>
						</div>
					</div>
					<?php if ( 'product' != $post_type && 'tribe_events' != $post_type ) { ?>
					<div class="eut-fields-wrapper">
						<div class="eut-label">
							<label for="eut-page-feature-element">
								<span class="eut-title"><?php esc_html_e( 'Add Post Slides', 'fildisi' ); ?></span>
							</label>
						</div>
						<div class="eut-field-items-wrapper">
							<div class="eut-field-item">
								<input type="button" class="eut-upload-feature-slider-post-button button-primary" value="<?php esc_attr_e( 'Insert Posts to Slider', 'fildisi' ); ?>"/>
							</div>
							<div class="eut-field-item">
								<input id="eut-upload-feature-slider-post-selection" type="hidden" class="eut-post-selector-select2"  value="" />
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div id="eut-feature-slider-container" data-mode="slider-full" class="eut-feature-section-item" <?php if ( 'slider' != $feature_element ) { ?> style="display:none;" <?php } ?>>
			<?php
				if( !empty( $slider_items ) ) {
					fildisi_eutf_print_admin_feature_slider_items( $slider_items );
				}
			?>
		</div>

		<div id="eut-feature-map-container" class="eut-feature-section-item" <?php if ( 'map' != $feature_element ) { ?> style="display:none;" <?php } ?>>
			<div class="eut-map-item postbox">
				<h3 class="eut-title">
					<span><?php esc_html_e( 'Map', 'fildisi' ); ?></span>
				</h3>
				<div class="inside">
					<div class="eut-fields-wrapper">
						<div class="eut-label">
							<label for="eut-page-feature-element">
								<span class="eut-title"><?php esc_html_e( 'Single Point Zoom', 'fildisi' ); ?></span>
							</label>
						</div>
						<div class="eut-field-items-wrapper">
							<div class="eut-field-item">
								<select id="eut-page-feature-map-zoom" name="_fildisi_eutf_page_feature_map_zoom">
									<?php for ( $i=1; $i < 20; $i++ ) { ?>
										<option value="<?php echo esc_attr( $i ); ?>" <?php selected( $i, $map_zoom ); ?>><?php echo esc_html( $i ); ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="eut-fields-wrapper">
						<div class="eut-label">
							<label for="eut-page-feature-element">
								<span class="eut-title"><?php esc_html_e( 'Global Marker', 'fildisi' ); ?></span>
							</label>
						</div>
						<div class="eut-field-items-wrapper">
							<div class="eut-field-item eut-field-item-fullwidth">
								<input type="text" class="eut-upload-simple-media-field" id="eut-page-feature-map-marker" name="_fildisi_eutf_page_feature_map_marker" value="<?php echo esc_attr( $map_marker ); ?>"/>
								<label></label>
								<input type="button" data-media-type="image" class="eut-upload-simple-media-button button-primary" value="<?php esc_attr_e( 'Insert Marker', 'fildisi' ); ?>"/>
								<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'fildisi' ); ?>"/>
							</div>
						</div>
					</div>
					<div class="eut-fields-wrapper">
						<div class="eut-label">
							<label for="eut-page-feature-element">
								<span class="eut-title"><?php esc_html_e( 'Disable Custom Style', 'fildisi' ); ?></span>
							</label>
						</div>
						<div class="eut-field-items-wrapper">
							<div class="eut-field-item">
								<select id="eut-page-feature-map-disable-style" name="_fildisi_eutf_page_feature_map_disable_style">
									<option value="no" <?php selected( "no", $map_disable_style ); ?>><?php esc_html_e( 'No', 'fildisi' ); ?></option>
									<option value="yes" <?php selected( "yes", $map_disable_style ); ?>><?php esc_html_e( 'Yes', 'fildisi' ); ?></option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="eut-fields-wrapper">
					<div class="eut-label">
						<label for="eut-page-feature-element">
							<span class="eut-title"><?php esc_html_e( 'Add Map Points', 'fildisi' ); ?></span>
						</label>
					</div>
					<div class="eut-field-items-wrapper">
						<div class="eut-field-item">
							<input type="button" id="eut-upload-multi-map-point" class="eut-upload-multi-map-point button-primary" value="<?php esc_attr_e( 'Insert Point to Map', 'fildisi' ); ?>"/>
							<span id="eut-upload-multi-map-button-spinner" class="eut-action-spinner"></span>
						</div>
					</div>
				</div>
			</div>
			<?php fildisi_eutf_print_admin_feature_map_items( $map_items ); ?>
		</div>

		<div id="eut-feature-single-container" class="eut-feature-section-item" <?php if ( 'title' != $feature_element && 'image' != $feature_element && 'video' != $feature_element && 'youtube' != $feature_element ) { ?> style="display:none;" <?php } ?>>
			<div class="eut-video-item postbox">
				<span class="eut-modal-spinner"></span>
				<h3 class="eut-title">
					<span><?php esc_html_e( 'Options', 'fildisi' ); ?></span>
				</h3>
				<div class="inside">

					<!--  METABOXES -->
					<div class="eut-metabox-content">

						<!-- TABS -->
						<div class="eut-tabs">

							<ul class="eut-tab-links">
								<li class="eut-feature-required eut-item-feature-video-settings active"><a id="eut-feature-single-tab-video-link" href="#eut-feature-single-tab-video"><?php esc_html_e( 'Video', 'fildisi' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-youtube-settings active"><a id="eut-feature-single-tab-youtube-link" href="#eut-feature-single-tab-youtube"><?php esc_html_e( 'YouTube', 'fildisi' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-bg-settings"><a id="eut-feature-single-tab-bg-link" href="#eut-feature-single-tab-bg"><?php esc_html_e( 'Background', 'fildisi' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-content-settings"><a id="eut-feature-single-tab-content-link" href="#eut-feature-single-tab-content"><?php esc_html_e( 'Content', 'fildisi' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-revslider-settings"><a id="eut-feature-single-tab-revslider-link" href="#eut-feature-single-tab-revslider"><?php esc_html_e( 'Revolution Slider', 'fildisi' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-button-settings"><a href="#eut-feature-single-tab-button"><?php esc_html_e( 'First Button', 'fildisi' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-button-settings"><a href="#eut-feature-single-tab-button2"><?php esc_html_e( 'Second Button', 'fildisi' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-extra-settings"><a href="#eut-feature-single-tab-extra"><?php esc_html_e( 'Extra', 'fildisi' ); ?></a></li>
							</ul>

							<div class="eut-tab-content">
								<div id="eut-feature-single-tab-video" class="eut-tab-item active">
									<?php fildisi_eutf_print_admin_feature_item_video_options( $feature_single_item ); ?>
								</div>
								<div id="eut-feature-single-tab-youtube" class="eut-tab-item">
									<?php fildisi_eutf_print_admin_feature_item_youtube_options( $feature_single_item ); ?>
								</div>
								<div id="eut-feature-single-tab-revslider" class="eut-tab-item">
									<?php
										fildisi_eutf_print_admin_option(
												array(
													'type' => 'select',
													'options' => fildisi_eutf_get_revolution_selection(),
													'name' => '_fildisi_eutf_page_revslider',
													'value' => $revslider_alias,
													'label' => array(
														'title' => esc_html__( 'Revolution Slider', 'fildisi' ),
													),
												)
											);
									?>
								</div>
								<div id="eut-feature-single-tab-bg" class="eut-tab-item">
									<?php fildisi_eutf_print_admin_feature_item_background_options( $feature_single_item ); ?>
									<?php fildisi_eutf_print_admin_feature_item_overlay_options( $feature_single_item ); ?>
								</div>
								<div id="eut-feature-single-tab-content" class="eut-tab-item">
									<?php fildisi_eutf_print_admin_feature_item_content_options( $feature_single_item ); ?>
								</div>
								<div id="eut-feature-single-tab-button" class="eut-tab-item">
									<?php fildisi_eutf_print_admin_feature_item_button_options( $feature_single_item_button, '_fildisi_eutf_single_item_button_' ); ?>
								</div>
								<div id="eut-feature-single-tab-button2" class="eut-tab-item">
									<?php fildisi_eutf_print_admin_feature_item_button_options( $feature_single_item_button2, '_fildisi_eutf_single_item_button2_' ); ?>
								</div>
								<div id="eut-feature-single-tab-extra" class="eut-tab-item">
									<?php fildisi_eutf_print_admin_feature_item_extra_options( $feature_single_item ); ?>
								</div>
							</div>

						</div>
						<!-- END TABS -->

					</div>
					<!-- END  METABOXES -->
				</div>
			</div>
		</div>
<?php
}

function fildisi_eutf_admin_save_feature_section( $post_id ) {

	//Feature Section variable
	$feature_section = array();


	if ( isset( $_POST['_fildisi_eutf_page_feature_element'] ) ) {

		//Feature Settings

		$feature_section['feature_settings'] = array (
			'element' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_element'] ),
			'size' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_size'] ),
			'height' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_height'] ),
			'min_height' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_min_height'] ),
			'header_position' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_header_position'] ),
			'bg_color' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_bg_color'] ),
			'bg_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_bg_color_custom'] ),
			'bg_gradient_color_1' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_bg_gradient_color_1'] ),
			'bg_gradient_color_2' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_bg_gradient_color_2'] ),
			'bg_gradient_direction' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_bg_gradient_direction'] ),
			'separator_bottom' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_separator_bottom'] ),
			'separator_bottom_color' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_separator_bottom_color'] ),
			'separator_bottom_size' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_separator_bottom_size'] ),
		);


		//Feature Revolution Slider
		if ( isset( $_POST['_fildisi_eutf_page_revslider'] ) ) {
			$feature_section['revslider_alias'] = sanitize_text_field( $_POST['_fildisi_eutf_page_revslider'] );
		}

		//Feature Single Item
		if ( isset( $_POST['_fildisi_eutf_single_item_title'] ) ) {


			$feature_section['single_item'] = array (

				'title' => wp_filter_post_kses( $_POST['_fildisi_eutf_single_item_title'] ),
				'content_bg_color' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_title_content_bg_color'] ),
				'content_bg_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_title_content_bg_color_custom'] ),
				'title_color' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_title_color'] ),
				'title_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_title_color_custom'] ),
				'title_tag' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_title_tag'] ),
				'caption' => wp_filter_post_kses( $_POST['_fildisi_eutf_single_item_caption'] ),
				'caption_color' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_caption_color'] ),
				'caption_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_caption_color_custom'] ),
				'caption_tag' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_caption_tag'] ),
				'subheading' => wp_filter_post_kses( $_POST['_fildisi_eutf_single_item_subheading'] ),
				'subheading_color' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_subheading_color'] ),
				'subheading_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_subheading_color_custom'] ),
				'subheading_tag' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_subheading_tag'] ),
				'subheading_family' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_subheading_family'] ),
				'title_family' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_title_family'] ),
				'caption_family' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_caption_family'] ),
				'content_size' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_content_size'] ),
				'content_align' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_content_align'] ),
				'content_position' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_content_position'] ),
				'content_animation' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_content_animation'] ),
				'container_size' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_container_size'] ),
				'content_image_id' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_content_image_id'] ),
				'content_image_size' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_content_image_size'] ),
				'content_image_max_height' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_content_image_max_height'] ),
				'content_image_responsive_max_height' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_content_image_responsive_max_height'] ),
				'pattern_overlay' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_pattern_overlay'] ),
				'color_overlay' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_color_overlay'] ),
				'color_overlay_custom' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_color_overlay_custom'] ),
				'opacity_overlay' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_opacity_overlay'] ),
				'gradient_overlay_custom_1' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_gradient_overlay_custom_1'] ),
				'gradient_overlay_custom_1_opacity' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_gradient_overlay_custom_1_opacity'] ),
				'gradient_overlay_custom_2' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_gradient_overlay_custom_2'] ),
				'gradient_overlay_custom_2_opacity' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_gradient_overlay_custom_2_opacity'] ),
				'gradient_overlay_direction' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_gradient_overlay_direction'] ),
				'bg_image_id' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_bg_image_id'] ),
				'bg_image_size' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_bg_image_size'] ),
				'bg_position' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_bg_position'] ),
				'bg_tablet_sm_position' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_bg_tablet_sm_position'] ),
				'image_effect' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_image_effect'] ),
				'video_webm' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_webm'] ),
				'video_mp4' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_mp4'] ),
				'video_ogv' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_ogv'] ),
				'video_poster' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_poster'] ),
				'video_device' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_device'] ),
				'video_loop' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_loop'] ),
				'video_muted' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_muted'] ),
				'video_effect' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_effect'] ),
				'video_url' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_url'] ),
				'video_start' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_start'] ),
				'video_end' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_video_end'] ),
				'button' => array(
					'id' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button_id'] ),
					'text' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button_text'] ),
					'url' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button_url'] ),
					'target' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button_target'] ),
					'color' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button_color'] ),
					'hover_color' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button_hover_color'] ),
					'size' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button_size'] ),
					'shape' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button_shape'] ),
					'type' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button_type'] ),
					'class' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button_class'] ),
				),
				'button2' => array(
					'id' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button2_id'] ),
					'text' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button2_text'] ),
					'url' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button2_url'] ),
					'target' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button2_target'] ),
					'color' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button2_color'] ),
					'hover_color' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button2_hover_color'] ),
					'size' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button2_size'] ),
					'shape' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button2_shape'] ),
					'type' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button2_type'] ),
					'class' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_button2_class'] ),
				),
				'arrow_enabled' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_arrow_enabled'] ),
				'arrow_color' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_arrow_color'] ),
				'arrow_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_arrow_color_custom'] ),
				'el_class' => sanitize_text_field( $_POST['_fildisi_eutf_single_item_el_class'] ),

			);
		}

		//Feature Slider Items
		$slider_items = array();
		if ( isset( $_POST['_fildisi_eutf_slider_item_id'] ) ) {

			$num_of_images = sizeof( $_POST['_fildisi_eutf_slider_item_id'] );
			for ( $i=0; $i < $num_of_images; $i++ ) {

				$slide = array (
					'id' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_id'][ $i ] ),
					'type' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_type'][ $i ] ),
					'post_id' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_post_id'][ $i ] ),
					'bg_image_id' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_bg_image_id'][ $i ] ),
					'bg_image_size' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_bg_image_size'][ $i ] ),
					'bg_position' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_bg_position'][ $i ] ),
					'bg_tablet_sm_position' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_bg_tablet_sm_position'][ $i ] ),
					'header_style' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_header_style'][ $i ] ),
					'title' => wp_filter_post_kses( $_POST['_fildisi_eutf_slider_item_title'][ $i ] ),
					'content_bg_color' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_title_content_bg_color'][ $i ] ),
					'content_bg_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_title_content_bg_color_custom'][ $i ] ),
					'title_color' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_title_color'][ $i ] ),
					'title_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_title_color_custom'][ $i ] ),
					'title_tag' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_title_tag'][ $i ] ),
					'caption' => wp_filter_post_kses( $_POST['_fildisi_eutf_slider_item_caption'][ $i ] ),
					'caption_color' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_caption_color'][ $i ] ),
					'caption_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_caption_color_custom'][ $i ] ),
					'caption_tag' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_caption_tag'][ $i ] ),
					'subheading' => wp_filter_post_kses( $_POST['_fildisi_eutf_slider_item_subheading'][ $i ] ),
					'subheading_color' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_subheading_color'][ $i ] ),
					'subheading_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_subheading_color_custom'][ $i ] ),
					'subheading_tag' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_subheading_tag'][ $i ] ),
					'subheading_family' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_subheading_family'][ $i ] ),
					'title_family' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_title_family'][ $i ] ),
					'caption_family' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_caption_family'][ $i ] ),
					'content_size' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_content_size'][ $i ] ),
					'content_align' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_content_align'][ $i ] ),
					'content_position' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_content_position'][ $i ] ),
					'content_animation' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_content_animation'][ $i ] ),
					'container_size' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_container_size'][ $i ] ),
					'content_image_id' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_content_image_id'][ $i ] ),
					'content_image_size' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_content_image_size'][ $i ] ),
					'content_image_max_height' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_content_image_max_height'][ $i ] ),
					'content_image_responsive_max_height' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_content_image_responsive_max_height'][ $i ] ),
					'pattern_overlay' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_pattern_overlay'][ $i ] ),
					'color_overlay' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_color_overlay'][ $i ] ),
					'color_overlay_custom' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_color_overlay_custom'][ $i ] ),
					'opacity_overlay' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_opacity_overlay'][ $i ] ),
					'gradient_overlay_custom_1' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_gradient_overlay_custom_1'][ $i ] ),
					'gradient_overlay_custom_1_opacity' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_gradient_overlay_custom_1_opacity'][ $i ] ),
					'gradient_overlay_custom_2' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_gradient_overlay_custom_2'][ $i ] ),
					'gradient_overlay_custom_2_opacity' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_gradient_overlay_custom_2_opacity'][ $i ] ),
					'gradient_overlay_direction' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_gradient_overlay_direction'][ $i ] ),
					'button' => array(
						'id' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button_id'][ $i ] ),
						'text' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button_text'][ $i ] ),
						'url' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button_url'][ $i ] ),
						'target' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button_target'][ $i ] ),
						'color' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button_color'][ $i ] ),
						'hover_color' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button_hover_color'][ $i ] ),
						'size' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button_size'][ $i ] ),
						'shape' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button_shape'][ $i ] ),
						'type' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button_type'][ $i ] ),
						'class' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button_class'][ $i ] ),
					),
					'button2' => array(
						'id' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button2_id'][ $i ] ),
						'text' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button2_text'][ $i ] ),
						'url' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button2_url'][ $i ] ),
						'target' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button2_target'][ $i ] ),
						'color' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button2_color'][ $i ] ),
						'hover_color' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button2_hover_color'][ $i ] ),
						'size' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button2_size'][ $i ] ),
						'shape' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button2_shape'][ $i ] ),
						'type' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button2_type'][ $i ] ),
						'class' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_button2_class'][ $i ] ),
					),
					'arrow_enabled' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_arrow_enabled'][ $i ] ),
					'arrow_color' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_arrow_color'][ $i ] ),
					'arrow_color_custom' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_arrow_color_custom'][ $i ] ),
					'el_class' => sanitize_text_field( $_POST['_fildisi_eutf_slider_item_el_class'][ $i ] ),
				);

				$slider_items[] = $slide;
			}

		}



		if( !empty( $slider_items ) ) {
			$feature_section['slider_items'] = $slider_items;

			$feature_section['slider_settings'] = array (
				'slideshow_speed' => sanitize_text_field( $_POST['_fildisi_eutf_page_slider_settings_speed'] ),
				'direction_nav' => sanitize_text_field( $_POST['_fildisi_eutf_page_slider_settings_direction_nav'] ),
				'slider_pause' => sanitize_text_field( $_POST['_fildisi_eutf_page_slider_settings_pause'] ),
				'transition' => sanitize_text_field( $_POST['_fildisi_eutf_page_slider_settings_transition'] ),
				'slider_effect' => sanitize_text_field( $_POST['_fildisi_eutf_page_slider_settings_effect'] ),
				'pagination' => sanitize_text_field( $_POST['_fildisi_eutf_page_slider_settings_pagination'] ),
			);
		}

		//Feature Map Items
		$map_items = array();
		if ( isset( $_POST['_fildisi_eutf_map_item_point_id'] ) ) {

			$num_of_map_points = sizeof( $_POST['_fildisi_eutf_map_item_point_id'] );
			for ( $i=0; $i < $num_of_map_points; $i++ ) {

				$this_point = array (
					'id' => sanitize_text_field( $_POST['_fildisi_eutf_map_item_point_id'][ $i ] ),
					'lat' => sanitize_text_field( $_POST['_fildisi_eutf_map_item_point_lat'][ $i ] ),
					'lng' => sanitize_text_field( $_POST['_fildisi_eutf_map_item_point_lng'][ $i ] ),
					'marker' => sanitize_text_field( $_POST['_fildisi_eutf_map_item_point_marker'][ $i ] ),
					'title' => sanitize_text_field( $_POST['_fildisi_eutf_map_item_point_title'][ $i ] ),
					'info_text' => wp_filter_post_kses( $_POST['_fildisi_eutf_map_item_point_infotext'][ $i ] ),
					'info_text_open' => sanitize_text_field( $_POST['_fildisi_eutf_map_item_point_infotext_open'][ $i ] ),
					'button_text' => sanitize_text_field( $_POST['_fildisi_eutf_map_item_point_button_text'][ $i ] ),
					'button_url' => sanitize_text_field( $_POST['_fildisi_eutf_map_item_point_button_url'][ $i ] ),
					'button_target' => sanitize_text_field( $_POST['_fildisi_eutf_map_item_point_button_target'][ $i ] ),
					'button_class' => sanitize_text_field( $_POST['_fildisi_eutf_map_item_point_button_class'][ $i ] ),
				);
				$map_items[] =  $this_point;
			}

		}

		if( !empty( $map_items ) ) {

			$feature_section['map_items'] = $map_items;
			$feature_section['map_settings'] = array (
				'zoom' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_map_zoom'] ),
				'marker' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_map_marker'] ),
				'disable_style' => sanitize_text_field( $_POST['_fildisi_eutf_page_feature_map_disable_style'] ),
			);

		}

	}

	//Save Feature Section

	$new_meta_value = $feature_section;
	$meta_key = '_fildisi_eutf_feature_section';
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	if ( $new_meta_value && '' == $meta_value ) {
		if ( !add_post_meta( $post_id, $meta_key, $new_meta_value, true ) ) {
			update_post_meta( $post_id, $meta_key, $new_meta_value );
		}
	} elseif ( $new_meta_value && $new_meta_value != $meta_value ) {
		update_post_meta( $post_id, $meta_key, $new_meta_value );
	} elseif ( '' == $new_meta_value && $meta_value ) {
		delete_post_meta( $post_id, $meta_key, $meta_value );
	}

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
