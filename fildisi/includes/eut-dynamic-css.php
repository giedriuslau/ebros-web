<?php
/**
 *  Dynamic css style
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
 */

$css = "";


/* =========================================================================== */

/* Body
/* Container Size
/* Boxed Size
/* Single Post Content Width
/* Top Bar

/* Default Header
	/* - Default Header Colors
	/* - Default Header Menu Colors
	/* - Default Header Sub Menu Colors
	/* - Default Header Layout
	/* - Default Header Overlaping

/* Logo On Top Header
	/* - Logo On Top Header Colors
	/* - Logo On Top Header Menu Colors
	/* - Logo On Top Header Sub Menu Colors
	/* - Logo On Top Header Layout
	/* - Logo On Top Header Overlaping

/* Light Header
/* Dark Header

/* Sticky Header
	/* - Sticky Default Header
	/* - Sticky Logo On Top Header
	/* - Sticky Header Colors
	/* - Fildisi Sticky Header

/* Side Area Colors
/* Modals Colors

/* Responsive Header
	/* - Header Layout
	/* - Responsive Menu
	/* - Responsive Header Elements

/* Spinner
/* Box Item
/* Primary Text Color
/* Primary Bg Color
/* Anchor Menu
/* Breadcrumbs
/* Main Content
	/* - Main Content Borders
	/* - Widget Colors

/* Bottom Bar Colors
/* Post Navigation Bar
/* Portfolio Navigation Bar
/* Single Post Tags & Categories
/* Footer
	/* - Widget Area
	/* - Footer Widget Colors
	/* - Footer Bar Colors




/* =========================================================================== */


/* Body
============================================================================= */
$css .= "
a {
	color: " . fildisi_eutf_option( 'body_text_link_color' ) . ";

}

a:hover {
	color: " . fildisi_eutf_option( 'body_text_link_hover_color' ) . ";
}
";

$fildisi_eutf_container_size_threshold = fildisi_eutf_option( 'container_size', '1170' );
$fildisi_eutf_container_size_threshold = filter_var( $fildisi_eutf_container_size_threshold, FILTER_SANITIZE_NUMBER_INT );

$fildisi_eutf_responsive_header_threshold = fildisi_eutf_option( 'responsive_header_threshold', '1024' );
$fildisi_eutf_responsive_header_threshold = filter_var( $fildisi_eutf_responsive_header_threshold, FILTER_SANITIZE_NUMBER_INT );

/* Container Size
============================================================================= */
$css .= "

.eut-container,
#disqus_thread,
#eut-content.eut-left-sidebar .eut-content-wrapper,
#eut-content.eut-right-sidebar .eut-content-wrapper {
	max-width: " . fildisi_eutf_option( 'container_size', 1170 ) . "px;
}

@media only screen and (max-width: " . esc_attr( $fildisi_eutf_container_size_threshold + 60 ) . "px) {
	.eut-container,
	#disqus_thread,
	#eut-content.eut-left-sidebar .eut-content-wrapper,
	#eut-content.eut-right-sidebar .eut-content-wrapper {
		width: 90%;
		max-width: " . fildisi_eutf_option( 'container_size', 1170 ) . "px;
	}
}

@media only screen and (min-width: 960px) {

	#eut-theme-wrapper.eut-header-side .eut-container,
	#eut-theme-wrapper.eut-header-side #eut-content.eut-left-sidebar .eut-content-wrapper,
	#eut-theme-wrapper.eut-header-side #eut-content.eut-right-sidebar .eut-content-wrapper {
		width: 90%;
		max-width: " . fildisi_eutf_option( 'container_size', 1170 ) . "px;
	}

}

";

/* Boxed Size
============================================================================= */
$css .= "

body.eut-boxed #eut-theme-wrapper {
	width: 100%;
	max-width: " . fildisi_eutf_option( 'boxed_size', 1220 ) . "px;
}

.eut-body.eut-boxed #eut-header.eut-fixed #eut-main-header,
.eut-body.eut-boxed #eut-fildisi-sticky-header,
.eut-body.eut-boxed .eut-anchor-menu .eut-anchor-wrapper.eut-sticky,
.eut-body.eut-boxed #eut-footer.eut-fixed-footer,
.eut-body.eut-boxed #eut-top-bar.eut-fixed .eut-wrapper {
	max-width: " . fildisi_eutf_option( 'boxed_size', 1220 ) . "px;
}

@media only screen and (max-width: 1200px) {

	.eut-body.eut-boxed #eut-header.eut-sticky-header #eut-main-header.eut-header-default,
	.eut-body.eut-boxed #eut-header.eut-fixed #eut-main-header {
		max-width: 90%;
	}

	.eut-body.eut-boxed #eut-top-bar.eut-fixed .eut-wrapper {
		max-width: 90%;
	}
}

";

/* Framed Size
============================================================================= */
$fildisi_eutf_theme_layout = fildisi_eutf_option( 'theme_layout', 'stretched' );
$fildisi_eutf_frame_size = fildisi_eutf_option( 'frame_size', 30 );
if ( 'framed' == $fildisi_eutf_theme_layout ) {
	$css .= "

	@media only screen and (min-width: " . esc_attr( $fildisi_eutf_responsive_header_threshold ) . "px) {
		body.eut-framed {
			margin: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
		}
		.eut-frame {
			background-color: " . fildisi_eutf_option( 'frame_color' ) . ";
		}
		.eut-frame.eut-top {
			top: 0;
			left: 0;
			width: 100%;
			height: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
		}
		.eut-frame.eut-left {
			top: 0;
			left: 0;
			width: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
			height: 100%;
		}
		.eut-frame.eut-right {
			top: 0;
			right: 0;
			width: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
			height: 100%;
		}
		.eut-frame.eut-bottom {
			bottom: 0;
			left: 0;
			width: 100%;
			height: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
		}

		#eut-body.admin-bar .eut-frame.eut-top {
			top: 32px;
		}

		#eut-header.eut-fixed #eut-main-header,
		#eut-fildisi-sticky-header,
		#eut-top-bar.eut-sticky-topbar.eut-fixed .eut-wrapper,
		#eut-theme-wrapper:not(.eut-header-side) .eut-anchor-menu .eut-anchor-wrapper.eut-sticky {
			width: auto;
			left: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
			right: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
		}

		#eut-main-header.eut-header-side {
			top: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
			left: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
		}

		#eut-main-header.eut-header-side .eut-header-elements-wrapper {
			bottom: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
		}

		.eut-back-top {
			bottom: -" . ( esc_attr( $fildisi_eutf_frame_size ) + 60 ) . "px;
			right: " . ( esc_attr( $fildisi_eutf_frame_size ) + 20 ) . "px;
		}
		.eut-back-top.show {
			-webkit-transform: translate(0, -" . ( esc_attr( $fildisi_eutf_frame_size * 2  ) + 80 ) . "px);
			-moz-transform:    translate(0, -" . ( esc_attr( $fildisi_eutf_frame_size * 2  ) + 80 ) . "px);
			-ms-transform:     translate(0, -" . ( esc_attr( $fildisi_eutf_frame_size * 2  ) + 80 ) . "px);
			-o-transform:      translate(0, -" . ( esc_attr( $fildisi_eutf_frame_size * 2  ) + 80 ) . "px);
			transform:         translate(0, -" . ( esc_attr( $fildisi_eutf_frame_size * 2  ) + 80 ) . "px);
		}

		.eut-close-modal {
			top: " . ( esc_attr( $fildisi_eutf_frame_size ) + 20 ) . "px;
			right: " . ( esc_attr( $fildisi_eutf_frame_size ) + 20 ) . "px;
		}

		.eut-hiddenarea-wrapper {
			top: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
		}

		#fp-nav.right,
		#pp-nav.right {
			right: " . ( esc_attr( $fildisi_eutf_frame_size ) + 20 ) . "px;
		}

		.eut-navigation-bar.eut-layout-3 {
			left: calc(100% - " . esc_attr( $fildisi_eutf_frame_size ) . "px);
		}

		#eut-top-bar.eut-sticky-topbar.eut-fixed .eut-wrapper {
			top: " . esc_attr( $fildisi_eutf_frame_size ) . "px;
		}

		.mfp-arrow {
			right: " . ( esc_attr( $fildisi_eutf_frame_size ) + 20 ) . "px;
		}

		.mfp-arrow-left {
			top: 111px;
		}

		.mfp-arrow-right {
			top: 172px;
		}

	}
	";
}


/* Single Post Content Width
============================================================================= */
if ( is_singular( 'post' ) ) {
	$fildisi_eutf_post_content_width = fildisi_eutf_post_meta( '_fildisi_eutf_post_content_width', fildisi_eutf_option( 'post_content_width', 990 ) );

	if ( !is_numeric( $fildisi_eutf_post_content_width ) ) {
		$fildisi_eutf_post_content_width = fildisi_eutf_option( 'container_size', 1170 );
	}

$css .= "

.single-post #eut-content:not(.eut-right-sidebar):not(.eut-left-sidebar) .eut-container {
	max-width: " . esc_attr( $fildisi_eutf_post_content_width ) . "px;
}

";

}


/* Top Bar
============================================================================= */
$css .= "
#eut-top-bar .eut-wrapper {
	padding-top: " . fildisi_eutf_option( 'top_bar_spacing', '', 'padding-top' ) . ";
	padding-bottom: " . fildisi_eutf_option( 'top_bar_spacing', '', 'padding-bottom'  ) . ";
}

#eut-top-bar .eut-wrapper,
#eut-top-bar .eut-language > li > ul,
#eut-top-bar .eut-top-bar-menu ul.sub-menu {
	background-color: " . fildisi_eutf_option( 'top_bar_bg_color' ) . ";
	color: " . fildisi_eutf_option( 'top_bar_font_color' ) . ";
}

#eut-top-bar a {
	color: " . fildisi_eutf_option( 'top_bar_link_color' ) . ";
}

#eut-top-bar a:hover {
	color: " . fildisi_eutf_option( 'top_bar_hover_color' ) . ";
}

";


/* Default Header
============================================================================= */
$fildisi_eutf_header_mode = fildisi_eutf_option( 'header_mode', 'default' );
if ( 'default' == $fildisi_eutf_header_mode ) {

	/* - Default Header Colors
	============================================================================= */

	$fildisi_eutf_default_header_background_color = fildisi_eutf_option( 'default_header_background_color', '#ffffff' );
	$fildisi_eutf_default_header_border_color = fildisi_eutf_option( 'default_header_border_color', '#000000' );
	$css .= "

	#eut-main-header {
		background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_default_header_background_color ) . "," . fildisi_eutf_option( 'default_header_background_color_opacity', '1') . ");
	}

	#eut-main-header.eut-transparent,
	#eut-main-header.eut-light,
	#eut-main-header.eut-dark {
		background-color: transparent;
	}

	#eut-main-header.eut-header-default,
	.eut-header-elements {
		border-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_default_header_border_color ) . "," . fildisi_eutf_option( 'default_header_border_color_opacity', '1') . ");
	}

	";

	/* - Default Header Menu Colors
	========================================================================= */
	$css .= "

	.eut-logo-text a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li > a,
	.eut-header-element > a,
	.eut-header-element .eut-purchased-items {
		color: " . fildisi_eutf_option( 'default_header_menu_text_color' ) . ";
	}

	.eut-hidden-menu-btn a .eut-item:not(.eut-with-text) span {
		background-color: " . fildisi_eutf_option( 'default_header_menu_text_color' ) . ";
	}

	.eut-logo-text a:hover,
	#eut-header .eut-main-menu .eut-wrapper > ul > li.eut-current > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li:hover > a,
	.eut-header-element > a:hover {
		color: " . fildisi_eutf_option( 'default_header_menu_text_hover_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span {
		border-color: " . fildisi_eutf_option( 'default_header_menu_type_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span,
	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.active > a span {
		border-color: " . fildisi_eutf_option( 'default_header_menu_type_color_hover' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a .eut-item:after {
		background-color: " . fildisi_eutf_option( 'default_header_menu_type_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a .eut-item:after,
	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li.active > a .eut-item:after {
		background-color: " . fildisi_eutf_option( 'default_header_menu_type_color_hover' ) . ";
	}

	";


	/* - Default Header Sub Menu Colors
	========================================================================= */
	$css .= "
	#eut-header .eut-main-menu .eut-wrapper > ul > li ul  {
		background-color: " . fildisi_eutf_option( 'default_header_submenu_bg_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li a {
		color: " . fildisi_eutf_option( 'default_header_submenu_text_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li a:hover,
	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li.current-menu-item > a,
	#eut-header .eut-main-menu .eut-wrapper > ul li li.current-menu-ancestor > a {
		color: " . fildisi_eutf_option( 'default_header_submenu_text_hover_color' ) . ";
		background-color: " . fildisi_eutf_option( 'default_header_submenu_text_bg_hover_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li > a {
		color: " . fildisi_eutf_option( 'default_header_submenu_column_text_color' ) . ";
		background-color: transparent;
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li:hover > a {
		color: " . fildisi_eutf_option( 'default_header_submenu_column_text_hover_color' ) . ";
	}

	#eut-header .eut-horizontal-menu ul.eut-menu li.megamenu > .sub-menu > li {
		border-color: " . fildisi_eutf_option( 'default_header_submenu_border_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li.eut-menu-type-button a {
		background-color: transparent;
	}

	";

	/* - Sub Menu Position
	========================================================================= */
	$fildisi_eutf_submenu_top_position = fildisi_eutf_option( 'submenu_top_position', '0' );
	if( 0 != $fildisi_eutf_submenu_top_position ) {
		$css .= "
		#eut-header:not(.eut-sticky-header) .eut-horizontal-menu ul.eut-menu ul,
		#eut-header.eut-sticky-header[data-sticky='simple'] .eut-horizontal-menu ul.eut-menu ul  {
			margin-top: -" . fildisi_eutf_option( 'submenu_top_position' ) . "px;
		}
		";
	}

	/* - Default Header Layout
	========================================================================= */
	$css .= "
	#eut-main-header,
	.eut-logo {
		height: " . fildisi_eutf_option( 'header_height', 120 ) . "px;
	}

	.eut-logo a {
		height: " . fildisi_eutf_option( 'logo_height', 20 ) . "px;
	}

	.eut-logo.eut-logo-text a {
		line-height: " . fildisi_eutf_option( 'header_height', 120 ) . "px;
	}

	.eut-logo .eut-wrapper img.eut-sticky {
		-webkit-transform: translateY(" . fildisi_eutf_option( 'header_height', 120 ) . "px);
		-moz-transform:    translateY(" . fildisi_eutf_option( 'header_height', 120 ) . "px);
		-ms-transform:     translateY(" . fildisi_eutf_option( 'header_height', 120 ) . "px);
		-o-transform:      translateY(" . fildisi_eutf_option( 'header_height', 120 ) . "px);
		transform:         translateY(" . fildisi_eutf_option( 'header_height', 120 ) . "px);
	}

	#eut-header.eut-sticky-header #eut-main-header .eut-logo .eut-wrapper img.eut-sticky {
		-webkit-transform: translateY(0);
		-moz-transform:    translateY(0);
		-ms-transform:     translateY(0);
		-o-transform:      translateY(0);
		transform:         translateY(0);
	}

	#eut-main-menu .eut-wrapper > ul > li > a,
	.eut-header-element > a,
	.eut-no-assigned-menu {
		line-height: " . fildisi_eutf_option( 'header_height', 120 ) . "px;
	}

	.eut-logo .eut-wrapper img {
		padding-top: 0;
		padding-bottom: 0;
	}

	";

	/* Go to section Position */
	$css .= "
	#eut-theme-wrapper.eut-feature-below #eut-goto-section-wrapper {
		margin-bottom: " . fildisi_eutf_option( 'header_height', 120 ) . "px;
	}
	";

	/* - Default Header Overlaping
	========================================================================= */
	$css .= "
	@media only screen and (min-width: " . esc_attr( $fildisi_eutf_responsive_header_threshold ) . "px) {
		#eut-header.eut-overlapping + .eut-page-title,
		#eut-header.eut-overlapping + #eut-feature-section,
		#eut-header.eut-overlapping + #eut-content,
		#eut-header.eut-overlapping + .eut-single-wrapper,
		#eut-header.eut-overlapping + .eut-product-area {
			top: -" . fildisi_eutf_option( 'header_height', 120 ) . "px;
			margin-bottom: -" . fildisi_eutf_option( 'header_height', 120 ) . "px;
		}

		#eut-feature-section + #eut-header.eut-overlapping {
			top: -" . fildisi_eutf_option( 'header_height', 120 ) . "px;
		}

		#eut-header {
			height: " . fildisi_eutf_option( 'header_height', 120 ) . "px;
		}
	}

	";
	/* Sticky Sidebar with header overlaping */
	$css .= "
	@media only screen and (min-width: 1024px) {
		#eut-header.eut-overlapping + #eut-content .eut-sidebar.eut-fixed-sidebar,
		#eut-header.eut-overlapping + .eut-single-wrapper .eut-sidebar.eut-fixed-sidebar {
			top: " . fildisi_eutf_option( 'header_height', 120 ) . "px;
		}
	}
	";

/* Logo On Top Header
============================================================================= */
} else if ( 'logo-top' == $fildisi_eutf_header_mode ) {


	/* - Logo On Top Header Colors
	============================================================================= */
	$fildisi_eutf_logo_top_logo_area_background_color = fildisi_eutf_option( 'logo_top_header_logo_area_background_color', '#ffffff' );
	$fildisi_eutf_logo_top_menu_area_background_color = fildisi_eutf_option( 'logo_top_header_menu_area_background_color', '#ffffff' );
	$fildisi_eutf_logo_top_border_color = fildisi_eutf_option( 'logo_top_header_border_color', '#000000' );
	$css .= "

	#eut-main-header #eut-top-header {
		background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_logo_top_logo_area_background_color ) . "," . fildisi_eutf_option( 'logo_top_header_logo_area_background_color_opacity', '1') . ");
	}

	#eut-main-header #eut-bottom-header {
		background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_logo_top_menu_area_background_color ) . "," . fildisi_eutf_option( 'logo_top_header_menu_area_background_color_opacity', '1') . ");
		border-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_logo_top_border_color ) . "," . fildisi_eutf_option( 'logo_top_header_border_color_opacity', '1') . ");
	}
	#eut-main-header {
		border-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_logo_top_border_color ) . "," . fildisi_eutf_option( 'logo_top_header_border_color_opacity', '1') . ");
	}

	.eut-header-elements {
		border-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_logo_top_border_color ) . "," . fildisi_eutf_option( 'logo_top_header_border_color_opacity', '1') . ");
	}

	#eut-main-header.eut-transparent #eut-top-header,
	#eut-main-header.eut-light #eut-top-header,
	#eut-main-header.eut-dark #eut-top-header,
	#eut-main-header.eut-transparent #eut-bottom-header,
	#eut-main-header.eut-light #eut-bottom-header,
	#eut-main-header.eut-dark #eut-bottom-header {
		background-color: transparent;
	}

	";

	/* - Logo On Top Header Menu Colors
	========================================================================= */
	$css .= "
	.eut-logo-text a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li > a,
	.eut-header-element > a,
	.eut-header-element .eut-purchased-items {
		color: " . fildisi_eutf_option( 'logo_top_header_menu_text_color' ) . ";
	}

	.eut-hidden-menu-btn a .eut-item:not(.eut-with-text) span {
		background-color: " . fildisi_eutf_option( 'logo_top_header_menu_text_color' ) . ";
	}

	.eut-logo-text a:hover,
	#eut-header .eut-main-menu .eut-wrapper > ul > li.eut-current > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li:hover > a,
	.eut-header-element > a:hover {
		color: " . fildisi_eutf_option( 'logo_top_header_menu_text_hover_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span {
		border-color: " . fildisi_eutf_option( 'logo_top_header_menu_type_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span,
	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.active > a span {
		border-color: " . fildisi_eutf_option( 'logo_top_header_menu_type_color_hover' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a .eut-item:after {
		background-color: " . fildisi_eutf_option( 'logo_top_header_menu_type_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a .eut-item:after,
	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li.active > a .eut-item:after {
		background-color: " . fildisi_eutf_option( 'logo_top_header_menu_type_color_hover' ) . ";
	}


	";


	/* - Logo On Top Header Sub Menu Colors
	========================================================================= */
	$css .= "
	#eut-header .eut-main-menu .eut-wrapper > ul > li ul  {
		background-color: " . fildisi_eutf_option( 'logo_top_header_submenu_bg_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li a {
		color: " . fildisi_eutf_option( 'logo_top_header_submenu_text_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li a:hover,
	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li.current-menu-item > a,
	#eut-header .eut-main-menu .eut-wrapper > ul li li.current-menu-ancestor > a {
		color: " . fildisi_eutf_option( 'logo_top_header_submenu_text_hover_color' ) . ";
		background-color: " . fildisi_eutf_option( 'logo_top_header_submenu_text_bg_hover_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li > a {
		color: " . fildisi_eutf_option( 'logo_top_header_submenu_column_text_color' ) . ";
		background-color: transparent;
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li:hover > a {
		color: " . fildisi_eutf_option( 'logo_top_header_submenu_column_text_hover_color' ) . ";
	}

	#eut-header .eut-horizontal-menu ul.eut-menu li.megamenu > .sub-menu > li {
		border-color: " . fildisi_eutf_option( 'logo_top_header_submenu_border_color' ) . ";
	}

	";

	/* - Logo On Top Header Layout
	========================================================================= */
	$fildisi_eutf_header_height = intval( fildisi_eutf_option( 'header_top_height', 120 ) ) + intval( fildisi_eutf_option( 'header_bottom_height', 50 ) + 1 );
	$css .= "

	#eut-top-header,
	.eut-logo {
		height: " . fildisi_eutf_option( 'header_top_height', 120 ) . "px;
	}

	@media only screen and (min-width: " . esc_attr( $fildisi_eutf_responsive_header_threshold ) . "px) {
		#eut-header {
			height: " . esc_attr( $fildisi_eutf_header_height ) . "px;
		}
	}

	.eut-logo a {
		height: " . fildisi_eutf_option( 'header_top_logo_height', 30 ) . "px;
	}

	.eut-logo.eut-logo-text a {
		line-height: " . fildisi_eutf_option( 'header_top_height', 120 ) . "px;
	}

	#eut-bottom-header,
	#eut-main-menu {
		height: " . ( fildisi_eutf_option( 'header_bottom_height', 50 ) + 1 ) . "px;
	}

	#eut-main-menu .eut-wrapper > ul > li > a,
	.eut-header-element > a,
	.eut-no-assigned-menu {
		line-height: " . fildisi_eutf_option( 'header_bottom_height', 50 ) . "px;
	}

	";

	/* Go to section Position */
	$css .= "
	#eut-theme-wrapper.eut-feature-below #eut-goto-section-wrapper {
		margin-bottom: " . esc_attr( $fildisi_eutf_header_height ) . "px;
	}
	";

	/* - Logo On Top Header Overlaping
	========================================================================= */
	$css .= "

	@media only screen and (min-width: " . esc_attr( $fildisi_eutf_responsive_header_threshold ) . "px) {
		#eut-header.eut-overlapping + .eut-page-title,
		#eut-header.eut-overlapping + #eut-feature-section,
		#eut-header.eut-overlapping + #eut-content,
		#eut-header.eut-overlapping + .eut-single-wrapper,
		#eut-header.eut-overlapping + .eut-product-area {
			top: -" . esc_attr( $fildisi_eutf_header_height ) . "px;
			margin-bottom: -" . esc_attr( $fildisi_eutf_header_height ) . "px;
		}

		#eut-header.eut-overlapping:not(.eut-header-below) + .eut-page-title .eut-wrapper,
		#eut-header.eut-overlapping:not(.eut-header-below) + #eut-feature-section .eut-wrapper:not(.eut-map)  {
			padding-top: " . fildisi_eutf_option( 'header_top_height', 120 ) . "px;
		}

		#eut-feature-section + #eut-header.eut-overlapping {
			top: -" . esc_attr( $fildisi_eutf_header_height ) . "px;
		}

		.eut-feature-below #eut-feature-section:not(.eut-with-map) .eut-wrapper {
			margin-bottom: " . fildisi_eutf_option( 'header_top_height', 120 ) . "px;
		}
	}

	";

	/* Sticky Sidebar with header overlaping */
	$css .= "
	@media only screen and (min-width: " . esc_attr( $fildisi_eutf_responsive_header_threshold ) . "px) {
		#eut-header.eut-overlapping + #eut-content .eut-sidebar.eut-fixed-sidebar,
		#eut-header.eut-overlapping + .eut-single-wrapper .eut-sidebar.eut-fixed-sidebar {
			top: " . fildisi_eutf_option( 'header_height', 120 ) . "px;
		}
	}
	";


} else {

	/* - Side Header Colors
	============================================================================= */
	$fildisi_eutf_side_header_background_color = fildisi_eutf_option( 'side_header_background_color', '#ffffff' );
	$css .= "
	#eut-main-header {
		background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_side_header_background_color ) . "," . fildisi_eutf_option( 'side_header_background_color_opacity', '1') . ");
	}

	#eut-main-header.eut-transparent,
	#eut-main-header.eut-light,
	#eut-main-header.eut-dark {
		background-color: transparent;
	}

	";

	/* - Side Header Menu Colors
	========================================================================= */
	$css .= "
	.eut-logo-text a,
	#eut-main-menu .eut-wrapper > ul > li > a,
	.eut-header-element > a,
	.eut-header-element .eut-purchased-items {
		color: " . fildisi_eutf_option( 'side_header_menu_text_color' ) . ";

	}

	.eut-hidden-menu-btn a .eut-item:not(.eut-with-text) span {
		background-color: " . fildisi_eutf_option( 'side_header_menu_text_color' ) . ";
	}

	.eut-logo-text a:hover,
	#eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-main-menu .eut-wrapper > ul > li > a:hover,
	.eut-header-element > a:hover ,
	#eut-main-menu .eut-wrapper > ul > li ul li.eut-goback a {
		color: " . fildisi_eutf_option( 'side_header_menu_text_hover_color' ) . ";
	}

	";


	/* - Side Header Sub Menu Colors
	========================================================================= */
	$fildisi_eutf_side_header_border_color = fildisi_eutf_option( 'side_header_border_color', '#ffffff' );
	$css .= "

	#eut-main-menu .eut-wrapper > ul > li ul li a {
		color: " . fildisi_eutf_option( 'side_header_submenu_text_color' ) . ";
	}

	#eut-main-menu .eut-wrapper > ul > li ul li a:hover,
	#eut-main-menu .eut-wrapper > ul > li ul li.current-menu-item > a,
	.eut-slide-menu ul.eut-menu .eut-arrow:hover {
		color: " . fildisi_eutf_option( 'side_header_submenu_text_hover_color' ) . ";
	}

	#eut-main-menu.eut-vertical-menu  ul li a,
	#eut-main-header.eut-header-side .eut-header-elements {
		border-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_side_header_border_color ) . "," . fildisi_eutf_option( 'side_header_border_opacity', '1') . ");
	}

	";

	/* - Side Header Layout
	========================================================================= */
	$css .= "
	.eut-logo a {
		height: " . fildisi_eutf_option( 'header_side_logo_height', 30 ) . "px;
	}
	#eut-main-header.eut-header-side .eut-logo {
		padding-top: " . fildisi_eutf_option( 'header_side_logo_spacing', '', 'padding-top' ) . ";
		padding-bottom: " . fildisi_eutf_option( 'header_side_logo_spacing', '', 'padding-bottom'  ) . ";
	}
	#eut-main-header.eut-header-side .eut-content,
	#eut-main-header.eut-header-side .eut-header-elements-wrapper {
		padding-left: " . fildisi_eutf_option( 'header_side_spacing', '', 'padding-left' ) . ";
		padding-right: " . fildisi_eutf_option( 'header_side_spacing', '', 'padding-right'  ) . ";
	}

	@media only screen and (min-width: " . esc_attr( $fildisi_eutf_responsive_header_threshold ) . "px) {
		#eut-theme-wrapper.eut-header-side,
		#eut-footer.eut-fixed-footer {
			padding-left: " . fildisi_eutf_option( 'header_side_width', 120 ) . "px;
		}

		.eut-body.rtl #eut-theme-wrapper.eut-header-side,
		.eut-body.rtl #eut-footer.eut-fixed-footer {
			padding-left: 0;
			padding-right: " . fildisi_eutf_option( 'header_side_width', 120 ) . "px;
		}

		#eut-main-header.eut-header-side,
		#eut-main-header.eut-header-side .eut-content {
			width: " . fildisi_eutf_option( 'header_side_width', 120 ) . "px;
		}

		.eut-body.eut-boxed #eut-theme-wrapper.eut-header-side #eut-main-header.eut-header-side,
		#eut-footer.eut-fixed-footer {
			margin-left: -" . fildisi_eutf_option( 'header_side_width', 120 ) . "px;
		}

		.eut-body.eut-boxed.rtl #eut-theme-wrapper.eut-header-side #eut-main-header.eut-header-side,
		.eut-body.rtl #eut-footer.eut-fixed-footer {
			margin-left: 0;
			margin-right: -" . fildisi_eutf_option( 'header_side_width', 120 ) . "px;
		}

		#eut-main-header.eut-header-side .eut-main-header-wrapper {
			width: " . intval( fildisi_eutf_option( 'header_side_width', 120 ) + 30 ) . "px;
		}

		.eut-anchor-menu .eut-anchor-wrapper.eut-sticky {
			width: calc(100% - " . fildisi_eutf_option( 'header_side_width', 120 ) . "px);
		}

		body.eut-boxed .eut-anchor-menu .eut-anchor-wrapper.eut-sticky {
			max-width: calc(" . fildisi_eutf_option( 'boxed_size', 1220 ) . "px - " . fildisi_eutf_option( 'header_side_width', 120 ) . "px);
		}
	}

	";
}

/* Menu Label
============================================================================= */
$css .= "
#eut-header .eut-main-menu .eut-item .label.eut-bg-default,
#eut-hidden-menu .eut-item .label.eut-bg-default {
	background-color: " . fildisi_eutf_option( 'default_header_label_bg_color' ) . ";
	color: " . fildisi_eutf_option( 'default_header_label_text_color' ) . ";
}
";

/* Light Header
============================================================================= */
$fildisi_eutf_light_header_border_color = fildisi_eutf_option( 'light_header_border_color', '#ffffff' );
$css .= "
#eut-main-header.eut-light .eut-logo-text a,
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li > a,
#eut-main-header.eut-light .eut-header-element > a,
#eut-main-header.eut-light .eut-header-element .eut-purchased-items {
	color: #ffffff;
	color: rgba(255,255,255,0.7);
}

#eut-main-header.eut-light .eut-hidden-menu-btn a .eut-item:not(.eut-with-text) span {
	background-color: #ffffff;
}

#eut-main-header.eut-light .eut-logo-text a:hover,
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li.eut-current > a,
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li:hover > a,
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
#eut-main-header.eut-light .eut-header-element > a:hover {
	color: " . fildisi_eutf_option( 'light_menu_text_hover_color' ) . ";
}

#eut-main-header.eut-light #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
#eut-main-header.eut-light #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span,
#eut-main-header.eut-light #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span {
	border-color: " . fildisi_eutf_option( 'light_menu_type_color_hover' ) . ";
}

#eut-main-header.eut-light #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a .eut-item:after,
#eut-main-header.eut-light #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a .eut-item:after {
	background-color: " . fildisi_eutf_option( 'light_menu_type_color_hover' ) . ";
}

#eut-main-header.eut-light,
#eut-main-header.eut-light .eut-header-elements,
#eut-main-header.eut-header-default.eut-light,
#eut-main-header.eut-light #eut-bottom-header {
	border-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_light_header_border_color ) . "," . fildisi_eutf_option( 'light_header_border_color_opacity', '1') . ");
}

";

/* Dark Header
============================================================================= */
$fildisi_eutf_dark_header_border_color = fildisi_eutf_option( 'dark_header_border_color', '#ffffff' );
$css .= "
#eut-main-header.eut-dark .eut-logo-text a,
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li > a,
#eut-main-header.eut-dark .eut-header-element > a,
#eut-main-header.eut-dark .eut-header-element .eut-purchased-items {
	color: #000000;
	color: rgba(0,0,0,0.5);
}

#eut-main-header.eut-dark .eut-hidden-menu-btn a .eut-item:not(.eut-with-text) span {
	background-color: #000000;
}

#eut-main-header.eut-dark .eut-logo-text a:hover,
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li.eut-current > a,
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li:hover > a,
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
#eut-main-header.eut-dark .eut-header-element > a:hover {
	color: " . fildisi_eutf_option( 'dark_menu_text_hover_color' ) . ";
}

#eut-main-header.eut-dark #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
#eut-main-header.eut-dark #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span,
#eut-main-header.eut-dark #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span {
	border-color: " . fildisi_eutf_option( 'dark_menu_type_color_hover' ) . ";
}

#eut-main-header.eut-dark #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a .eut-item:after,
#eut-main-header.eut-dark #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a .eut-item:after {
	background-color: " . fildisi_eutf_option( 'dark_menu_type_color_hover' ) . ";
}

#eut-main-header.eut-dark,
#eut-main-header.eut-dark .eut-header-elements,
#eut-main-header.eut-header-default.eut-dark,
#eut-main-header.eut-dark #eut-bottom-header {
	border-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_dark_header_border_color ) . "," . fildisi_eutf_option( 'dark_header_border_color_opacity', '1') . ");
}

";


/* - Advanced HiddenMenu Collors
============================================================================= */
if ( 'default' == $fildisi_eutf_header_mode ) {
	$css .= "

	#eut-header.eut-header-hover:not(.eut-sticky-header) #eut-main-header {
		background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_default_header_background_color ) . "," . fildisi_eutf_option( 'default_header_background_color_opacity', '1') . ") !important;
	}

	#eut-header.eut-header-hover:not(.eut-sticky-header) #eut-main-header.eut-header-default,
	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-header-elements {
		border-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_default_header_border_color ) . "," . fildisi_eutf_option( 'default_header_border_color_opacity', '1') . ") !important;
	}

	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu .eut-wrapper > ul > li > a,
	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-header-element > a,
	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-header-element .eut-purchased-items {
		color: " . fildisi_eutf_option( 'default_header_menu_text_color' ) . " !important;
	}

	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-hidden-menu-btn a .eut-item:not(.eut-with-text) span {
		background-color: " . fildisi_eutf_option( 'default_header_menu_text_color' ) . " !important;
	}

	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu .eut-wrapper > ul > li.eut-current > a,
	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu .eut-wrapper > ul > li:hover > a,
	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-header-element > a:hover {
		color: " . fildisi_eutf_option( 'default_header_menu_text_hover_color' ) . " !important;
	}

	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span {
		border-color: " . fildisi_eutf_option( 'default_header_menu_type_color' ) . " !important;
	}

	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span,
	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.active > a span {
		border-color: " . fildisi_eutf_option( 'default_header_menu_type_color_hover' ) . " !important;
	}

	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a .eut-item:after {
		background-color: " . fildisi_eutf_option( 'default_header_menu_type_color' ) . " !important;
	}

	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a .eut-item:after,
	#eut-header.eut-header-hover:not(.eut-sticky-header) .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li.active > a .eut-item:after {
		background-color: " . fildisi_eutf_option( 'default_header_menu_type_color_hover' ) . " !important;
	}

	";
}

/* Sticky Header
============================================================================= */

	/* - Sticky Default Header
	========================================================================= */
	if ( 'default' == $fildisi_eutf_header_mode ) {
		$css .= "
			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky {
				height: " . fildisi_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}

			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky .eut-logo,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-logo {
				height: " . fildisi_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}

			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky .eut-logo a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-logo a {
				height: " . fildisi_eutf_option( 'header_sticky_shrink_logo_height', 20 ) . "px;
			}

			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky .eut-logo.eut-logo-text a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-logo.eut-logo-text a {
				line-height: " . fildisi_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}

			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky #eut-main-menu .eut-wrapper > ul > li > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky .eut-header-element > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky .eut-no-assigned-menu,

			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky #eut-main-menu .eut-wrapper > ul > li > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-header-element > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-no-assigned-menu {
				line-height: " . fildisi_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}

			#eut-header.eut-sticky-header.eut-scroll-up #eut-main-header.eut-advanced-sticky {
				-webkit-transform: translateY(" . fildisi_eutf_option( 'header_height', 120 ) . "px);
				-moz-transform:    translateY(" . fildisi_eutf_option( 'header_height', 120 ) . "px);
				-ms-transform:     translateY(" . fildisi_eutf_option( 'header_height', 120 ) . "px);
				-o-transform:      translateY(" . fildisi_eutf_option( 'header_height', 120 ) . "px);
				transform:         translateY(" . fildisi_eutf_option( 'header_height', 120 ) . "px);
			}

		";

	/* - Sticky Logo On Top Header
	========================================================================= */
	} else if ( 'logo-top' == $fildisi_eutf_header_mode ) {
		$fildisi_eutf_header_height = intval( fildisi_eutf_option( 'header_sticky_shrink_height', 120 ) ) + intval( fildisi_eutf_option( 'header_bottom_height', 50 ) );
		$css .= "

			#eut-header.eut-sticky-header #eut-main-header.eut-simple-sticky,
			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky {
				-webkit-transform: translateY(-" . fildisi_eutf_option( 'header_top_height', 120 ) . "px);
				-moz-transform:    translateY(-" . fildisi_eutf_option( 'header_top_height', 120 ) . "px);
				-ms-transform:     translateY(-" . fildisi_eutf_option( 'header_top_height', 120 ) . "px);
				-o-transform:      translateY(-" . fildisi_eutf_option( 'header_top_height', 120 ) . "px);
				transform:         translateY(-" . fildisi_eutf_option( 'header_top_height', 120 ) . "px);
			}

			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky #eut-bottom-header,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky #eut-bottom-header {
				height: " . fildisi_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}

			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky #eut-main-menu .eut-wrapper > ul > li > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky .eut-header-element > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-shrink-sticky .eut-no-assigned-menu,

			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky #eut-main-menu .eut-wrapper > ul > li > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-header-element > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-no-assigned-menu {
				line-height: " . fildisi_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}


			#eut-header.eut-sticky-header.eut-scroll-up #eut-main-header.eut-advanced-sticky {
				-webkit-transform: translateY(" . fildisi_eutf_option( 'header_bottom_height', 50 ) . "px);
				-moz-transform:    translateY(" . fildisi_eutf_option( 'header_bottom_height', 50 ) . "px);
				-ms-transform:     translateY(" . fildisi_eutf_option( 'header_bottom_height', 50 ) . "px);
				-o-transform:      translateY(" . fildisi_eutf_option( 'header_bottom_height', 50 ) . "px);
				transform:         translateY(" . fildisi_eutf_option( 'header_bottom_height', 50 ) . "px);
			}

		";
	}


	/* - Sticky Header Colors
	========================================================================= */
	$fildisi_eutf_header_sticky_border_color = fildisi_eutf_option( 'header_sticky_border_color', '#ffffff' );
	$fildisi_eutf_header_sticky_background_color = fildisi_eutf_option( 'header_sticky_background_color', '#ffffff' );
	$css .= "

	#eut-header.eut-sticky-header #eut-main-header:not(.eut-header-logo-top),
	#eut-header.eut-sticky-header #eut-main-header #eut-bottom-header {
		background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_header_sticky_background_color ) . "," . fildisi_eutf_option( 'header_sticky_background_color_opacity', '1') . ");
	}

	#eut-header.eut-header-logo-top.eut-sticky-header #eut-main-header {
		background-color: transparent;
	}

	#eut-header.eut-sticky-header .eut-logo-text a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li > a,
	#eut-header.eut-sticky-header #eut-main-header .eut-header-element > a,
	#eut-header.eut-sticky-header .eut-header-element .eut-purchased-items {
		color: " . fildisi_eutf_option( 'sticky_menu_text_color' ) . ";
	}

	#eut-header.eut-sticky-header .eut-logo-text a:hover,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li.eut-current > a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li:hover > a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li.active > a,
	#eut-header.eut-sticky-header #eut-main-header .eut-header-element > a:hover {
		color: " . fildisi_eutf_option( 'sticky_menu_text_hover_color' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-header .eut-hidden-menu-btn a .eut-item:not(.eut-with-text) span {
		background-color: " . fildisi_eutf_option( 'sticky_menu_text_hover_color' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
	#eut-header.eut-sticky-header #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span {
		border-color: " . fildisi_eutf_option( 'header_sticky_menu_type_color' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span {
		border-color: " . fildisi_eutf_option( 'header_sticky_menu_type_color_hover' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a .eut-item:after {
		background-color: " . fildisi_eutf_option( 'header_sticky_menu_type_color' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a .eut-item:after {
		background-color: " . fildisi_eutf_option( 'header_sticky_menu_type_color_hover' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-header.eut-header-default,
	#eut-header.eut-sticky-header #eut-main-header .eut-header-elements {
		border-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_header_sticky_border_color ) . "," . fildisi_eutf_option( 'header_sticky_border_color_opacity', '1') . ");
	}

	";

	/* - Fildisi Sticky Header
	========================================================================= */
	$css .= "

	#eut-fildisi-sticky-header,
	#eut-fildisi-sticky-header .eut-logo,
	#eut-fildisi-sticky-header:before {
		height: " . fildisi_eutf_option( 'header_sticky_shrink_height', 120 ) . "px;
	}

	#eut-fildisi-sticky-header .eut-logo a {
		height: " . fildisi_eutf_option( 'header_sticky_shrink_logo_height', 20 ) . "px;
	}

	#eut-fildisi-sticky-header .eut-logo.eut-logo-text a {
		line-height: " . fildisi_eutf_option( 'header_sticky_shrink_height', 120 ) . "px;
	}

	#eut-fildisi-sticky-header .eut-main-menu .eut-wrapper > ul > li > a,
	#eut-fildisi-sticky-header .eut-header-element > a,
	#eut-fildisi-sticky-header .eut-no-assigned-menu {
		line-height: " . fildisi_eutf_option( 'header_sticky_shrink_height', 120 ) . "px;
	}

	#eut-fildisi-sticky-header:before,
	#eut-fildisi-sticky-header .eut-logo,
	#eut-fildisi-sticky-header .eut-header-element > a.eut-safe-button {
		background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_header_sticky_background_color ) . "," . fildisi_eutf_option( 'header_sticky_background_color_opacity', '1') . ");
	}

	#eut-fildisi-sticky-header .eut-logo,
	#eut-fildisi-sticky-header .eut-header-element > a.eut-safe-button {
		min-width: " . fildisi_eutf_option( 'header_sticky_shrink_height', 120 ) . "px;
	}

	#eut-fildisi-sticky-header .eut-main-menu .eut-wrapper > ul > li > a,
	#eut-fildisi-sticky-header .eut-header-element > a {
		color: " . fildisi_eutf_option( 'sticky_menu_text_color' ) . ";
	}

	#eut-fildisi-sticky-header .eut-hidden-menu-btn a .eut-item:not(.eut-with-text) span {
		background-color: " . fildisi_eutf_option( 'sticky_menu_text_hover_color' ) . ";
	}

	#eut-fildisi-sticky-header .eut-main-menu .eut-wrapper > ul > li.eut-current > a,
	#eut-fildisi-sticky-header .eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-fildisi-sticky-header .eut-main-menu .eut-wrapper > ul > li:hover > a,
	#eut-fildisi-sticky-header .eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-fildisi-sticky-header .eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-fildisi-sticky-header .eut-main-menu .eut-wrapper > ul > li.active > a,
	#eut-fildisi-sticky-header .eut-header-element > a:hover {
		color: " . fildisi_eutf_option( 'sticky_menu_text_hover_color' ) . ";
	}

	#eut-fildisi-sticky-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span {
		border-color: " . fildisi_eutf_option( 'header_sticky_menu_type_color_hover' ) . ";
	}

	#eut-fildisi-sticky-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a .eut-item:after {
		background-color: " . fildisi_eutf_option( 'header_sticky_menu_type_color' ) . ";
	}

	#eut-fildisi-sticky-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a .eut-item:after {
		background-color: " . fildisi_eutf_option( 'header_sticky_menu_type_color_hover' ) . ";
	}

	";

/* Side Area Colors
============================================================================= */
$fildisi_eutf_sliding_area_overflow_background_color = fildisi_eutf_option( 'sliding_area_overflow_background_color', '#000000' );
$css .= "
#eut-sidearea {
	background-color: " . fildisi_eutf_option( 'sliding_area_background_color' ) . ";
	color: " . fildisi_eutf_option( 'sliding_area_text_color' ) . ";
}

#eut-sidearea .widget,
#eut-sidearea form,
#eut-sidearea form p,
#eut-sidearea form div,
#eut-sidearea form span {
	color: " . fildisi_eutf_option( 'sliding_area_text_color' ) . ";
}

#eut-sidearea h1,
#eut-sidearea h2,
#eut-sidearea h3,
#eut-sidearea h4,
#eut-sidearea h5,
#eut-sidearea h6,
#eut-sidearea .widget .eut-widget-title {
	color: " . fildisi_eutf_option( 'sliding_area_title_color' ) . ";
}

#eut-sidearea a {
	color: " . fildisi_eutf_option( 'sliding_area_link_color' ) . ";
}

#eut-sidearea .widget li a .eut-arrow:after,
#eut-sidearea .widget li a .eut-arrow:before {
	color: " . fildisi_eutf_option( 'sliding_area_link_color' ) . ";
}

#eut-sidearea a:hover {
	color: " . fildisi_eutf_option( 'sliding_area_link_hover_color' ) . ";
}

#eut-sidearea .eut-close-btn:after,
#eut-sidearea .eut-close-btn:before,
#eut-sidearea .eut-close-btn span {
	background-color: " . fildisi_eutf_option( 'sliding_area_close_btn_color' ) . ";
}

#eut-sidearea .eut-border,
#eut-sidearea form,
#eut-sidearea form p,
#eut-sidearea form div,
#eut-sidearea form span,
#eut-sidearea .widget a,
#eut-sidearea .widget ul,
#eut-sidearea .widget li,
#eut-sidearea .widget table,
#eut-sidearea .widget table td,
#eut-sidearea .widget table th,
#eut-sidearea .widget table tr,
#eut-sidearea table,
#eut-sidearea tr,
#eut-sidearea td,
#eut-sidearea th,
#eut-sidearea .widget,
#eut-sidearea .widget ul,
#eut-sidearea .widget li,
#eut-sidearea .widget div,
#eut-theme-wrapper #eut-sidearea form,
#eut-theme-wrapper #eut-sidearea .wpcf7-form-control-wrap {
	border-color: " . fildisi_eutf_option( 'sliding_area_border_color' ) . ";
}

#eut-sidearea-overlay {
	background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_sliding_area_overflow_background_color ) . "," . fildisi_eutf_option( 'sliding_area_overflow_background_color_opacity', '0.9') . ");
}
";


/* Modals Colors
============================================================================= */
$fildisi_eutf_modal_overflow_background_color = fildisi_eutf_option( 'modal_overflow_background_color', '#000000' );
$css .= "

#eut-modal-overlay,
.mfp-bg,
#eut-loader-overflow {
	background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_modal_overflow_background_color ) . "," . fildisi_eutf_option( 'modal_overflow_background_color_opacity', '0.9') . ");
}

.eut-page-curtain {
	background-color: #18252a;
}

#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h1,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h2,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h3,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h4,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h5,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h6,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) .eut-modal-title,
.mfp-title,
.mfp-counter,
#eut-theme-wrapper .eut-modal-content .eut-heading-color {
	color: " . fildisi_eutf_option( 'modal_title_color' ) . ";
}

#eut-theme-wrapper .eut-modal .eut-border,
#eut-theme-wrapper .eut-modal form,
#eut-theme-wrapper .eut-modal form p,
#eut-theme-wrapper .eut-modal form div,
#eut-theme-wrapper .eut-modal form span,
#eut-theme-wrapper .eut-login-modal-footer,
#eut-socials-modal .eut-social li a,
#eut-language-modal ul li a {
	color: " . fildisi_eutf_option( 'modal_text_color' ) . ";
	border-color: " . fildisi_eutf_option( 'modal_border_color' ) . ";
}
";

$fildisi_eutf_close_cursor_color = fildisi_eutf_option( 'modal_cursor_color_color', 'dark' );
if ( 'dark' == $fildisi_eutf_close_cursor_color ) {
	$css .= "
	.eut-close-modal,
	button.mfp-arrow {
		background-color: #000000;
		color: #ffffff;
	}
	";
} else {
	$css .= "
	.eut-close-modal,
	button.mfp-arrow {
		background-color: #ffffff;
		color: #000000;
	}
	";
}

/* Responsive Header
============================================================================= */
$fildisi_eutf_responsive_header_background_color = fildisi_eutf_option( 'responsive_header_background_color', '#000000' );
$css .= "
#eut-responsive-header #eut-main-responsive-header {
	background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_responsive_header_background_color ) . "," . fildisi_eutf_option( 'responsive_header_background_opacity', '1') . ");
}
";
	/* - Header Layout
	========================================================================= */
	$css .= "
	#eut-responsive-header {
		height: " . fildisi_eutf_option( 'responsive_header_height' ) . "px;
	}

	#eut-responsive-header .eut-logo {
		height: " . fildisi_eutf_option( 'responsive_header_height' ) . "px;
	}

	#eut-responsive-header .eut-header-element > a {
		line-height: " . fildisi_eutf_option( 'responsive_header_height' ) . "px;
	}

	#eut-responsive-header .eut-logo a {
		height: " . fildisi_eutf_option( 'responsive_logo_height' ) . "px;
	}

	#eut-responsive-header .eut-logo.eut-logo-text a {
		line-height: " . fildisi_eutf_option( 'responsive_header_height' ) . "px;
	}

	#eut-responsive-header .eut-logo .eut-wrapper img {
		padding-top: 0;
		padding-bottom: 0;
	}
	";

	/* - Responsive Header / Responsive Menu
	========================================================================= */
	$css .= "

	@media only screen and (max-width: " . esc_attr( $fildisi_eutf_responsive_header_threshold - 1 ) . "px) {
		#eut-main-header,
		#eut-bottom-header {
			display: none;
		}

		#eut-main-menu,
		#eut-responsive-hidden-menu-wrapper {
			display: none;
		}

		#eut-responsive-header {
			display: block;
		}
		.eut-header-responsive-elements {
			display: block;
		}

		#eut-logo.eut-position-center,
		#eut-logo.eut-position-center .eut-wrapper {
			position: relative;
			left: 0;
		}

		#eut-responsive-menu-wrapper {
			display: block;
		}
	}
	";

	/* - Responsive Header Overlaping
	========================================================================= */
	$css .= "

	@media only screen and (max-width: " . esc_attr( $fildisi_eutf_responsive_header_threshold - 1 ) . "px) {
		#eut-header.eut-responsive-overlapping + * {
			top: -" . fildisi_eutf_option( 'responsive_header_height' ) . "px;
			margin-bottom: -" . fildisi_eutf_option( 'responsive_header_height' ) . "px;
		}

		#eut-header.eut-responsive-overlapping + #eut-page-anchor {
			top: 0px;
			margin-bottom: 0px;
		}

		#eut-feature-section + #eut-header.eut-responsive-overlapping {
			top: -" . fildisi_eutf_option( 'responsive_header_height' ) . "px;
		}

		#eut-header.eut-responsive-overlapping + .eut-page-title .eut-wrapper,
		#eut-header.eut-responsive-overlapping + #eut-feature-section .eut-wrapper {
			padding-top: " . fildisi_eutf_option( 'responsive_header_height' ) . "px;
		}

	}
	";

	/* - Responsive Menu
	========================================================================= */
	$fildisi_eutf_responsive_menu_overflow_background_color = fildisi_eutf_option( 'responsive_menu_overflow_background_color', '#000000' );
	$css .= "

	#eut-hidden-menu {
		background-color: " . fildisi_eutf_option( 'responsive_menu_background_color' ) . ";
	}

	#eut-hidden-menu a {
		color: " . fildisi_eutf_option( 'responsive_menu_link_color' ) . ";
	}

	#eut-hidden-menu:not(.eut-slide-menu) ul.eut-menu li a .eut-arrow:after,
	#eut-hidden-menu:not(.eut-slide-menu) ul.eut-menu li a .eut-arrow:before {
		background-color: " . fildisi_eutf_option( 'responsive_menu_link_color' ) . ";
	}

	#eut-hidden-menu ul.eut-menu li.open > a .eut-arrow:after,
	#eut-hidden-menu ul.eut-menu li.open > a .eut-arrow:before {
		background-color: " . fildisi_eutf_option( 'responsive_menu_link_hover_color' ) . ";
	}

	#eut-hidden-menu.eut-slide-menu ul.eut-menu li > .eut-arrow:hover {
		color: " . fildisi_eutf_option( 'responsive_menu_link_hover_color' ) . ";
	}

	#eut-theme-wrapper .eut-header-responsive-elements form,
	#eut-theme-wrapper .eut-header-responsive-elements form p,
	#eut-theme-wrapper .eut-header-responsive-elements form div,
	#eut-theme-wrapper .eut-header-responsive-elements form span {
		color: " . fildisi_eutf_option( 'responsive_menu_link_color' ) . ";
	}

	#eut-hidden-menu a:hover,
	#eut-hidden-menu ul.eut-menu > li.current-menu-item > a,
	#eut-hidden-menu ul.eut-menu > li.current-menu-ancestor > a,
	#eut-hidden-menu ul.eut-menu li.current-menu-item > a,
	#eut-hidden-menu ul.eut-menu li.open > a {
		color: " . fildisi_eutf_option( 'responsive_menu_link_hover_color' ) . ";
	}

	#eut-hidden-menu .eut-close-btn {
		color: " . fildisi_eutf_option( 'responsive_menu_close_btn_color' ) . ";
	}

	#eut-hidden-menu ul.eut-menu li,
	#eut-hidden-menu ul.eut-menu li a,
	#eut-theme-wrapper .eut-header-responsive-elements form,
	#eut-theme-wrapper .eut-header-responsive-elements form p,
	#eut-theme-wrapper .eut-header-responsive-elements form div,
	#eut-theme-wrapper .eut-header-responsive-elements form span {
		border-color: " . fildisi_eutf_option( 'responsive_menu_border_color' ) . ";
	}

	#eut-hidden-menu-overlay {
		background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_responsive_menu_overflow_background_color ) . "," . fildisi_eutf_option( 'responsive_menu_overflow_background_color_opacity', '0.9') . ");
	}

	";

	/* - Responsive Header Elements
	========================================================================= */
	$css .= "
	#eut-responsive-header .eut-logo-text a,
	#eut-responsive-header .eut-header-element > a,
	#eut-responsive-header .eut-header-element .eut-purchased-items {
		color: " . fildisi_eutf_option( 'responsive_header_elements_color' ) . ";
	}

	#eut-responsive-header .eut-logo-text a:hover,
	#eut-responsive-header .eut-header-element > a:hover {
		color: " . fildisi_eutf_option( 'responsive_header_elements_hover_color' ) . ";
	}

	#eut-responsive-header .eut-hidden-menu-btn a .eut-item:not(.eut-with-text) span {
		background-color: " . fildisi_eutf_option( 'responsive_header_elements_color' ) . ";
	}

	";


/* Spinner
============================================================================= */


$spinner_image_id = fildisi_eutf_option( 'spinner_image', '', 'id' );
if ( empty( $spinner_image_id ) ) {
	$css .= "
	.eut-spinner {
		display: inline-block;
		position: absolute !important;
		top: 50%;
		left: 50%;
		margin-top: -1.500em;
		margin-left: -1.500em;
		text-indent: -9999em;
		-webkit-transform: translateZ(0);
		-ms-transform: translateZ(0);
		transform: translateZ(0);
	}
	.eut-isotope .eut-spinner {
		top: 50px;
	}
	.eut-spinner:not(.custom) {
		font-size: 14px;
		border-top: 0.200em solid rgba(127, 127, 127, 0.3);
		border-right: 0.200em solid rgba(127, 127, 127, 0.3);
		border-bottom: 0.200em solid rgba(127, 127, 127, 0.3);
		border-left: 0.200em solid;
		-webkit-animation: spinnerAnim 1.1s infinite linear;
		animation: spinnerAnim 1.1s infinite linear;
	}

	.eut-spinner:not(.custom) {
		border-left-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	}

	.eut-spinner:not(.custom),
	.eut-spinner:not(.custom):after {
		border-radius: 50%;
		width: 3.000em;
		height: 3.000em;
	}

	@-webkit-keyframes spinnerAnim {
		0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
	}

	@keyframes spinnerAnim {
		0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
	}
	";
} else {

	$spinner_src = wp_get_attachment_image_src( $spinner_image_id, 'full' );
	$spinner_image_url = $spinner_src[0];
	$spinner_width = $spinner_src[1];
	$spinner_height = $spinner_src[2];

	$css .= "

	.eut-spinner:not(.custom) {
		width: " . intval( $spinner_width ) . "px;
		height: " . intval( $spinner_height ) . "px;
		background-image: url(" . esc_url( $spinner_image_url ) . ");
		background-position: center center;
		display: inline-block;
		position: absolute;
		top: 50%;
		left: 50%;
		margin-top: -" . intval( $spinner_height / 2 ) . "px;
		margin-left: -" . intval( $spinner_width / 2 ) . "px;
	}

	";
}

/* Box Item
============================================================================= */
$css .= "
#eut-theme-wrapper .eut-box-item.eut-bg-white {
	color: #000000;
	color: rgba(0,0,0,0.30);
	background-color: #ffffff;
	-webkit-box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.25);
	-moz-box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.25);
	box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.25);
}

#eut-theme-wrapper .eut-box-item.eut-bg-black {
	color: #ffffff;
	color: rgba(255,255,255,0.60);
	background-color: #000000;
	-webkit-box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.25);
	-moz-box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.25);
	box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.25);
}

#eut-theme-wrapper .eut-box-item.eut-bg-white .eut-heading-color {
	color: #000000;
}

#eut-theme-wrapper .eut-box-item.eut-bg-black .eut-heading-color {
	color: #ffffff;
}

";


function fildisi_eutf_print_css_colors() {

	$fildisi_eutf_colors = fildisi_eutf_get_color_array();

	$css = '';

	foreach ( $fildisi_eutf_colors as $key => $value ) {

		//Gutenberg Editor Colors
		$css .= "
			#eut-theme-wrapper .has-" . esc_attr( $key ) . "-color {
				color: " . esc_attr( $value ) . ";
			}
			#eut-theme-wrapper .has-" . esc_attr( $key ) . "-background-color {
				background-color: " . esc_attr( $value ) . ";
			}
		";

	}

	return $css;
}

$css .= fildisi_eutf_print_css_colors();

/* Primary Text Color
============================================================================= */
$css .= "
::-moz-selection {
    color: #ffffff;
    background: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
}

::selection {
    color: #ffffff;
    background: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
}
";

/* Headings Colors */
$css .= "

h1,h2,h3,h4,h5,h6,
.eut-h1,
.eut-h2,
.eut-h3,
.eut-h4,
.eut-h5,
.eut-h6,
.eut-heading-color,
.eut-heading-hover-color:hover,
p.eut-dropcap:first-letter,
h3#reply-title {
	color: " . fildisi_eutf_option( 'body_heading_color' ) . ";
}

.eut-heading.eut-headings-primary-1,
.eut-headings-primary-1 h1,
.eut-headings-primary-1 h2,
.eut-headings-primary-1 h3,
.eut-headings-primary-1 h4,
.eut-headings-primary-1 h5,
.eut-headings-primary-1 h6,
.eut-headings-primary-1 .eut-heading-color,
.wpb_column.eut-headings-primary-1 h1,
.wpb_column.eut-headings-primary-1 h2,
.wpb_column.eut-headings-primary-1 h3,
.wpb_column.eut-headings-primary-1 h4,
.wpb_column.eut-headings-primary-1 h5,
.wpb_column.eut-headings-primary-1 h6,
.wpb_column.eut-headings-primary-1 .eut-heading-color,
.eut-blog ul.eut-post-meta a:hover,
#eut-content .widget.widget_nav_menu li.current-menu-item a,
#eut-content .widget.widget_nav_menu li a:hover {
	color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
}

.eut-heading.eut-headings-primary-2,
.eut-headings-primary-2 h1,
.eut-headings-primary-2 h2,
.eut-headings-primary-2 h3,
.eut-headings-primary-2 h4,
.eut-headings-primary-2 h5,
.eut-headings-primary-2 h6,
.eut-headings-primary-2 .eut-heading-color,
.wpb_column.eut-headings-primary-2 h1,
.wpb_column.eut-headings-primary-2 h2,
.wpb_column.eut-headings-primary-2 h3,
.wpb_column.eut-headings-primary-2 h4,
.wpb_column.eut-headings-primary-2 h5,
.wpb_column.eut-headings-primary-2 h6,
.wpb_column.eut-headings-primary-2 .eut-heading-color {
	color: " . fildisi_eutf_option( 'body_primary_2_color' ) . ";
}

.eut-heading.eut-headings-primary-3,
.eut-headings-primary-3 h1,
.eut-headings-primary-3 h2,
.eut-headings-primary-3 h3,
.eut-headings-primary-3 h4,
.eut-headings-primary-3 h5,
.eut-headings-primary-3 h6,
.eut-headings-primary-3 .eut-heading-color,
.wpb_column.eut-headings-primary-3 h1,
.wpb_column.eut-headings-primary-3 h2,
.wpb_column.eut-headings-primary-3 h3,
.wpb_column.eut-headings-primary-3 h4,
.wpb_column.eut-headings-primary-3 h5,
.wpb_column.eut-headings-primary-3 h6,
.wpb_column.eut-headings-primary-3 .eut-heading-color {
	color: " . fildisi_eutf_option( 'body_primary_3_color' ) . ";
}

.eut-heading.eut-headings-primary-4,
.eut-headings-primary-4 h1,
.eut-headings-primary-4 h2,
.eut-headings-primary-4 h3,
.eut-headings-primary-4 h4,
.eut-headings-primary-4 h5,
.eut-headings-primary-4 h6,
.eut-headings-primary-4 .eut-heading-color,
.wpb_column.eut-headings-primary-4 h1,
.wpb_column.eut-headings-primary-4 h2,
.wpb_column.eut-headings-primary-4 h3,
.wpb_column.eut-headings-primary-4 h4,
.wpb_column.eut-headings-primary-4 h5,
.wpb_column.eut-headings-primary-4 h6,
.wpb_column.eut-headings-primary-4 .eut-heading-color {
	color: " . fildisi_eutf_option( 'body_primary_4_color' ) . ";
}

.eut-heading.eut-headings-primary-5,
.eut-headings-primary-5 h1,
.eut-headings-primary-5 h2,
.eut-headings-primary-5 h3,
.eut-headings-primary-5 h4,
.eut-headings-primary-5 h5,
.eut-headings-primary-5 h6,
.eut-headings-primary-5 .eut-heading-color,
.wpb_column.eut-headings-primary-5 h1,
.wpb_column.eut-headings-primary-5 h2,
.wpb_column.eut-headings-primary-5 h3,
.wpb_column.eut-headings-primary-5 h4,
.wpb_column.eut-headings-primary-5 h5,
.wpb_column.eut-headings-primary-5 h6,
.wpb_column.eut-headings-primary-5 .eut-heading-color {
	color: " . fildisi_eutf_option( 'body_primary_5_color' ) . ";
}

.eut-heading.eut-headings-primary-6,
.eut-headings-primary-6 h1,
.eut-headings-primary-6 h2,
.eut-headings-primary-6 h3,
.eut-headings-primary-6 h4,
.eut-headings-primary-6 h5,
.eut-headings-primary-6 h6,
.eut-headings-primary-6 .eut-heading-color,
.wpb_column.eut-headings-primary-6 h1,
.wpb_column.eut-headings-primary-6 h2,
.wpb_column.eut-headings-primary-6 h3,
.wpb_column.eut-headings-primary-6 h4,
.wpb_column.eut-headings-primary-6 h5,
.wpb_column.eut-headings-primary-6 h6,
.wpb_column.eut-headings-primary-6 .eut-heading-color {
	color: " . fildisi_eutf_option( 'body_primary_6_color' ) . ";
}

.eut-heading.eut-headings-dark,
.eut-headings-dark h1,
.eut-headings-dark h2,
.eut-headings-dark h3,
.eut-headings-dark h4,
.eut-headings-dark h5,
.eut-headings-dark h6,
.eut-headings-dark .eut-heading-color,
.wpb_column.eut-headings-dark h1,
.wpb_column.eut-headings-dark h2,
.wpb_column.eut-headings-dark h3,
.wpb_column.eut-headings-dark h4,
.wpb_column.eut-headings-dark h5,
.wpb_column.eut-headings-dark h6,
.wpb_column.eut-headings-dark .eut-heading-color {
	color: #000000;
}

.eut-heading.eut-headings-light,
.eut-headings-light h1,
.eut-headings-light h2,
.eut-headings-light h3,
.eut-headings-light h4,
.eut-headings-light h5,
.eut-headings-light h6,
.eut-headings-light .eut-heading-color,
.wpb_column.eut-headings-light h1,
.wpb_column.eut-headings-light h2,
.wpb_column.eut-headings-light h3,
.wpb_column.eut-headings-light h4,
.wpb_column.eut-headings-light h5,
.wpb_column.eut-headings-light h6,
.wpb_column.eut-headings-light .eut-heading-color {
	color: #ffffff;
}

.eut-heading.eut-headings-green,
.eut-headings-green h1,
.eut-headings-green h2,
.eut-headings-green h3,
.eut-headings-green h4,
.eut-headings-green h5,
.eut-headings-green h6,
.eut-headings-green .eut-heading-color,
.wpb_column.eut-headings-green h1,
.wpb_column.eut-headings-green h2,
.wpb_column.eut-headings-green h3,
.wpb_column.eut-headings-green h4,
.wpb_column.eut-headings-green h5,
.wpb_column.eut-headings-green h6,
.wpb_column.eut-headings-green .eut-heading-color {
	color: #6ECA09;
}

.eut-heading.eut-headings-red,
.eut-headings-red h1,
.eut-headings-red h2,
.eut-headings-red h3,
.eut-headings-red h4,
.eut-headings-red h5,
.eut-headings-red h6,
.eut-headings-red .eut-heading-color,
.wpb_column.eut-headings-red h1,
.wpb_column.eut-headings-red h2,
.wpb_column.eut-headings-red h3,
.wpb_column.eut-headings-red h4,
.wpb_column.eut-headings-red h5,
.wpb_column.eut-headings-red h6,
.wpb_column.eut-headings-red .eut-heading-color {
	color: #D0021B;
}

.eut-heading.eut-headings-orange,
.eut-headings-orange h1,
.eut-headings-orange h2,
.eut-headings-orange h3,
.eut-headings-orange h4,
.eut-headings-orange h5,
.eut-headings-orange h6,
.eut-headings-orange .eut-heading-color,
.wpb_column.eut-headings-orange h1,
.wpb_column.eut-headings-orange h2,
.wpb_column.eut-headings-orange h3,
.wpb_column.eut-headings-orange h4,
.wpb_column.eut-headings-orange h5,
.wpb_column.eut-headings-orange h6,
.wpb_column.eut-headings-orange .eut-heading-color {
	color: #FAB901;
}

.eut-heading.eut-headings-aqua,
.eut-headings-aqua h1,
.eut-headings-aqua h2,
.eut-headings-aqua h3,
.eut-headings-aqua h4,
.eut-headings-aqua h5,
.eut-headings-aqua h6,
.eut-headings-aqua .eut-heading-color,
.wpb_column.eut-headings-aqua h1,
.wpb_column.eut-headings-aqua h2,
.wpb_column.eut-headings-aqua h3,
.wpb_column.eut-headings-aqua h4,
.wpb_column.eut-headings-aqua h5,
.wpb_column.eut-headings-aqua h6,
.wpb_column.eut-headings-aqua .eut-heading-color {
	color: #28d2dc;
}

.eut-heading.eut-headings-blue,
.eut-headings-blue h1,
.eut-headings-blue h2,
.eut-headings-blue h3,
.eut-headings-blue h4,
.eut-headings-blue h5,
.eut-headings-blue h6,
.eut-headings-blue .eut-heading-color,
.wpb_column.eut-headings-blue h1,
.wpb_column.eut-headings-blue h2,
.wpb_column.eut-headings-blue h3,
.wpb_column.eut-headings-blue h4,
.wpb_column.eut-headings-blue h5,
.wpb_column.eut-headings-blue h6,
.wpb_column.eut-headings-blue .eut-heading-color {
	color: #15c7ff;
}

.eut-heading.eut-headings-purple,
.eut-headings-purple h1,
.eut-headings-purple h2,
.eut-headings-purple h3,
.eut-headings-purple h4,
.eut-headings-purple h5,
.eut-headings-purple h6,
.eut-headings-purple .eut-heading-color,
.wpb_column.eut-headings-purple h1,
.wpb_column.eut-headings-purple h2,
.wpb_column.eut-headings-purple h3,
.wpb_column.eut-headings-purple h4,
.wpb_column.eut-headings-purple h5,
.wpb_column.eut-headings-purple h6,
.wpb_column.eut-headings-purple .eut-heading-color {
	color: #7639e2;
}

.eut-heading.eut-headings-grey,
.eut-headings-grey h1,
.eut-headings-grey h2,
.eut-headings-grey h3,
.eut-headings-grey h4,
.eut-headings-grey h5,
.eut-headings-grey h6,
.eut-headings-grey .eut-heading-color,
.wpb_column.eut-headings-grey h1,
.wpb_column.eut-headings-grey h2,
.wpb_column.eut-headings-grey h3,
.wpb_column.eut-headings-grey h4,
.wpb_column.eut-headings-grey h5,
.wpb_column.eut-headings-grey h6,
.wpb_column.eut-headings-grey .eut-heading-color {
	color: #888888;
}

";

/* Primary Text */
$css .= "
.eut-text-primary-1,
#eut-theme-wrapper .eut-text-hover-primary-1:hover,
#eut-theme-wrapper a.eut-text-hover-primary-1:hover,
#eut-theme-wrapper a .eut-text-hover-primary-1:hover,
.eut-blog .eut-post-meta-wrapper li a:hover,
.eut-search button[type='submit']:hover,
.widget.widget_calendar table tbody a,
blockquote > p:before,
.eut-filter.eut-filter-style-classic ul li:hover,
.eut-filter.eut-filter-style-classic ul li.selected {
	color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
}

.eut-text-primary-1.eut-svg-icon {
	stroke: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
}

.eut-text-primary-2,
#eut-theme-wrapper .eut-text-hover-primary-2:hover,
#eut-theme-wrapper a.eut-text-hover-primary-2:hover,
#eut-theme-wrapper a .eut-text-hover-primary-2:hover {
	color: " . fildisi_eutf_option( 'body_primary_2_color' ) . ";
}

.eut-text-primary-2.eut-svg-icon {
	stroke: " . fildisi_eutf_option( 'body_primary_2_color' ) . ";
}

.eut-text-primary-3,
#eut-theme-wrapper .eut-text-hover-primary-3:hover,
#eut-theme-wrapper a.eut-text-hover-primary-3:hover,
#eut-theme-wrapper a .eut-text-hover-primary-3:hover  {
	color: " . fildisi_eutf_option( 'body_primary_3_color' ) . ";
}

.eut-text-primary-3.eut-svg-icon {
	stroke: " . fildisi_eutf_option( 'body_primary_3_color' ) . ";
}

.eut-text-primary-4,
#eut-theme-wrapper .eut-text-hover-primary-4:hover,
#eut-theme-wrapper a.eut-text-hover-primary-4:hover,
#eut-theme-wrapper a .eut-text-hover-primary-4:hover  {
	color: " . fildisi_eutf_option( 'body_primary_4_color' ) . ";
}

.eut-text-primary-4.eut-svg-icon {
	stroke: " . fildisi_eutf_option( 'body_primary_4_color' ) . ";
}

.eut-text-primary-5,
#eut-theme-wrapper .eut-text-hover-primary-5:hover,
#eut-theme-wrapper a.eut-text-hover-primary-5:hover,
#eut-theme-wrapper a .eut-text-hover-primary-5:hover  {
	color: " . fildisi_eutf_option( 'body_primary_5_color' ) . ";
}

.eut-text-primary-5.eut-svg-icon {
	stroke: " . fildisi_eutf_option( 'body_primary_5_color' ) . ";
}

.eut-text-primary-6,
#eut-theme-wrapper .eut-text-hover-primary-6:hover,
#eut-theme-wrapper a.eut-text-hover-primary-6:hover,
#eut-theme-wrapper a .eut-text-hover-primary-6:hover  {
	color: " . fildisi_eutf_option( 'body_primary_6_color' ) . ";
}

.eut-text-primary-6.eut-svg-icon {
	stroke: " . fildisi_eutf_option( 'body_primary_6_color' ) . ";
}

";

/* Dark */
$css .= "
.eut-text-dark,
#eut-content .eut-text-dark,
a.eut-text-dark,
.eut-text-dark-hover:hover,
a:hover .eut-text-dark-hover,
.eut-blog.eut-with-shadow .eut-blog-item:not(.eut-style-2) .eut-post-title,
.eut-blog.eut-with-shadow .eut-blog-item:not(.eut-style-2) .eut-read-more {
	color: #000000;
}
.eut-text-dark.eut-svg-icon {
	stroke: #000000;
}

";

/* Light */
$css .= "
.eut-text-light,
#eut-content .eut-text-light,
a.eut-text-light,
.eut-text-light-hover:hover,
a:hover .eut-text-light-hover,
.eut-carousel-style-2 .eut-blog-carousel .eut-post-title {
	color: #ffffff;
}
.eut-text-light.eut-svg-icon {
	stroke: #ffffff;
}

";

/* Green Text */
$css .= "

.eut-text-green,
.eut-text-hover-green:hover,
a.eut-text-hover-green:hover,
a:hover .eut-text-hover-green {
	color: #6ECA09;
}
.eut-text-green.eut-svg-icon {
	stroke: #6ECA09;
}

";

/* Red Text */
$css .= "

.eut-text-red,
.eut-text-hover-red:hover,
a.eut-text-hover-red:hover,
a:hover .eut-text-hover-red {
	color: #D0021B;
}
.eut-text-red.eut-svg-icon {
	stroke: #D0021B;
}

";

/* Orange Text */
$css .= "

.eut-text-orange,
.eut-text-hover-orange:hover,
a.eut-text-hover-orange:hover,
a:hover .eut-text-hover-orange {
	color: #FAB901;
}
.eut-text-orange.eut-svg-icon {
	stroke: #FAB901;
}

";

/* Aqua Text */
$css .= "

.eut-text-aqua,
.eut-text-hover-aqua:hover,
a.eut-text-hover-aqua:hover,
a:hover .eut-text-hover-aqua {
	color: #28d2dc;
}
.eut-text-aqua.eut-svg-icon {
	stroke: #28d2dc;
}

";

/* Blue Text */
$css .= "

.eut-text-blue,
.eut-text-hover-blue:hover,
a.eut-text-hover-blue:hover,
a:hover .eut-text-hover-blue {
	color: #15c7ff;
}
.eut-text-blue.eut-svg-icon {
	stroke: #15c7ff;
}

";

/* Purple Text */
$css .= "

.eut-text-purple,
.eut-text-hover-purple:hover,
a.eut-text-hover-purple:hover,
a:hover .eut-text-hover-purple {
	color: #7639e2;
}
.eut-text-purple.eut-svg-icon {
	stroke: #7639e2;
}

";

/* Black Text */
$css .= "

.eut-text-black,
.eut-text-hover-black:hover,
a.eut-text-hover-black:hover,
a:hover .eut-text-hover-black {
	color: #000000;
}
.eut-text-black.eut-svg-icon {
	stroke: #000000;
}

";

/* Grey Text */
$css .= "

.eut-text-grey,
.eut-text-hover-grey:hover,
a.eut-text-hover-grey:hover,
a:hover .eut-text-hover-grey {
	color: #888888;
}
.eut-text-grey.eut-svg-icon {
	stroke: #888888;
}

";

/* White Text */
$css .= "

.eut-text-white,
.eut-text-hover-white:hover,
a.eut-text-hover-white:hover,
a:hover .eut-text-hover-white {
	color: #ffffff;
}
.eut-text-white.eut-svg-icon {
	stroke: #ffffff;
}

";


/* Primary Bg Color
============================================================================= */
/* Primary Background */
$css .= "
#eut-theme-wrapper .eut-bg-primary-1,
#eut-theme-wrapper .eut-bg-hover-primary-1:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-1:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-1:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-1,
.eut-filter.eut-filter-style-button.eut-filter-color-primary-1 ul li.selected,
#eut-theme-wrapper .eut-widget.eut-social li a.eut-outline:hover,
#eut-theme-wrapper .eut-with-line:after,
#eut-single-post-tags .eut-tags li a:hover,
#eut-single-post-categories .eut-categories li a:hover,
#eut-socials-modal .eut-social li a:hover,
.eut-hover-underline:after,
.eut-language-element ul li a:hover,
.eut-language-element ul li a.active,
#eut-language-modal ul li a:hover,
#eut-language-modal ul li a.active,
.eut-tabs-title .eut-tab-title.active .eut-title:after,
#eut-section-nav .eut-nav-item.active:after,
#eut-section-nav .eut-nav-item:hover:after {
	background-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-1 {
	background-color: transparent;
	border-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-1:hover {
	background-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-1 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-1 > a:hover .eut-item {
	background-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-bg-primary-2,
#eut-theme-wrapper .eut-bg-hover-primary-2:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-2:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-2:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-2,
.eut-filter.eut-filter-style-button.eut-filter-color-primary-2 ul li.selected {
	background-color: " . fildisi_eutf_option( 'body_primary_2_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_2_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-2 {
	background-color: transparent;
	border-color: " . fildisi_eutf_option( 'body_primary_2_color' ) . ";
	color: " . fildisi_eutf_option( 'body_primary_2_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-2:hover {
	background-color: " . fildisi_eutf_option( 'body_primary_2_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_2_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-2 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-2 > a:hover .eut-item {
	background-color: " . fildisi_eutf_option( 'body_primary_2_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-bg-primary-3,
#eut-theme-wrapper .eut-bg-hover-primary-3:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-3:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-3:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-3,
.eut-filter.eut-filter-style-button.eut-filter-color-primary-3 ul li.selected {
	background-color: " . fildisi_eutf_option( 'body_primary_3_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_3_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-3 {
	background-color: transparent;
	border-color: " . fildisi_eutf_option( 'body_primary_3_color' ) . ";
	color: " . fildisi_eutf_option( 'body_primary_3_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-3:hover {
	background-color: " . fildisi_eutf_option( 'body_primary_3_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_3_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-3 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-3 > a:hover .eut-item {
	background-color: " . fildisi_eutf_option( 'body_primary_3_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-bg-primary-4,
#eut-theme-wrapper .eut-bg-hover-primary-4:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-4:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-4:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-4,
.eut-filter.eut-filter-style-button.eut-filter-color-primary-4 ul li.selected {
	background-color: " . fildisi_eutf_option( 'body_primary_4_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_4_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-4 {
	background-color: transparent;
	border-color: " . fildisi_eutf_option( 'body_primary_4_color' ) . ";
	color: " . fildisi_eutf_option( 'body_primary_4_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-4:hover {
	background-color: " . fildisi_eutf_option( 'body_primary_4_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_4_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-4 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-4 > a:hover .eut-item {
	background-color: " . fildisi_eutf_option( 'body_primary_4_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-bg-primary-5,
#eut-theme-wrapper .eut-bg-hover-primary-5:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-5:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-5:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-5,
.eut-filter.eut-filter-style-button.eut-filter-color-primary-5 ul li.selected {
	background-color: " . fildisi_eutf_option( 'body_primary_5_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_5_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-5 {
	background-color: transparent;
	border-color: " . fildisi_eutf_option( 'body_primary_5_color' ) . ";
	color: " . fildisi_eutf_option( 'body_primary_5_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-5:hover {
	background-color: " . fildisi_eutf_option( 'body_primary_5_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_5_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-5 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-5 > a:hover .eut-item {
	background-color: " . fildisi_eutf_option( 'body_primary_5_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-bg-primary-6,
#eut-theme-wrapper .eut-bg-hover-primary-6:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-6:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-6:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-6,
.eut-filter.eut-filter-style-button.eut-filter-color-primary-6 ul li.selected {
	background-color: " . fildisi_eutf_option( 'body_primary_6_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_6_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-6 {
	background-color: transparent;
	border-color: " . fildisi_eutf_option( 'body_primary_6_color' ) . ";
	color: " . fildisi_eutf_option( 'body_primary_6_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-6:hover {
	background-color: " . fildisi_eutf_option( 'body_primary_6_color' ) . ";
	border-color: " . fildisi_eutf_option( 'body_primary_6_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-6 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-6 > a:hover .eut-item {
	background-color: " . fildisi_eutf_option( 'body_primary_6_color' ) . ";
	color: #ffffff;
}

";
/* Dark Background */
$css .= "
.eut-bg-dark,
a.eut-bg-dark:hover,
.eut-outline-btn a.eut-bg-dark:hover {
	background-color: #000000;
	color: #ffffff;
}

.eut-outline-btn a.eut-bg-dark {
	background-color: transparent;
	border-color: #000000;
	color: #000000;
}

";
/* Light Background */
$css .= "
.eut-bg-light,
a.eut-bg-light:hover {
	background-color: #ffffff;
	color: #000000;
}

.eut-outline-btn a.eut-bg-light:hover {
	background-color: #ffffff;
	color: #000000;
}

.eut-outline-btn a.eut-bg-light {
	background-color: transparent;
	border-color: #ffffff;
	color: #ffffff;
}
";


/* Green Background */
$css .= "
#eut-theme-wrapper .eut-bg-green,
#eut-theme-wrapper .eut-bg-hover-green:hover,
#eut-theme-wrapper a.eut-bg-hover-green:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-green,
.eut-filter.eut-filter-style-button.eut-filter-color-green ul li.selected {
	background-color: #6ECA09;
	border-color: #6ECA09;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-green {
	background-color: transparent;
	border-color: #6ECA09;
	color: #6ECA09;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-green:hover {
	background-color: #6ECA09;
	border-color: #6ECA09;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-green > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-green > a:hover .eut-item {
	background-color: #6ECA09;
	color: #ffffff;
}

";


/* Red Background */
$css .= "
#eut-theme-wrapper .eut-bg-red,
#eut-theme-wrapper .eut-bg-hover-red:hover,
#eut-theme-wrapper a.eut-bg-hover-red:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-red,
.eut-filter.eut-filter-style-button.eut-filter-color-red ul li.selected {
	background-color: #D0021B;
	border-color: #D0021B;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-red {
	background-color: transparent;
	border-color: #D0021B;
	color: #D0021B;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-red:hover {
	background-color: #D0021B;
	border-color: #D0021B;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-red > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-red > a:hover .eut-item {
	background-color: #D0021B;
	color: #ffffff;
}

";

/* Orange Background */
$css .= "
#eut-theme-wrapper .eut-bg-orange,
#eut-theme-wrapper .eut-bg-hover-orange:hover,
#eut-theme-wrapper a.eut-bg-hover-orange:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-orange,
.eut-filter.eut-filter-style-button.eut-filter-color-orange ul li.selected {
	background-color: #FAB901;
	border-color: #FAB901;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-orange {
	background-color: transparent;
	border-color: #FAB901;
	color: #FAB901;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-orange:hover {
	background-color: #FAB901;
	border-color: #FAB901;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-orange > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-orange > a:hover .eut-item {
	background-color: #FAB901;
	color: #ffffff;
}

";

/* Aqua Background */
$css .= "
#eut-theme-wrapper .eut-bg-aqua,
#eut-theme-wrapper .eut-bg-hover-aqua:hover,
#eut-theme-wrapper a.eut-bg-hover-aqua:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-aqua,
.eut-filter.eut-filter-style-button.eut-filter-color-aqua ul li.selected {
	background-color: #28d2dc;
	border-color: #28d2dc;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-aqua {
	background-color: transparent;
	border-color: #28d2dc;
	color: #28d2dc;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-aqua:hover {
	background-color: #28d2dc;
	border-color: #28d2dc;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-aqua > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-aqua > a:hover .eut-item {
	background-color: #28d2dc;
	color: #ffffff;
}

";


/* Blue Background */
$css .= "
#eut-theme-wrapper .eut-bg-blue,
#eut-theme-wrapper .eut-bg-hover-blue:hover,
#eut-theme-wrapper a.eut-bg-hover-blue:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-blue,
.eut-filter.eut-filter-style-button.eut-filter-color-blue ul li.selected {
	background-color: #15c7ff;
	border-color: #15c7ff;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-blue {
	background-color: transparent;
	border-color: #15c7ff;
	color: #15c7ff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-blue:hover {
	background-color: #15c7ff;
	border-color: #15c7ff;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-blue > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-blue > a:hover .eut-item {
	background-color: #15c7ff;
	color: #ffffff;
}

";

/* Purple Background */
$css .= "
#eut-theme-wrapper .eut-bg-purple,
#eut-theme-wrapper .eut-bg-hover-purple:hover,
#eut-theme-wrapper a.eut-bg-hover-purple:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-purple,
.eut-filter.eut-filter-style-button.eut-filter-color-purple ul li.selected {
	background-color: #7639e2;
	border-color: #7639e2;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-purple {
	background-color: transparent;
	border-color: #7639e2;
	color: #7639e2;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-purple:hover {
	background-color: #7639e2;
	border-color: #7639e2;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-purple > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-purple > a:hover .eut-item {
	background-color: #7639e2;
	color: #ffffff;
}

";

/* Black Background */
$css .= "
#eut-theme-wrapper .eut-bg-black,
#eut-theme-wrapper .eut-bg-hover-black:hover,
#eut-theme-wrapper a.eut-bg-hover-black:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-black,
.eut-filter.eut-filter-style-button.eut-filter-color-black ul li.selected {
	background-color: #000000;
	border-color: #000000;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-black {
	background-color: transparent;
	border-color: #000000;
	color: #000000;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-black:hover {
	background-color: #000000;
	border-color: #000000;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-black > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-black > a:hover .eut-item {
	background-color: #000000;
	color: #ffffff;
}

";

/* Grey Background */
$css .= "
#eut-theme-wrapper .eut-bg-grey,
#eut-theme-wrapper .eut-bg-hover-grey:hover,
#eut-theme-wrapper a.eut-bg-hover-grey:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-grey,
.eut-filter.eut-filter-style-button.eut-filter-color-grey ul li.selected {
	background-color: #cccccc;
	border-color: #cccccc;
	color: #484848;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-grey {
	background-color: transparent;
	border-color: #cccccc;
	color: #cccccc;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-grey:hover {
	background-color: #cccccc;
	border-color: #cccccc;
	color: #484848;
}

#eut-theme-wrapper .eut-menu-type-button.eut-grey > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-grey > a:hover .eut-item {
	background-color: #cccccc;
	color: #484848;
}

";

/* White Background */
$css .= "
#eut-theme-wrapper .eut-bg-white,
#eut-theme-wrapper .eut-bg-hover-white:hover,
#eut-theme-wrapper a.eut-bg-hover-white:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-white,
.eut-filter.eut-filter-style-button.eut-filter-color-white ul li.selected {
	background-color: #ffffff;
	border-color: #ffffff;
	color: #000000;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-white {
	background-color: transparent;
	border-color: #ffffff;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-white:hover {
	background-color: #ffffff;
	border-color: #ffffff;
	color: #000000;
}

#eut-theme-wrapper .eut-menu-type-button.eut-white > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-white > a:hover .eut-item {
	background-color: #ffffff;
	color: #000000;
}

";

/* Anchor Menu
============================================================================= */

// Anchor Colors
$css .= "

.eut-anchor-menu .eut-anchor-wrapper,
.eut-anchor-menu .eut-container ul {
	background-color: " . fildisi_eutf_option( 'page_anchor_menu_background_color' ) . ";
}

.eut-anchor-menu .eut-anchor-wrapper,
.eut-anchor-menu .eut-container > ul > li > a,
.eut-anchor-menu .eut-container ul li a,
.eut-anchor-menu .eut-container > ul > li:last-child > a {
	border-color: " . fildisi_eutf_option( 'page_anchor_menu_border_color' ) . ";
}

.eut-anchor-menu a {
	color: " . fildisi_eutf_option( 'page_anchor_menu_text_color' ) . ";
	background-color: transparent;
}

.eut-anchor-menu a:hover,
.eut-anchor-menu .eut-container > ul > li.active > a {
	color: " . fildisi_eutf_option( 'page_anchor_menu_text_hover_color' ) . ";
	background-color: " . fildisi_eutf_option( 'page_anchor_menu_background_hover_color' ) . ";
}

.eut-anchor-menu a .eut-arrow:after,
.eut-anchor-menu a .eut-arrow:before {
	background-color: " . fildisi_eutf_option( 'page_anchor_menu_text_hover_color' ) . ";
}

";

// Page Anchor Size
$css .= "

#eut-page-anchor {
	height: " . intval( fildisi_eutf_option( 'page_anchor_menu_height', 120 ) + 2 ) . "px;
}

#eut-page-anchor .eut-anchor-wrapper {
	line-height: " . fildisi_eutf_option( 'page_anchor_menu_height' ) . "px;
}

#eut-page-anchor.eut-anchor-menu .eut-anchor-btn {
	width: " . fildisi_eutf_option( 'page_anchor_menu_height' ) . "px;
}

";

// Post Anchor Size
$css .= "

#eut-post-anchor {
	height: " . intval( fildisi_eutf_option( 'post_anchor_menu_height', 120 ) + 2 ) . "px;
}

#eut-post-anchor .eut-anchor-wrapper {
	line-height: " . fildisi_eutf_option( 'post_anchor_menu_height' ) . "px;
}

#eut-post-anchor.eut-anchor-menu .eut-anchor-btn {
	width: " . fildisi_eutf_option( 'page_anchor_menu_height' ) . "px;
}

";

// Portfolio Anchor Size
$css .= "

#eut-portfolio-anchor {
	height: " . intval( fildisi_eutf_option( 'portfolio_anchor_menu_height', 120 ) + 2 ) . "px;
}

#eut-portfolio-anchor .eut-anchor-wrapper {
	line-height: " . fildisi_eutf_option( 'portfolio_anchor_menu_height' ) . "px;
}

#eut-portfolio-anchor.eut-anchor-menu .eut-anchor-btn {
	width: " . fildisi_eutf_option( 'portfolio_anchor_menu_height' ) . "px;
}

";


/* Breadcrumbs
============================================================================= */
$css .= "
.eut-breadcrumbs {
	background-color: " . fildisi_eutf_option( 'page_breadcrumbs_background_color' ) . ";
	border-color: " . fildisi_eutf_option( 'page_breadcrumbs_border_color' ) . ";
}

.eut-breadcrumbs ul li {
	color: " . fildisi_eutf_option( 'page_breadcrumbs_divider_color' ) . ";
}

.eut-breadcrumbs ul li a {
	color: " . fildisi_eutf_option( 'page_breadcrumbs_text_color' ) . ";
}

.eut-breadcrumbs ul li a:hover {
	color: " . fildisi_eutf_option( 'page_breadcrumbs_text_hover_color' ) . ";
}

";

// Page Breadcrumbs Size
$css .= "

#eut-page-breadcrumbs {
	line-height: " . fildisi_eutf_option( 'page_breadcrumbs_height' ) . "px;
}

";

// Post Breadcrumbs Size
$css .= "

#eut-post-breadcrumbs {
	line-height: " . fildisi_eutf_option( 'post_breadcrumbs_height' ) . "px;
}

";

// Portfolio Breadcrumbs Size
$css .= "

#eut-portfolio-breadcrumbs {
	line-height: " . fildisi_eutf_option( 'portfolio_breadcrumbs_height' ) . "px;
}

";

// Product Breadcrumbs Size
$css .= "

#eut-product-breadcrumbs {
	line-height: " . fildisi_eutf_option( 'product_breadcrumbs_height' ) . "px;
}

";

/* Main Content
============================================================================= */
$css .= "

#eut-content,
.eut-single-wrapper,
#eut-main-content .eut-section,
.eut-anchor-menu,
#eut-bottom-bar {
	background-color: " . fildisi_eutf_option( 'main_content_background_color' ) . ";
	color: " . fildisi_eutf_option( 'body_text_color' ) . ";
}

body,
.eut-text-content,
.eut-text-content a,
#eut-content form,
#eut-content form p,
#eut-content form div,
#eut-content form span:not(.eut-heading-color),
table,
.eut-blog.eut-with-shadow .eut-blog-item:not(.eut-style-2) .eut-post-meta,
.eut-blog.eut-with-shadow .eut-blog-item:not(.eut-style-2) p {
	color: " . fildisi_eutf_option( 'body_text_color' ) . ";
}

";
	/* - Main Content Borders
	========================================================================= */
	$css .= "
	#eut-theme-wrapper .eut-border,
	a.eut-border,
	#eut-content table,
	#eut-content tr,
	#eut-content td,
	#eut-content th,
	#eut-theme-wrapper form,
	#eut-theme-wrapper form p,
	#eut-theme-wrapper .wpcf7-form-control-wrap,
	#eut-theme-wrapper label,
	#eut-content form div,
	hr,
	.eut-hr.eut-element div,
	.eut-title-double-line span:before,
	.eut-title-double-line span:after,
	.eut-title-double-bottom-line span:after,
	.vc_tta.vc_general .vc_tta-panel-title,
	#eut-single-post-tags .eut-tags li a,
	#eut-single-post-categories .eut-categories li a {
		border-color: " . fildisi_eutf_option( 'body_border_color' ) . " !important;
	}

	#eut-single-post-categories .eut-categories li a {
		background-color: " . fildisi_eutf_option( 'body_border_color' ) . ";
	}

	";

	/* Primary Border */
	$css .= "
	.eut-border-primary-1,
	#eut-content .eut-blog-large .eut-blog-item.sticky ul.eut-post-meta,
	.eut-carousel-pagination-2 .eut-carousel .owl-controls .owl-page.active span,
	.eut-carousel-pagination-2 .eut-carousel .owl-controls.clickable .owl-page:hover span,
	.eut-carousel-pagination-2.eut-testimonial .owl-controls .owl-page.active span,
	.eut-carousel-pagination-2.eut-testimonial .owl-controls.clickable .owl-page:hover span,
	.eut-carousel-pagination-2 .eut-flexible-carousel .owl-controls .owl-page.active span,
	.eut-carousel-pagination-2 .eut-flexible-carousel .owl-controls.clickable .owl-page:hover span,
	#eut-content .eut-read-more:after,
	#eut-content .more-link:after,
	.eut-blog-large .eut-blog-item.sticky .eut-blog-item-inner:after,
	.eut-quote-text,
	blockquote p {
		border-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	}
	";

	/* - Widget Colors
	========================================================================= */
	$css .= "
	#eut-content .widget .eut-widget-title {
		color: " . fildisi_eutf_option( 'widget_title_color' ) . ";
	}

	.widget {
		color: " . fildisi_eutf_option( 'body_text_color' ) . ";
	}

	.widget,
	.widget ul,
	.widget li,
	.widget div {
		border-color: " . fildisi_eutf_option( 'body_border_color' ) . ";
	}

	.eut-widget.eut-social li a.eut-outline:hover {
		border-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	}

	.widget a:not(.eut-outline):not(.eut-btn) {
		color: " . fildisi_eutf_option( 'body_text_color' ) . ";
	}

	.widget:not(.eut-social) a:not(.eut-outline):not(.eut-btn):hover,
	.widget.widget_nav_menu li.open > a {
		color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
	}
	";

/* Bottom Bar Colors
============================================================================= */


/* Post Navigation Bar
============================================================================= */

if ( 'layout-1' == fildisi_eutf_option( 'post_nav_bar_layout', 'layout-1' ) ) {
	$css .= "
	#eut-post-bar .eut-post-bar-item:not(.eut-post-navigation),
	#eut-post-bar .eut-post-bar-item .eut-nav-item {
		padding-top: " . fildisi_eutf_option( 'post_nav_spacing', '', 'padding-top' ) . ";
		padding-bottom: " . fildisi_eutf_option( 'post_nav_spacing', '', 'padding-bottom'  ) . ";
	}
	";
}

$css .= "
#eut-post-bar,
#eut-post-bar.eut-layout-3 .eut-post-bar-item .eut-item-icon,
#eut-post-bar.eut-layout-3 .eut-post-bar-item {
	background-color: " . fildisi_eutf_option( 'post_bar_background_color' ) . ";
	border-color: " . fildisi_eutf_option( 'post_bar_border_color' ) . ";
}

#eut-post-bar .eut-post-bar-item,
#eut-post-bar.eut-layout-1 .eut-post-bar-item .eut-nav-item,
#eut-post-bar.eut-layout-2:not(.eut-nav-columns-1) .eut-post-bar-item .eut-next,
#eut-post-bar.eut-layout-2.eut-nav-columns-1 .eut-post-bar-item .eut-prev + .eut-next  {
	border-color: " . fildisi_eutf_option( 'post_bar_border_color' ) . ";
}

#eut-post-bar .eut-nav-item .eut-title {
	color: " . fildisi_eutf_option( 'post_bar_nav_title_color' ) . ";
}

#eut-post-bar .eut-bar-socials li {
	border-color: " . fildisi_eutf_option( 'post_bar_border_color' ) . ";
}

#eut-post-bar .eut-arrow,
#eut-post-bar.eut-layout-3 .eut-post-bar-item .eut-item-icon {
	color: " . fildisi_eutf_option( 'post_bar_arrow_color' ) . ";
}

#eut-post-bar .eut-backlink-icon {
	background-color: " . fildisi_eutf_option( 'post_bar_nav_title_color' ) . ";
	box-shadow: .5em 0 " . fildisi_eutf_option( 'post_bar_nav_title_color' ) . ", 0 .5em " . fildisi_eutf_option( 'post_bar_nav_title_color' ) . ", .5em .5em " . fildisi_eutf_option( 'post_bar_nav_title_color' ) . ";
}

#eut-post-bar .eut-backlink:hover .eut-backlink-icon {
	box-shadow: .8em 0 " . fildisi_eutf_option( 'post_bar_nav_title_color' ) . ", 0 .8em " . fildisi_eutf_option( 'post_bar_nav_title_color' ) . ", .8em .8em " . fildisi_eutf_option( 'post_bar_nav_title_color' ) . ";
}

";

/* Portfolio Navigation Bar
============================================================================= */
if ( 'layout-1' == fildisi_eutf_option( 'portfolio_nav_bar_layout', 'layout-1' ) ) {
	$css .= "
	#eut-portfolio-bar {
		padding-top: " . fildisi_eutf_option( 'portfolio_nav_spacing', '', 'padding-top' ) . ";
		padding-bottom: " . fildisi_eutf_option( 'portfolio_nav_spacing', '', 'padding-bottom'  ) . ";
	}
	";
}

$css .= "
#eut-portfolio-bar,
#eut-portfolio-bar.eut-layout-3 .eut-post-bar-item .eut-item-icon,
#eut-portfolio-bar.eut-layout-3 .eut-post-bar-item {
	background-color: " . fildisi_eutf_option( 'portfolio_bar_background_color' ) . ";
	border-color: " . fildisi_eutf_option( 'portfolio_bar_border_color' ) . ";
}

#eut-portfolio-bar .eut-post-bar-item,
#eut-portfolio-bar.eut-layout-1 .eut-post-bar-item .eut-nav-item,
#eut-portfolio-bar.eut-layout-2:not(.eut-nav-columns-1) .eut-post-bar-item .eut-next,
#eut-portfolio-bar.eut-layout-2.eut-nav-columns-1 .eut-post-bar-item .eut-prev + .eut-next  {
	border-color: " . fildisi_eutf_option( 'portfolio_bar_border_color' ) . ";
}

#eut-portfolio-bar .eut-nav-item .eut-title {
	color: " . fildisi_eutf_option( 'portfolio_bar_nav_title_color' ) . ";
}

#eut-portfolio-bar .eut-bar-socials li {
	border-color: " . fildisi_eutf_option( 'portfolio_bar_border_color' ) . ";
}

#eut-portfolio-bar .eut-arrow,
#eut-portfolio-bar.eut-layout-3 .eut-post-bar-item .eut-item-icon {
	color: " . fildisi_eutf_option( 'portfolio_bar_arrow_color' ) . ";
}

#eut-portfolio-bar .eut-backlink-icon {
	background-color: " . fildisi_eutf_option( 'portfolio_bar_nav_title_color' ) . ";
	box-shadow: .5em 0 " . fildisi_eutf_option( 'portfolio_bar_nav_title_color' ) . ", 0 .5em " . fildisi_eutf_option( 'portfolio_bar_nav_title_color' ) . ", .5em .5em " . fildisi_eutf_option( 'portfolio_bar_nav_title_color' ) . ";
}

#eut-portfolio-bar .eut-backlink:hover .eut-backlink-icon {
	box-shadow: .8em 0 " . fildisi_eutf_option( 'portfolio_bar_nav_title_color' ) . ", 0 .8em " . fildisi_eutf_option( 'portfolio_bar_nav_title_color' ) . ", .8em .8em " . fildisi_eutf_option( 'portfolio_bar_nav_title_color' ) . ";
}

";

/* Footer
============================================================================= */

	/* - Widget Area
	========================================================================= */
	$css .= "
	#eut-footer .eut-widget-area {
		background-color: " . fildisi_eutf_option( 'footer_widgets_bg_color' ) . ";
	}
	";
	/* - Footer Widget Colors
	========================================================================= */
	$css .= "
	#eut-footer .eut-widget-area .widget .eut-widget-title,
	#eut-footer .eut-widget-area h1,
	#eut-footer .eut-widget-area h2,
	#eut-footer .eut-widget-area h3,
	#eut-footer .eut-widget-area h4,
	#eut-footer .eut-widget-area h5,
	#eut-footer .eut-widget-area h6 {
		color: " . fildisi_eutf_option( 'footer_widgets_headings_color' ) . ";
	}

	#eut-footer .eut-widget-area .widget,
	#eut-footer .eut-widget-area form,
	#eut-footer .eut-widget-area form p,
	#eut-footer .eut-widget-area form div,
	#eut-footer .eut-widget-area form span {
		color: " . fildisi_eutf_option( 'footer_widgets_font_color' ) . ";
	}

	#eut-footer .eut-widget-area,
	#eut-footer .eut-widget-area .eut-container,
	#eut-footer .eut-widget-area .widget,
	#eut-footer .eut-widget-area .widget a:not(.eut-outline):not(.eut-btn),
	#eut-footer .eut-widget-area .widget ul,
	#eut-footer .eut-widget-area .widget li,
	#eut-footer .eut-widget-area .widget div,
	#eut-footer .eut-widget-area table,
	#eut-footer .eut-widget-area tr,
	#eut-footer .eut-widget-area td,
	#eut-footer .eut-widget-area th,
	#eut-footer .eut-widget-area form,
	#eut-footer .eut-widget-area .wpcf7-form-control-wrap,
	#eut-footer .eut-widget-area label,
	#eut-footer .eut-widget-area .eut-border,
	#eut-footer .eut-widget-area form,
	#eut-footer .eut-widget-area form p,
	#eut-footer .eut-widget-area form div,
	#eut-footer .eut-widget-area form span,
	#eut-footer .eut-widget-area .eut-widget-area {
		border-color: " . fildisi_eutf_option( 'footer_widgets_border_color' ) . ";
	}

	#eut-footer .eut-widget-area .widget a:not(.eut-outline):not(.eut-btn) {
		color: " . fildisi_eutf_option( 'footer_widgets_link_color' ) . ";
	}

	#eut-footer .eut-widget-area .widget:not(.widget_tag_cloud) a:not(.eut-outline):not(.eut-btn):hover,
	#eut-footer .eut-widget-area .widget.widget_nav_menu li.open > a {
		color: " . fildisi_eutf_option( 'footer_widgets_hover_color' ) . ";
	}

	";
	/* - Footer Bar Colors
	========================================================================= */
	$fildisi_eutf_footer_bar_background_color = fildisi_eutf_option( 'footer_bar_bg_color', '#000000' );
	$css .= "
	#eut-footer .eut-footer-bar {
		color: " . fildisi_eutf_option( 'footer_bar_font_color' ) . ";
		background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_footer_bar_background_color ) . "," . fildisi_eutf_option( 'footer_bar_bg_color_opacity', '1') . ");
	}

	#eut-footer .eut-footer-bar a {
		color: " . fildisi_eutf_option( 'footer_bar_link_color' ) . ";
	}

	#eut-footer .eut-footer-bar a:hover {
		color: " . fildisi_eutf_option( 'footer_bar_hover_color' ) . ";
	}
	";

	/* - Back To Top Colors
	========================================================================= */
	$css .= "
	.eut-back-top .eut-wrapper-color {
		background-color: " . fildisi_eutf_option( 'back_to_top_shape_color' ) . ";
	}

	.eut-back-top .eut-back-top-icon {
		color: " . fildisi_eutf_option( 'back_to_top_icon_color' ) . ";
	}
	";

/* Tag Cloud
============================================================================= */
if ( '1' != fildisi_eutf_option( 'wp_tagcloud', '0' ) ) {
	$css .= "
	.widget.widget_tag_cloud a {
		display: inline-block;
		margin-bottom: 4px;
		margin-right: 4px;
		font-size: 12px !important;
		border: 2px solid;
		border-color: inherit;
		-webkit-border-radius: 50px;
		border-radius: 50px;
		line-height: 30px;
		padding: 0 15px;
		color: inherit;
		-webkit-transition : all .3s;
		-moz-transition    : all .3s;
		-ms-transition     : all .3s;
		-o-transition      : all .3s;
		transition         : all .3s;
	}

	#eut-theme-wrapper .widget.widget_tag_cloud a {
		border-color: " . fildisi_eutf_option( 'body_border_color' ) . ";
	}

	#eut-theme-wrapper .widget.widget_tag_cloud a:hover,
	#eut-theme-wrapper #eut-sidearea .widget.widget_tag_cloud a:hover {
		background-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
		border-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
		color: #ffffff;
	}

	#eut-theme-wrapper #eut-sidearea .widget.widget_tag_cloud a {
		border-color: " . fildisi_eutf_option( 'sliding_area_border_color' ) . ";
	}

	#eut-footer .eut-widget-area .widget.widget_tag_cloud a:hover {
		background-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
		border-color: " . fildisi_eutf_option( 'body_primary_1_color' ) . ";
		color: #ffffff;
	}
	";
} else {
	$css .= "
	.widget.widget_tag_cloud a {
		display: inline-block;
		vertical-align: middle;
		margin-bottom: 4px;
		margin-right: 8px;
		line-height: 1.4;
		-webkit-transition : all .3s;
		-moz-transition    : all .3s;
		-ms-transition     : all .3s;
		-o-transition      : all .3s;
		transition         : all .3s;
	}

	#eut-footer .eut-widget-area .widget.widget_tag_cloud a:hover {
		color: " . fildisi_eutf_option( 'footer_widgets_hover_color' ) . ";
	}
	";
}

/* GDPR Privacy
============================================================================= */
$fildisi_eutf_privacy_bar_bg_color = fildisi_eutf_option( 'privacy_bar_bg_color', '#000000' );
$css .= "

#eut-privacy-bar {
	background-color: rgba(" . fildisi_eutf_hex2rgb( $fildisi_eutf_privacy_bar_bg_color ) . "," . fildisi_eutf_option( 'privacy_bar_bg_opacity', '0.90') . ");
	color: " . fildisi_eutf_option( 'privacy_bar_text_color' ) . ";
}

.eut-privacy-agreement {
	background-color: " . fildisi_eutf_option( 'privacy_bar_button_bg_color' ) . ";
	color: " . fildisi_eutf_option( 'privacy_bar_button_text_color' ) . ";
}

.eut-privacy-refresh-btn {
	background-color: " . fildisi_eutf_option( 'privacy_modal_button_bg_color' ) . ";
	color: " . fildisi_eutf_option( 'privacy_modal_button_text_color' ) . ";
}

.eut-privacy-preferences {
	background-color: transparent;
	color: " . fildisi_eutf_option( 'privacy_bar_text_color' ) . ";
}

.eut-privacy-agreement:hover {
	background-color: " . fildisi_eutf_option( 'privacy_bar_button_bg_hover_color' ) . ";
}

.eut-privacy-refresh-btn:hover {
	background-color: " . fildisi_eutf_option( 'privacy_modal_button_bg_hover_color' ) . ";
}

.eut-privacy-switch .eut-switch input[type='checkbox']:checked + .eut-switch-slider {
    background-color: " . fildisi_eutf_option( 'privacy_modal_button_bg_color' ) . ";
}

";

/* Composer Front End Fix*/
$css .= "

.compose-mode .vc_element .eut-row {
    margin-top: 30px;
}

.compose-mode .vc_vc_column .wpb_column {
    width: 100% !important;
    margin-bottom: 30px;
    border: 1px dashed rgba(125, 125, 125, 0.4);
}

.compose-mode .vc_controls > .vc_controls-out-tl {
    left: 15px;
}

.compose-mode .vc_controls > .vc_controls-bc {
    bottom: 15px;
}

.compose-mode .vc_welcome .vc_buttons {
    margin-top: 60px;
}

.compose-mode .eut-image img {
    opacity: 1;
}

.compose-mode .vc_controls > div {
    z-index: 9;
}
.compose-mode .eut-bg-image {
    opacity: 1;
}

.compose-mode #eut-theme-wrapper .eut-section.eut-fullwidth-background,
.compose-mode #eut-theme-wrapper .eut-section.eut-fullwidth-element {
	visibility: visible;
}

.compose-mode .eut-animated-item {
	opacity: 1;
}

";

$fildisi_eutf_gap_size = array (
	array(
		'gap' => '5',
	),
	array(
		'gap' => '10',
	),
	array(
		'gap' => '15',
	),
	array(
		'gap' => '20',
	),
	array(
		'gap' => '25',
	),
	array(
		'gap' => '30',
	),
	array(
		'gap' => '35',
	),
	array(
		'gap' => '40',
	),
	array(
		'gap' => '45',
	),
	array(
		'gap' => '50',
	),
	array(
		'gap' => '55',
	),
	array(
		'gap' => '60',
	),
);

function fildisi_eutf_print_gap_size( $fildisi_eutf_gap_size = array()) {

	$css = '';

	foreach ( $fildisi_eutf_gap_size as $size ) {

		$fildisi_eutf_gap_size = $size['gap'];
		$fildisi_eutf_gap_half_size = $size['gap'] * 0.5;

		$css .= "

			.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " {
				margin-left: -" . esc_attr( $fildisi_eutf_gap_half_size ) . "px;
				margin-right: -" . esc_attr( $fildisi_eutf_gap_half_size ) . "px;
			}
			.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-column {
				padding-left: " . esc_attr( $fildisi_eutf_gap_half_size ) . "px;
				padding-right: " . esc_attr( $fildisi_eutf_gap_half_size ) . "px;
			}

			@media only screen and (max-width: 767px) {
				.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-column .eut-column-wrapper {
					margin-bottom: 30px;
				}

				.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-column:last-child .eut-column-wrapper {
					margin-bottom: 0px;
				}
			}

			.eut-section.eut-fullwidth .eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " {
				padding-left: " . esc_attr( $fildisi_eutf_gap_half_size ) . "px;
				padding-right: " . esc_attr( $fildisi_eutf_gap_half_size ) . "px;
			}

			.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-row-inner {
				margin-left: -" . esc_attr( $fildisi_eutf_gap_half_size ) . "px;
				margin-right: -" . esc_attr( $fildisi_eutf_gap_half_size ) . "px;
			}

			@media only screen and (max-width: 767px) {
				.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-row-inner {
					margin-bottom: " . esc_attr( $fildisi_eutf_gap_size ) . "px;
				}

				.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-row-inner:last-child {
					margin-bottom: 0px;
				}
			}

			.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-column-inner {
				padding-left: " . esc_attr( $fildisi_eutf_gap_half_size ) . "px;
				padding-right: " . esc_attr( $fildisi_eutf_gap_half_size ) . "px;
			}

		";

	}

	return $css;
}

$css .= fildisi_eutf_print_gap_size( $fildisi_eutf_gap_size );

$fildisi_eutf_space_size = array (
	array(
		'id' => '1x',
		'percentage' => 1,
	),
	array(
		'id' => '2x',
		'percentage' => 2,
	),
	array(
		'id' => '3x',
		'percentage' => 3,
	),
	array(
		'id' => '4x',
		'percentage' => 4,
	),
	array(
		'id' => '5x',
		'percentage' => 5,
	),
	array(
		'id' => '6x',
		'percentage' => 6,
	),
);

function fildisi_eutf_print_space_size( $fildisi_eutf_space_size = array() , $ratio = 1) {
	$default_space_size = 30;
	$min_space_size = 18;
	$css = '';

	foreach ( $fildisi_eutf_space_size as $size ) {

		$fildisi_eutf_space_size = ( $default_space_size * $size['percentage'] ) * $ratio;
		if ( $fildisi_eutf_space_size < $default_space_size ) {
			$fildisi_eutf_space_size = $min_space_size;
		}
		$css .= "
			#eut-theme-wrapper .eut-padding-top-" . esc_attr( $size['id'] ) . "{ padding-top: " . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-padding-bottom-" . esc_attr( $size['id'] ) . "{ padding-bottom: " . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-margin-top-" . esc_attr( $size['id'] ) . "{ margin-top: " . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-margin-bottom-" . esc_attr( $size['id'] ) . "{ margin-bottom: " . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-height-" . esc_attr( $size['id'] ) . "{ height: " . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-top-" . esc_attr( $size['id'] ) . "{ top: " . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-left-" . esc_attr( $size['id'] ) . "{ left: " . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-right-" . esc_attr( $size['id'] ) . "{ right: " . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-bottom-" . esc_attr( $size['id'] ) . "{ bottom: " . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-top-minus-" . esc_attr( $size['id'] ) . "{ top: -" . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-left-minus-" . esc_attr( $size['id'] ) . "{ left: -" . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-right-minus-" . esc_attr( $size['id'] ) . "{ right: -" . esc_attr( $fildisi_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-bottom-minus-" . esc_attr( $size['id'] ) . "{ bottom: -" . esc_attr( $fildisi_eutf_space_size ) . "px; }

			#eut-theme-wrapper .eut-padding-none { padding: 0px !important; }
			#eut-theme-wrapper .eut-margin-none { margin: 0px !important; }
		";

	}

	$css .= "
	#eut-main-content .eut-main-content-wrapper,
	#eut-sidebar {
		padding-top: " . $default_space_size * 3  * $ratio . "px;
		padding-bottom: " . $default_space_size * 3  * $ratio . "px;
	}
	#eut-single-media.eut-portfolio-media.eut-without-sidebar {
		padding-top: " . $default_space_size * 3  * $ratio . "px;
	}
	#eut-single-media.eut-portfolio-media.eut-with-sidebar {
		padding-bottom: " . $default_space_size * 3  * $ratio . "px;
	}
	";

	return $css;
}

$css .= fildisi_eutf_print_space_size( $fildisi_eutf_space_size, 1 );

$css .= "
	@media only screen and (max-width: 1200px) {
		" . fildisi_eutf_print_space_size( $fildisi_eutf_space_size, 0.8 ). "
	}
	@media only screen and (max-width: 768px) {
		" . fildisi_eutf_print_space_size( $fildisi_eutf_space_size, 0.6 ). "
	}
";

if ( is_singular() ) {

	$fildisi_eutf_padding_top = fildisi_eutf_post_meta( '_fildisi_eutf_padding_top' );
	$fildisi_eutf_padding_bottom = fildisi_eutf_post_meta( '_fildisi_eutf_padding_bottom' );
	if( '' != $fildisi_eutf_padding_top || '' != $fildisi_eutf_padding_bottom ) {
		$css .= "#eut-main-content .eut-main-content-wrapper, #eut-sidebar {";
		if( '' != $fildisi_eutf_padding_top ) {
			$css .= 'padding-top: '. ( preg_match('/(px|em|\%|pt|cm)$/', $fildisi_eutf_padding_top) ? esc_attr( $fildisi_eutf_padding_top ) : esc_attr( $fildisi_eutf_padding_top ) . 'px').';';
		}
		if( '' != $fildisi_eutf_padding_bottom  ) {
			$css .= 'padding-bottom: '.( preg_match('/(px|em|\%|pt|cm)$/', $fildisi_eutf_padding_bottom) ? esc_attr( $fildisi_eutf_padding_bottom ) : esc_attr(  $fildisi_eutf_padding_bottom ) .'px').';';
		}
		$css .= "}";

		$css .= "#eut-single-media.eut-portfolio-media.eut-without-sidebar {";
		if( '' != $fildisi_eutf_padding_top ) {
			$css .= 'padding-top: '. ( preg_match('/(px|em|\%|pt|cm)$/', $fildisi_eutf_padding_top) ? esc_attr( $fildisi_eutf_padding_top ) : esc_attr( $fildisi_eutf_padding_top ) . 'px').';';
		}
		$css .= "}";

		$css .= "#eut-single-media.eut-portfolio-media.eut-with-sidebar {";
		if( '' != $fildisi_eutf_padding_top ) {
			$css .= 'padding-bottom: '. ( preg_match('/(px|em|\%|pt|cm)$/', $fildisi_eutf_padding_top) ? esc_attr( $fildisi_eutf_padding_top ) : esc_attr( $fildisi_eutf_padding_top ) . 'px').';';
		}
		$css .= "}";
	}

}

if ( is_singular( 'portfolio' ) ) {
	$fildisi_eutf_media_margin_bottom = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_media_margin_bottom' );
	if( '' != $fildisi_eutf_media_margin_bottom ) {
		$css .= "#eut-single-media.eut-portfolio-media {";
		$css .= 'margin-bottom: '. ( preg_match('/(px|em|\%|pt|cm)$/', $fildisi_eutf_media_margin_bottom) ? esc_attr( $fildisi_eutf_media_margin_bottom ) : esc_attr( $fildisi_eutf_media_margin_bottom ) . 'px').';';
		$css .= "}";
	}
}

wp_add_inline_style( 'fildisi-eutf-custom-style', fildisi_eutf_compress_css( $css ) );

//Omit closing PHP tag to avoid accidental whitespace output errors.
