<?php get_header(); ?>


<?php fildisi_eutf_print_header_title( 'blog' ); ?>
<?php fildisi_eutf_print_header_breadcrumbs( 'post' ); ?>

<!-- CONTENT -->
<div id="eut-content" class="clearfix <?php echo fildisi_eutf_sidebar_class( 'blog' ); ?>">
	<div class="eut-content-wrapper">
		<!-- MAIN CONTENT -->
		<div id="eut-main-content">
			<div class="eut-main-content-wrapper clearfix">

				<div class="eut-section" style="margin-bottom: 0px;">

					<div class="eut-container">

						<!-- ROW -->
						<div class="eut-row">

							<!-- COLUMN 1 -->
							<div class="wpb_column eut-column-1">
								<div class="eut-column-wrapper">
									<!-- Blog FitRows -->
									<?php
										$fildisi_eutf_blog_mode = fildisi_eutf_option( 'blog_mode', 'large' );
										$fildisi_eutf_blog_class = fildisi_eutf_get_blog_class();
									?>
									<div class="<?php echo esc_attr( $fildisi_eutf_blog_class ); ?>" <?php fildisi_eutf_print_blog_data(); ?>>

										<?php
										if ( have_posts() ) :
											if ( 'large' == $fildisi_eutf_blog_mode || 'small' == $fildisi_eutf_blog_mode ) {
										?>
											<div class="eut-standard-container">
										<?php
											} else {
										?>
											<div class="eut-isotope-container">
										<?php
											}

										// Start the Loop.
										while ( have_posts() ) : the_post();
											//Get post template
											get_template_part( 'content', get_post_format() );
										endwhile;

										?>
										</div>
										<?php
											// Previous/next post navigation.
											fildisi_eutf_paginate_links();
										else :
											// If no content, include the "No posts found" template.
											get_template_part( 'content', 'none' );
										endif;
										?>

									</div>
									<!-- End Element Blog -->
								</div>
							</div>
							<!-- END COLUMN 1 -->

						</div>
						<!-- END ROW -->

					</div>

				</div>

			</div>
		</div>
		<!-- End Content -->
		<?php
			fildisi_eutf_set_current_view( 'blog' );
			if ( is_front_page() ) {
				//fildisi_eutf_set_current_view( 'frontpage' );
			}
		?>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer();

//Omit closing PHP tag to avoid accidental whitespace output errors.
