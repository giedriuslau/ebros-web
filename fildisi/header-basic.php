<!doctype html>
<!--[if lt IE 10]>
<html class="ie9 no-js" <?php language_attributes(); ?>>
<![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

	<head>
		<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) { ?>
		<!-- allow pinned sites -->
		<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
		<?php } ?>
		<?php wp_head(); ?>
	</head>
<?php
		// Theme Layout
		$fildisi_eutf_theme_layout = fildisi_eutf_option( 'theme_layout', 'stretched' );
		$fildisi_eutf_frame_size = fildisi_eutf_option( 'frame_size', 30 );
?>
	<body <?php body_class(); ?>>
		<?php do_action( 'fildisi_eutf_body_top' ); ?>
		<?php if ( 'framed' == $fildisi_eutf_theme_layout ) { ?>
		<div id="eut-frames" data-frame-size="<?php echo esc_attr( $fildisi_eutf_frame_size ); ?>">
			<div class="eut-frame eut-top"></div>
			<div class="eut-frame eut-left"></div>
			<div class="eut-frame eut-right"></div>
			<div class="eut-frame eut-bottom"></div>
		</div>
		<?php } ?>
		<?php fildisi_eutf_print_theme_loader(); ?>

		<!-- Theme Wrapper -->
		<div id="eut-theme-wrapper">
			<div id="eut-theme-content">
