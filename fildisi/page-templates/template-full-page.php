<?php
/*
Template Name: Scrolling Full Screen Sections
*/
?>
<?php get_header(); ?>

<?php the_post(); ?>

<?php

	$scrolling_page = fildisi_eutf_post_meta( '_fildisi_eutf_scrolling_page' );
	$responsive_scrolling_page = fildisi_eutf_post_meta( '_fildisi_eutf_responsive_scrolling', 'yes' );
	$scrolling_lock_anchors = fildisi_eutf_post_meta( '_fildisi_eutf_scrolling_lock_anchors', 'yes' );
	$scrolling_loop = fildisi_eutf_post_meta( '_fildisi_eutf_scrolling_loop', 'none' );
	$scrolling_speed = fildisi_eutf_post_meta( '_fildisi_eutf_scrolling_speed', 1000 );

	$wrapper_attributes = array();
	if( 'pilling' == $scrolling_page ) {
		$scrolling_page_id = 'eut-pilling-page';
		$scrolling_direction = fildisi_eutf_post_meta( '_fildisi_eutf_scrolling_direction', 'vertical' );
		$wrapper_attributes[] = 'data-scroll-direction="' . esc_attr( $scrolling_direction ) . '"';
	} else {
		$scrolling_page_id = 'eut-fullpage';
	}
	$wrapper_attributes[] = 'id="' . esc_attr( $scrolling_page_id ) . '"';
	$wrapper_attributes[] = 'data-device-scrolling="' . esc_attr( $responsive_scrolling_page ) . '"';
	$wrapper_attributes[] = 'data-lock-anchors="' . esc_attr( $scrolling_lock_anchors ) . '"';
	$wrapper_attributes[] = 'data-scroll-loop="' . esc_attr( $scrolling_loop ) . '"';
	$wrapper_attributes[] = 'data-scroll-speed="' . esc_attr( $scrolling_speed ) . '"';

?>

			<!-- CONTENT -->
			<div id="eut-content" class="clearfix">
				<div class="eut-content-wrapper">
					<!-- MAIN CONTENT -->
					<div id="eut-main-content">
						<div class="eut-main-content-wrapper clearfix" style="padding: 0;">

							<!-- PAGE CONTENT -->
							<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
									<div <?php echo implode( ' ', $wrapper_attributes ); ?>>
									<?php the_content(); ?>
								</div>
							</div>
							<!-- END PAGE CONTENT -->

						</div>
					</div>
					<!-- END MAIN CONTENT -->

				</div>
			</div>
			<!-- END CONTENT -->

			<!-- SIDE AREA -->
			<?php
				$fildisi_eutf_sidearea_data = fildisi_eutf_get_sidearea_data();
				fildisi_eutf_print_side_area( $fildisi_eutf_sidearea_data );
			?>
			<!-- END SIDE AREA -->

			<!-- HIDDEN MENU -->
			<?php fildisi_eutf_print_hidden_menu(); ?>
			<!-- END HIDDEN MENU -->

			<?php fildisi_eutf_print_search_modal(); ?>
			<?php fildisi_eutf_print_form_modals(); ?>
			<?php fildisi_eutf_print_language_modal(); ?>
			<?php fildisi_eutf_print_login_modal(); ?>
			<?php fildisi_eutf_print_social_modal(); ?>

			<?php do_action( 'fildisi_eutf_footer_modal_container' ); ?>

			</div> <!-- end #eut-theme-content -->
		</div> <!-- end #eut-theme-wrapper -->

		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>