<?php
/*
*	Template Post Related
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/


$fildisi_eutf_link = get_permalink();
$fildisi_eutf_target = '_self';

if ( 'link' == get_post_format() ) {
	$fildisi_eutf_link = get_post_meta( get_the_ID(), '_fildisi_eutf_post_link_url', true );
	$new_window = get_post_meta( get_the_ID(), '_fildisi_eutf_post_link_new_window', true );
	if( empty( $fildisi_eutf_link ) ) {
		$fildisi_eutf_link = get_permalink();
	}

	if( !empty( $new_window ) ) {
		$fildisi_eutf_target = '_blank';
	}
}
?>

	<div class="eut-column wpb_column eut-column-1-3">
		<div class="eut-column-wrapper">
				<?php if ( has_post_thumbnail() ) { ?>
				<article id="eut-post-related-<?php the_ID(); ?><?php echo uniqid('-'); ?>" class="eut-element eut-hover-item eut-hover-style-2">
					<figure class="eut-image-hover eut-media eut-zoom-none">
						<a class="eut-item-url" href="<?php echo esc_url( $fildisi_eutf_link ); ?>" target="<?php echo esc_attr( $fildisi_eutf_target ); ?>"></a>
						<div class="eut-bg-dark eut-hover-overlay eut-opacity-40"></div>
						<?php $image_size = 'fildisi-eutf-small-rect-horizontal'; ?>
						<?php the_post_thumbnail( $image_size ); ?>
						<figcaption class="eut-content eut-align-center">
							<h3 class="eut-title eut-h6 eut-text-light"><?php the_title(); ?></h3>
						</figcaption>
					</figure>
				</article>
				<?php } else {?>
				<article id="eut-post-related-<?php the_ID(); ?><?php echo uniqid('-'); ?>" class="eut-element">
					<figure class="eut-without-thumb">
						<a class="eut-item-url" href="<?php echo esc_url( $fildisi_eutf_link ); ?>" target="<?php echo esc_attr( $fildisi_eutf_target ); ?>"></a>
						<figcaption class="eut-content eut-align-center">
							<h3 class="eut-title eut-h6"><?php the_title(); ?></h3>
						</figcaption>
					</figure>
				</article>
				<?php } ?>
		</div>
	</div>