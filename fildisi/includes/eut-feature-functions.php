<?php

/*
*	Feature Helper functions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/


/**
 * Get Validate Header Style
 */

function fildisi_eutf_validate_header_style( $fildisi_eutf_header_style ) {

	$header_styles = array( 'default', 'dark', 'light' );
	if ( !in_array( $fildisi_eutf_header_style, $header_styles ) ) {
		$fildisi_eutf_header_style = 'default';
	}
	return $fildisi_eutf_header_style;

}

/**
 * Get Header Feature Header Section Data
 */

function fildisi_eutf_get_feature_header_data() {
	global $post;

	$fildisi_eutf_header_position = 'above';
	if( fildisi_eutf_is_woo_tax() ) {
		$fildisi_eutf_header_style = fildisi_eutf_option( 'product_tax_header_style', 'default' );
		$fildisi_eutf_header_overlapping = fildisi_eutf_option( 'product_tax_header_overlapping', 'no' );

	} elseif ( fildisi_eutf_events_calendar_is_overview() || is_post_type_archive( 'tribe_events' ) ) {
		$fildisi_eutf_header_style = fildisi_eutf_option( 'event_tax_header_style', 'default' );
		$fildisi_eutf_header_overlapping = fildisi_eutf_option( 'event_tax_header_overlapping', 'no' );
	} else {
		$fildisi_eutf_header_style = fildisi_eutf_option( 'blog_header_style', 'default' );
		$fildisi_eutf_header_overlapping = fildisi_eutf_option( 'blog_header_overlapping', 'no' );
	}

	$feature_size = '';

	$fildisi_eutf_woo_shop = fildisi_eutf_is_woo_shop();

	if ( is_search() ) {
		$fildisi_eutf_header_style =  fildisi_eutf_option( 'search_page_header_style' );
		$fildisi_eutf_header_overlapping =  fildisi_eutf_option( 'search_page_header_overlapping' );
	}

	if ( is_singular() || $fildisi_eutf_woo_shop ) {

		if ( $fildisi_eutf_woo_shop ) {
			$post_id = wc_get_page_id( 'shop' );
		} else {
			$post_id = $post->ID;
		}
		$post_type = get_post_type( $post_id );

		switch( $post_type ) {
			case 'product':
				$fildisi_eutf_header_style =  fildisi_eutf_post_meta( '_fildisi_eutf_header_style', fildisi_eutf_option( 'product_header_style' ) );
				$fildisi_eutf_header_overlapping =  fildisi_eutf_post_meta( '_fildisi_eutf_header_overlapping', fildisi_eutf_option( 'product_header_overlapping' ) );
			break;
			case 'portfolio':
				$fildisi_eutf_header_style =  fildisi_eutf_post_meta( '_fildisi_eutf_header_style', fildisi_eutf_option( 'portfolio_header_style' ) );
				$fildisi_eutf_header_overlapping =  fildisi_eutf_post_meta( '_fildisi_eutf_header_overlapping', fildisi_eutf_option( 'portfolio_header_overlapping' ) );
			break;
			case 'post':
				$fildisi_eutf_header_style =  fildisi_eutf_post_meta( '_fildisi_eutf_header_style', fildisi_eutf_option( 'post_header_style' ) );
				$fildisi_eutf_header_overlapping =  fildisi_eutf_post_meta( '_fildisi_eutf_header_overlapping', fildisi_eutf_option( 'post_header_overlapping' ) );
			break;
			case 'tribe_events':
				$fildisi_eutf_header_style =  fildisi_eutf_post_meta( '_fildisi_eutf_header_style', fildisi_eutf_option( 'event_header_style' ) );
				$fildisi_eutf_header_overlapping =  fildisi_eutf_post_meta( '_fildisi_eutf_header_overlapping', fildisi_eutf_option( 'event_header_overlapping' ) );
			break;
			case 'tribe_organizer':
			case 'tribe_venue':
				$fildisi_eutf_header_style = 'default';
				$fildisi_eutf_header_overlapping = 'no';
			break;
			case 'page':
			default:
				if ( $fildisi_eutf_woo_shop ) {
					$fildisi_eutf_header_style =  fildisi_eutf_post_meta_shop( '_fildisi_eutf_header_style', fildisi_eutf_option( 'page_header_style' ) );
					$fildisi_eutf_header_overlapping =  fildisi_eutf_post_meta_shop( '_fildisi_eutf_header_overlapping', fildisi_eutf_option( 'page_header_overlapping' ) );
				} else {
					$fildisi_eutf_header_style =  fildisi_eutf_post_meta( '_fildisi_eutf_header_style', fildisi_eutf_option( 'page_header_style' ) );
					$fildisi_eutf_header_overlapping =  fildisi_eutf_post_meta( '_fildisi_eutf_header_overlapping', fildisi_eutf_option( 'page_header_overlapping' ) );
				}
			break;
		}

		//Force Overlapping for Scrolling Full Width Sections Template
		if ( is_page_template( 'page-templates/template-full-page.php' ) ) {
			$fildisi_eutf_header_overlapping = 'yes';
		} else {

			$feature_section_post_types = fildisi_eutf_option( 'feature_section_post_types');

			if ( !empty( $feature_section_post_types ) && in_array( $post_type, $feature_section_post_types ) ) {

				$feature_section = get_post_meta( $post_id, '_fildisi_eutf_feature_section', true );
				$feature_settings = fildisi_eutf_array_value( $feature_section, 'feature_settings' );
				$feature_element = fildisi_eutf_array_value( $feature_settings, 'element' );

				if ( !empty( $feature_element ) ) {

					$feature_single_item = fildisi_eutf_array_value( $feature_section, 'single_item' );
					$fildisi_eutf_header_position = fildisi_eutf_array_value( $feature_settings, 'header_position' );
					if ( 'slider' ==  $feature_element ) {

						$slider_items = fildisi_eutf_array_value( $feature_section, 'slider_items' );
						if ( !empty( $slider_items ) ) {
							$fildisi_eutf_header_style = isset( $slider_items[0]['header_style'] ) ? $slider_items[0]['header_style'] : 'default';
						}

					}
				}
			}
		}
	}
	if( fildisi_eutf_is_bbpress() ) {
		$fildisi_eutf_header_style =  fildisi_eutf_option( 'forum_header_style' );
		$fildisi_eutf_header_overlapping =  fildisi_eutf_option( 'forum_header_overlapping' );
	}

	if( is_404() ) {
		$fildisi_eutf_header_style =  fildisi_eutf_option( 'page_404_header_style' );
		$fildisi_eutf_header_overlapping =  fildisi_eutf_option( 'page_404_header_overlapping' );
	}

	return array(
		'data_overlap' => $fildisi_eutf_header_overlapping,
		'data_header_position' => $fildisi_eutf_header_position,
		'header_style' => fildisi_eutf_validate_header_style( $fildisi_eutf_header_style ),
	);

}

/**
 * Prints Header Feature Section Page/Post/Portfolio
 */
function fildisi_eutf_print_header_feature() {

	//Skip for  Scrolling Full Width Sections Template
	if ( is_page_template( 'page-templates/template-full-page.php' ) ) {
		return false;
	}

	global $post;

	$fildisi_eutf_woo_shop = fildisi_eutf_is_woo_shop();

	if ( is_singular() || $fildisi_eutf_woo_shop ) {

		if ( $fildisi_eutf_woo_shop ) {
			$post_id = wc_get_page_id( 'shop' );
		} else {
			$post_id = $post->ID;
		}
		$post_type = get_post_type( $post_id );
		$feature_section_post_types = fildisi_eutf_option( 'feature_section_post_types');
		if ( !empty( $feature_section_post_types ) && in_array( $post_type, $feature_section_post_types ) ) {

			$feature_section = get_post_meta( $post_id, '_fildisi_eutf_feature_section', true );
			$feature_settings = fildisi_eutf_array_value( $feature_section, 'feature_settings' );
			$feature_element = fildisi_eutf_array_value( $feature_settings, 'element' );

			if ( !empty( $feature_element ) ) {

				$feature_single_item = fildisi_eutf_array_value( $feature_section, 'single_item' );

				switch( $feature_element ) {
					case 'title':
						if ( !empty( $feature_single_item ) ) {
							fildisi_eutf_print_header_feature_single( $feature_settings, $feature_single_item, 'title' );
						}
						break;
					case 'image':
						if ( !empty( $feature_single_item ) ) {
							fildisi_eutf_print_header_feature_single( $feature_settings, $feature_single_item, 'image' );
						}
						break;
					case 'video':
						if ( !empty( $feature_single_item ) ) {
							fildisi_eutf_print_header_feature_single( $feature_settings, $feature_single_item, 'video' );
						}
						break;
					case 'youtube':
						if ( !empty( $feature_single_item ) ) {
							fildisi_eutf_print_header_feature_single( $feature_settings, $feature_single_item, 'youtube' );
						}
						break;
					case 'slider':
						$slider_items = fildisi_eutf_array_value( $feature_section, 'slider_items' );
						$slider_settings = fildisi_eutf_array_value( $feature_section, 'slider_settings' );
						if ( !empty( $slider_items ) ) {
							fildisi_eutf_print_header_feature_slider( $feature_settings, $slider_items, $slider_settings );
						}
						break;
					case 'map':
						$map_items = fildisi_eutf_array_value( $feature_section, 'map_items' );
						$map_settings = fildisi_eutf_array_value( $feature_section, 'map_settings' );
						if ( !empty( $map_items ) ) {
							fildisi_eutf_print_header_feature_map( $feature_settings, $map_items, $map_settings );
						}
						break;
					case 'revslider':
						$revslider_alias = fildisi_eutf_array_value( $feature_section, 'revslider_alias' );
						if ( !empty( $revslider_alias ) ) {
							fildisi_eutf_print_header_feature_revslider( $feature_settings, $revslider_alias, $feature_single_item );
						}
						break;
					default:
						break;

				}
			}
		}
	}
}


/**
 * Prints Overlay Container
 */
function fildisi_eutf_print_overlay_container( $item ) {


	$pattern_overlay = fildisi_eutf_array_value( $item, 'pattern_overlay' );
	$color_overlay = fildisi_eutf_array_value( $item, 'color_overlay', 'dark' );
	$opacity_overlay = fildisi_eutf_array_value( $item, 'opacity_overlay', '0' );

	if ( 'gradient' == $color_overlay ) {
		$gradient_overlay_custom_1 = fildisi_eutf_array_value( $item, 'gradient_overlay_custom_1', '#034e90' );
		$gradient_overlay_custom_1_opacity = fildisi_eutf_array_value( $item, 'gradient_overlay_custom_1_opacity', '0.90' );
		$gradient_overlay_custom_1_rgba = fildisi_eutf_get_hex2rgba( $gradient_overlay_custom_1 , $gradient_overlay_custom_1_opacity );
		$gradient_overlay_custom_2 = fildisi_eutf_array_value( $item, 'gradient_overlay_custom_2', '#19b4d7' );
		$gradient_overlay_custom_2_opacity = fildisi_eutf_array_value( $item, 'gradient_overlay_custom_2_opacity', '0.90' );
		$gradient_overlay_custom_2_rgba = fildisi_eutf_get_hex2rgba( $gradient_overlay_custom_2 , $gradient_overlay_custom_2_opacity );
		$gradient_overlay_direction  = fildisi_eutf_array_value( $item, 'gradient_overlay_direction', '90' );
	} else {
		$color_overlay_custom = fildisi_eutf_array_value( $item, 'color_overlay_custom', '#000000' );
		$color_overlay_custom = fildisi_eutf_get_color( $color_overlay, $color_overlay_custom );
		$overlay_rgba = fildisi_eutf_get_hex2rgba( $color_overlay_custom , $opacity_overlay );
	}

	if ( 'default' == $pattern_overlay ) {
		echo '<div class="eut-pattern"></div>';
	}

	$overlay_classes = array('eut-bg-overlay');
	$overlay_string = implode( ' ', $overlay_classes );
	if ( 'gradient' == $color_overlay ) {
		echo '<div class="' . esc_attr( $overlay_string ) . '" style="background:' . esc_attr( $gradient_overlay_custom_1_rgba ) . '; background: linear-gradient(' . esc_attr( $gradient_overlay_direction ) . 'deg,' . esc_attr( $gradient_overlay_custom_1_rgba ) . ' 0%,' . esc_attr( $gradient_overlay_custom_2_rgba ) . ' 100%);"></div>';
	} else {
		if ( '0' != $opacity_overlay && !empty( $opacity_overlay ) ) {
			echo '<div class="' . esc_attr( $overlay_string ) . '" style="background-color:' . esc_attr( $overlay_rgba ) . ';"></div>';
		}
	}
}

/**
 * Prints Background Image Container
 */
function fildisi_eutf_print_bg_image_container( $item ) {

	$bg_position = fildisi_eutf_array_value( $item, 'bg_position', 'center-center' );
	$bg_tablet_sm_position = fildisi_eutf_array_value( $item, 'bg_tablet_sm_position' );

	$bg_image_id = fildisi_eutf_array_value( $item, 'bg_image_id' );
	$bg_image_size = fildisi_eutf_array_value( $item, 'bg_image_size' );

	$full_src = wp_get_attachment_image_src( $bg_image_id, 'fildisi-eutf-fullscreen' );
	$image_url = $full_src[0];
	if( !empty( $image_url ) ) {

		//Adaptive Background URL

		if ( empty ( $bg_image_size ) ) {
			$bg_image_size = fildisi_eutf_option( 'feature_section_bg_size' );
		}

		$image_url = fildisi_eutf_get_adaptive_url( $bg_image_id, $bg_image_size );

		$bg_image_classes = array( 'eut-bg-image' );
		$bg_image_classes[] = 'eut-bg-' . $bg_position;
		if ( !empty( $bg_tablet_sm_position ) ) {
			$bg_image_classes[] = 'eut-bg-tablet-sm-' . $bg_tablet_sm_position;
		}
		$bg_image_classes[] = 'eut-bg-image-id-' . $bg_image_id;

		$bg_image_classes_string = implode( ' ', $bg_image_classes );

		echo '<div class="' . esc_attr( $bg_image_classes_string ) . '" style="background-image: url(' . esc_url( $image_url ) . ');"></div>';
	}

}


/**
 * Prints Background Video Container
 */
function fildisi_eutf_print_bg_video_container( $item ) {

	$bg_video_webm = fildisi_eutf_array_value( $item, 'video_webm' );
	$bg_video_mp4 = fildisi_eutf_array_value( $item, 'video_mp4' );
	$bg_video_ogv = fildisi_eutf_array_value( $item, 'video_ogv' );
	$bg_video_poster = fildisi_eutf_array_value( $item, 'video_poster', 'no' );
	$bg_video_device = fildisi_eutf_array_value( $item, 'video_device', 'no' );
	$bg_image_id = fildisi_eutf_array_value( $item, 'bg_image_id' );

	$loop = fildisi_eutf_array_value( $item, 'video_loop', 'yes' );
	$muted = fildisi_eutf_array_value( $item, 'video_muted', 'yes' );

	$full_src = wp_get_attachment_image_src( $bg_image_id, 'fildisi-eutf-fullscreen' );
	$image_url = esc_url( $full_src[0] );

	$video_poster = $playsinline = '';

	if ( !empty( $image_url ) && 'yes' == $bg_video_poster ) {
		$video_poster = $image_url;
	}
	if ( wp_is_mobile() ) {
		if ( 'yes' == $bg_video_device ) {
			if( !empty( $image_url ) ) {
				$video_poster = $image_url;
			}
			$muted = 'yes';
			$playsinline = 'yes';
		} else {
			return;
		}
	}

	if ( fildisi_eutf_browser_webkit_check() ) {
		$muted = 'yes';
	}

	$video_settings = array(
		'preload' => 'auto',
		'autoplay' => 'yes',
		'loop' => $loop,
		'muted' => $muted,
		'poster' => $video_poster,
		'playsinline' => $playsinline,
	);

	$video_settings = apply_filters( 'fildisi_eutf_feature_video_settings', $video_settings );


	if ( !empty ( $bg_video_webm ) || !empty ( $bg_video_mp4 ) || !empty ( $bg_video_ogv ) ) {
?>
		<div class="eut-bg-video eut-html5-bg-video" data-video-device="<?php echo esc_attr( $bg_video_device ); ?>">
			<video <?php echo fildisi_eutf_print_media_video_settings( $video_settings );?>>
			<?php if ( !empty ( $bg_video_webm ) ) { ?>
				<source src="<?php echo esc_url( $bg_video_webm ); ?>" type="video/webm">
			<?php } ?>
			<?php if ( !empty ( $bg_video_mp4 ) ) { ?>
				<source src="<?php echo esc_url( $bg_video_mp4 ); ?>" type="video/mp4">
			<?php } ?>
			<?php if ( !empty ( $bg_video_ogv ) ) { ?>
				<source src="<?php echo esc_url( $bg_video_ogv ); ?>" type="video/ogg">
			<?php } ?>
			</video>
		</div>
<?php
	}

}

/**
 * Prints Background YouTube Container
 */
function fildisi_eutf_print_bg_youtube_container( $item ) {
	$video_url = fildisi_eutf_array_value( $item, 'video_url' );
	$video_start = fildisi_eutf_array_value( $item, 'video_start' );
	$video_end = fildisi_eutf_array_value( $item, 'video_end' );
	$has_video_bg = ( ! empty( $video_url ) && fildisi_eutf_extract_youtube_id( $video_url ) );
	if ( $has_video_bg ) {
		wp_enqueue_script( 'youtube-iframe-api' );
		$wrapper_attributes = array();
		$wrapper_attributes[] = 'data-video-bg-url="' . esc_attr( $video_url ) . '"';
		if ( !empty( $video_start ) ) {
			$wrapper_attributes[] = 'data-video-start="' . esc_attr( $video_start ) . '"';
		}
		if ( !empty( $video_end ) ) {
			$wrapper_attributes[] = 'data-video-end="' . esc_attr( $video_end ) . '"';
		}
		$wrapper_attributes[] = 'class="eut-bg-video eut-yt-bg-video"';
?>		<div <?php echo implode( ' ', $wrapper_attributes ); ?>></div>
<?php
	}
}

/**
 * Prints Bottom Separator Container
 */
function fildisi_eutf_print_bottom_seperator_container( $feature_settings ) {
	$separator_bottom = fildisi_eutf_array_value( $feature_settings, 'separator_bottom' );
	$separator_bottom_color = fildisi_eutf_array_value( $feature_settings, 'separator_bottom_color' );
	$separator_bottom_size = fildisi_eutf_array_value( $feature_settings, 'separator_bottom_size' );

	if( !empty ( $separator_bottom ) ) {
		echo '<div class="eut-separator-bottom">';
		echo fildisi_eutf_build_separator( $separator_bottom, $separator_bottom_color, $separator_bottom_size );
		echo '</div>';
	}
}

/**
 * Get Feature Section data
 */
function fildisi_eutf_get_feature_data( $feature_settings, $item_type, $item_effect = '', $el_class = '' ) {


	$wrapper_attributes = array();
	$style = "";

	//Background Color
	$bg_color = fildisi_eutf_array_value( $feature_settings, 'bg_color', 'dark' );
	if ( 'gradient' == $bg_color ) {
		$bg_gradient_color_1 = fildisi_eutf_array_value( $feature_settings, 'bg_gradient_color_1', '#034e90' );
		$bg_gradient_color_1_rgba = fildisi_eutf_get_hex2rgba( $bg_gradient_color_1 , 1 );
		$bg_gradient_color_2 = fildisi_eutf_array_value( $feature_settings, 'bg_gradient_color_2', '#19b4d7' );
		$bg_gradient_color_2_rgba = fildisi_eutf_get_hex2rgba( $bg_gradient_color_2 , 1 );
		$bg_gradient_direction = fildisi_eutf_array_value( $feature_settings, 'bg_gradient_direction', '90' );
	} else {
		$bg_color_custom = fildisi_eutf_array_value( $feature_settings, 'bg_color_custom', '#000000' );
		$bg_color_custom = fildisi_eutf_get_color( $bg_color, $bg_color_custom );
	}

	//Data and Style
	if( 'revslider' != $item_type ) {
		$feature_size = fildisi_eutf_array_value( $feature_settings, 'size' );
		$feature_height = fildisi_eutf_array_value( $feature_settings, 'height', '60' );
		$feature_min_height = fildisi_eutf_array_value( $feature_settings, 'min_height', '200' );
		if ( 'gradient' == $bg_color ) {
			$style .= fildisi_eutf_get_css_color( 'background', $bg_gradient_color_1_rgba );
			$style .= 'background: linear-gradient(' . $bg_gradient_direction . 'deg,' . $bg_gradient_color_1_rgba . ' 0%,' . $bg_gradient_color_2_rgba .' 100%);';
		} else {
			$style .= 'background-color: ' . esc_attr( $bg_color_custom ) . ';';
		}
		if ( !empty($feature_size) ) {
			if ( empty( $feature_height ) ) {
				$feature_height = "60";
			}
			$style .= 'min-height:' . esc_attr( $feature_min_height ) . 'px;';
			$wrapper_attributes[] = 'data-height="' . esc_attr( $feature_height ) . '"';
		}
		$wrapper_attributes[] = 'style="'. esc_attr( $style ) . '"';
	}

	//Classes
	$feature_item_classes = array( 'eut-with-' . $item_type  );

	if( 'revslider' != $item_type ) {
		if ( empty( $feature_size ) ) {
			$feature_item_classes[] = 'eut-fullscreen';
		} else {
			if ( is_numeric( $feature_height ) ) { //Custom Size
				$feature_item_classes[] = 'eut-custom-size';
			} else {
				$feature_item_classes[] = 'eut-' . $feature_height . '-height';
			}
		}

		if ( !empty( $item_effect ) ) {
			$feature_item_classes[] = 'eut-bg-' . $item_effect;
		}
	}

	$separator_bottom = fildisi_eutf_array_value( $feature_settings, 'separator_bottom' );
	$separator_bottom_size = fildisi_eutf_array_value( $feature_settings, 'separator_bottom_size' );

	if ( !empty( $separator_bottom ) && '100%' ==  $separator_bottom_size ) {
		$feature_item_classes[] = 'eut-separator-fullheight';
	}

	if ( !empty ( $el_class ) ) {
		$feature_item_classes[] = $el_class;
	}
	$feature_item_class_string = implode( ' ', $feature_item_classes );

	//Add Classes
	$wrapper_attributes[] = 'class="' . esc_attr( $feature_item_class_string ) . '"';

	return $wrapper_attributes;
}

/**
 * Get Feature Height
 */
function fildisi_eutf_get_feature_height( $feature_settings ) {

	//Data and Style
	$feature_style_height = '';

	$feature_size = fildisi_eutf_array_value( $feature_settings, 'size' );
	$feature_height = fildisi_eutf_array_value( $feature_settings, 'height', '60' );
	$feature_min_height = fildisi_eutf_array_value( $feature_settings, 'min_height', '200' );
	if ( !empty($feature_size) ) {
		if ( is_numeric( $feature_height ) ) { //Custom Size
			$feature_style_height = 'style="height:' . esc_attr( $feature_height ) . 'vh; min-height:' . esc_attr( $feature_min_height ) . 'px;"';
		} else {
			$feature_style_height = 'style="min-height:' . esc_attr( $feature_min_height ) . 'px;"';
		}
	}
	return $feature_style_height;
}


/**
 * Prints Header Section Feature Single Item
 */
function fildisi_eutf_print_header_feature_single( $feature_settings, $item, $item_type  ) {

	if( 'image' == $item_type ) {
		$item_effect = fildisi_eutf_array_value( $item, 'image_effect' );
	} elseif( 'video' == $item_type ) {
		$item_effect = fildisi_eutf_array_value( $item, 'video_effect' );
	} else {
		$item_effect = '';
	}

	$el_class = fildisi_eutf_array_value( $item, 'el_class' );

	$feature_data = fildisi_eutf_get_feature_data( $feature_settings, $item_type, $item_effect, $el_class );

?>
	<div id="eut-feature-section" <?php echo implode( ' ', $feature_data ); ?>>
		<div class="eut-wrapper clearfix" <?php echo fildisi_eutf_get_feature_height( $feature_settings ); ?>>
			<?php fildisi_eutf_print_header_feature_content( $feature_settings, $item, $item_type ); ?>
		</div>
		<div class="eut-background-wrapper">
		<?php
			if( 'image' == $item_type || 'video' == $item_type || 'youtube' == $item_type ) {
				fildisi_eutf_print_bg_image_container( $item );
			}
			if( 'video' == $item_type ) {
				fildisi_eutf_print_bg_video_container( $item );
			}
			if( 'youtube' == $item_type ) {
				fildisi_eutf_print_bg_youtube_container( $item );
			}
			fildisi_eutf_print_overlay_container( $item  );
		?>
		</div>
		<?php fildisi_eutf_print_bottom_seperator_container( $feature_settings ); ?>
	</div>
<?php
}

/**
 * Prints Feature Slider
 */
function fildisi_eutf_print_header_feature_slider( $feature_settings, $slider_items, $slider_settings ) {

	$slider_speed = fildisi_eutf_array_value( $slider_settings, 'slideshow_speed', '3500' );
	$slider_pause = fildisi_eutf_array_value( $slider_settings, 'slider_pause', 'no' );
	$slider_transition = fildisi_eutf_array_value( $slider_settings, 'transition', 'slide' );
	$slider_dir_nav = fildisi_eutf_array_value( $slider_settings, 'direction_nav', '1' );
	$slider_effect = fildisi_eutf_array_value( $slider_settings, 'slider_effect', '' );
	$slider_pagination = fildisi_eutf_array_value( $slider_settings, 'pagination', 'yes' );

	$feature_data = fildisi_eutf_get_feature_data( $feature_settings, 'slider', $slider_effect  );

	$fildisi_eutf_header_style = isset( $slider_items[0]['header_style'] ) ? $slider_items[0]['header_style'] : 'default';

?>
	<div id="eut-feature-section" <?php echo implode( ' ', $feature_data ); ?>>

		<?php echo fildisi_eutf_element_navigation( $slider_dir_nav, $fildisi_eutf_header_style ); ?>

		<div id="eut-feature-slider" data-slider-speed="<?php echo esc_attr( $slider_speed ); ?>" data-pagination="<?php echo esc_attr( $slider_pagination ); ?>" data-slider-pause="<?php echo esc_attr( $slider_pause ); ?>" data-slider-transition="<?php echo esc_attr( $slider_transition ); ?>">

<?php

			foreach ( $slider_items as $item ) {

				$header_style = fildisi_eutf_array_value( $item, 'header_style', 'default' );
				$fildisi_eutf_header_style = fildisi_eutf_validate_header_style( $header_style );

				$slide_type = fildisi_eutf_array_value( $item, 'type' );
				$slide_post_id = fildisi_eutf_array_value( $item, 'post_id' );
				if( 'post' == $slide_type &&  !empty( $slide_post_id ) ) {
					if( has_post_thumbnail( $slide_post_id ) && empty( $item['bg_image_id'] ) ) {
						$post_type = get_post_type( $slide_post_id );
						$slide_post_id = apply_filters( 'wpml_object_id', $slide_post_id, $post_type, TRUE  );
						$item['bg_image_id'] = get_post_thumbnail_id( $slide_post_id );
					}
				}

				$el_class = fildisi_eutf_array_value( $item, 'el_class' );
				$el_id = fildisi_eutf_array_value( $item, 'id', uniqid() );

?>
				<div class="eut-slider-item eut-slider-item-id-<?php echo esc_attr( $el_id ); ?> <?php echo esc_attr( $el_class ); ?>" data-header-color="<?php echo esc_attr( $fildisi_eutf_header_style ); ?>">
					<div class="eut-wrapper clearfix" <?php echo fildisi_eutf_get_feature_height( $feature_settings ); ?>>
						<?php fildisi_eutf_print_header_feature_content( $feature_settings, $item ); ?>
					</div>
					<div class="eut-background-wrapper">
					<?php
						fildisi_eutf_print_bg_image_container( $item );
						fildisi_eutf_print_overlay_container( $item  );
					?>
					</div>
				</div>
<?php
			}
?>
		</div>
		<?php fildisi_eutf_print_bottom_seperator_container( $feature_settings ); ?>
	</div>
<?php

}

/**
 * Prints Header Feature Map
 */
function fildisi_eutf_print_header_feature_map( $feature_settings, $map_items, $map_settings ) {

	wp_enqueue_script( 'fildisi-eutf-maps-script');

	$feature_data = fildisi_eutf_get_feature_data( $feature_settings, 'map' );

	$map_marker = fildisi_eutf_array_value( $map_settings, 'marker', get_template_directory_uri() . '/images/markers/markers.png' );
	$map_zoom = fildisi_eutf_array_value( $map_settings, 'zoom', 14 );
	$map_disable_style = fildisi_eutf_array_value( $map_settings, 'disable_style', 'no' );

	$map_lat = fildisi_eutf_array_value( $map_items[0], 'lat', '51.516221' );
	$map_lng = fildisi_eutf_array_value( $map_items[0], 'lng', '-0.136986' );

?>
	<div id="eut-feature-section" <?php echo implode( ' ', $feature_data ); ?>>
		<div class="eut-map eut-wrapper clearfix" <?php echo fildisi_eutf_get_feature_height( $feature_settings ); ?> data-lat="<?php echo esc_attr( $map_lat ); ?>" data-lng="<?php echo esc_attr( $map_lng ); ?>" data-zoom="<?php echo esc_attr( $map_zoom ); ?>" data-disable-style="<?php echo esc_attr( $map_disable_style ); ?>"><?php echo apply_filters( 'fildisi_eutf_privacy_gmap_fallback', '', $map_lat, $map_lng ); ?></div>
		<?php
			foreach ( $map_items as $map_item ) {
				fildisi_eutf_print_feature_map_point( $map_item, $map_marker );
			}
		?>
		<?php fildisi_eutf_print_bottom_seperator_container( $feature_settings ); ?>
	</div>
<?php
}

function fildisi_eutf_print_feature_map_point( $map_item, $default_marker ) {

	$map_lat = fildisi_eutf_array_value( $map_item, 'lat', '51.516221' );
	$map_lng = fildisi_eutf_array_value( $map_item, 'lng', '-0.136986' );
	$map_marker = fildisi_eutf_array_value( $map_item, 'marker', $default_marker );

	$map_title = fildisi_eutf_array_value( $map_item, 'title' );
	$map_infotext = fildisi_eutf_array_value( $map_item, 'info_text','' );
	$map_infotext_open = fildisi_eutf_array_value( $map_item, 'info_text_open', 'no' );

	$button_text = fildisi_eutf_array_value( $map_item, 'button_text' );
	$button_url = fildisi_eutf_array_value( $map_item, 'button_url' );
	$button_target = fildisi_eutf_array_value( $map_item, 'button_target', '_self' );
	$button_class = fildisi_eutf_array_value( $map_item, 'button_class' );

?>
	<div style="display:none" class="eut-map-point" data-point-lat="<?php echo esc_attr( $map_lat ); ?>" data-point-lng="<?php echo esc_attr( $map_lng ); ?>" data-point-marker="<?php echo esc_attr( $map_marker ); ?>" data-point-title="<?php echo esc_attr( $map_title ); ?>" data-point-open="<?php echo esc_attr( $map_infotext_open ); ?>">
		<?php if ( !empty( $map_title ) || !empty( $map_infotext ) || !empty( $button_text ) ) { ?>
		<div class="eut-map-infotext">
			<?php if ( !empty( $map_title ) ) { ?>
			<h6 class="eut-infotext-title"><?php echo esc_html( $map_title ); ?></h6>
			<?php } ?>
			<?php if ( !empty( $map_infotext ) ) { ?>
			<p class="eut-infotext-description"><?php echo wp_kses_post( $map_infotext ); ?></p>
			<?php } ?>
			<?php if ( !empty( $button_text ) ) { ?>
			<a class="eut-infotext-link <?php echo esc_attr( $button_class ); ?>" href="<?php echo esc_url( $button_url ); ?>" target="<?php echo esc_attr( $button_target ); ?>"><?php echo esc_html( $button_text ); ?></a>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
<?php

}

/**
 * Prints Header Feature Revolution Slider
 */
function fildisi_eutf_print_header_feature_revslider( $feature_settings, $revslider_alias, $item  ) {

	$el_class = fildisi_eutf_array_value( $item, 'el_class' );
	$feature_data = fildisi_eutf_get_feature_data( $feature_settings, 'revslider', '', $el_class );

?>
	<div id="eut-feature-section" <?php echo implode( ' ', $feature_data ); ?>>
		<?php echo do_shortcode( '[rev_slider ' . $revslider_alias . ']' ); ?>
	</div>

<?php
}

/**
 * Prints Header Feature Go to Section ( Bottom Arrow )
 */
function fildisi_eutf_print_feature_go_to_section( $feature_settings, $item ) {

	$arrow_enabled = fildisi_eutf_array_value( $item, 'arrow_enabled', 'no' );
	$arrow_color = fildisi_eutf_array_value( $item, 'arrow_color', 'light' );
	$arrow_color_custom = fildisi_eutf_array_value( $item, 'arrow_color_custom', '#ffffff' );
	$arrow_color_custom = fildisi_eutf_get_color( $arrow_color, $arrow_color_custom );

	if( 'yes' == $arrow_enabled ) {
?>
		<div id="eut-goto-section-wrapper">
			<i id="eut-goto-section" class="eut-icon-nav-down" style=" color: <?php echo esc_attr( $arrow_color_custom ); ?>;"></i>
		</div>
<?php
	}

}

/**
 * Prints Header Feature Content Image
 */
function fildisi_eutf_print_feature_content_image( $item ) {

	$media_id = fildisi_eutf_array_value( $item, 'content_image_id', '0' );
	$media_size = fildisi_eutf_array_value( $item, 'content_image_size', 'medium' );

	if( !empty( $media_id ) ) {
?>
		<div class="eut-graphic">
			<?php echo wp_get_attachment_image( $media_id, $media_size ); ?>
		</div>
<?php
	}

}


/**
 * Prints Header Section Feature Content
 */
function fildisi_eutf_print_header_feature_content( $feature_settings, $item, $mode = ''  ) {

	$feature_size = fildisi_eutf_array_value( $feature_settings, 'size' );

	$title = fildisi_eutf_array_value( $item, 'title' );
	$caption = fildisi_eutf_array_value( $item, 'caption' );
	$subheading = fildisi_eutf_array_value( $item, 'subheading' );

	$subheading_tag = fildisi_eutf_array_value( $item, 'subheading_tag', 'div' );
	$title_tag = fildisi_eutf_array_value( $item, 'title_tag', 'div' );
	$caption_tag = fildisi_eutf_array_value( $item, 'caption_tag', 'div' );

	$fildisi_eutf_content_container_classes = array( 'eut-content' );
	$fildisi_eutf_subheading_classes = array( 'eut-subheading', 'eut-title-categories' );
	$fildisi_eutf_title_classes = array( 'eut-title' );
	$fildisi_eutf_caption_classes = array( 'eut-description' );
	$fildisi_eutf_title_meta_classes = array( 'eut-title-meta-content', 'eut-link-text' );
	$fildisi_eutf_content_classes = array( 'eut-title-content-wrapper' );


	//Content Container Classes
	$content_position = fildisi_eutf_array_value( $item, 'content_position', 'center-center' );
	$container_size = fildisi_eutf_array_value( $item, 'container_size' );
	$fildisi_eutf_content_container_classes[] = 'eut-align-' . $content_position;
	if ( 'large' == $container_size ) {
		$fildisi_eutf_content_container_classes[] = 'eut-fullwidth';
	}

	$content_bg_color = fildisi_eutf_array_value( $item, 'content_bg_color', 'none' );
	$content_align = fildisi_eutf_array_value( $item, 'content_align', 'center' );
	$content_size = fildisi_eutf_array_value( $item, 'content_size', 'large' );
	if ( 'custom' != $content_bg_color ) {
		$fildisi_eutf_content_classes[] = 'eut-bg-' . $content_bg_color;
	}
	$fildisi_eutf_content_classes[] = 'eut-align-' . $content_align;
	$fildisi_eutf_content_classes[] = 'eut-content-' . $content_size;


	$subheading_color = fildisi_eutf_array_value( $item, 'subheading_color', 'light' );
	$title_color = fildisi_eutf_array_value( $item, 'title_color', 'light' );
	$caption_color = fildisi_eutf_array_value( $item, 'caption_color', 'light' );

	$subheading_family = fildisi_eutf_array_value( $item, 'subheading_family' );
	$title_family = fildisi_eutf_array_value( $item, 'title_family' );
	$caption_family = fildisi_eutf_array_value( $item, 'caption_family' );

	if ( !empty( $subheading_family ) ) {
		$fildisi_eutf_subheading_classes[] = 'eut-' . $subheading_family;
	}
	if ( !empty( $title_family ) ) {
		$fildisi_eutf_title_classes[] = 'eut-' . $title_family;
	}
	if ( !empty( $caption_family ) ) {
		$fildisi_eutf_caption_classes[] = 'eut-' . $caption_family;
	}

	if ( 'custom' != $subheading_color ) {
		$fildisi_eutf_subheading_classes[] = 'eut-text-' . $subheading_color;
		$fildisi_eutf_title_meta_classes[] = 'eut-text-' . $subheading_color;
	}
	if ( 'custom' != $title_color ) {
		$fildisi_eutf_title_classes[] = 'eut-text-' . $title_color;
	}
	if ( 'custom' != $caption_color ) {
		$fildisi_eutf_caption_classes[] = 'eut-text-' . $caption_color;
	}

	$fildisi_eutf_content_container_classes = implode( ' ', $fildisi_eutf_content_container_classes );
	$fildisi_eutf_subheading_classes = implode( ' ', $fildisi_eutf_subheading_classes );
	$fildisi_eutf_title_classes = implode( ' ', $fildisi_eutf_title_classes );
	$fildisi_eutf_caption_classes = implode( ' ', $fildisi_eutf_caption_classes );
	$fildisi_eutf_title_meta_classes = implode( ' ', $fildisi_eutf_title_meta_classes );
	$fildisi_eutf_content_classes = implode( ' ', $fildisi_eutf_content_classes );

	$content_animation = fildisi_eutf_array_value( $item, 'content_animation', 'fade-in' );

	$button = fildisi_eutf_array_value( $item, 'button' );
	$button2 = fildisi_eutf_array_value( $item, 'button2' );

	$button_text = fildisi_eutf_array_value( $button, 'text' );
	$button_text2 = fildisi_eutf_array_value( $button2, 'text' );

	$slide_type = fildisi_eutf_array_value( $item, 'type' );
	$slide_post_id = fildisi_eutf_array_value( $item, 'post_id' );
	if( 'post' == $slide_type &&  !empty( $slide_post_id ) ) {
		$post_type = get_post_type( $slide_post_id );
		$slide_post_id = apply_filters( 'wpml_object_id', $slide_post_id, $post_type, TRUE  );
		$title = get_the_title ( $slide_post_id  );
		$caption = get_post_meta( $slide_post_id, '_fildisi_eutf_description', true );
		$link_url = get_permalink( $slide_post_id ) ;
	}

?>
	<div class="<?php echo esc_attr( $fildisi_eutf_content_container_classes ); ?>" data-animation="<?php echo esc_attr( $content_animation ); ?>">
		<div class="eut-container">
			<div class="<?php echo esc_attr( $fildisi_eutf_content_classes ); ?>">
			<?php if( 'post' == $slide_type &&  !empty( $slide_post_id ) ) { ?>
				<div class="<?php echo esc_attr( $fildisi_eutf_subheading_classes ); ?>">
					<?php fildisi_eutf_print_post_title_categories( $slide_post_id ); ?>
				</div>
				<a href="<?php echo esc_url( $link_url ); ?>">
					<<?php echo tag_escape( $title_tag ); ?> class="<?php echo esc_attr( $fildisi_eutf_title_classes ); ?>"><span><?php echo wp_kses_post( $title ); ?></span></<?php echo tag_escape( $title_tag ); ?>>
				</a>
				<?php if ( !empty( $caption ) ) { ?>
				<<?php echo tag_escape( $caption_tag ); ?> class="<?php echo esc_attr( $fildisi_eutf_caption_classes ); ?>"><span><?php echo wp_kses_post( $caption ); ?></span></<?php echo tag_escape( $caption_tag ); ?>>
				<?php } ?>
				<div class="<?php echo esc_attr( $fildisi_eutf_title_meta_classes ); ?>">
				<?php fildisi_eutf_print_feature_post_title_meta( $slide_post_id ); ?>
				</div>
			<?php } else { ?>
				<?php fildisi_eutf_print_feature_content_image( $item ); ?>
				<?php if ( !empty( $subheading ) ) { ?>
				<<?php echo tag_escape( $subheading_tag ); ?> class="<?php echo esc_attr( $fildisi_eutf_subheading_classes ); ?>"><span><?php echo wp_kses_post( $subheading ); ?></span></<?php echo tag_escape( $subheading_tag ); ?>>
				<?php } ?>
				<?php if ( !empty( $title ) ) { ?>
				<<?php echo tag_escape( $title_tag ); ?> class="<?php echo esc_attr( $fildisi_eutf_title_classes ); ?>"><span><?php echo wp_kses_post( $title ); ?></span></<?php echo tag_escape( $title_tag ); ?>>
				<?php } ?>
				<?php if ( !empty( $caption ) ) { ?>
				<<?php echo tag_escape( $caption_tag ); ?> class="<?php echo esc_attr( $fildisi_eutf_caption_classes ); ?>"><span><?php echo wp_kses_post( $caption ); ?></span></<?php echo tag_escape( $caption_tag ); ?>>
				<?php } ?>

				<?php
					if( 'title' != $mode && ( !empty( $button_text ) || !empty( $button_text2 ) ) ) {
					$btn1_class = $btn2_class = 'eut-btn-1';
					if ( !empty( $button_text ) && !empty( $button_text2 ) ) {
						$btn2_class = 'eut-btn-2';
					}
				?>
					<div class="eut-button-wrapper">
						<?php fildisi_eutf_print_feature_button( $button, $btn1_class ); ?>
						<?php fildisi_eutf_print_feature_button( $button2, $btn2_class ); ?>
					</div>
				<?php
					}
				?>
				<?php fildisi_eutf_print_feature_go_to_section( $feature_settings, $item ); ?>
			<?php } ?>
			</div>
		</div>
	</div>
<?php
}

/**
 * Prints Header Feature Button
 */
function fildisi_eutf_print_feature_button( $item, $extra_class = 'eut-btn-1' ) {

	$button_id = fildisi_eutf_array_value( $item, 'id' );
	$button_text = fildisi_eutf_array_value( $item, 'text' );
	$button_url = fildisi_eutf_array_value( $item, 'url' );
	$button_type = fildisi_eutf_array_value( $item, 'type' );
	$button_size = fildisi_eutf_array_value( $item, 'size', 'medium' );
	$button_color = fildisi_eutf_array_value( $item, 'color', 'primary-1' );
	$button_hover_color = fildisi_eutf_array_value( $item, 'hover_color', 'black' );
	$button_shape = fildisi_eutf_array_value( $item, 'shape', 'square' );
	$button_target = fildisi_eutf_array_value( $item, 'target', '_self' );
	$button_class = fildisi_eutf_array_value( $item, 'class' );

	if ( !empty( $button_text ) ) {

		//Button Classes
		$button_classes = array( 'eut-btn' );

		$button_classes[] = $extra_class;
		$button_classes[] = 'eut-btn-' . $button_size;
		$button_classes[] = 'eut-' . $button_shape;
		if ( 'outline' == $button_type ) {
			$button_classes[] = 'eut-btn-line';
		}
		if ( !empty( $button_class ) ) {
			$button_classes[] = $button_class;
		}
		$button_classes[] = 'eut-bg-' . $button_color;
		$button_classes[] = 'eut-bg-hover-' . $button_hover_color;

		$button_class_string = implode( ' ', $button_classes );

		if ( !empty( $button_url ) ) {
			$url = $button_url;
			$target = $button_target;
		} else {
			$url = "#";
			$target= "_self";
		}

?>
		<a class="<?php echo esc_attr( $button_class_string ); ?>" href="<?php echo esc_url( $url ); ?>"  target="<?php echo esc_attr( $target ); ?>">
			<span><?php echo esc_html( $button_text ); ?></span>
		</a>
<?php

	}

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
