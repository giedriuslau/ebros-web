<?php
/*
*	Helper Functions for meta options ( Post / Page / Portfolio / Product )
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/


/**
 * Functions to print global metaboxes
 */
add_action( 'save_post', 'fildisi_eutf_generic_options_save_postdata', 10, 2 );

/**
 * Page Options Metabox
 */
function fildisi_eutf_page_options_box( $post ) {

	global $fildisi_eutf_button_color_selection;

	$post_type = get_post_type( $post->ID );
	$fildisi_eutf_page_title_selection = array(
		'' => esc_html__( '-- Inherit --', 'fildisi' ),
		'custom' => esc_html__( 'Custom Advanced Title', 'fildisi' ),
	);
	$fildisi_eutf_area_colors_info = esc_html__( 'Inherit : Appearance - Customize', 'fildisi' );
	$fildisi_eutf_theme_options_info_text_empty = $fildisi_eutf_desc_info = "";

	switch( $post_type ) {
		case 'tribe_events':
			$fildisi_eutf_theme_options_info = esc_html__( 'Inherit : Theme Options - Events Calendar Options - Single Event Settings.', 'fildisi' );
		break;
		case 'portfolio':
			$fildisi_eutf_theme_options_info = esc_html__( 'Inherit : Theme Options - Portfolio Options.', 'fildisi' );
			$fildisi_eutf_theme_options_info_text_empty =  esc_html__('If empty, text is configured from: Theme Options - Portfolio Options.', 'fildisi' );
			$fildisi_eutf_desc_info = esc_html__( 'Enter your portfolio description.', 'fildisi' );
		break;
		case 'post':
			$fildisi_eutf_theme_options_info = esc_html__( 'Inherit : Theme Options - Blog Options - Single Post.', 'fildisi' );
			$fildisi_eutf_theme_options_info_text_empty =  esc_html__('If empty, text is configured from: Theme Options - Blog Options - Single Post.', 'fildisi' );
			$fildisi_eutf_desc_info = esc_html__( 'Enter your post description( Not available in Simple Title ).', 'fildisi' );
			$fildisi_eutf_page_title_selection = array(
				'' => esc_html__( '-- Inherit --', 'fildisi' ),
				'custom' => esc_html__( 'Custom Advanced Title', 'fildisi' ),
				'simple' => esc_html__( 'Simple Title', 'fildisi' ),
			);
		break;
		case 'product':
			$fildisi_eutf_area_colors_info = esc_html__( 'Inherit : Appearance - Customize - Colors - Shop/Product - Colors - Product Area.', 'fildisi' );
			$fildisi_eutf_theme_options_info = esc_html__( 'Inherit : Theme Options - WooCommerce Options - Single Product.', 'fildisi' );
			$fildisi_eutf_theme_options_info_text_empty =  esc_html__('If empty, text is configured from: Theme Options - WooCommerce Options - Single Product.', 'fildisi' );
			$fildisi_eutf_desc_info = esc_html__( 'Enter your product description( Not available in Simple Title ).', 'fildisi' );
			$fildisi_eutf_page_title_selection = array(
				'' => esc_html__( '-- Inherit --', 'fildisi' ),
				'custom' => esc_html__( 'Custom Advanced Title', 'fildisi' ),
				'simple' => esc_html__( 'Simple Title', 'fildisi' ),
			);
		break;
		case 'page':
		default:
			$fildisi_eutf_theme_options_info = esc_html__( 'Inherit : Theme Options - Page Options.', 'fildisi' );
			$fildisi_eutf_theme_options_info_text_empty =  esc_html__('If empty, text is configured from: Theme Options - Page Options.', 'fildisi' );
			$fildisi_eutf_desc_info = esc_html__( 'Enter your page description.', 'fildisi' );
		break;
	}

	$fildisi_eutf_page_padding_selection = array(
		'' => esc_html__( '-- Inherit --', 'fildisi' ),
		'none' => esc_html__( 'None', 'fildisi' ),
		'1x' => esc_html__( '1x', 'fildisi' ),
		'2x' => esc_html__( '2x', 'fildisi' ),
		'3x' => esc_html__( '3x', 'fildisi' ),
		'4x' => esc_html__( '4x', 'fildisi' ),
		'5x' => esc_html__( '5x', 'fildisi' ),
		'6x' => esc_html__( '6x', 'fildisi' ),
	);

	wp_nonce_field( 'fildisi_eutf_nonce_page_save', '_fildisi_eutf_nonce_page_save' );


	$fildisi_eutf_custom_title_options = get_post_meta( $post->ID, '_fildisi_eutf_custom_title_options', true );
	$fildisi_eutf_description = get_post_meta( $post->ID, '_fildisi_eutf_description', true );


	//Product Area
	$fildisi_eutf_area_colors = get_post_meta( $post->ID, '_fildisi_eutf_area_colors', true );
	$fildisi_eutf_area_section_type = get_post_meta( $post->ID, '_fildisi_eutf_area_section_type', true );
	$fildisi_eutf_area_padding_top_multiplier = get_post_meta( $post->ID, '_fildisi_eutf_area_padding_top_multiplier', true );
	$fildisi_eutf_area_padding_bottom_multiplier = get_post_meta( $post->ID, '_fildisi_eutf_area_padding_bottom_multiplier', true );
	$fildisi_eutf_area_image_id = get_post_meta( $post->ID, '_fildisi_eutf_area_image_id', true );


	//Layout Fields
	$fildisi_eutf_padding_top = get_post_meta( $post->ID, '_fildisi_eutf_padding_top', true );
	$fildisi_eutf_padding_bottom = get_post_meta( $post->ID, '_fildisi_eutf_padding_bottom', true );
	$fildisi_eutf_layout = get_post_meta( $post->ID, '_fildisi_eutf_layout', true );
	$fildisi_eutf_sidebar = get_post_meta( $post->ID, '_fildisi_eutf_sidebar', true );
	$fildisi_eutf_fixed_sidebar = get_post_meta( $post->ID, '_fildisi_eutf_fixed_sidebar', true );
	$fildisi_eutf_post_content_width = get_post_meta( $post->ID, '_fildisi_eutf_post_content_width', true ); //Post/Product/Event Only

	//Sliding Area
	$fildisi_eutf_sidearea_visibility = get_post_meta( $post->ID, '_fildisi_eutf_sidearea_visibility', true );
	$fildisi_eutf_sidearea_sidebar = get_post_meta( $post->ID, '_fildisi_eutf_sidearea_sidebar', true );

	//Scrolling Page
	$fildisi_eutf_scrolling_page = get_post_meta( $post->ID, '_fildisi_eutf_scrolling_page', true );
	$fildisi_eutf_responsive_scrolling = get_post_meta( $post->ID, '_fildisi_eutf_responsive_scrolling', true );
	$fildisi_eutf_scrolling_lock_anchors = get_post_meta( $post->ID, '_fildisi_eutf_scrolling_lock_anchors', true );
	$fildisi_eutf_scrolling_direction = get_post_meta( $post->ID, '_fildisi_eutf_scrolling_direction', true );
	$fildisi_eutf_scrolling_loop = get_post_meta( $post->ID, '_fildisi_eutf_scrolling_loop', true );
	$fildisi_eutf_scrolling_speed = get_post_meta( $post->ID, '_fildisi_eutf_scrolling_speed', true );

	//Header - Main Menu Fields
	$fildisi_eutf_header_overlapping = get_post_meta( $post->ID, '_fildisi_eutf_header_overlapping', true );
	$fildisi_eutf_header_style = get_post_meta( $post->ID, '_fildisi_eutf_header_style', true );
	$fildisi_eutf_main_navigation_menu = get_post_meta( $post->ID, '_fildisi_eutf_main_navigation_menu', true );
	$fildisi_eutf_responsive_navigation_menu = get_post_meta( $post->ID, '_fildisi_eutf_responsive_navigation_menu', true );
	$fildisi_eutf_sticky_header_type = get_post_meta( $post->ID, '_fildisi_eutf_sticky_header_type', true );
	$fildisi_eutf_menu_type = get_post_meta( $post->ID, '_fildisi_eutf_menu_type', true );
	$fildisi_eutf_responsive_header_overlapping = get_post_meta( $post->ID, '_fildisi_eutf_responsive_header_overlapping', true );

	//Extras
	$fildisi_eutf_details_title = get_post_meta( $post->ID, '_fildisi_eutf_details_title', true ); //Portfolio Only
	$fildisi_eutf_details = get_post_meta( $post->ID, '_fildisi_eutf_details', true ); //Portfolio Only

	$fildisi_eutf_details_link_text = get_post_meta( $post->ID, '_fildisi_eutf_details_link_text', true ); //Portfolio Only
	$fildisi_eutf_details_link_url = get_post_meta( $post->ID, '_fildisi_eutf_details_link_url', true ); //Portfolio Only
	$fildisi_eutf_details_link_new_window = get_post_meta( $post->ID, '_fildisi_eutf_details_link_new_window', true ); //Portfolio Only
	$fildisi_eutf_details_link_extra_class = get_post_meta( $post->ID, '_fildisi_eutf_details_link_extra_class', true ); //Portfolio Only
	$fildisi_eutf_social_bar_layout = get_post_meta( $post->ID, '_fildisi_eutf_social_bar_layout', true ); //Portfolio Only

	$fildisi_eutf_backlink_id = get_post_meta( $post->ID, '_fildisi_eutf_backlink_id', true ); //Portfolio. Post, Product

	$fildisi_eutf_anchor_navigation_menu = get_post_meta( $post->ID, '_fildisi_eutf_anchor_navigation_menu', true );
	$fildisi_eutf_theme_loader = get_post_meta( $post->ID, '_fildisi_eutf_theme_loader', true );

	//Visibility Fields
	$fildisi_eutf_disable_top_bar = get_post_meta( $post->ID, '_fildisi_eutf_disable_top_bar', true );
	$fildisi_eutf_disable_logo = get_post_meta( $post->ID, '_fildisi_eutf_disable_logo', true );
	$fildisi_eutf_disable_menu = get_post_meta( $post->ID, '_fildisi_eutf_disable_menu', true );
	$fildisi_eutf_disable_menu_items = get_post_meta( $post->ID, '_fildisi_eutf_disable_menu_items', true );
	$fildisi_eutf_disable_breadcrumbs = get_post_meta( $post->ID, '_fildisi_eutf_disable_breadcrumbs', true );
	$fildisi_eutf_disable_title = get_post_meta( $post->ID, '_fildisi_eutf_disable_title', true );
	$fildisi_eutf_disable_media = get_post_meta( $post->ID, '_fildisi_eutf_disable_media', true ); //Post Only
	$fildisi_eutf_disable_content = get_post_meta( $post->ID, '_fildisi_eutf_disable_content', true ); //Page Only
	$fildisi_eutf_disable_recent_entries = get_post_meta( $post->ID, '_fildisi_eutf_disable_recent_entries', true );//Portfolio Only
	$fildisi_eutf_disable_back_to_top = get_post_meta( $post->ID, '_fildisi_eutf_disable_back_to_top', true );

	$fildisi_eutf_bottom_bar_area = get_post_meta( $post->ID, '_fildisi_eutf_bottom_bar_area', true );
	$fildisi_eutf_footer_widgets_visibility = get_post_meta( $post->ID, '_fildisi_eutf_footer_widgets_visibility', true );
	$fildisi_eutf_footer_bar_visibility = get_post_meta( $post->ID, '_fildisi_eutf_footer_bar_visibility', true );

?>

	<!--  METABOXES -->
	<div class="eut-metabox-content">

		<!-- TABS -->
		<div class="eut-tabs">

			<ul class="eut-tab-links">
				<li class="active"><a href="#eut-page-option-tab-header"><?php esc_html_e( 'Header / Main Menu', 'fildisi' ); ?></a></li>
				<li><a href="#eut-page-option-tab-title"><?php esc_html_e( 'Title / Description', 'fildisi' ); ?></a></li>
				<?php if( 'product' == $post_type ) { ?>
				<li><a href="#eut-page-option-tab-section-area"><?php esc_html_e( 'Product Area', 'fildisi' ); ?></a></li>
				<?php } ?>
				<li><a href="#eut-page-option-tab-layout"><?php esc_html_e( 'Content / Sidebars', 'fildisi' ); ?></a></li>
				<li><a href="#eut-page-option-tab-sliding-area"><?php esc_html_e( 'Sliding Area', 'fildisi' ); ?></a></li>
				<?php if( 'page' == $post_type ) { ?>
				<li><a href="#eut-page-option-tab-scrolling-sections"><?php esc_html_e( 'Scrolling Sections', 'fildisi' ); ?></a></li>
				<?php } ?>
				<li><a href="#eut-page-option-tab-bottom-footer-areas"><?php esc_html_e( 'Bottom / Footer Areas', 'fildisi' ); ?></a></li>
				<li><a href="#eut-page-option-tab-extras"><?php esc_html_e( 'Extras', 'fildisi' ); ?></a></li>
				<li><a href="#eut-page-option-tab-visibility"><?php esc_html_e( 'Visibility', 'fildisi' ); ?></a></li>
			</ul>
			<div class="eut-tab-content">

				<div id="eut-page-option-tab-header" class="eut-tab-item active">
					<?php

						//Header Overlapping Option
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_header_overlapping',
								'id' => '_fildisi_eutf_header_overlapping',
								'value' => $fildisi_eutf_header_overlapping,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'yes' => esc_html__( 'Yes', 'fildisi' ),
									'no' => esc_html__( 'No', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Header Overlapping', 'fildisi' ),
									"desc" => esc_html__( 'Select if you want to overlap your page header', 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
							)
						);

						//Header Style Option
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_header_style',
								'id' => '_fildisi_eutf_header_style',
								'value' => $fildisi_eutf_header_style,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'default' => esc_html__( 'Default', 'fildisi' ),
									'dark' => esc_html__( 'Dark', 'fildisi' ),
									'light' => esc_html__( 'Light', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Header Style', 'fildisi' ),
									"desc" => esc_html__( 'With this option you can change the coloring of your header. In case that you use Slider in Feature Section, select the header style per slide/image.', 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
							)
						);

						//Main Navigation Menu Option
						fildisi_eutf_print_admin_option_wrapper_start(
							array(
								'type' => 'select',
								'label' => array(
									"title" => esc_html__( 'Main Navigation Menu', 'fildisi' ),
									"desc" => esc_html__( 'Select alternative main navigation menu.', 'fildisi' ),
									"info" => esc_html__( 'Inherit : Menus - Theme Locations - Header Menu.', 'fildisi' ),
								),
							)
						);
						fildisi_eutf_print_menu_selection( $fildisi_eutf_main_navigation_menu, 'eut-main-navigation-menu', '_fildisi_eutf_main_navigation_menu', 'default' );
						fildisi_eutf_print_admin_option_wrapper_end();

						//Responsive Navigation Menu Option
						fildisi_eutf_print_admin_option_wrapper_start(
							array(
								'type' => 'select',
								'label' => array(
									"title" => esc_html__( 'Responsive Navigation Menu', 'fildisi' ),
									"desc" => esc_html__( 'Select alternative responsive navigation menu.', 'fildisi' ),
									"info" => esc_html__( 'Inherit : Menus - Theme Locations - Responsive Menu.', 'fildisi' ),
								),
							)
						);
						fildisi_eutf_print_menu_selection( $fildisi_eutf_responsive_navigation_menu, 'eut-responsive-navigation-menu', '_fildisi_eutf_responsive_navigation_menu', 'default' );
						fildisi_eutf_print_admin_option_wrapper_end();

						//Menu Type
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_menu_type',
								'id' => '_fildisi_eutf_menu_type',
								'value' => $fildisi_eutf_menu_type,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'classic' => esc_html__( 'Classic', 'fildisi' ),
									'button' => esc_html__( 'Button Style', 'fildisi' ),
									'underline' => esc_html__( 'Underline', 'fildisi' ),
									'hidden' => esc_html__( 'Hidden', 'fildisi' ),
									'advanced-hidden' => esc_html__( 'Advanced Hidden', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Menu Type', 'fildisi' ),
									"desc" => esc_html__( 'With this option you can select the type of the menu ( Not available for Side Header Mode ).', 'fildisi' ),
									"info" => esc_html__( 'Inherit : Theme Options - Header Options.', 'fildisi' ),
								),
							)
						);

						//Sticky Header Type
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_sticky_header_type',
								'id' => '_fildisi_eutf_sticky_header_type',
								'value' => $fildisi_eutf_sticky_header_type,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'none' => esc_html__( '-- None --', 'fildisi' ),
									'simple' => esc_html__( 'Simple', 'fildisi' ),
									'shrink' => esc_html__( 'Shrink', 'fildisi' ),
									'advanced' => esc_html__( 'Scroll Up', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Sticky Header Type', 'fildisi' ),
									"desc" => esc_html__( 'With this option you can select the type of sticky header.', 'fildisi' ),
									"info" => esc_html__( 'Inherit : Theme Options - Header Options - Sticky Header Options.', 'fildisi' ),
								),
							)
						);

						//Responsive Header Overlapping Option
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_responsive_header_overlapping',
								'id' => '_fildisi_eutf_responsive_header_overlapping',
								'value' => $fildisi_eutf_responsive_header_overlapping,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'yes' => esc_html__( 'Yes', 'fildisi' ),
									'no' => esc_html__( 'No', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Responsive Header Overlapping', 'fildisi' ),
									"desc" => esc_html__( 'Select if you want to overlap your responsive header', 'fildisi' ),
									"info" => esc_html__( 'Inherit : Theme Options - Header Options - Responsive Header Options.', 'fildisi' ),
								),
							)
						);
					?>
				</div>
				<div id="eut-page-option-tab-title" class="eut-tab-item">
					<?php

						echo '<div id="_fildisi_eutf_page_title">';

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_disable_title',
								'id' => '_fildisi_eutf_disable_title',
								'value' => $fildisi_eutf_disable_title,
								'options' => array(
									'' => esc_html__( 'Visible', 'fildisi' ),
									'yes' => esc_html__( 'Hidden', 'fildisi' ),

								),
								'label' => array(
									"title" => esc_html__( 'Title/Description Visibility', 'fildisi' ),
									"desc" => esc_html__( 'Select if you want to hide your title and decription .', 'fildisi' ),
								),
								'group_id' => '_fildisi_eutf_page_title',
							)
						);
						if( 'tribe_events' != $post_type ) {
							//Description Option
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'textarea',
									'name' => '_fildisi_eutf_description',
									'id' => '_fildisi_eutf_description',
									'value' => $fildisi_eutf_description,
									'label' => array(
										'title' => esc_html__( 'Description', 'fildisi' ),
										'desc' => $fildisi_eutf_desc_info,
									),
									'width' => 'fullwidth',
									'rows' => 2,
								)
							);
						}

						//Custom Title Option

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_page_title_custom',
								'id' => '_fildisi_eutf_page_title_custom',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'custom' ),
								'options' => $fildisi_eutf_page_title_selection,
								'label' => array(
									"title" => esc_html__( 'Title Options', 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
								'group_id' => '_fildisi_eutf_page_title',
								'highlight' => 'highlight',
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);



						global $fildisi_eutf_area_height;
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'options' => $fildisi_eutf_area_height,
								'name' => '_fildisi_eutf_page_title_height',
								'id' => '_fildisi_eutf_page_title_height',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'height', '40' ),
								'label' => array(
									"title" => esc_html__( 'Title Area Height', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'textfield',
								'name' => '_fildisi_eutf_page_title_min_height',
								'id' => '_fildisi_eutf_page_title_min_height',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'min_height', '200' ),
								'label' => array(
									"title" => esc_html__( 'Title Area Minimum Height in px', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_page_title_container_size',
								'id' => '_fildisi_eutf_page_title_container_size',
								'options' => array(
									'' => esc_html__( 'Default', 'fildisi' ),
									'large' => esc_html__( 'Large', 'fildisi' ),
								),
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'container_size' ),
								'label' => array(
									"title" => esc_html__( 'Container Size', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-colorpicker',
								'name' => '_fildisi_eutf_page_title_bg_color',
								'id' => '_fildisi_eutf_page_title_bg_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'bg_color', 'dark' ),
								'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'bg_color_custom', '#000000' ),
								'label' => array(
									"title" => esc_html__( 'Background Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
								'multiple' => 'multi',
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-colorpicker',
								'name' => '_fildisi_eutf_page_title_content_bg_color',
								'id' => '_fildisi_eutf_page_title_content_bg_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_bg_color', 'none' ),
								'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_bg_color_custom', '#ffffff' ),
								'label' => array(
									"title" => esc_html__( 'Content Background Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
								'multiple' => 'multi',
								'type_usage' => 'title-content-bg',
							)
						);

						if( 'post' == $post_type ) {
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'select-colorpicker',
									'name' => '_fildisi_eutf_page_title_subheading_color',
									'id' => '_fildisi_eutf_page_title_subheading_color',
									'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'subheading_color', 'light' ),
									'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'subheading_color_custom', '#ffffff' ),
									'label' => array(
										"title" => esc_html__( 'Categories/Meta Color', 'fildisi' ),
									),
									'dependency' =>
									'[
										{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
										{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
									]',
									'multiple' => 'multi',
								)
							);
						}

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-colorpicker',
								'name' => '_fildisi_eutf_page_title_title_color',
								'id' => '_fildisi_eutf_page_title_title_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'title_color', 'light' ),
								'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'title_color_custom', '#ffffff' ),
								'label' => array(
									"title" => esc_html__( 'Title Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
								'multiple' => 'multi',
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-colorpicker',
								'name' => '_fildisi_eutf_page_title_caption_color',
								'id' => '_fildisi_eutf_page_title_caption_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'caption_color', 'light' ),
								'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'caption_color_custom', '#ffffff' ),
								'label' => array(
									"title" => esc_html__( 'Description Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
								'multiple' => 'multi',
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_page_title_content_size',
								'id' => '_fildisi_eutf_page_title_content_size',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_size', 'large' ),
								'options' => array(
									'large' => esc_html__( 'Large', 'fildisi' ),
									'medium' => esc_html__( 'Medium', 'fildisi' ),
									'small' => esc_html__( 'Small', 'fildisi' ),
								),
								'label' => esc_html__( 'Content Size', 'fildisi' ),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-align',
								'name' => '_fildisi_eutf_page_title_content_alignment',
								'id' => '_fildisi_eutf_page_title_content_alignment',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_alignment', 'center' ),
								'label' => esc_html__( 'Content Alignment', 'fildisi' ),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);


						global $fildisi_eutf_media_bg_position_selection;
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_page_title_content_position',
								'id' => '_fildisi_eutf_page_title_content_position',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_position', 'center-center' ),
								'options' => $fildisi_eutf_media_bg_position_selection,
								'label' => array(
									"title" => esc_html__( 'Content Position', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-text-animation',
								'name' => '_fildisi_eutf_page_title_content_animation',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_animation', 'fade-in' ),
								'label' => esc_html__( 'Content Animation', 'fildisi' ),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);


						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_page_title_bg_mode',
								'id' => '_fildisi_eutf_page_title_bg_mode',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'bg_mode'),
								'options' => array(
									'' => esc_html__( 'Color Only', 'fildisi' ),
									'featured' => esc_html__( 'Featured Image', 'fildisi' ),
									'custom' => esc_html__( 'Custom Image', 'fildisi' ),

								),
								'label' => array(
									"title" => esc_html__( 'Background', 'fildisi' ),
								),
								'group_id' => '_fildisi_eutf_page_title',
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }

								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-image',
								'name' => '_fildisi_eutf_page_title_bg_image_id',
								'id' => '_fildisi_eutf_page_title_bg_image_id',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'bg_image_id'),
								'label' => array(
									"title" => esc_html__( 'Background Image', 'fildisi' ),
								),
								'width' => 'fullwidth',
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_page_title_bg_mode", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }

								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-bg-position',
								'name' => '_fildisi_eutf_page_title_bg_position',
								'id' => '_fildisi_eutf_page_title_bg_position',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'bg_position', 'center-center'),
								'label' => array(
									"title" => esc_html__( 'Background Position', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_page_title_bg_mode", "values" : ["featured","custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-pattern-overlay',
								'name' => '_fildisi_eutf_page_title_pattern_overlay',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'pattern_overlay'),
								'label' => esc_html__( 'Pattern Overlay', 'fildisi' ),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_page_title_bg_mode", "values" : ["featured","custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-colorpicker',
								'name' => '_fildisi_eutf_page_title_color_overlay',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'color_overlay', 'dark' ),
								'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'color_overlay_custom', '#000000' ),
								'label' => esc_html__( 'Color Overlay', 'fildisi' ),
								'multiple' => 'multi',
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_page_title_bg_mode", "values" : ["featured","custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-opacity',
								'name' => '_fildisi_eutf_page_title_opacity_overlay',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'opacity_overlay', '0' ),
								'label' => esc_html__( 'Opacity Overlay', 'fildisi' ),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_fildisi_eutf_page_title_bg_mode", "values" : ["featured","custom"] },
									{ "id" : "_fildisi_eutf_disable_title", "values" : [""] }
								]',
							)
						);

						echo '</div>';
					?>
				</div>

				<?php if( 'product' == $post_type ) { ?>
				<div id="eut-page-option-tab-section-area" class="eut-tab-item">
					<?php

						echo '<div id="_fildisi_eutf_page_section_area">';

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_area_section_type',
								'id' => '_fildisi_eutf_area_section_type',
								'value' => $fildisi_eutf_area_section_type,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'fullwidth-background' => esc_html__( 'No', 'fildisi' ),
									'fullwidth-element' => esc_html__( 'Yes', 'fildisi' ),
								),
								'label' => array(
									'title' => esc_html__( 'Area Full Width', 'fildisi' ),
									'desc' => esc_html__( "Select if you prefer a full-width Area.", 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_area_padding_top_multiplier',
								'id' => '_fildisi_eutf_area_padding_top_multiplier',
								'value' => $fildisi_eutf_area_padding_top_multiplier,
								'options' => $fildisi_eutf_page_padding_selection,
								'label' => array(
									'title' => esc_html__( 'Top Padding', 'fildisi' ),
									'desc' => esc_html__( "Select the space above the area.", 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_area_padding_bottom_multiplier',
								'id' => '_fildisi_eutf_area_padding_bottom_multiplier',
								'value' => $fildisi_eutf_area_padding_bottom_multiplier,
								'options' => $fildisi_eutf_page_padding_selection,
								'label' => array(
									'title' => esc_html__( 'Bottom Padding', 'fildisi' ),
									'desc' => esc_html__( "Select the space below the area.", 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select-image',
								'name' => '_fildisi_eutf_area_image_id',
								'id' => '_fildisi_eutf_area_image_id',
								'value' => $fildisi_eutf_area_image_id,
								'label' => array(
									"title" => esc_html__( 'Custom Image', 'fildisi' ),
									"desc" => esc_html__( 'If selected this image will replace the Feature Image of this area.', 'fildisi' ),
								),
								'width' => 'fullwidth',
							)
						);

						//Custom colors

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_area_colors_custom',
								'id' => '_fildisi_eutf_area_colors_custom',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'custom' ),
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'custom' => esc_html__( 'Custom', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Area Color Options', 'fildisi' ),
									"info" => $fildisi_eutf_area_colors_info,
								),
								'group_id' => '_fildisi_eutf_page_section_area',
								'highlight' => 'highlight',
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'colorpicker',
								'name' => '_fildisi_eutf_area_bg_color',
								'id' => '_fildisi_eutf_area_bg_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'bg_color', '#eeeeee' ),
								'label' => array(
									"title" => esc_html__( 'Background Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_area_colors_custom", "values" : ["custom"] }
								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'colorpicker',
								'name' => '_fildisi_eutf_area_headings_color',
								'id' => '_fildisi_eutf_area_headings_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'headings_color', '#000000' ),
								'label' => array(
									"title" => esc_html__( 'Headings Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_area_colors_custom", "values" : ["custom"] }
								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'colorpicker',
								'name' => '_fildisi_eutf_area_font_color',
								'id' => '_fildisi_eutf_area_font_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'font_color', '#999999' ),
								'label' => array(
									"title" => esc_html__( 'Font Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_area_colors_custom", "values" : ["custom"] }
								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'colorpicker',
								'name' => '_fildisi_eutf_area_link_color',
								'id' => '_fildisi_eutf_area_link_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'link_color', '#FF7D88' ),
								'label' => array(
									"title" => esc_html__( 'Link Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_area_colors_custom", "values" : ["custom"] }
								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'colorpicker',
								'name' => '_fildisi_eutf_area_hover_color',
								'id' => '_fildisi_eutf_area_hover_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'hover_color', '#000000' ),
								'label' => array(
									"title" => esc_html__( 'Hover Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_area_colors_custom", "values" : ["custom"] }
								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'colorpicker',
								'name' => '_fildisi_eutf_area_border_color',
								'id' => '_fildisi_eutf_area_border_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'border_color', '#e0e0e0' ),
								'label' => array(
									"title" => esc_html__( 'Border Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_area_colors_custom", "values" : ["custom"] }
								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_area_button_color',
								'id' => '_fildisi_eutf_area_button_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'button_color', 'primary-1' ),
								'options' => $fildisi_eutf_button_color_selection,
								'label' => array(
									"title" => esc_html__( 'Button Color', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_area_colors_custom", "values" : ["custom"] }
								]',
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_button_hover_color',
								'id' => '_fildisi_eutf_button_hover_color',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_area_colors, 'button_hover_color', 'black' ),
								'options' => $fildisi_eutf_button_color_selection,
								'label' => array(
									"title" => esc_html__( 'Button HoverColor', 'fildisi' ),
								),
								'dependency' =>
								'[
									{ "id" : "_fildisi_eutf_area_colors_custom", "values" : ["custom"] }
								]',
							)
						);

						echo '</div>';
					?>
				</div>
				<?php } ?>

				<div id="eut-page-option-tab-layout" class="eut-tab-item">
					<?php

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'textfield',
								'name' => '_fildisi_eutf_padding_top',
								'id' => '_fildisi_eutf_padding_top',
								'value' => $fildisi_eutf_padding_top,
								'label' => array(
									'title' => esc_html__( 'Top Padding', 'fildisi' ),
									'desc' => esc_html__( "Define the space above the content area.", 'fildisi' ) . " " . esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'fildisi' ),
									'info' => esc_html__( "Enter 0 to eliminate the space. Leave this empty for the default value ( default vaule is 90px )", 'fildisi' ),
								),
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'textfield',
								'name' => '_fildisi_eutf_padding_bottom',
								'id' => '_fildisi_eutf_padding_bottom',
								'value' => $fildisi_eutf_padding_bottom,
								'label' => array(
									'title' => esc_html__( 'Bottom Padding', 'fildisi' ),
									'desc' => esc_html__( "Define the space below the content area.", 'fildisi' ) . " " . esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'fildisi' ),
									'info' => esc_html__( "Enter 0 to eliminate the space. Leave this empty for the default value ( default vaule is 90px )", 'fildisi' ),
								),
							)
						);


						//Layout Option
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_layout',
								'id' => '_fildisi_eutf_layout',
								'value' => $fildisi_eutf_layout,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'none' => esc_html__( 'Full Width', 'fildisi' ),
									'left' => esc_html__( 'Left Sidebar', 'fildisi' ),
									'right' => esc_html__( 'Right Sidebar', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Layout', 'fildisi' ),
									"desc" => esc_html__( 'Select page content and sidebar alignment.', 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
							)
						);

						//Sidebar Option
						fildisi_eutf_print_admin_option_wrapper_start(
							array(
								'type' => 'select',
								'label' => array(
									"title" => esc_html__( 'Sidebar', 'fildisi' ),
									"desc" => esc_html__( 'Select page sidebar.', 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
							)
						);
						fildisi_eutf_print_sidebar_selection( $fildisi_eutf_sidebar, '_fildisi_eutf_sidebar', '_fildisi_eutf_sidebar' );
						fildisi_eutf_print_admin_option_wrapper_end();

						//Fixed Sidebar Option
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_fixed_sidebar',
								'id' => '_fildisi_eutf_fixed_sidebar',
								'value' => $fildisi_eutf_fixed_sidebar,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'no' => esc_html__( 'No', 'fildisi' ),
									'yes' => esc_html__( 'Yes', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Fixed Sidebar', 'fildisi' ),
									"desc" => esc_html__( 'If selected, sidebar will be fixed.', 'fildisi' ),
								),
							)
						);

						//Posts Content Width
						if ( 'post' == $post_type || 'product' == $post_type || 'tribe_events' ) {

							fildisi_eutf_print_admin_option(
								array(
									'type' => 'select',
									'name' => '_fildisi_eutf_post_content_width',
									'id' => '_fildisi_eutf_post_content_width',
									'value' => $fildisi_eutf_post_content_width,
									'options' => array(
										'' => esc_html__( '-- Inherit --', 'fildisi' ),
										'container' => esc_html__( 'Container Size' , 'fildisi' ),
										'1170' => esc_html__( 'Large' , 'fildisi' ),
										'970' => esc_html__( 'Medium' , 'fildisi' ),
										'770' => esc_html__( 'Small' , 'fildisi' ),
									),
									'label' => array(
										"title" => esc_html__( 'Content Width', 'fildisi' ),
										"desc" => esc_html__( 'Select the Content Width (only for Full Width Layout)', 'fildisi' ),
										"info" => $fildisi_eutf_theme_options_info,
									),
								)
							);
						}

					?>
				</div>
				<div id="eut-page-option-tab-sliding-area" class="eut-tab-item">
					<?php
						//Sidearea Visibility
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_sidearea_visibility',
								'id' => '_fildisi_eutf_sidearea_visibility',
								'value' => $fildisi_eutf_sidearea_visibility,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'no' => esc_html__( 'No', 'fildisi' ),
									'yes' => esc_html__( 'Yes', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Sliding Area Visibility', 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
							)
						);

						//Sidearea Sidebar Option
						fildisi_eutf_print_admin_option_wrapper_start(
							array(
								'type' => 'select',
								'label' => array(
									"title" => esc_html__( 'Sliding Area Sidebar', 'fildisi' ),
									"desc" => esc_html__( 'Select sliding area sidebar.', 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
							)
						);
						fildisi_eutf_print_sidebar_selection( $fildisi_eutf_sidearea_sidebar, '_fildisi_eutf_sidearea_sidebar', '_fildisi_eutf_sidearea_sidebar' );
						fildisi_eutf_print_admin_option_wrapper_end();
					?>
				</div>
				<div id="eut-page-option-tab-bottom-footer-areas" class="eut-tab-item">
					<?php
						//Bottom / Footer Areas Visibility

						fildisi_eutf_print_admin_option_wrapper_start(
							array(
								'type' => 'select',
								'label' => array(
									"title" => esc_html__( 'Bottom Bar Area', 'fildisi' ),
									"desc" => esc_html__( 'Select an area item for your Bottom Bar Area.', 'fildisi' ),
									"info" => esc_html__( 'Inherit : Theme Options - Bottom / Footer Areas - Bottom Bar Area Item.', 'fildisi' ),
								),
							)
						);
						fildisi_eutf_print_area_selection( $fildisi_eutf_bottom_bar_area, 'eut-bottom-bar-area-item', '_fildisi_eutf_bottom_bar_area' );
						fildisi_eutf_print_admin_option_wrapper_end();

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_footer_widgets_visibility',
								'id' => '_fildisi_eutf_footer_widgets_visibility',
								'value' => $fildisi_eutf_footer_widgets_visibility,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'no' => esc_html__( 'No', 'fildisi' ),
									'yes' => esc_html__( 'Yes', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Footer Widgets Visibility', 'fildisi' ),
									"desc" => esc_html__( 'Inherit : Theme Options - Bottom / Footer Areas - Footer Widgets Settings.', 'fildisi' ),
								),
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_footer_bar_visibility',
								'id' => '_fildisi_eutf_footer_bar_visibility',
								'value' => $fildisi_eutf_footer_bar_visibility,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'no' => esc_html__( 'No', 'fildisi' ),
									'yes' => esc_html__( 'Yes', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Footer Bar Visibility', 'fildisi' ),
									"desc" => esc_html__( 'Inherit : Theme Options - Bottom / Footer Areas - Footer Bar Settings.', 'fildisi' ),
								),
							)
						);
					?>
				</div>
				<div id="eut-page-option-tab-extras" class="eut-tab-item">
					<?php

						//Details Option
						if ( 'portfolio' == $post_type) {
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'textfield',
									'name' => '_fildisi_eutf_details_title',
									'id' => '_fildisi_eutf_details_title',
									'value' => $fildisi_eutf_details_title,
									'label' => array(
										'title' => esc_html__( 'Details Title', 'fildisi' ),
										'desc' => esc_html__( 'Enter your details title.', 'fildisi' ),
										'info' => $fildisi_eutf_theme_options_info_text_empty,
									),
									'width' => 'fullwidth',
								)
							);
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'textarea',
									'name' => '_fildisi_eutf_details',
									'id' => '_fildisi_eutf_details',
									'value' => $fildisi_eutf_details,
									'label' => array(
										'title' => esc_html__( 'Details Area', 'fildisi' ),
										'desc' => esc_html__( 'Enter your details area.', 'fildisi' ),
									),
									'width' => 'fullwidth',
								)
							);

							fildisi_eutf_print_admin_option(
								array(
									'type' => 'textfield',
									'name' => '_fildisi_eutf_details_link_text',
									'id' => '_fildisi_eutf_details_link_text',
									'value' => $fildisi_eutf_details_link_text,
									'label' => array(
										'title' => esc_html__( 'Link Text', 'fildisi' ),
										'desc' => esc_html__( 'Enter your details link text.', 'fildisi' ),
										'info' => $fildisi_eutf_theme_options_info_text_empty,
									),
									'width' => 'fullwidth',
								)
							);
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'textfield',
									'name' => '_fildisi_eutf_details_link_url',
									'value' => $fildisi_eutf_details_link_url,
									'label' => array(
										'title' => esc_html__( 'Link URL', 'fildisi' ),
										'desc' => esc_html__( 'Enter the full URL of your link.', 'fildisi' ),
									),
									'width' => 'fullwidth',
								)
							);
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'checkbox',
									'name' => '_fildisi_eutf_details_link_new_window',
									'value' => $fildisi_eutf_details_link_new_window,
									'label' => array(
										'title' => esc_html__( 'Open Link in new window', 'fildisi' ),
										'desc' => esc_html__( 'If selected, link will open in a new window.', 'fildisi' ),
									),
								)
							);
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'textfield',
									'name' => '_fildisi_eutf_details_link_extra_class',
									'value' => $fildisi_eutf_details_link_extra_class,
									'label' => array(
										'title' => esc_html__( 'Link extra class name', 'fildisi' ),
									),
								)
							);
							fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_social_bar_layout',
								'id' => '_fildisi_eutf_social_bar_layout',
								'value' => $fildisi_eutf_social_bar_layout,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'layout-1' => esc_html__( 'Bottom Layout', 'fildisi' ),
									'layout-2' => esc_html__( 'Side Layout', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Social Layout', 'fildisi' ),
									"desc" => esc_html__( 'Select your social layout.', 'fildisi' ),
									"info" => $fildisi_eutf_theme_options_info,
								),
							)
						);

						}

						if ( 'post' == $post_type || 'portfolio' == $post_type || 'product' == $post_type ) {
							//Backlink page
							fildisi_eutf_print_admin_option_wrapper_start(
								array(
									'type' => 'select',
									'label' => array(
										"title" => esc_html__( 'Backlink', 'fildisi' ),
										"desc" => esc_html__( 'Select your backlink page.', 'fildisi' ),
										"info" => $fildisi_eutf_theme_options_info,
									),
								)
							);
							fildisi_eutf_print_page_selection( $fildisi_eutf_backlink_id, 'eut-backlink-id', '_fildisi_eutf_backlink_id' );
							fildisi_eutf_print_admin_option_wrapper_end();
						}

						//Anchor Navigation Menu Option

						fildisi_eutf_print_admin_option_wrapper_start(
							array(
								'type' => 'select',
								'label' => array(
									"title" => esc_html__( 'Anchor Navigation Menu', 'fildisi' ),
									"desc" => esc_html__( 'Select page anchor navigation menu.', 'fildisi' ),
								),
							)
						);
						fildisi_eutf_print_menu_selection( $fildisi_eutf_anchor_navigation_menu, 'eut-page-navigation-menu', '_fildisi_eutf_anchor_navigation_menu' );
						fildisi_eutf_print_admin_option_wrapper_end();

						//Theme Loader
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_fildisi_eutf_theme_loader',
								'id' => '_fildisi_eutf_theme_loader',
								'value' => $fildisi_eutf_theme_loader,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'fildisi' ),
									'no' => esc_html__( 'No', 'fildisi' ),
									'yes' => esc_html__( 'Yes', 'fildisi' ),
								),
								'label' => array(
									"title" => esc_html__( 'Theme Loader Visibility', 'fildisi' ),
									"info" => esc_html__( 'Inherit : Theme Options - General Settings.', 'fildisi' ),
								),
							)
						);
					?>
				</div>
				<div id="eut-page-option-tab-scrolling-sections" class="eut-tab-item">
					<?php

						//Responsive Scrolling Option
						if ( 'page' == $post_type) {

							echo '<div id="_fildisi_eutf_page_scrolling_section">';

							fildisi_eutf_print_admin_option(
								array(
									'type' => 'select',
									'name' => '_fildisi_eutf_scrolling_page',
									'id' => '_fildisi_eutf_scrolling_page',
									'value' => $fildisi_eutf_scrolling_page,
									'options' => array(
										'' => esc_html__( 'Full Page', 'fildisi' ),
										'pilling' => esc_html__( 'Page Pilling', 'fildisi' ),
									),
									'label' => array(
										'title' => esc_html__( 'Scrolling Sections Plugin', 'fildisi' ),
										'desc' => esc_html__( 'Select the scrolling sections plugin you want to use.', 'fildisi' ),
										'info' => esc_html__( 'Note: The following options are available only for Scrolling Full Screen Sections Template.', 'fildisi' ),
									),
									'highlight' => 'highlight',
									'group_id' => '_fildisi_eutf_page_scrolling_section',
								)
							);
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'select',
									'name' => '_fildisi_eutf_scrolling_lock_anchors',
									'id' => '_fildisi_eutf_scrolling_lock_anchors',
									'value' => $fildisi_eutf_scrolling_lock_anchors,
									'options' => array(
										'' => esc_html__( 'URL without /#', 'fildisi' ),
										'no' => esc_html__( 'Allow Anchor Links in URL', 'fildisi' ),
									),
									'label' => array(
										'title' => esc_html__( 'Anchor Links', 'fildisi' ),
										'desc' => esc_html__( 'Select if you want to allow anchor links.', 'fildisi' ),
									),
								)
							);
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'select',
									'name' => '_fildisi_eutf_scrolling_loop',
									'id' => '_fildisi_eutf_scrolling_loop',
									'value' => $fildisi_eutf_scrolling_loop,
									'options' => array(
										'' => esc_html__( 'None', 'fildisi' ),
										'top' => esc_html__( 'Loop Top', 'fildisi' ),
										'bottom' => esc_html__( 'Loop Bottom', 'fildisi' ),
										'both' => esc_html__( 'Loop Top/Bottom', 'fildisi' ),
									),
									'label' => array(
										'title' => esc_html__( 'Loop', 'fildisi' ),
										'desc' => esc_html__( 'Select if you want to loop.', 'fildisi' ),
									),
								)
							);
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'select',
									'name' => '_fildisi_eutf_scrolling_direction',
									'id' => '_fildisi_eutf_scrolling_direction',
									'value' => $fildisi_eutf_scrolling_direction,
									'options' => array(
										'' => esc_html__( 'Vertical', 'fildisi' ),
										'horizontal' => esc_html__( 'Horizontal', 'fildisi' ),
									),
									'label' => array(
										'title' => esc_html__( 'Direction', 'fildisi' ),
										'desc' => esc_html__( 'Select scrolling direction.', 'fildisi' ),
									),
									'dependency' =>
									'[
										{ "id" : "_fildisi_eutf_scrolling_page", "values" : ["pilling"] }
									]',
								)
							);
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'textfield',
									'name' => '_fildisi_eutf_scrolling_speed',
									'id' => '_fildisi_eutf_scrolling_speed',
									'value' => $fildisi_eutf_scrolling_speed,
									'label' => array(
										'title' => esc_html__( 'Speed (ms)', 'fildisi' ),
										'desc' => esc_html__( 'Enter scrolling speed.', 'fildisi' ),
									),
									'default_value' => '1000',

								)
							);
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'select',
									'name' => '_fildisi_eutf_responsive_scrolling',
									'id' => '_fildisi_eutf_responsive_scrolling',
									'value' => $fildisi_eutf_responsive_scrolling,
									'options' => array(
										'' => esc_html__( 'Yes', 'fildisi' ),
										'no' => esc_html__( 'No', 'fildisi' ),
									),
									'label' => array(
										'title' => esc_html__( 'Responsive Scrolling Full Sections', 'fildisi' ),
										'desc' => esc_html__( 'Select if you want to maintain the scrolling feature on devices.', 'fildisi' ),
									),
								)
							);

							echo '</div>';
						}

					?>
				</div>
				<div id="eut-page-option-tab-visibility" class="eut-tab-item">
					<?php

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_fildisi_eutf_disable_top_bar',
								'id' => '_fildisi_eutf_disable_top_bar',
								'value' => $fildisi_eutf_disable_top_bar,
								'label' => array(
									"title" => esc_html__( 'Disable Top Bar', 'fildisi' ),
									"desc" => esc_html__( 'If selected, top bar will be hidden.', 'fildisi' ),
								),
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_fildisi_eutf_disable_logo',
								'id' => '_fildisi_eutf_disable_logo',
								'value' => $fildisi_eutf_disable_logo,
								'label' => array(
									"title" => esc_html__( 'Disable Logo', 'fildisi' ),
									"desc" => esc_html__( 'If selected, logo will be disabled.', 'fildisi' ),
								),
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_fildisi_eutf_disable_menu',
								'id' => '_fildisi_eutf_disable_menu',
								'value' => $fildisi_eutf_disable_menu,
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu', 'fildisi' ),
									"desc" => esc_html__( 'If selected, main menu will be hidden.', 'fildisi' ),
								),
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_fildisi_eutf_disable_menu_item_search',
								'id' => '_fildisi_eutf_disable_menu_item_search',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_disable_menu_items, 'search'),
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu Item Search', 'fildisi' ),
									"desc" => esc_html__( 'If selected, main menu item will be hidden.', 'fildisi' ),
								),
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_fildisi_eutf_disable_menu_item_form',
								'id' => '_fildisi_eutf_disable_menu_item_form',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_disable_menu_items, 'form'),
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu Item Contact Form', 'fildisi' ),
									"desc" => esc_html__( 'If selected, main menu item will be hidden.', 'fildisi' ),
								),
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_fildisi_eutf_disable_menu_item_language',
								'id' => '_fildisi_eutf_disable_menu_item_language',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_disable_menu_items, 'language'),
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu Item Language Selector', 'fildisi' ),
									"desc" => esc_html__( 'If selected, main menu item will be hidden.', 'fildisi' ),
								),
							)
						);
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_fildisi_eutf_disable_menu_item_cart',
								'id' => '_fildisi_eutf_disable_menu_item_cart',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_disable_menu_items, 'cart'),
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu Item Shopping Cart', 'fildisi' ),
									"desc" => esc_html__( 'If selected, main menu item will be hidden.', 'fildisi' ),
								),
							)
						);
						// fildisi_eutf_print_admin_option(
							// array(
								// 'type' => 'checkbox',
								// 'name' => '_fildisi_eutf_disable_menu_item_login',
								// 'id' => '_fildisi_eutf_disable_menu_item_login',
								// 'value' => fildisi_eutf_array_value( $fildisi_eutf_disable_menu_items, 'login'),
								// 'label' => array(
									// "title" => esc_html__( 'Disable Main Menu Item Login', 'fildisi' ),
									// "desc" => esc_html__( 'If selected, main menu item will be hidden.', 'fildisi' ),
								// ),
							// )
						// );
						fildisi_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_fildisi_eutf_disable_menu_item_social',
								'id' => '_fildisi_eutf_disable_menu_item_social',
								'value' => fildisi_eutf_array_value( $fildisi_eutf_disable_menu_items, 'social'),
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu Item Social Icons', 'fildisi' ),
									"desc" => esc_html__( 'If selected, main menu item will be hidden.', 'fildisi' ),
								),
							)
						);

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_fildisi_eutf_disable_breadcrumbs',
								'id' => '_fildisi_eutf_disable_breadcrumbs',
								'value' => $fildisi_eutf_disable_breadcrumbs,
								'label' => array(
									"title" => esc_html__( 'Disable Breadcrumbs', 'fildisi' ),
									"desc" => esc_html__( 'If selected, breadcrumbs items will be hidden.', 'fildisi' ),
								),
							)
						);

						if ( 'page' == $post_type ) {
							if ( fildisi_eutf_woocommerce_enabled() && $post->ID == wc_get_page_id( 'shop' ) ) {
								//Skip
							} else {
								fildisi_eutf_print_admin_option(
									array(
										'type' => 'checkbox',
										'name' => '_fildisi_eutf_disable_content',
										'id' => '_fildisi_eutf_disable_content',
										'value' => $fildisi_eutf_disable_content,
										'label' => array(
											"title" => esc_html__( 'Disable Content Area', 'fildisi' ),
											"desc" => esc_html__( 'If selected, content area will be hidden (including sidebar and comments).', 'fildisi' ),
										),
									)
								);
							}
						}

						if ( 'post' == $post_type ) {
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'checkbox',
									'name' => '_fildisi_eutf_disable_media',
									'id' => '_fildisi_eutf_disable_media',
									'value' => $fildisi_eutf_disable_media,
									'label' => array(
										"title" => esc_html__( 'Disable Media Area', 'fildisi' ),
										"desc" => esc_html__( 'If selected, media area will be hidden.', 'fildisi' ),
									),
								)
							);
						}
						if ( 'portfolio' == $post_type ) {
							fildisi_eutf_print_admin_option(
								array(
									'type' => 'checkbox',
									'name' => '_fildisi_eutf_disable_recent_entries',
									'id' => '_fildisi_eutf_disable_recent_entries',
									'value' => $fildisi_eutf_disable_recent_entries,
									'label' => array(
										"title" => esc_html__( 'Disable Recent Entries', 'fildisi' ),
										"desc" => esc_html__( 'If selected, recent entries area will be hidden.', 'fildisi' ),
									),
								)
							);
						}

						fildisi_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_fildisi_eutf_disable_back_to_top',
								'id' => '_fildisi_eutf_disable_back_to_top',
								'value' => $fildisi_eutf_disable_back_to_top,
								'label' => array(
									"title" => esc_html__( 'Disable Back to Top', 'fildisi' ),
									"desc" => esc_html__( 'If selected, Back to Top button will be hidden.', 'fildisi' ),
								),
							)
						);

					?>
				</div>
			</div>
		</div>
	</div>

<?php
}

function fildisi_eutf_page_feature_section_box( $post ) {

	wp_nonce_field( 'fildisi_eutf_nonce_feature_save', '_fildisi_eutf_nonce_feature_save' );

	$post_id = $post->ID;
	fildisi_eutf_admin_get_feature_section( $post_id );

}

function fildisi_eutf_generic_options_save_postdata( $post_id , $post ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( isset( $_POST['_fildisi_eutf_nonce_feature_save'] ) && wp_verify_nonce( $_POST['_fildisi_eutf_nonce_feature_save'], 'fildisi_eutf_nonce_feature_save' ) ) {

		if ( fildisi_eutf_check_permissions( $post_id ) ) {
			fildisi_eutf_admin_save_feature_section( $post_id );
		}

	}

	if ( isset( $_POST['_fildisi_eutf_nonce_page_save'] ) && wp_verify_nonce( $_POST['_fildisi_eutf_nonce_page_save'], 'fildisi_eutf_nonce_page_save' ) ) {

		if ( fildisi_eutf_check_permissions( $post_id ) ) {

			$fildisi_eutf_page_options = array (
				array(
					'name' => 'Description',
					'id' => '_fildisi_eutf_description',
					'html' => true,
				),
				array(
					'name' => 'Details Title',
					'id' => '_fildisi_eutf_details_title',
					'html' => true,
				),
				array(
					'name' => 'Details',
					'id' => '_fildisi_eutf_details',
					'html' => true,
				),
				array(
					'name' => 'Details Link Text',
					'id' => '_fildisi_eutf_details_link_text',
					'html' => true,
				),
				array(
					'name' => 'Details Link URL',
					'id' => '_fildisi_eutf_details_link_url',
				),
				array(
					'name' => 'Details Link New Window',
					'id' => '_fildisi_eutf_details_link_new_window',
				),
				array(
					'name' => 'Details Link Extra Class',
					'id' => '_fildisi_eutf_details_link_extra_class',
				),
				array(
					'name' => 'Social Bar Layout',
					'id' => '_fildisi_eutf_social_bar_layout',
				),

				array(
					'name' => 'Backlink',
					'id' => '_fildisi_eutf_backlink_id',
				),
				array(
					'name' => 'Area Section Type',
					'id' => '_fildisi_eutf_area_section_type',
				),
				array(
					'name' => 'Area Top Padding Multiplier',
					'id' => '_fildisi_eutf_area_padding_top_multiplier',
				),
				array(
					'name' => 'Area Bottom Padding Multiplier',
					'id' => '_fildisi_eutf_area_padding_bottom_multiplier',
				),
				array(
					'name' => 'Area Image ID',
					'id' => '_fildisi_eutf_area_image_id',
				),
				array(
					'name' => 'Top Padding',
					'id' => '_fildisi_eutf_padding_top',
				),
				array(
					'name' => 'Bottom Padding',
					'id' => '_fildisi_eutf_padding_bottom',
				),
				array(
					'name' => 'Layout',
					'id' => '_fildisi_eutf_layout',
				),
				array(
					'name' => 'Sidebar',
					'id' => '_fildisi_eutf_sidebar',
				),
				array(
					'name' => 'Post Content width',
					'id' => '_fildisi_eutf_post_content_width',
				),
				array(
					'name' => 'Sidearea Area Visibility',
					'id' => '_fildisi_eutf_sidearea_visibility',
				),
				array(
					'name' => 'Sidearea Sidebar',
					'id' => '_fildisi_eutf_sidearea_sidebar',
				),
				array(
					'name' => 'Fixed Sidebar',
					'id' => '_fildisi_eutf_fixed_sidebar',
				),
				array(
					'name' => 'Header Overlapping',
					'id' => '_fildisi_eutf_header_overlapping',
				),
				array(
					'name' => 'Header Style',
					'id' => '_fildisi_eutf_header_style',
				),
				array(
					'name' => 'Navigation Anchor Menu',
					'id' => '_fildisi_eutf_anchor_navigation_menu',
				),
				array(
					'name' => 'Theme Loader',
					'id' => '_fildisi_eutf_theme_loader',
				),
				array(
					'name' => 'Scrolling Page',
					'id' => '_fildisi_eutf_scrolling_page',
				),
				array(
					'name' => 'Responsive Scrolling',
					'id' => '_fildisi_eutf_responsive_scrolling',
				),
				array(
					'name' => 'Scrolling Lock Anchors',
					'id' => '_fildisi_eutf_scrolling_lock_anchors',
				),
				array(
					'name' => 'Scrolling Direction',
					'id' => '_fildisi_eutf_scrolling_direction',
				),
				array(
					'name' => 'Scrolling Loop',
					'id' => '_fildisi_eutf_scrolling_loop',
				),
				array(
					'name' => 'Scrolling Speed',
					'id' => '_fildisi_eutf_scrolling_speed',
				),
				array(
					'name' => 'Main Navigation Menu',
					'id' => '_fildisi_eutf_main_navigation_menu',
				),
				array(
					'name' => 'Responsive Navigation Menu',
					'id' => '_fildisi_eutf_responsive_navigation_menu',
				),
				array(
					'name' => 'Menu Type',
					'id' => '_fildisi_eutf_menu_type',
				),
				array(
					'name' => 'Responsive Header Overlapping',
					'id' => '_fildisi_eutf_responsive_header_overlapping',
				),
				array(
					'name' => 'Sticky Header Type',
					'id' => '_fildisi_eutf_sticky_header_type',
				),
				array(
					'name' => 'Bottom Bar',
					'id' => '_fildisi_eutf_bottom_bar_area',
				),
				array(
					'name' => 'Footer Widgets',
					'id' => '_fildisi_eutf_footer_widgets_visibility',
				),
				array(
					'name' => 'Footer Bar',
					'id' => '_fildisi_eutf_footer_bar_visibility',
				),
				array(
					'name' => 'Disable Top Bar',
					'id' => '_fildisi_eutf_disable_top_bar',
				),
				array(
					'name' => 'Disable Logo',
					'id' => '_fildisi_eutf_disable_logo',
				),
				array(
					'name' => 'Disable Menu',
					'id' => '_fildisi_eutf_disable_menu',
				),
				array(
					'name' => 'Disable Menu Items',
					'id' => '_fildisi_eutf_disable_menu_items',
				),
				array(
					'name' => 'disable Breadcrumbs',
					'id' => '_fildisi_eutf_disable_breadcrumbs',
				),
				array(
					'name' => 'Disable Title',
					'id' => '_fildisi_eutf_disable_title',
				),
				array(
					'name' => 'Disable Media',
					'id' => '_fildisi_eutf_disable_media',
				),
				array(
					'name' => 'Disable Content',
					'id' => '_fildisi_eutf_disable_content',
				),
				array(
					'name' => 'Disable Recent Entries',
					'id' => '_fildisi_eutf_disable_recent_entries',
				),
				array(
					'name' => 'Disable Back to Top',
					'id' => '_fildisi_eutf_disable_back_to_top',
				),
			);

			$fildisi_eutf_disable_menu_items_options = array (
				array(
					'param_id' => 'search',
					'id' => '_fildisi_eutf_disable_menu_item_search',
					'default' => '',
				),
				array(
					'param_id' => 'form',
					'id' => '_fildisi_eutf_disable_menu_item_form',
					'default' => '',
				),
				array(
					'param_id' => 'language',
					'id' => '_fildisi_eutf_disable_menu_item_language',
					'default' => '',
				),
				array(
					'param_id' => 'cart',
					'id' => '_fildisi_eutf_disable_menu_item_cart',
					'default' => '',
				),
				array(
					'param_id' => 'social',
					'id' => '_fildisi_eutf_disable_menu_item_social',
					'default' => '',
				),
			);

			//Title Options
			$fildisi_eutf_page_title_options = array (
				array(
					'param_id' => 'custom',
					'id' => '_fildisi_eutf_page_title_custom',
					'default' => '',
				),
				array(
					'param_id' => 'height',
					'id' => '_fildisi_eutf_page_title_height',
					'default' => '40',
				),
				array(
					'param_id' => 'min_height',
					'id' => '_fildisi_eutf_page_title_min_height',
					'default' => '200',
				),
				array(
					'param_id' => 'bg_color',
					'id' => '_fildisi_eutf_page_title_bg_color',
					'default' => 'light',
				),
				array(
					'param_id' => 'bg_color_custom',
					'id' => '_fildisi_eutf_page_title_bg_color_custom',
					'default' => '#ffffff',
				),
				array(
					'param_id' => 'subheading_color',
					'id' => '_fildisi_eutf_page_title_subheading_color',
					'default' => 'light',
				),
				array(
					'param_id' => 'subheading_color_custom',
					'id' => '_fildisi_eutf_page_title_subheading_color_custom',
					'default' => '#ffffff',
				),
				array(
					'param_id' => 'title_color',
					'id' => '_fildisi_eutf_page_title_title_color',
					'default' => 'dark',
				),
				array(
					'param_id' => 'title_color_custom',
					'id' => '_fildisi_eutf_page_title_title_color_custom',
					'default' => '#000000',
				),
				array(
					'param_id' => 'caption_color',
					'id' => '_fildisi_eutf_page_title_caption_color',
					'default' => 'dark',
				),
				array(
					'param_id' => 'caption_color_custom',
					'id' => '_fildisi_eutf_page_title_caption_color_custom',
					'default' => '#000000',
				),
				array(
					'param_id' => 'content_bg_color',
					'id' => '_fildisi_eutf_page_title_content_bg_color',
					'default' => 'none',
				),
				array(
					'param_id' => 'content_bg_color_custom',
					'id' => '_fildisi_eutf_page_title_content_bg_color_custom',
					'default' => '#000000',
				),
				array(
					'param_id' => 'container_size',
					'id' => '_fildisi_eutf_page_title_container_size',
					'default' => '',
				),
				array(
					'param_id' => 'content_size',
					'id' => '_fildisi_eutf_page_title_content_size',
					'default' => 'large',
				),
				array(
					'param_id' => 'content_alignment',
					'id' => '_fildisi_eutf_page_title_content_alignment',
					'default' => 'center',
				),
				array(
					'param_id' => 'content_position',
					'id' => '_fildisi_eutf_page_title_content_position',
					'default' => 'center-center',
				),
				array(
					'param_id' => 'content_animation',
					'id' => '_fildisi_eutf_page_title_content_animation',
					'default' => 'fade-in',
				),
				array(
					'param_id' => 'bg_mode',
					'id' => '_fildisi_eutf_page_title_bg_mode',
					'default' => '',
				),
				array(
					'param_id' => 'bg_image_id',
					'id' => '_fildisi_eutf_page_title_bg_image_id',
					'default' => '0',
				),
				array(
					'param_id' => 'bg_position',
					'id' => '_fildisi_eutf_page_title_bg_position',
					'default' => 'center-center',
				),
				array(
					'param_id' => 'pattern_overlay',
					'id' => '_fildisi_eutf_page_title_pattern_overlay',
					'default' => '',
				),
				array(
					'param_id' => 'color_overlay',
					'id' => '_fildisi_eutf_page_title_color_overlay',
					'default' => 'dark',
				),
				array(
					'param_id' => 'color_overlay_custom',
					'id' => '_fildisi_eutf_page_title_color_overlay_custom',
					'default' => '#000000',
				),
				array(
					'param_id' => 'opacity_overlay',
					'id' => '_fildisi_eutf_page_title_opacity_overlay',
					'default' => '0',
				),
			);

			//Area Colors
			$fildisi_eutf_area_colors = array (
				array(
					'param_id' => 'custom',
					'id' => '_fildisi_eutf_area_colors_custom',
					'default' => '',
				),
				array(
					'param_id'    => 'bg_color',
					'id'          => '_fildisi_eutf_area_bg_color',
					'default'     => '#eeeeee',
				),
				array(
					'param_id'    => 'headings_color',
					'id'          => '_fildisi_eutf_area_headings_color',
					'default'     => '#000000',
				),
				array(
					'param_id'    => 'font_color',
					'id'          => '_fildisi_eutf_area_font_color',
					'default'     => '#999999',
				),
				array(
					'param_id'    => 'link_color',
					'id'          => '_fildisi_eutf_area_link_color',
					'default'     => '#FF7D88',
				),
				array(
					'param_id'    => 'hover_color',
					'id'          => '_fildisi_eutf_area_hover_color',
					'default'     => '#000000',
				),
				array(
					'param_id'    => 'border_color',
					'id'          => '_fildisi_eutf_area_border_color',
					'default'     => '#e0e0e0',
				),
				array(
					'param_id'    => 'button_color',
					'id'          =>'_fildisi_eutf_area_button_color',
					'default'     => 'primary-1',
				),
				array(
					'param_id'    => 'button_hover_color',
					'id'          =>'_fildisi_eutf_button_hover_color',
					'default'     => 'black',
				),
			);

			//Update Single custom fields
			foreach ( $fildisi_eutf_page_options as $value ) {
				$allow_html = ( isset( $value['html'] ) ? $value['html'] : false );
				if( $allow_html ) {
					$new_meta_value = ( isset( $_POST[$value['id']] ) ? wp_filter_post_kses( $_POST[$value['id']] ) : '' );
				} else {
					$new_meta_value = ( isset( $_POST[$value['id']] ) ? sanitize_text_field( $_POST[$value['id']] ) : '' );
				}
				$meta_key = $value['id'];


				$meta_value = get_post_meta( $post_id, $meta_key, true );

				if ( '' != $new_meta_value  && '' == $meta_value ) {
					if ( !add_post_meta( $post_id, $meta_key, $new_meta_value, true ) ) {
						update_post_meta( $post_id, $meta_key, $new_meta_value );
					}
				} elseif ( '' != $new_meta_value && $new_meta_value != $meta_value ) {
					update_post_meta( $post_id, $meta_key, $new_meta_value );
				} elseif ( '' == $new_meta_value && '' != $meta_value ) {
					delete_post_meta( $post_id, $meta_key );
				}
			}

			//Update Menu Items Visibility array
			fildisi_eutf_update_meta_array( $post_id, '_fildisi_eutf_disable_menu_items', $fildisi_eutf_disable_menu_items_options );
			//Update Title Options array
			fildisi_eutf_update_meta_array( $post_id, '_fildisi_eutf_custom_title_options', $fildisi_eutf_page_title_options );
			//Update Area Colors array
			fildisi_eutf_update_meta_array( $post_id, '_fildisi_eutf_area_colors', $fildisi_eutf_area_colors );
		}
	}

}

/**
 * Function update meta array
 */
function fildisi_eutf_update_meta_array( $post_id, $param_id, $param_array_options ) {

	$array_options = array();

	if( !empty( $param_array_options ) ) {

		foreach ( $param_array_options as $value ) {

			$meta_key = $value['param_id'];
			$meta_default = $value['default'];

			$allow_html = ( isset( $value['html'] ) ? $value['html'] : false );
			if( $allow_html ) {
				$new_meta_value = ( isset( $_POST[$value['id']] ) ? wp_filter_post_kses( $_POST[$value['id']] ) : $meta_default );
			} else {
				$new_meta_value = ( isset( $_POST[$value['id']] ) ? sanitize_text_field( $_POST[$value['id']] ) : $meta_default );
			}

			if( !empty( $new_meta_value ) ) {
				$array_options[$meta_key] = $new_meta_value;
			}
		}

	}

	if( !empty( $array_options ) ) {
		update_post_meta( $post_id, $param_id, $array_options );
	} else {
		delete_post_meta( $post_id, $param_id );
	}
}

/**
 * Function to check post type permissions
 */

function fildisi_eutf_check_permissions( $post_id ) {

	if ( 'post' == $_POST['post_type'] ) {
		if ( !current_user_can( 'edit_post', $post_id ) ) {
			return false;
		}
	} else {
		if ( !current_user_can( 'edit_page', $post_id ) ) {
			return false;
		}
	}
	return true;
}

/**
 * Function to print menu selector
 */
function fildisi_eutf_print_menu_selection( $menu_id, $id, $name, $default = 'none' ) {

	?>
	<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
		<option value="" <?php selected( '', $menu_id ); ?>>
			<?php
				if ( 'none' == $default ){
					esc_html_e( 'None', 'fildisi' );
				} else {
					esc_html_e( '-- Inherit --', 'fildisi' );
				}
			?>
		</option>
	<?php
		$menus = wp_get_nav_menus();
		if ( ! empty( $menus ) ) {
			foreach ( $menus as $item ) {
	?>
				<option value="<?php echo esc_attr( $item->term_id ); ?>" <?php selected( $item->term_id, $menu_id ); ?>>
					<?php echo esc_html( $item->name ); ?>
				</option>
	<?php
			}
		}
	?>
	</select>
	<?php
}

/**
 * Function to print layout selector
 */
function fildisi_eutf_print_layout_selection( $layout, $id, $name ) {

	$layouts = array(
		'' => esc_html__( '-- Inherit --', 'fildisi' ),
		'none' => esc_html__( 'Full Width', 'fildisi' ),
		'left' => esc_html__( 'Left Sidebar', 'fildisi' ),
		'right' => esc_html__( 'Right Sidebar', 'fildisi' ),
	);

	?>
	<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
	<?php
		foreach ( $layouts as $key => $value ) {
			if ( $value ) {
	?>
				<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $layout ); ?>><?php echo esc_html( $value ); ?></option>
	<?php
			}
		}
	?>
	</select>
	<?php
}

/**
 * Function to print sidebar selector
 */
function fildisi_eutf_print_sidebar_selection( $sidebar, $id, $name ) {
	global $wp_registered_sidebars;

	?>
	<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
		<option value="" <?php selected( '', $sidebar ); ?>><?php echo esc_html__( '-- Inherit --', 'fildisi' ); ?></option>
	<?php
	foreach ( $wp_registered_sidebars as $key => $value ) {
		?>
		<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $sidebar ); ?>><?php echo esc_html( $value['name'] ); ?></option>
		<?php
	}
	?>
	</select>
	<?php
}

/**
 * Function to print page selector
 */
function fildisi_eutf_print_page_selection( $page_id, $id, $name ) {

?>
	<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
		<option value="" <?php selected( '', $page_id ); ?>>
			<?php esc_html_e( '-- Inherit --', 'fildisi' ); ?>
		</option>
<?php
		$pages = get_pages();
		foreach ( $pages as $page ) {
?>
			<option value="<?php echo esc_attr( $page->ID ); ?>" <?php selected( $page->ID, $page_id ); ?>>
				<?php echo esc_html( $page->post_title ); ?>
			</option>
<?php
		}
?>
	</select>
<?php

}


/**
 * Function to print page selector
 */
function fildisi_eutf_print_area_selection( $area_id, $id, $name ) {

?>
	<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
		<option value="" <?php selected( '', $area_id ); ?>>
			<?php esc_html_e( '-- Inherit --', 'fildisi' ); ?>
		</option>
		<option value="none" <?php selected( 'none', $area_id ); ?>>
			<?php esc_html_e( '-- None --', 'fildisi' ); ?>
		</option>
<?php
		$args = array( 'post_type' => 'area-item', 'numberposts' => -1 );
		$posts = get_posts( $args );
		if ( ! empty ( $posts ) ) {
			foreach ( $posts as $post ) {
?>
			<option value="<?php echo esc_attr( $post->ID ); ?>" <?php selected( $post->ID, $area_id ); ?>>
				<?php echo esc_html( $post->post_title ); ?>
			</option>
<?php
			}
		}
		wp_reset_postdata();
?>
	</select>
<?php

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
