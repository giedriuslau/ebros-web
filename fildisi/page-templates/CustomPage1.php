<?php get_header(); ?>



<?php the_post(); ?>


<div class="eut-single-wrapper">

	<!-- CONTENT -->

	<div id="eut-content" class="clearfix <?php echo fildisi_eutf_sidebar_class('page'); ?>">

		<div class="eut-content-wrapper">

			<!-- MAIN CONTENT -->

			<div id="eut-main-content">

				<div class="eut-main-content-wrapper clearfix">



					<!-- PAGE CONTENT -->

					<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php the_content(); ?>
						<h1>Test</h1>

					</div>

					<!-- END PAGE CONTENT -->

				</div>

			</div>

			<!-- END MAIN CONTENT -->



			<?php fildisi_eutf_set_current_view('page'); ?>

			<?php get_sidebar(); ?>



		</div>

	</div>

	<!-- END CONTENT -->



	<?php if (fildisi_eutf_visibility('page_comments_visibility')) { ?>

		<div id="eut-comments-section">

			<?php comments_template(); ?>

		</div>

	<?php } ?>



</div>



<?php get_footer(); ?>



<?php



//Omit closing PHP tag to avoid accidental whitespace output errors.
