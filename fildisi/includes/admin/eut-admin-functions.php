<?php

/*
*	Admin functions and definitions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

/**
 * Default hidden metaboxes for portfolio
 */
function fildisi_eutf_change_default_hidden( $hidden, $screen ) {
    if ( 'portfolio' == $screen->id ) {
        $hidden = array_flip( $hidden );
        unset( $hidden['portfolio_categorydiv'] ); //show portfolio category box
        $hidden = array_flip( $hidden );
        $hidden[] = 'postexcerpt';
		$hidden[] = 'postcustom';
		$hidden[] = 'commentstatusdiv';
		$hidden[] = 'commentsdiv';
		$hidden[] = 'authordiv';
    }
    return $hidden;
}
add_filter( 'default_hidden_meta_boxes', 'fildisi_eutf_change_default_hidden', 10, 2 );

/**
 * Enqueue scripts and styles for the back end.
 */
function fildisi_eutf_backend_scripts( $hook ) {
	global $post, $pagenow;

	wp_register_style( 'fildisi-eutf-page-feature-section', get_template_directory_uri() . '/includes/css/eut-page-feature-section.css', array(), time() );
	wp_register_style( 'fildisi-eutf-admin-meta', get_template_directory_uri() . '/includes/css/eut-admin-meta.css', array(), time() );
	wp_register_style( 'fildisi-eutf-custom-sidebars', get_template_directory_uri() . '/includes/css/eut-custom-sidebars.css', array(), time() );
	wp_register_style( 'fildisi-eutf-status', get_template_directory_uri() . '/includes/css/eut-status.css', array(), time() );
	wp_register_style( 'fildisi-eutf-admin-panel', get_template_directory_uri() . '/includes/css/eut-admin-panel.css', array(), time() );
	wp_register_style( 'fildisi-eutf-custom-nav-menu', get_template_directory_uri() . '/includes/css/eut-custom-nav-menu.css', array(), time() );
	wp_register_style( 'select2-css', get_template_directory_uri() . '/includes/admin/extensions/vendor_support/vendor/select2/select2.css', array(), time() );

	$fildisi_eutf_upload_slider_texts = array(
		'modal_title' => esc_html__( 'Insert Images', 'fildisi' ),
		'modal_button_title' => esc_html__( 'Insert Images', 'fildisi' ),
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
		'nonce_post_titles' => wp_create_nonce( 'fildisi-eutf-get-post-titles' ),
		'nonce_post_titles_select_lookup' => wp_create_nonce( 'fildisi-eutf-post-select-lookup' ),
		'nonce_feature_slider_media' => wp_create_nonce( 'fildisi-eutf-get-feature-slider-media' ),
		'nonce_slider_media' => wp_create_nonce( 'fildisi-eutf-get-slider-media' ),
	);

	$fildisi_eutf_upload_image_replace_texts = array(
		'modal_title' => esc_html__( 'Replace Image', 'fildisi' ),
		'modal_button_title' => esc_html__( 'Replace Image', 'fildisi' ),
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
		'nonce_replace' => wp_create_nonce( 'fildisi-eutf-get-replaced-image' ),
	);

	$fildisi_eutf_upload_media_texts = array(
		'modal_title' => esc_html__( 'Insert Media', 'fildisi' ),
		'modal_button_title' => esc_html__( 'Insert Media', 'fildisi' ),
	);

	$fildisi_eutf_feature_section_texts = array(
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
		'nonce_map_point' => wp_create_nonce( 'fildisi-eutf-get-map-point' ),
	);

	$fildisi_eutf_custom_sidebar_texts = array(
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
		'nonce_custom_sidebar' => wp_create_nonce( 'fildisi-eutf-get-custom-sidebar' ),
	);

	wp_register_script( 'fildisi-eutf-status', get_template_directory_uri() . '/includes/js/eut-status.js', array( 'jquery'), time(), false );
	wp_register_script( 'fildisi-eutf-codes-script', get_template_directory_uri().'/includes/js/eut-codes.js', array( 'jquery'), time(), false );

	wp_register_script( 'fildisi-eutf-custom-sidebars', get_template_directory_uri() . '/includes/js/eut-custom-sidebars.js', array( 'jquery'), time(), false );
	wp_localize_script( 'fildisi-eutf-custom-sidebars', 'fildisi_eutf_custom_sidebar_texts', $fildisi_eutf_custom_sidebar_texts );

	wp_register_script( 'fildisi-eutf-upload-slider-script', get_template_directory_uri() . '/includes/js/eut-upload-slider.js', array( 'jquery'), time(), false );
	wp_localize_script( 'fildisi-eutf-upload-slider-script', 'fildisi_eutf_upload_slider_texts', $fildisi_eutf_upload_slider_texts );

	wp_register_script( 'fildisi-eutf-upload-feature-slider-script', get_template_directory_uri() . '/includes/js/eut-upload-feature-slider.js', array( 'jquery'), time(), false );
	wp_localize_script( 'fildisi-eutf-upload-feature-slider-script', 'fildisi_eutf_upload_feature_slider_texts', $fildisi_eutf_upload_slider_texts );

	wp_register_script( 'fildisi-eutf-upload-image-replace-script', get_template_directory_uri() . '/includes/js/eut-upload-image-replace.js', array( 'jquery'), time(), false );
	wp_localize_script( 'fildisi-eutf-upload-image-replace-script', 'fildisi_eutf_upload_image_replace_texts', $fildisi_eutf_upload_image_replace_texts );

	wp_register_script( 'fildisi-eutf-upload-simple-media-script', get_template_directory_uri() . '/includes/js/eut-upload-simple.js', array( 'jquery'), time(), false );
	wp_localize_script( 'fildisi-eutf-upload-simple-media-script', 'fildisi_eutf_upload_media_texts', $fildisi_eutf_upload_media_texts );

	wp_register_script( 'fildisi-eutf-page-feature-section-script', get_template_directory_uri() . '/includes/js/eut-page-feature-section.js', array( 'jquery', 'wp-color-picker' ), time(), false );
	wp_localize_script( 'fildisi-eutf-page-feature-section-script', 'fildisi_eutf_feature_section_texts', $fildisi_eutf_feature_section_texts );

	wp_register_script( 'fildisi-eutf-post-options-script', get_template_directory_uri() . '/includes/js/eut-post-options.js', array( 'jquery'), time(), false );
	wp_register_script( 'fildisi-eutf-portfolio-options-script', get_template_directory_uri() . '/includes/js/eut-portfolio-options.js', array( 'jquery'), time(), false );

	wp_register_script( 'fildisi-eutf-custom-nav-menu-script', get_template_directory_uri().'/includes/js/eut-custom-nav-menu.js', array( 'jquery'), time(), false );

	wp_register_script( 'select2-js', get_template_directory_uri().'/includes/admin/extensions/vendor_support/vendor/select2/select2.js', array( 'jquery'), time(), false );

	if ( $hook == 'post-new.php' || $hook == 'post.php' ) {

		$enable_select2 = false;
		if ( 'product' != $post->post_type && 'tribe_events' != $post->post_type ) {
			$enable_select2 = true;
		}

		$feature_section_post_types = fildisi_eutf_option( 'feature_section_post_types' );

		if ( !empty( $feature_section_post_types ) && in_array( $post->post_type, $feature_section_post_types ) && 'attachment' != $post->post_type ) {

			wp_enqueue_style( 'fildisi-eutf-admin-meta' );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'fildisi-eutf-page-feature-section' );
			if ( $enable_select2 ) {
				wp_enqueue_style( 'select2-css' );
			}

			wp_enqueue_script( 'fildisi-eutf-upload-simple-media-script' );
			wp_enqueue_script( 'fildisi-eutf-upload-slider-script' );
			wp_enqueue_script( 'fildisi-eutf-upload-feature-slider-script' );
			wp_enqueue_script( 'fildisi-eutf-upload-image-replace-script' );
			if ( $enable_select2 ) {
				wp_enqueue_script( 'select2-js' );
			}
			wp_enqueue_script( 'fildisi-eutf-page-feature-section-script' );
		}


        if ( 'post' === $post->post_type ) {

			wp_enqueue_style( 'fildisi-eutf-admin-meta' );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'fildisi-eutf-page-feature-section' );

			wp_enqueue_script( 'fildisi-eutf-upload-simple-media-script' );
			wp_enqueue_script( 'fildisi-eutf-upload-slider-script' );
			wp_enqueue_script( 'fildisi-eutf-upload-feature-slider-script' );
			wp_enqueue_script( 'fildisi-eutf-upload-image-replace-script' );
			wp_enqueue_script( 'fildisi-eutf-page-feature-section-script' );
			wp_enqueue_script( 'fildisi-eutf-post-options-script' );

        } else if ( 'page' === $post->post_type || 'portfolio' === $post->post_type || 'product' === $post->post_type || 'tribe_events' === $post->post_type ) {

			wp_enqueue_style( 'fildisi-eutf-admin-meta' );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'fildisi-eutf-page-feature-section' );

			wp_enqueue_script( 'fildisi-eutf-upload-simple-media-script' );
			wp_enqueue_script( 'fildisi-eutf-upload-slider-script' );
			wp_enqueue_script( 'fildisi-eutf-upload-feature-slider-script' );
			wp_enqueue_script( 'fildisi-eutf-upload-image-replace-script' );
			wp_enqueue_script( 'fildisi-eutf-page-feature-section-script' );

			wp_enqueue_script( 'fildisi-eutf-portfolio-options-script' );

        } else if ( 'testimonial' === $post->post_type ) {

			wp_enqueue_style( 'fildisi-eutf-admin-meta' );

        }
    }

	if ( $hook == 'edit-tags.php' || $hook == 'term.php') {
		wp_enqueue_style( 'fildisi-eutf-admin-meta' );
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_style( 'fildisi-eutf-page-feature-section' );


		wp_enqueue_media();
		wp_enqueue_script( 'fildisi-eutf-page-feature-section-script' );
		wp_enqueue_script( 'fildisi-eutf-upload-image-replace-script' );

	}

	if ( $hook == 'nav-menus.php' ) {
		wp_enqueue_style( 'fildisi-eutf-custom-nav-menu' );

		wp_enqueue_media();
		wp_enqueue_script( 'fildisi-eutf-upload-simple-media-script' );
		wp_enqueue_script( 'fildisi-eutf-custom-nav-menu-script' );
	}

	//Admin Screens
	if ( isset( $_GET['page'] ) && ( 'fildisi' == $_GET['page'] ) ) {
		wp_enqueue_style( 'fildisi-eutf-admin-panel' );
	}
	if ( isset( $_GET['page'] ) && ( 'fildisi-sidebars' == $_GET['page'] ) ) {
		wp_enqueue_style( 'fildisi-eutf-custom-sidebars' );
		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_script( 'fildisi-eutf-custom-sidebars' );
	}
	if ( isset( $_GET['page'] ) && ( 'fildisi-status' == $_GET['page'] ) ) {
		wp_enqueue_style( 'fildisi-eutf-status' );
		wp_enqueue_script( 'fildisi-eutf-status' );
	}
	if ( isset( $_GET['page'] ) && ( 'fildisi-import' == $_GET['page'] ) ) {
		wp_enqueue_style( 'fildisi-eutf-admin-panel' );
	}

	if ( isset( $_GET['page'] ) && ( 'fildisi-codes' == $_GET['page'] ) ) {
		wp_enqueue_style( 'fildisi-eutf-admin-panel' );
		wp_enqueue_code_editor( array( 'type' => 'text/html' ) );
		wp_enqueue_script( 'fildisi-eutf-codes-script' );
	}


	wp_enqueue_style(
		'redux-custom-css',
		get_template_directory_uri() . '/includes/css/eut-redux-panel.css',
		array(),
		time(),
		'all'
	);



}
add_action( 'admin_enqueue_scripts', 'fildisi_eutf_backend_scripts', 10, 1 );

/**
 * Helper function to get custom fields with fallback
 */
function fildisi_eutf_post_meta( $id, $fallback = false ) {
	global $post;
	$post_id = $post->ID;
	if ( $fallback == false ) $fallback = '';
	$post_meta = get_post_meta( $post_id, $id, true );
	$output = ( $post_meta !== '' ) ? $post_meta : $fallback;
	return $output;
}

function fildisi_eutf_admin_post_meta( $post_id, $id, $fallback = false ) {
	if ( $fallback == false ) $fallback = '';
	$post_meta = get_post_meta( $post_id, $id, true );
	$output = ( $post_meta !== '' ) ? $post_meta : $fallback;
	return $output;
}

function fildisi_eutf_get_term_meta( $term_id, $meta_key ) {
	$fildisi_eutf_term_meta  = '';

	if ( function_exists( 'get_term_meta' ) ) {
		$fildisi_eutf_term_meta = get_term_meta( $term_id, $meta_key, true );
	}
	if( empty ( $fildisi_eutf_term_meta ) ) {
		$fildisi_eutf_term_meta = array();
	}
	return $fildisi_eutf_term_meta;
}

function fildisi_eutf_update_term_meta( $term_id , $meta_key, $meta_value ) {

	if ( function_exists( 'update_term_meta' ) ) {
		update_term_meta( $term_id, $meta_key, $meta_value );
	}
}

/**
 * Helper function to get theme options with fallback
 */
function fildisi_eutf_option( $id, $fallback = false, $param = false ) {
	global $fildisi_eutf_options;
	$eut_theme_options = $fildisi_eutf_options;

	if ( $fallback == false ) $fallback = '';
	$output = ( isset($eut_theme_options[$id]) && $eut_theme_options[$id] !== '' ) ? $eut_theme_options[$id] : $fallback;
	if ( !empty($eut_theme_options[$id]) && $param ) {
		$output = ( isset($eut_theme_options[$id][$param]) && $eut_theme_options[$id][$param] !== '' ) ? $eut_theme_options[$id][$param] : $fallback;
		if ( 'font-family' == $param ) {
			$output = urldecode( $output );
			if ( strpos($output, ' ') && !strpos($output, ',') ) {
				$output = '"' . $output . '"';
			}
		}
	}
	return $output;
}

/**
 * Helper function to print css code if not empty
 */
function fildisi_eutf_css_option( $id, $fallback = false, $param = false ) {
	$option = fildisi_eutf_option( $id, $fallback, $param );
	if ( !empty( $option ) && 0 !== $option && $param ) {
		return $param . ': ' . $option . ';';
	}
}

/**
 * Helper function to get array value with fallback
 */
function fildisi_eutf_array_value( $input_array, $id, $fallback = false, $param = false ) {

	if ( $fallback == false ) $fallback = '';
	$output = ( isset($input_array[$id]) && $input_array[$id] !== '' ) ? $input_array[$id] : $fallback;
	if ( !empty($input_array[$id]) && $param ) {
		$output = ( isset($input_array[$id][$param]) && $input_array[$id][$param] !== '' ) ? $input_array[$id][$param] : $fallback;
	}
	return $output;
}

/**
 * Helper function to return trimmed css code
 */
if ( ! function_exists( 'fildisi_eutf_compress_css' ) ) {
	function fildisi_eutf_compress_css( $css ) {
		$css_trim =  preg_replace( '/\s+/', ' ', $css );
		return $css_trim;
	}
}

/**
 * Helper functions to set/get current template
 */
function fildisi_eutf_set_current_view( $id ) {
	global $fildisi_eutf_options;
	$fildisi_eutf_options['current_view'] = $id;
}
function fildisi_eutf_get_current_view( $fallback = '' ) {
	global $fildisi_eutf_options;
	$fildisi_eutf_theme_options = $fildisi_eutf_options;

	if ( $fallback == false ) $fallback = '';
	$output = ( isset($fildisi_eutf_theme_options['current_view']) && $fildisi_eutf_theme_options['current_view'] !== '' ) ? $fildisi_eutf_theme_options['current_view'] : $fallback;
	return $output;
}

/**
 * Helper function convert hex to rgb
 */
function fildisi_eutf_hex2rgb( $hex ) {
	$hex = str_replace("#", "", $hex);

	if(strlen($hex) == 3) {
		$r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1) );
		$g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1) );
		$b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1) );
	} else {
		$r = hexdec( substr( $hex, 0, 2) );
		$g = hexdec( substr( $hex, 2, 2) );
		$b = hexdec( substr( $hex, 4, 2) );
	}
	$rgb = array($r, $g, $b);
	return implode(",", $rgb);
}

/**
 * Helper function to get theme visibility options
 */
function fildisi_eutf_visibility( $id, $fallback = '' ) {
	$visibility = fildisi_eutf_option( $id, $fallback  );
	if ( '1' == $visibility ) {
		return true;
	}
	return false;
}

/**
 * Get Color
 */
function fildisi_eutf_get_color( $color = 'dark', $color_custom = '#000000' ) {

	switch( $color ) {

		case 'dark':
			$color_custom = '#000000';
			break;
		case 'light':
			$color_custom = '#ffffff';
			break;
		case 'primary-1':
			$color_custom = fildisi_eutf_option( 'body_primary_1_color' );
			break;
		case 'primary-2':
			$color_custom = fildisi_eutf_option( 'body_primary_2_color' );
			break;
		case 'primary-3':
			$color_custom = fildisi_eutf_option( 'body_primary_3_color' );
			break;
		case 'primary-4':
			$color_custom = fildisi_eutf_option( 'body_primary_4_color' );
			break;
		case 'primary-5':
			$color_custom = fildisi_eutf_option( 'body_primary_5_color' );
			break;
		case 'primary-6':
			$color_custom = fildisi_eutf_option( 'body_primary_6_color' );
			break;
	}

	return $color_custom;
}

/**
 * Backend Theme Activation Actions
 */
function fildisi_eutf_backend_theme_activation() {
	global $pagenow;

	if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) {

		$catalog = array(
			'width'   => '500',    // px
			'height'  => '500',    // px
			'crop'	  => 1,        // true
		);

		$single = array(
			'width'   => '800',    // px
			'height'  => '800',    // px
			'crop'    => 1,        // true
		);

		$thumbnail = array(
			'width'   => '120',    // px
			'height'  => '120',    // px
			'crop'    => 1,        // true
		);

		update_option( 'shop_catalog_image_size', $catalog );
		update_option( 'shop_single_image_size', $single );
		update_option( 'shop_thumbnail_image_size', $thumbnail );
		update_option( 'woocommerce_enable_lightbox', false );

		//Redirect to Welcome
		header( 'Location: ' . esc_url( admin_url() ) . 'admin.php?page=fildisi' ) ;
	}
}

add_action('admin_init','fildisi_eutf_backend_theme_activation');

/**
 * Check if Revolution slider is active
 */

/**
 * Check if to replace Backend Logo
 */
function fildisi_eutf_admin_login_logo() {
	$replace_logo = fildisi_eutf_option( 'replace_admin_logo' );
	if ( $replace_logo ) {
		$admin_logo = fildisi_eutf_option( 'admin_logo','','url' );
		$admin_logo_height = fildisi_eutf_option( 'admin_logo_height','84');
		$admin_logo_height = preg_match('/(px|em|\%|pt|cm)$/', $admin_logo_height) ? $admin_logo_height : $admin_logo_height . 'px';
		if( empty( $admin_logo ) ) {
			$admin_logo = fildisi_eutf_option( 'logo','','url' );
		}
		if ( !empty( $admin_logo ) ) {
			$admin_logo = str_replace( array( 'http:', 'https:' ), '', $admin_logo );
			echo "
			<style>
			.login h1 a {
				background-image: url('" . esc_url( $admin_logo ) . "');
				width: 100%;
				max-width: 300px;
				background-size: auto " . esc_attr( $admin_logo_height ) . ";
				height: " . esc_attr( $admin_logo_height ) . ";
			}
			</style>
			";
		}
	}

}
add_action( 'login_head', 'fildisi_eutf_admin_login_logo' );

function fildisi_eutf_login_headerurl( $url ){
	$replace_logo = fildisi_eutf_option( 'replace_admin_logo' );
	if ( $replace_logo ) {
		return esc_url( home_url( '/' ) );
	}
	return esc_url( $url );
}
add_filter('login_headerurl', 'fildisi_eutf_login_headerurl');

function fildisi_eutf_login_headertitle( $title ) {
	$replace_logo = fildisi_eutf_option( 'replace_admin_logo' );
	if ( $replace_logo ) {
		return esc_attr( get_bloginfo( 'name' ) );
	}
	return esc_attr( $title );
}
add_filter('login_headertext', 'fildisi_eutf_login_headertitle' );

/**
 * Disable SEO Page Analysis
 */
function fildisi_eutf_disable_page_analysis( $bool ) {
	if( '1' == fildisi_eutf_option( 'disable_seo_page_analysis', '0' ) ) {
		return false;
	}
	return $bool;
}
add_filter( 'wpseo_use_page_analysis', 'fildisi_eutf_disable_page_analysis' );


/**
 * Scroll Check
 */
if ( ! function_exists( 'fildisi_eutf_scroll_check' ) ) {
	function fildisi_eutf_scroll_check() {
		$scroll_mode = fildisi_eutf_option( 'scroll_mode', 'auto' );
		if ( 'on' == $scroll_mode ) {
			return true;
		} elseif ( 'off' == $scroll_mode ) {
			return false;
		} else {
			return fildisi_eutf_browser_webkit_check();
		}
	}
}

/**
 * Browser Webkit Check
 */
if ( ! function_exists( 'fildisi_eutf_browser_webkit_check' ) ) {
	function fildisi_eutf_browser_webkit_check() {
		if ( function_exists( 'fildisi_ext_browser_webkit_check' ) ) {
			return fildisi_ext_browser_webkit_check();
		} else {
			return false;
		}
	}
}

/**
 * Add Hooks for Page Redirect ( Coming Soon )
 */
add_filter( 'template_include', 'fildisi_eutf_redirect_page_template', 99 );

if ( ! function_exists( 'fildisi_eutf_redirect_page_template' ) ) {
	function fildisi_eutf_redirect_page_template( $template ) {
		if ( fildisi_eutf_visibility('coming_soon_enabled' )  && !is_user_logged_in() ) {
			$redirect_page = fildisi_eutf_option( 'coming_soon_page' );
			$redirect_template = fildisi_eutf_option( 'coming_soon_template' );
			if ( !empty( $redirect_page ) && 'content' == $redirect_template ) {
				$new_template = get_template_directory() . '/page-templates/template-content-only.php';
				return $new_template ;
			}
		}
		return $template;
	}
}

add_filter( 'template_redirect', 'fildisi_eutf_redirect' );

if ( ! function_exists( 'fildisi_eutf_redirect' ) ) {
	function fildisi_eutf_redirect() {
		if ( fildisi_eutf_visibility('coming_soon_enabled' ) && !is_user_logged_in() ) {
			$redirect_page = fildisi_eutf_option( 'coming_soon_page' );
			if ( !empty( $redirect_page )
				&& !in_array( $GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php') )
				&& !is_admin()
				&& !is_page( $redirect_page ) ) {
				wp_redirect( get_permalink( $redirect_page ) );
				exit();
			}
		}
		return false;
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
