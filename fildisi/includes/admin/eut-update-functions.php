<?php

/*
*	Theme update functions
*
 * 	@version	1.0
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
*/

/**
 * Display theme update notices in the admin area
 */
function fildisi_eutf_admin_notices() {

	$message = '';
	if ( is_super_admin() ) {

		if ( ( defined( 'FILDISI_EXT_VERSION' ) && version_compare( '2.0', FILDISI_EXT_VERSION, '>' ) ) && !get_user_meta( get_current_user_id(), 'fildisi_eutf_dismissed_notice_update_plugins', true ) ) {

		$plugins_link = 'admin.php?page=fildisi-tgmpa-install-plugins';
			$message = esc_html__( "Note: Fildisi v2 is a major theme release so be sure to update the required plugins, especially Fildisi Extension, to avoid any compatibility issues.", 'fildisi' ) .
						" <a href='" . esc_url( admin_url() . $plugins_link ) . "'>" . esc_html__( "Theme Plugins", 'fildisi' ) . "</a>";
			$message .=  '<br/><span><a href="' .esc_url( wp_nonce_url( add_query_arg( 'fildisi-eutf-dismiss', 'dismiss_update_plugins_notice' ), 'fildisi-eutf-dismiss-' . esc_attr( get_current_user_id() ) ) ) . '" class="dismiss-notice" target="_parent">' . esc_html__( "Dismiss this notice", 'fildisi' ) . '</a><span>';
			add_settings_error(
				'eut-theme-update-message',
				'plugins_update_required',
				$message,
				'updated'
			);
			if ( 'options-general' !== $GLOBALS['current_screen']->parent_base ) {
				settings_errors( 'eut-theme-update-message' );
			}

		}

		if ( !class_exists( 'Envato_Market' ) && !get_user_meta( get_current_user_id(), 'fildisi_eutf_dismissed_notice_envato_market', true ) ) {

			$envato_market_link = 'admin.php?page=fildisi-tgmpa-install-plugins';
			$message = esc_html__( "Note:", "fildisi" ) . " " . esc_html__( "Envato official solution is recommended for theme updates using the new Envato Market API.", 'fildisi' ) .
					"<br/>" . esc_html__( "You can now update the theme using the", 'fildisi' ) . " " . "<a href='" . esc_url( admin_url() . $envato_market_link ) . "'>" . esc_html__( "Envato Market", 'fildisi' ) . "</a>" . " ". esc_html__( "plugin", 'fildisi' ) . "." .
					" " . esc_html__( "For more information read the related article in our", 'fildisi' ) . " " . "<a href='//docs.euthemians.com/tutorials/envato-market-wordpress-plugin-for-theme-updates/' target='_blank'>" . esc_html__( "documentation", 'fildisi' ) . "</a>.";

			$message .=  '<br/><span><a href="' .esc_url( wp_nonce_url( add_query_arg( 'fildisi-eutf-dismiss', 'dismiss_envato_market_notice' ), 'fildisi-eutf-dismiss-' . esc_attr( get_current_user_id() ) ) ) . '" class="dismiss-notice" target="_parent">' . esc_html__( "Dismiss this notice", 'fildisi' ) . '</a><span>';

			add_settings_error(
				'eut-envato-market-message',
				'plugins_update_required',
				$message,
				'updated'
			);
			if ( 'options-general' !== $GLOBALS['current_screen']->parent_base ) {
				settings_errors( 'eut-envato-market-message' );
			}

		}

		if ( !get_user_meta( get_current_user_id(), 'fildisi_eutf_dismissed_notice_instagram_removal', true ) ) {

			//Instagram Legacy API
			$message = esc_html__( "Important Notice:", "fildisi" ) . " " . esc_html__( "The Instagram Legacy API Platform will be shut down on June 29, 2020.", 'fildisi' ) .
					" " . esc_html__( "Future releases of the theme will exclude the Instagram feed page builder elements and/or widgets.", 'fildisi' ) .
					"<br/>" . esc_html__( "For more information read the related article in our", 'fildisi' ) . " " . "<a href='//docs.euthemians.com/tutorials/instagram-feed-removal//' target='_blank'>" . esc_html__( "documentation", 'fildisi' ) . "</a>.";

			$message .=  '<br/><span><a href="' .esc_url( wp_nonce_url( add_query_arg( 'fildisi-eutf-dismiss', 'dismiss_instagram_removal_notice' ), 'fildisi-eutf-dismiss-' . esc_attr( get_current_user_id() ) ) ) . '" class="dismiss-notice" target="_parent">' . esc_html__( "Dismiss this notice", 'fildisi' ) . '</a><span>';

			add_settings_error(
				'eut-instagram-removal-message',
				'plugins_update_required',
				$message,
				'updated'
			);
			if ( 'options-general' !== $GLOBALS['current_screen']->parent_base ) {
				settings_errors( 'eut-instagram-removal-message' );
			}
		}

	}

}
add_action( 'admin_notices', 'fildisi_eutf_admin_notices' );

/**
 * Dismiss notices and update user meta data
 */
function fildisi_eutf_notice_dismiss() {
	if ( isset( $_GET['fildisi-eutf-dismiss'] ) && check_admin_referer( 'fildisi-eutf-dismiss-' . get_current_user_id() ) ) {
		$dismiss = $_GET['fildisi-eutf-dismiss'];
		if ( 'dismiss_envato_market_notice' == $dismiss ) {
			update_user_meta( get_current_user_id(), 'fildisi_eutf_dismissed_notice_envato_market' , 1 );
		} else if ( 'dismiss_update_plugins_notice' == $dismiss ) {
			update_user_meta( get_current_user_id(), 'fildisi_eutf_dismissed_notice_update_plugins' , 1 );
		} else if ( 'dismiss_instagram_removal_notice' == $dismiss ) {
			update_user_meta( get_current_user_id(), 'fildisi_eutf_dismissed_notice_instagram_removal' , 1 );
		}
	}
}
add_action( 'admin_head', 'fildisi_eutf_notice_dismiss' );

/**
 * Delete metadata for admin notices when switching themes
 */
function fildisi_eutf_update_dismiss() {
	delete_metadata( 'user', null, 'fildisi_eutf_dismissed_notice_envato_market', null, true );
	delete_metadata( 'user', null, 'fildisi_eutf_dismissed_notice_update_plugins', null, true );
	delete_metadata( 'user', null, 'fildisi_eutf_dismissed_notice_instagram_removal', null, true );
}
add_action( 'switch_theme', 'fildisi_eutf_update_dismiss' );


//Omit closing PHP tag to avoid accidental whitespace output errors.
