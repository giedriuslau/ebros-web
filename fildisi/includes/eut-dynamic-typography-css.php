<?php
/**
 *  Dynamic typography css style
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
 */

$typo_css = "";

/**
 * Typography
 * ----------------------------------------------------------------------------
 */

$typo_css .= "

body {
	font-size: " . fildisi_eutf_option( 'body_font', '14px', 'font-size'  ) . ";
	font-family: " . fildisi_eutf_option( 'body_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'body_font', 'normal', 'font-weight'  ) . ";
	line-height: " . fildisi_eutf_option( 'body_font', '36px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'body_font', '0px', 'letter-spacing'  ) . "
}

";

/* Logo as text */
$typo_css .= "

#eut-header .eut-logo.eut-logo-text a {
	font-family: " . fildisi_eutf_option( 'logo_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'logo_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'logo_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'logo_font', '11px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'logo_font', 'uppercase', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'logo_font', '0px', 'letter-spacing'  ) . "
}

";


/* Main Menu  */
$typo_css .= "

.eut-main-menu .eut-wrapper > ul > li > a,
.eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li > a,
.eut-toggle-hiddenarea .eut-label,
.eut-main-menu .eut-wrapper > ul > li ul li.eut-goback a {
	font-family: " . fildisi_eutf_option( 'main_menu_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'main_menu_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'main_menu_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'main_menu_font', '11px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'main_menu_font', 'uppercase', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'main_menu_font', '0px', 'letter-spacing'  ) . "
}

.eut-slide-menu .eut-main-menu .eut-wrapper ul li.megamenu ul li:not(.eut-goback) > a,
.eut-main-menu .eut-wrapper > ul > li ul li a,
#eut-header .eut-shoppin-cart-content {
	font-family: " . fildisi_eutf_option( 'sub_menu_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'sub_menu_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'sub_menu_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'sub_menu_font', '11px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'sub_menu_font', 'uppercase', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'sub_menu_font', '0px', 'letter-spacing'  ) . "
}

.eut-main-menu .eut-menu-description {
	font-family: " . fildisi_eutf_option( 'description_menu_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'description_menu_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'description_menu_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'description_menu_font', '11px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'description_menu_font', 'uppercase', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'description_menu_font', '0px', 'letter-spacing'  ) . "
}

";

/* Hidden Menu  */
$typo_css .= "
#eut-hidden-menu .eut-hiddenarea-content .eut-menu > li > a,
#eut-hidden-menu ul.eut-menu > li.megamenu > ul > li > a,
#eut-hidden-menu ul.eut-menu > li ul li.eut-goback a {
	font-family: " . fildisi_eutf_option( 'hidden_menu_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'hidden_menu_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'hidden_menu_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'hidden_menu_font', '11px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'hidden_menu_font', 'uppercase', 'text-transform'  ) . ";
}


#eut-hidden-menu.eut-slide-menu ul li.megamenu ul li:not(.eut-goback) > a,
#eut-hidden-menu.eut-slide-menu ul li ul li:not(.eut-goback) > a,
#eut-hidden-menu.eut-toggle-menu ul li.megamenu ul li > a,
#eut-hidden-menu.eut-toggle-menu ul li ul li > a {
	font-family: " . fildisi_eutf_option( 'sub_hidden_menu_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'sub_hidden_menu_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'sub_hidden_menu_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'sub_hidden_menu_font', '11px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'sub_hidden_menu_font', 'uppercase', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'sub_hidden_menu_font', '0px', 'letter-spacing'  ) . "
}

#eut-hidden-menu .eut-menu-description {
	font-family: " . fildisi_eutf_option( 'description_hidden_menu_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'description_hidden_menu_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'description_hidden_menu_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'description_hidden_menu_font', '11px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'description_hidden_menu_font', 'uppercase', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'description_hidden_menu_font', '0px', 'letter-spacing'  ) . "
}

";

/* Headings */
$typo_css .= "

h1,
.eut-h1,
#eut-theme-wrapper .eut-modal .eut-search input[type='text'],
.eut-dropcap span,
p.eut-dropcap:first-letter {
	font-family: " . fildisi_eutf_option( 'h1_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'h1_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'h1_font', 'normal', 'font-style'  ) . ";
	text-transform: " . fildisi_eutf_option( 'h1_font', ' none', 'text-transform'  ) . ";
	font-size: " . fildisi_eutf_option( 'h1_font', '56px', 'font-size'  ) . ";
	line-height: " . fildisi_eutf_option( 'h1_font', '60px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'h1_font', '0px', 'letter-spacing'  ) . "
}

h2,
.eut-h2 {
	font-family: " . fildisi_eutf_option( 'h2_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'h2_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'h2_font', 'normal', 'font-style'  ) . ";
	text-transform: " . fildisi_eutf_option( 'h2_font', ' none', 'text-transform'  ) . ";
	font-size: " . fildisi_eutf_option( 'h2_font', '36px', 'font-size'  ) . ";
	line-height: " . fildisi_eutf_option( 'h2_font', '40px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'h2_font', '0px', 'letter-spacing'  ) . "
}

h3,
.eut-h3 {
	font-family: " . fildisi_eutf_option( 'h3_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'h3_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'h3_font', 'normal', 'font-style'  ) . ";
	text-transform: " . fildisi_eutf_option( 'h3_font', ' none', 'text-transform'  ) . ";
	font-size: " . fildisi_eutf_option( 'h3_font', '30px', 'font-size'  ) . ";
	line-height: " . fildisi_eutf_option( 'h3_font', '33px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'h3_font', '0px', 'letter-spacing'  ) . "
}

h4,
.eut-h4 {
	font-family: " . fildisi_eutf_option( 'h4_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'h4_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'h4_font', 'normal', 'font-style'  ) . ";
	text-transform: " . fildisi_eutf_option( 'h4_font', ' none', 'text-transform'  ) . ";
	font-size: " . fildisi_eutf_option( 'h4_font', '23px', 'font-size'  ) . ";
	line-height: " . fildisi_eutf_option( 'h4_font', '26px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'h4_font', '0px', 'letter-spacing'  ) . "
}

h5,
.eut-h5 {
	font-family: " . fildisi_eutf_option( 'h5_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'h5_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'h5_font', 'normal', 'font-style'  ) . ";
	text-transform: " . fildisi_eutf_option( 'h5_font', ' none', 'text-transform'  ) . ";
	font-size: " . fildisi_eutf_option( 'h5_font', '18px', 'font-size'  ) . ";
	line-height: " . fildisi_eutf_option( 'h5_font', '20px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'h5_font', '0px', 'letter-spacing'  ) . "
}

h6,
.eut-h6,
.vc_tta.vc_general .vc_tta-panel-title,
#eut-main-content .vc_tta.vc_general .vc_tta-tab > a {
	font-family: " . fildisi_eutf_option( 'h6_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'h6_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'h6_font', 'normal', 'font-style'  ) . ";
	text-transform: " . fildisi_eutf_option( 'h6_font', ' none', 'text-transform'  ) . ";
	font-size: " . fildisi_eutf_option( 'h6_font', '16px', 'font-size'  ) . ";
	line-height: " . fildisi_eutf_option( 'h6_font', '18px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'h6_font', '0px', 'letter-spacing'  ) . "
}

";

/* Page Title */
$typo_css .= "

#eut-page-title .eut-title,
#eut-blog-title .eut-title,
#eut-search-page-title .eut-title {
	font-family: " . fildisi_eutf_option( 'page_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'page_title', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'page_title', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'page_title', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'page_title', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'page_title', '60px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'page_title', '0px', 'letter-spacing'  ) . "
}

#eut-page-title .eut-description,
#eut-blog-title .eut-description,
#eut-blog-title .eut-description p,
#eut-search-page-title .eut-description {
	font-family: " . fildisi_eutf_option( 'page_description', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'page_description', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'page_description', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'page_description', '24px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'page_description', 'none', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'page_description', '60px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'page_description', '0px', 'letter-spacing'  ) . "
}

";


/* Post Title */
$typo_css .= "

#eut-post-title .eut-title-categories {
	font-family: " . fildisi_eutf_option( 'post_title_meta', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'post_title_meta', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'post_title_meta', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'post_title_meta', '12px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'post_title_meta', 'none', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'post_title_meta', '24px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'post_title_meta', '0px', 'letter-spacing'  ) . "
}

#eut-post-title .eut-post-meta,
#eut-post-title .eut-post-meta li{
	font-family: " . fildisi_eutf_option( 'post_title_extra_meta', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'post_title_extra_meta', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'post_title_extra_meta', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'post_title_extra_meta', '12px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'post_title_extra_meta', 'none', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'post_title_extra_meta', '24px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'post_title_extra_meta', '0px', 'letter-spacing'  ) . "
}

.eut-single-simple-title {
	font-family: " . fildisi_eutf_option( 'post_simple_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'post_simple_title', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'post_simple_title', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'post_simple_title', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'post_simple_title', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'post_simple_title', '112px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'post_simple_title', '0px', 'letter-spacing'  ) . "
}

#eut-post-title .eut-title {
	font-family: " . fildisi_eutf_option( 'post_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'post_title', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'post_title', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'post_title', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'post_title', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'post_title', '112px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'post_title', '0px', 'letter-spacing'  ) . "
}

#eut-post-title .eut-description {
	font-family: " . fildisi_eutf_option( 'post_title_desc', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'post_title_desc', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'post_title_desc', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'post_title_desc', '26px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'post_title_desc', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'post_title_desc', '32px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'post_title_desc', '0px', 'letter-spacing'  ) . "
}

";

/* Portfolio Title */
$typo_css .= "

#eut-portfolio-title .eut-title {
	font-family: " . fildisi_eutf_option( 'portfolio_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'portfolio_title', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'portfolio_title', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'portfolio_title', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'portfolio_title', 'normal', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'portfolio_title', '72px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'portfolio_title', '0px', 'letter-spacing'  ) . "
}

#eut-portfolio-title .eut-description {
	font-family: " . fildisi_eutf_option( 'portfolio_description', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'portfolio_description', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'portfolio_description', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'portfolio_description', '18px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'portfolio_description', 'normal', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'portfolio_description', '30px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'portfolio_description', '0px', 'letter-spacing'  ) . "
}


";

/* Forum Title */
$typo_css .= "

#eut-forum-title .eut-title {
	font-family: " . fildisi_eutf_option( 'forum_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'forum_title', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'forum_title', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'forum_title', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'forum_title', 'normal', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'forum_title', '72px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'forum_title', '0px', 'letter-spacing'  ) . "
}


";

/* Product Title
============================================================================= */
$typo_css .= "

.eut-product-area .product_title {
	font-family: " . fildisi_eutf_option( 'product_simple_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'product_simple_title', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'product_simple_title', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'product_simple_title', '36px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'product_simple_title', 'normal', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'product_simple_title', '48px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'product_simple_title', '0px', 'letter-spacing'  ) . "
}

#eut-entry-summary .eut-short-description p {
	font-family: " . fildisi_eutf_option( 'product_short_description', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'product_short_description', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'product_short_description', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'product_short_description', '24px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'product_short_description', 'normal', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'product_short_description', '30px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'product_short_description', '0px', 'letter-spacing'  ) . "
}

#eut-product-title .eut-title,
#eut-product-tax-title .eut-title,
.woocommerce-page #eut-page-title .eut-title {
	font-family: " . fildisi_eutf_option( 'product_tax_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'product_tax_title', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'product_tax_title', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'product_tax_title', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'product_tax_title', 'normal', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'product_tax_title', '72px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'product_tax_title', '0px', 'letter-spacing'  ) . "
}

#eut-product-title .eut-description,
#eut-product-tax-title .eut-description,
#eut-product-tax-title .eut-description p,
.woocommerce-page #eut-page-title .eut-description {
	font-family: " . fildisi_eutf_option( 'product_tax_description', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'product_tax_description', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'product_tax_description', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'product_tax_description', '24px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'product_tax_description', 'normal', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'product_tax_description', '30px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'product_tax_description', '0px', 'letter-spacing'  ) . "
}

";

/* Feature Section Custom */
$typo_css .= "

#eut-feature-section .eut-subheading {
	font-family: " . fildisi_eutf_option( 'feature_subheading_custom_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'feature_subheading_custom_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'feature_subheading_custom_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'feature_subheading_custom_font', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'feature_subheading_custom_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'feature_subheading_custom_font', '112px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'feature_subheading_custom_font', '0px', 'letter-spacing'  ) . "
}

#eut-feature-section .eut-title {
	font-family: " . fildisi_eutf_option( 'feature_title_custom_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'feature_title_custom_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'feature_title_custom_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'feature_title_custom_font', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'feature_title_custom_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'feature_title_custom_font', '112px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'feature_title_custom_font', '0px', 'letter-spacing'  ) . "
}

#eut-feature-section .eut-description {
	font-family: " . fildisi_eutf_option( 'feature_desc_custom_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'feature_desc_custom_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'feature_desc_custom_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'feature_desc_custom_font', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'feature_desc_custom_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'feature_desc_custom_font', '112px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'feature_desc_custom_font', '0px', 'letter-spacing'  ) . "
}

";

/* Events Title
============================================================================= */
$typo_css .= "

#eut-event-title .eut-title,
#eut-event-tax-title .eut-title {
	font-family: " . fildisi_eutf_option( 'event_tax_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'event_tax_title', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'event_tax_title', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'event_tax_title', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'event_tax_title', 'normal', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'event_tax_title', '72px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'event_tax_title', '0px', 'letter-spacing'  ) . "
}

#eut-event-title .eut-description,
#eut-event-tax-title .eut-description,
#eut-event-tax-title .eut-description p {
	font-family: " . fildisi_eutf_option( 'event_tax_description', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'event_tax_description', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'event_tax_description', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'event_tax_description', '24px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'event_tax_description', 'normal', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'event_tax_description', '30px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'event_tax_description', '0px', 'letter-spacing'  ) . "
}

";

/* Feature Section Fullscreen */
$typo_css .= "

#eut-feature-section.eut-fullscreen .eut-subheading {
	font-family: " . fildisi_eutf_option( 'feature_subheading_full_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'feature_subheading_full_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'feature_subheading_full_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'feature_subheading_full_font', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'feature_subheading_full_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'feature_subheading_full_font', '112px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'feature_subheading_full_font', '0px', 'letter-spacing'  ) . "
}

#eut-feature-section.eut-fullscreen .eut-title {
	font-family: " . fildisi_eutf_option( 'feature_title_full_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'feature_title_full_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'feature_title_full_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'feature_title_full_font', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'feature_title_full_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'feature_title_full_font', '112px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'feature_title_full_font', '0px', 'letter-spacing'  ) . "
}

";

$typo_css .= "

#eut-feature-section.eut-fullscreen .eut-description {
	font-family: " . fildisi_eutf_option( 'feature_desc_full_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'feature_desc_full_font', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'feature_desc_full_font', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'feature_desc_full_font', '60px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'feature_desc_full_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'feature_desc_full_font', '112px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'feature_desc_full_font', '0px', 'letter-spacing'  ) . "
}

";


/* Special Text */
$typo_css .= "

.eut-leader-text,
.eut-leader-text p,
p.eut-leader-text,
.eut-quote-text,
blockquote p {
	font-family: " . fildisi_eutf_option( 'leader_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'leader_text', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'leader_text', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'leader_text', '34px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'leader_text', 'none', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'leader_text', '36px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'leader_text', '0px', 'letter-spacing'  ) . "
}

.eut-subtitle,
.eut-subtitle p,
.eut-subtitle-text {
	font-family: " . fildisi_eutf_option( 'subtitle_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'subtitle_text', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'subtitle_text', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'subtitle_text', '34px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'subtitle_text', 'none', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'subtitle_text', '36px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'subtitle_text', '0px', 'letter-spacing'  ) . "
}

.eut-small-text,
span.wpcf7-not-valid-tip,
div.wpcf7-mail-sent-ok,
div.wpcf7-validation-errors,
.eut-post-meta-wrapper .eut-categories li {
	font-family: " . fildisi_eutf_option( 'small_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'small_text', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'small_text', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'small_text', '34px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'small_text', 'none', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'small_text', '0px', 'letter-spacing'  ) . "
}

";

/* Link Text */
$fildisi_eutf_btn_size = fildisi_eutf_option( 'link_text', '13px', 'font-size'  );
$fildisi_eutf_btn_size = filter_var( $fildisi_eutf_btn_size, FILTER_SANITIZE_NUMBER_INT );

$fildisi_eutf_btn_size_xsm = $fildisi_eutf_btn_size * 0.7;
$fildisi_eutf_btn_size_sm = $fildisi_eutf_btn_size * 0.85;
$fildisi_eutf_btn_size_lg = $fildisi_eutf_btn_size * 1.2;
$fildisi_eutf_btn_size_xlg = $fildisi_eutf_btn_size * 1.35;

$typo_css .= "

.eut-link-text,
.eut-btn,
input[type='submit'],
input[type='reset'],
input[type='button'],
button:not(.mfp-arrow):not(.eut-search-btn),
#eut-header .eut-shoppin-cart-content .total,
#eut-header .eut-shoppin-cart-content .button,
#cancel-comment-reply-link,
.eut-anchor-menu .eut-anchor-wrapper .eut-container > ul > li > a,
.eut-anchor-menu .eut-anchor-wrapper .eut-container ul.sub-menu li a,
h3#reply-title {
	font-family: " . fildisi_eutf_option( 'link_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . " !important;
	font-weight: " . fildisi_eutf_option( 'link_text', 'normal', 'font-weight'  ) . " !important;
	font-style: " . fildisi_eutf_option( 'link_text', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'link_text', '13px', 'font-size'  ) . " !important;
	text-transform: " . fildisi_eutf_option( 'link_text', 'uppercase', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'link_text', '0px', 'letter-spacing'  ) . "
}

.eut-btn.eut-btn-extrasmall,
.widget.woocommerce button[type='submit'] {
	font-size: " . round( $fildisi_eutf_btn_size_xsm, 0 ) . "px !important;
}

.eut-btn.eut-btn-small {
	font-size: " . round( $fildisi_eutf_btn_size_sm, 0 ) . "px !important;
}

.eut-btn.eut-btn-large {
	font-size: " . round( $fildisi_eutf_btn_size_lg, 0 ) . "px !important;
}

.eut-btn.eut-btn-extralarge {
	font-size: " . round( $fildisi_eutf_btn_size_xlg, 0 ) . "px !important;
}


";

/* Widget Text */
$typo_css .= "

.eut-widget-title {
	font-family: " . fildisi_eutf_option( 'widget_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'widget_title', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'widget_title', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'widget_title', '34px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'widget_title', 'none', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'widget_title', '36px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'widget_title', '0px', 'letter-spacing'  ) . "
}

.widget,
.widgets,
.widget p {
	font-family: " . fildisi_eutf_option( 'widget_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'widget_text', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'widget_text', 'normal', 'font-style'  ) . ";
	font-size: " . fildisi_eutf_option( 'widget_text', '34px', 'font-size'  ) . ";
	text-transform: " . fildisi_eutf_option( 'widget_text', 'none', 'text-transform'  ) . ";
	line-height: " . fildisi_eutf_option( 'widget_text', '36px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'widget_text', '0px', 'letter-spacing'  ) . "
}


";

/* Single Post */
$typo_css .= "

.single-post #eut-single-content,
.single-product #tab-description,
.single-tribe_events #eut-single-content {
	font-size: " . fildisi_eutf_option( 'single_post_font', '18px', 'font-size'  ) . ";
	font-family: " . fildisi_eutf_option( 'single_post_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'single_post_font', 'normal', 'font-weight'  ) . ";
	line-height: " . fildisi_eutf_option( 'single_post_font', '36px', 'line-height'  ) . ";
	" . fildisi_eutf_css_option( 'single_post_font', '0px', 'letter-spacing'  ) . "
}

";


/* Custom Font Family */
$typo_css .= "
.eut-custom-font-1,
#eut-feature-section .eut-subheading.eut-custom-font-1,
#eut-feature-section.eut-fullscreen .eut-subheading.eut-custom-font-1,
#eut-feature-section .eut-title.eut-custom-font-1,
#eut-feature-section.eut-fullscreen .eut-title.eut-custom-font-1,
#eut-feature-section .eut-description.eut-custom-font-1,
#eut-feature-section.eut-fullscreen .eut-description.eut-custom-font-1 {
	font-family: " . fildisi_eutf_option( 'custom_font_family_1', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'custom_font_family_1', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'custom_font_family_1', 'normal', 'font-style'  ) . ";
	text-transform: " . fildisi_eutf_option( 'custom_font_family_1', 'none', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'custom_font_family_1', '0px', 'letter-spacing'  ) . "
}
.eut-custom-font-2,
#eut-feature-section .eut-subheading.eut-custom-font-2,
#eut-feature-section.eut-fullscreen .eut-subheading.eut-custom-font-2,
#eut-feature-section .eut-title.eut-custom-font-2,
#eut-feature-section.eut-fullscreen .eut-title.eut-custom-font-2,
#eut-feature-section .eut-description.eut-custom-font-2,
#eut-feature-section.eut-fullscreen .eut-description.eut-custom-font-2 {
	font-family: " . fildisi_eutf_option( 'custom_font_family_2', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'custom_font_family_2', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'custom_font_family_2', 'normal', 'font-style'  ) . ";
	text-transform: " . fildisi_eutf_option( 'custom_font_family_2', 'none', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'custom_font_family_2', '0px', 'letter-spacing'  ) . "
}
.eut-custom-font-3,
#eut-feature-section .eut-subheading.eut-custom-font-3,
#eut-feature-section.eut-fullscreen .eut-subheading.eut-custom-font-3,
#eut-feature-section .eut-title.eut-custom-font-3,
#eut-feature-section.eut-fullscreen .eut-title.eut-custom-font-3,
#eut-feature-section .eut-description.eut-custom-font-3,
#eut-feature-section.eut-fullscreen .eut-description.eut-custom-font-3 {
	font-family: " . fildisi_eutf_option( 'custom_font_family_3', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'custom_font_family_3', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'custom_font_family_3', 'normal', 'font-style'  ) . ";
	text-transform: " . fildisi_eutf_option( 'custom_font_family_3', 'none', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'custom_font_family_3', '0px', 'letter-spacing'  ) . "
}
.eut-custom-font-4,
#eut-feature-section .eut-subheading.eut-custom-font-4,
#eut-feature-section.eut-fullscreen .eut-subheading.eut-custom-font-4,
#eut-feature-section .eut-title.eut-custom-font-4,
#eut-feature-section.eut-fullscreen .eut-title.eut-custom-font-4,
#eut-feature-section .eut-description.eut-custom-font-4,
#eut-feature-section.eut-fullscreen .eut-description.eut-custom-font-4 {
	font-family: " . fildisi_eutf_option( 'custom_font_family_4', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . fildisi_eutf_option( 'custom_font_family_4', 'normal', 'font-weight'  ) . ";
	font-style: " . fildisi_eutf_option( 'custom_font_family_4', 'normal', 'font-style'  ) . ";
	text-transform: " . fildisi_eutf_option( 'custom_font_family_4', 'none', 'text-transform'  ) . ";
	" . fildisi_eutf_css_option( 'custom_font_family_4', '0px', 'letter-spacing'  ) . "
}

";



/* Blog Leader
============================================================================= */
$body_lineheight = fildisi_eutf_option( 'body_font', '36px', 'line-height'  );
$body_lineheight = filter_var( $body_lineheight, FILTER_SANITIZE_NUMBER_INT );
$typo_css .= "
.eut-blog-leader .eut-post-list .eut-post-content p {
	max-height: " . ( esc_attr( $body_lineheight ) * 2 ) . "px;
}

";


//Responsive Typography

$fildisi_eutf_responsive_fonts_group_headings =  array (
	array(
		'id'   => 'h1_font',
		'selector'  => 'h1,.eut-h1,#eut-theme-wrapper .eut-modal .eut-search input[type="text"],.eut-dropcap span,p.eut-dropcap:first-letter',
		'custom_selector'  => '.eut-h1',
	),
	array(
		'id'   => 'h2_font',
		'selector'  => 'h2,.eut-h2',
		'custom_selector'  => '.eut-h2',
	),
	array(
		'id'   => 'h3_font',
		'selector'  => 'h3,.eut-h3',
		'custom_selector'  => '.eut-h3',
	),
	array(
		'id'   => 'h4_font',
		'selector'  => 'h4,.eut-h4',
		'custom_selector'  => '.eut-h4',
	),
	array(
		'id'   => 'h5_font',
		'selector'  => 'h5,.eut-h5',
		'custom_selector'  => '.eut-h5',
	),
	array(
		'id'   => 'h6_font',
		'selector'  => 'h6,.eut-h6',
		'custom_selector'  => '.eut-h6',
	),
);


$fildisi_eutf_responsive_fonts_group_1 =  array (
	array(
		'id'   => 'page_title',
		'selector'  => '#eut-page-title .eut-title,#eut-blog-title .eut-title,#eut-search-page-title .eut-title',
	),
	array(
		'id'   => 'post_title',
		'selector'  => '#eut-post-title .eut-title',
	),
	array(
		'id'   => 'post_simple_title',
		'selector'  => '.eut-single-simple-title',
	),
	array(
		'id'   => 'portfolio_title',
		'selector'  => '#eut-portfolio-title .eut-title',
	),
	array(
		'id'   => 'forum_title',
		'selector'  => '#eut-forum-title .eut-title',
	),
	array(
		'id'   => 'product_simple_title',
		'selector'  => '.eut-product-area .product_title',
	),
	array(
		'id'   => 'product_tax_title',
		'selector'  => '#eut-product-title .eut-title,#eut-product-tax-title .eut-title,.woocommerce-page #eut-page-title .eut-title',
	),
	array(
		'id'   => 'event_tax_title',
		'selector'  => '#eut-event-title .eut-title,#eut-event-tax-title .eut-title',
	),
	array(
		'id'   => 'feature_title_custom_font',
		'selector'  => '#eut-feature-section .eut-title',
	),
	array(
		'id'   => 'feature_title_full_font',
		'selector'  => '#eut-feature-section.eut-fullscreen .eut-title',
	),
	array(
		'id'   => 'feature_desc_full_font',
		'selector'  => '#eut-feature-section.eut-fullscreen .eut-description',
	),
);

$fildisi_eutf_responsive_fonts_group_2 =  array (
	array(
		'id'   => 'page_description',
		'selector'  => '#eut-page-title .eut-description,#eut-blog-title .eut-description,#eut-blog-title .eut-description p,#eut-search-page-title .eut-description',
	),
	array(
		'id'   => 'post_title_meta',
		'selector'  => '#eut-post-title .eut-title-categories',
	),
	array(
		'id'   => 'post_title_extra_meta',
		'selector'  => '#eut-post-title .eut-post-meta, #eut-post-title .eut-post-meta li',
	),
	array(
		'id'   => 'post_title_desc',
		'selector'  => '#eut-post-title .eut-description',
	),
	array(
		'id'   => 'product_short_description',
		'selector'  => '#eut-entry-summary .eut-short-description p',
	),
	array(
		'id'   => 'product_tax_description',
		'selector'  => '#eut-product-title .eut-description,#eut-product-tax-title .eut-description,#eut-product-tax-title .eut-description p,.woocommerce-page #eut-page-title .eut-description',
	),
	array(
		'id'   => 'event_tax_description',
		'selector'  => '#eut-event-title .eut-description,#eut-event-tax-title .eut-description,#eut-event-tax-title .eut-description p',
	),
	array(
		'id'   => 'feature_subheading_custom_font',
		'selector'  => '#eut-feature-section .eut-subheading',
	),
	array(
		'id'   => 'feature_subheading_full_font',
		'selector'  => '#eut-feature-section.eut-fullscreen .eut-subheading',
	),
	array(
		'id'   => 'feature_desc_custom_font',
		'selector'  => '#eut-feature-section .eut-description',
	),
	array(
		'id'   => 'leader_text',
		'selector'  => '.eut-leader-text,.eut-leader-text p,p.eut-leader-text,.eut-quote-text,blockquote p',
	),
	array(
		'id'   => 'subtitle_text',
		'selector'  => '.eut-subtitle,.eut-subtitle-text',
	),
	array(
		'id'   => 'link_text',
		'selector'  => '#eut-theme-wrapper .eut-link-text,#eut-theme-wrapper a.eut-btn,#eut-theme-wrapper input[type="submit"],#eut-theme-wrapper input[type="reset"],#eut-theme-wrapper button:not(.mfp-arrow):not(.eut-search-btn),#cancel-comment-reply-link,h3#reply-title',
	),
	array(
		'id'   => 'main_menu_font',
		'selector'  => '.eut-main-menu .eut-wrapper > ul > li > a,.eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li > a, .eut-toggle-hiddenarea .eut-label,.eut-main-menu .eut-wrapper > ul > li ul li.eut-goback a',
	),
	array(
		'id'   => 'sub_menu_font',
		'selector'  => '.eut-slide-menu .eut-main-menu .eut-wrapper ul li.megamenu ul li:not(.eut-goback) > a,.eut-main-menu .eut-wrapper > ul > li ul li a,#eut-header .eut-shoppin-cart-content',
	),
	array(
		'id'   => 'description_menu_font',
		'selector'  => '.eut-main-menu .eut-menu-description',
	),
	array(
		'id'   => 'hidden_menu_font',
		'selector'  => '#eut-hidden-menu .eut-hiddenarea-content .eut-menu > li > a,#eut-hidden-menu ul.eut-menu > li.megamenu > ul > li > a,#eut-hidden-menu ul.eut-menu > li ul li.eut-goback a',
	),
	array(
		'id'   => 'sub_hidden_menu_font',
		'selector'  => '#eut-hidden-menu.eut-slide-menu ul li.megamenu ul li:not(.eut-goback) > a,#eut-hidden-menu.eut-slide-menu ul li ul li:not(.eut-goback) > a,#eut-hidden-menu.eut-toggle-menu ul li.megamenu ul li > a,#eut-hidden-menu.eut-toggle-menu ul li ul li > a',
	),
	array(
		'id'   => 'description_hidden_menu_font',
		'selector'  => '#eut-hidden-menu .eut-menu-description',
	),
);

function fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts = array() , $threshold = 35, $ratio = 0.7) {

	$css = '';

	if ( !empty( $fildisi_eutf_responsive_fonts ) && $ratio < 1 ) {

		foreach ( $fildisi_eutf_responsive_fonts as $font ) {
			$fildisi_eutf_size = fildisi_eutf_option( $font['id'], '32px', 'font-size'  );
			$fildisi_eutf_size = filter_var( $fildisi_eutf_size, FILTER_SANITIZE_NUMBER_INT );
			$line_height = fildisi_eutf_option( $font['id'], '32px', 'line-height'  );
			$line_height = filter_var( $line_height, FILTER_SANITIZE_NUMBER_INT );

			if ( $fildisi_eutf_size >= $threshold ) {
				$custom_line_height = $line_height / $fildisi_eutf_size;
				$custom_size = $fildisi_eutf_size * $ratio;

				if ( 'link_text' == $font['id'] ) {
					$css .= $font['selector'] . " {
						font-size: " . round( $custom_size, 0 ) . "px !important;
						line-height: " . round( $custom_line_height, 2 ) . "em;
					}
					";
				} else {
					$css .= $font['selector'] . " {
						font-size: " . round( $custom_size, 0 ) . "px;
						line-height: " . round( $custom_line_height, 2 ) . "em;
					}
					";
				}
			}

			if ( isset( $font['custom_selector'] ) ) {
				$sizes = array( '120', '140', '160', '180', '200', '250', '300' );
				foreach ( $sizes as $size ) {
					$custom_size = $fildisi_eutf_size * ( $size / 100 );
					if ( $custom_size >= $threshold ) {
						if ( '250' == $size || '300' == $size ) {
							$custom_size = $fildisi_eutf_size * ( $ratio / 1.7 );
						} elseif ( '200' == $size ) {
							$custom_size = $fildisi_eutf_size * ( $ratio / 1.4 );
						} else {
							$custom_size = $fildisi_eutf_size * ( $ratio / 1.15 );
						}
						$css .= $font['custom_selector'] . ".eut-heading-" . esc_attr( $size ) ." {
							font-size: " . round( $custom_size, 0 ) . "px;
						}
						";
					}
				}
			}

		}

	}

	return $css;
}

$small_desktop_threshold_headings = fildisi_eutf_option( 'typography_small_desktop_threshold_headings', 20 );
$small_desktop_ratio_headings = fildisi_eutf_option( 'typography_small_desktop_ratio_headings', 1 );
$tablet_landscape_threshold_headings = fildisi_eutf_option( 'typography_tablet_landscape_threshold_headings', 20 );
$tablet_landscape_ratio_headings = fildisi_eutf_option( 'typography_tablet_landscape_ratio_headings', 1 );
$tablet_portrait_threshold_headings = fildisi_eutf_option( 'typography_tablet_portrait_threshold_headings', 20 );
$tablet_portrait_ratio_headings = fildisi_eutf_option( 'typography_tablet_portrait_ratio_headings', 1 );
$mobile_threshold_headings = fildisi_eutf_option( 'typography_mobile_threshold_headings', 20 );
$mobile_ratio_headings = fildisi_eutf_option( 'typography_mobile_ratio_headings', 1 );

$small_desktop_threshold = fildisi_eutf_option( 'typography_small_desktop_threshold', 20 );
$small_desktop_ratio = fildisi_eutf_option( 'typography_small_desktop_ratio', 1 );
$tablet_landscape_threshold = fildisi_eutf_option( 'typography_tablet_landscape_threshold', 20 );
$tablet_landscape_ratio = fildisi_eutf_option( 'typography_tablet_landscape_ratio', 0.9 );
$tablet_portrait_threshold = fildisi_eutf_option( 'typography_tablet_portrait_threshold', 20 );
$tablet_portrait_ratio = fildisi_eutf_option( 'typography_tablet_portrait_ratio', 0.85 );
$mobile_threshold = fildisi_eutf_option( 'typography_mobile_threshold', 28 );
$mobile_ratio = fildisi_eutf_option( 'typography_mobile_ratio', 0.6 );

$small_desktop_threshold2 = fildisi_eutf_option( 'typography_small_desktop_threshold2', 14 );
$small_desktop_ratio2 = fildisi_eutf_option( 'typography_small_desktop_ratio2', 1 );
$tablet_landscape_threshold2 = fildisi_eutf_option( 'typography_tablet_landscape_threshold2', 14 );
$tablet_landscape_ratio2 = fildisi_eutf_option( 'typography_tablet_landscape_ratio2', 0.9 );
$tablet_portrait_threshold2 = fildisi_eutf_option( 'typography_tablet_portrait_threshold2', 14 );
$tablet_portrait_ratio2 = fildisi_eutf_option( 'typography_tablet_portrait_ratio2', 0.8 );
$mobile_threshold2 = fildisi_eutf_option( 'typography_mobile_threshold2', 13 );
$mobile_ratio2 = fildisi_eutf_option( 'typography_mobile_ratio2', 0.7 );

$typo_css .= "
	@media only screen and (min-width: 1201px) and (max-width: 1440px) {
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_headings, $small_desktop_threshold_headings, $small_desktop_ratio_headings ). "
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_1, $small_desktop_threshold, $small_desktop_ratio ). "
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_2, $small_desktop_threshold2, $small_desktop_ratio2 ). "
	}
	@media only screen and (min-width: 960px) and (max-width: 1200px) {
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_headings, $tablet_landscape_threshold_headings, $tablet_landscape_ratio_headings ). "
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_1, $tablet_landscape_threshold, $tablet_landscape_ratio ). "
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_2, $tablet_landscape_threshold2, $tablet_landscape_ratio2 ). "
	}
	@media only screen and (min-width: 768px) and (max-width: 959px) {
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_headings, $tablet_portrait_threshold_headings, $tablet_portrait_ratio_headings ). "
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_1, $tablet_portrait_threshold, $tablet_portrait_ratio ). "
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_2, $tablet_portrait_threshold2, $tablet_portrait_ratio2 ). "
	}
	@media only screen and (max-width: 767px) {
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_headings, $mobile_threshold_headings, $mobile_ratio_headings ). "
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_1, $mobile_threshold, $mobile_ratio ). "
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_2, $mobile_threshold2, $mobile_ratio2 ). "
	}
	@media print {
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_headings, $mobile_threshold_headings, $mobile_ratio_headings ). "
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_1, $mobile_threshold, $mobile_ratio ). "
		" . fildisi_eutf_print_typography_responsive( $fildisi_eutf_responsive_fonts_group_2, $mobile_threshold2, $mobile_ratio2 ). "
	}
";

wp_add_inline_style( 'fildisi-eutf-custom-style', fildisi_eutf_compress_css( $typo_css ) );


//Omit closing PHP tag to avoid accidental whitespace output errors.
