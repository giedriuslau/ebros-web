<?php

/*
*	Footer Helper functions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/


/**
 * Prints Footer Background Image
 */
if ( !function_exists('fildisi_eutf_print_footer_bg_image') ) {
	function fildisi_eutf_print_footer_bg_image() {
		if ( 'custom' == fildisi_eutf_option( 'footer_bg_mode' ) ) {
			$fildisi_eutf_footer_custom_bg = array(
				'bg_mode' => 'custom',
				'bg_image_id' => fildisi_eutf_option( 'footer_bg_image', '', 'id' ),
				'bg_position' => fildisi_eutf_option( 'footer_bg_position', 'center-center' ),
				'pattern_overlay' => fildisi_eutf_option( 'footer_pattern_overlay' ),
				'color_overlay' => fildisi_eutf_option( 'footer_color_overlay' ),
				'opacity_overlay' => fildisi_eutf_option( 'footer_opacity_overlay' ),
			);
			fildisi_eutf_print_title_bg_image( $fildisi_eutf_footer_custom_bg );
		}
	}
}

/**
 * Prints Footer Widgets
 */
if ( !function_exists('fildisi_eutf_print_footer_widgets') ) {
	function fildisi_eutf_print_footer_widgets() {
		$fildisi_section_visibility = 'no';
		if ( fildisi_eutf_visibility( 'footer_widgets_visibility' ) ) {
			$fildisi_section_visibility = 'yes';
		}
		if ( is_singular() ) {
			$fildisi_section_visibility = fildisi_eutf_post_meta( '_fildisi_eutf_footer_widgets_visibility', $fildisi_section_visibility );
		} else if( fildisi_eutf_is_woo_shop() ) {
			$fildisi_section_visibility = fildisi_eutf_post_meta_shop( '_fildisi_eutf_footer_widgets_visibility', $fildisi_section_visibility );
		}

		if ( 'yes' == $fildisi_section_visibility ) {

			$fildisi_eutf_footer_columns = fildisi_eutf_option('footer_widgets_layout');

			switch( $fildisi_eutf_footer_columns ) {
				case 'footer-1':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-3-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-4-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
					);
				break;
				case 'footer-2':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-2',
							'tablet-column' => '1',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-3-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
					);
				break;
				case 'footer-3':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-3-sidebar',
							'column' => '1-2',
							'tablet-column' => '1',
						),
					);
				break;
				case 'footer-4':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-2',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-2',
							'tablet-column' => '1-2',
						),
					);
				break;
				case 'footer-5':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-3',
							'tablet-column' => '1-3',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-3',
							'tablet-column' => '1-3',
						),
						array(
							'sidebar-id' => 'eut-footer-3-sidebar',
							'column' => '1-3',
							'tablet-column' => '1-3',
						),
					);
				break;
				case 'footer-6':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '2-3',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-3',
							'tablet-column' => '1-2',
						),
					);
				break;
				case 'footer-7':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-3',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '2-3',
							'tablet-column' => '1-2',
						),
					);
				break;
				case 'footer-8':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-3',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-2',
							'tablet-column' => '1-3',
						),
						array(
							'sidebar-id' => 'eut-footer-3-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-3',
						),
					);
				break;
				case 'footer-9':
				default:
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1',
							'tablet-column' => '1',
						),
					);
				break;
			}

			$section_type = fildisi_eutf_option( 'footer_section_type', 'fullwidth-background' );

			$fildisi_eutf_footer_class = array( 'eut-widget-area' );

			if( 'fullwidth-element' == $section_type ) {
				$fildisi_eutf_footer_class[] = 'eut-fullwidth';
			}
			$fildisi_eutf_footer_class_string = implode( ' ', $fildisi_eutf_footer_class );

			$footer_padding_top = fildisi_eutf_option( 'footer_padding_top_multiplier', '3x' );
			$footer_padding_bottom = fildisi_eutf_option( 'footer_padding_bottom_multiplier', '3x' );

	?>
			<!-- Footer -->
			<div class="<?php echo esc_attr( $fildisi_eutf_footer_class_string ); ?>">
				<div class="eut-container eut-padding-top-<?php echo esc_attr( $footer_padding_top ); ?> eut-padding-bottom-<?php echo esc_attr( $footer_padding_bottom ); ?> ">
					<div class="eut-row eut-columns-gap-30">
		<?php

					foreach ( $footer_sidebars as $footer_sidebar ) {
						echo '<div class="wpb_column eut-column eut-column-' . $footer_sidebar['column'] . ' eut-tablet-column-' . $footer_sidebar['tablet-column'] . '">';
						echo '<div class="eut-column-wrapper">';
						dynamic_sidebar( $footer_sidebar['sidebar-id'] );
						echo '</div>';
						echo '</div>';
					}
		?>
					</div>
				</div>
			</div>
	<?php

		}
	}
}

/**
 * Prints Footer Bar Area
 */

if ( !function_exists('fildisi_eutf_print_footer_bar') ) {
	function fildisi_eutf_print_footer_bar() {
		$fildisi_section_visibility = 'no';
		if ( fildisi_eutf_visibility( 'footer_bar_visibility' ) ) {
			$fildisi_section_visibility = 'yes';
		}
		if ( is_singular() ) {
			$fildisi_section_visibility = fildisi_eutf_post_meta( '_fildisi_eutf_footer_bar_visibility', $fildisi_section_visibility );
		} else if( fildisi_eutf_is_woo_shop() ) {
			$fildisi_section_visibility = fildisi_eutf_post_meta_shop( '_fildisi_eutf_footer_bar_visibility', $fildisi_section_visibility );
		}

		if ( 'yes' == $fildisi_section_visibility ) {

			$section_type = fildisi_eutf_option( 'footer_bar_section_type', 'fullwidth-background' );

			$fildisi_eutf_footer_bar_class = array( 'eut-footer-bar', 'eut-padding-top-1x', 'eut-padding-bottom-1x' );

			if( 'fullwidth-element' == $section_type ) {
				$fildisi_eutf_footer_bar_class[] = 'eut-fullwidth';
			}
			$fildisi_eutf_footer_bar_class_string = implode( ' ', $fildisi_eutf_footer_bar_class );

			$align_center = fildisi_eutf_option( 'footer_bar_align_center', 'no' );
			$second_area = fildisi_eutf_option( 'second_area_visibility', '1' );
	?>

			<div class="<?php echo esc_attr( $fildisi_eutf_footer_bar_class_string ); ?>" data-align-center="<?php echo esc_attr( $align_center ); ?>">
				<div class="eut-container">
					<?php if ( fildisi_eutf_visibility( 'footer_copyright_visibility' ) ) { ?>
					<div class="eut-bar-content eut-left-side">
						<div class="eut-copyright">
							<?php echo do_shortcode( fildisi_eutf_option( 'footer_copyright_text' ) ); ?>
						</div>
					</div>
					<?php } ?>
					<?php if ( '2' == $second_area ) { ?>
					<div class="eut-bar-content eut-right-side">
						<nav class="eut-footer-menu">
							<?php fildisi_eutf_footer_nav(); ?>
						</nav>
					</div>
					<?php
					} else if ( '3' == $second_area ) { ?>
					<div class="eut-bar-content eut-right-side">
						<?php
						global $fildisi_eutf_social_list;
						$options = fildisi_eutf_option('footer_social_options');
						$social_display = fildisi_eutf_option('footer_social_display', 'text');
						$social_options = fildisi_eutf_option('social_options');

						if ( !empty( $options ) && !empty( $social_options ) ) {
							if ( 'text' == $social_display ) {
								echo '<ul class="eut-social">';
								foreach ( $social_options as $key => $value ) {
									if ( isset( $options[$key] ) && 1 == $options[$key] && $value ) {
										if ( 'skype' == $key ) {
											echo '<li><a href="' . esc_url( $value, array( 'skype', 'http', 'https' ) ) . '">' . $fildisi_eutf_social_list[$key] . '</a></li>';
										} else {
											echo '<li><a href="' . esc_url( $value ) . '" target="_blank" rel="noopener noreferrer">' . $fildisi_eutf_social_list[$key] . '</a></li>';
										}
									}
								}
								echo '</ul>';
							} else {
								echo '<ul class="eut-social eut-social-icons">';
								foreach ( $social_options as $key => $value ) {
									if ( isset( $options[$key] ) && 1 == $options[$key] && $value ) {
										if ( 'skype' == $key ) {
											echo '<li><a href="' . esc_url( $value, array( 'skype', 'http', 'https' ) ) . '" class="fa fa-' . esc_attr( $key ) . '"></a></li>';
										} else {
											echo '<li><a href="' . esc_url( $value ) . '" target="_blank" rel="noopener noreferrer" class="fa fa-' . esc_attr( $key ) . '"></a></li>';
										}
									}
								}
								echo '</ul>';
							}
						}
						?>
					</div>
					<?php
					} else if ( '4' == $second_area ) { ?>
					<div class="eut-bar-content eut-right-side">
						<div class="eut-copyright">
							<?php echo do_shortcode( fildisi_eutf_option( 'footer_second_copyright_text' ) ); ?>
						</div>
					</div>
					<?php
					}
					?>
				</div>
			</div>

	<?php
		}
	}
}

/**
 * Prints Back To Top Link
 */
if ( !function_exists('fildisi_eutf_print_back_top') ) {
	function fildisi_eutf_print_back_top() {
		if ( ( is_singular() && 'yes' == fildisi_eutf_post_meta( '_fildisi_eutf_disable_back_to_top' ) ) || ( fildisi_eutf_is_woo_shop() && 'yes' == fildisi_eutf_post_meta_shop( '_fildisi_eutf_disable_back_to_top' ) ) ) {
			return;
		}

		if ( fildisi_eutf_visibility( 'back_to_top_enabled' )  ) {

			$fildisi_eutf_back_to_top_shape = fildisi_eutf_option( 'back_to_top_shape', 'none' );

			$fildisi_eutf_back_to_top_icon_wrapper_classes = array('eut-arrow-wrapper');
			if( 'none' != $fildisi_eutf_back_to_top_shape ){
				$fildisi_eutf_back_to_top_icon_wrapper_classes[] = 'eut-' . $fildisi_eutf_back_to_top_shape;
				$fildisi_eutf_back_to_top_icon_wrapper_classes[] = 'eut-wrapper-color';
			}
			$fildisi_eutf_back_to_top_icon_wrapper_class_string = implode( ' ', $fildisi_eutf_back_to_top_icon_wrapper_classes );

		?>
			<div class="eut-back-top">
				<div class="<?php echo esc_attr( $fildisi_eutf_back_to_top_icon_wrapper_class_string ); ?>">
					<i class="eut-icon-nav-up-small eut-back-top-icon"></i>
				</div>
			</div>
		<?php
		}
	}
}

 /**
 * Prints Bottom Bar
 */
if ( !function_exists('fildisi_eutf_print_bottom_bar') ) {
	function fildisi_eutf_print_bottom_bar() {

		$fildisi_area_id = fildisi_eutf_option('bottom_bar_area');
		if ( is_singular() ) {
			$fildisi_area_id = fildisi_eutf_post_meta( '_fildisi_eutf_bottom_bar_area', $fildisi_area_id );
		}
		if( fildisi_eutf_is_woo_shop() ) {
			$fildisi_area_id = fildisi_eutf_post_meta_shop( '_fildisi_eutf_bottom_bar_area', $fildisi_area_id );
		}

		if ( !empty( $fildisi_area_id ) && 'none' != $fildisi_area_id ) {
			$fildisi_area_id = apply_filters( 'wpml_object_id', $fildisi_area_id, 'area-item', TRUE  );
			$fildisi_content = get_post_field( 'post_content', $fildisi_area_id );
	?>
			<!-- BOTTOM BAR -->
			<div id="eut-bottom-bar" class="eut-bookmark">
				<?php echo apply_filters( 'fildisi_eutf_the_content', $fildisi_content ); ?>
			</div>
			<!-- END BOTTOM BAR -->
	<?php
		}
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
