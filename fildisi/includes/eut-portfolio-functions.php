<?php

/*
*	Portfolio Helper functions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/


/**
 * Prints portfolio feature image
 */
function fildisi_eutf_print_portfolio_feature_image( $image_size = 'large', $second_image_id = "" ) {

	if( empty( $second_image_id ) ) {
		if ( has_post_thumbnail() ) {
?>
		<div class="eut-media clearfix">
			<?php the_post_thumbnail( $image_size ); ?>
		</div>
<?php

		}
	} else {
?>
		<div class="eut-media clearfix">
			<?php echo wp_get_attachment_image( $second_image_id, $image_size ); ?>
		</div>
<?php

	}



}

/**
 * Prints Portfolio socials if used
 */
function fildisi_eutf_print_portfolio_media() {
	global $post;
	$post_id = $post->ID;

	$portfolio_media = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_media_selection' );
	$portfolio_image_mode = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_media_image_mode' );
	$portfolio_media_fullwidth = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_media_fullwidth' );
	$image_size_slider = 'fildisi-eutf-large-rect-horizontal';
	if ( 'resize' == $portfolio_image_mode || 'yes' == $portfolio_media_fullwidth ) {
		if( fildisi_eutf_option( 'has_sidebar' ) ) {
			$image_size_slider = "medium_large";
			if ( 'yes' == $portfolio_media_fullwidth ) {
				$image_size_slider = "large";
			}
		} else {
			$image_size_slider = "fildisi-eutf-fullscreen";
		}
	}

	switch( $portfolio_media ) {

		case 'slider':
			$slider_items = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_slider_items' );
			fildisi_eutf_print_gallery_slider( 'slider', $slider_items, $image_size_slider );
			break;
		case 'gallery':
			$slider_items = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_slider_items' );
			fildisi_eutf_print_gallery_slider( 'gallery', $slider_items, '', 'eut-classic-style' );
			break;
		case 'gallery-vertical':
			$slider_items = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_slider_items' );
			fildisi_eutf_print_gallery_slider( 'gallery-vertical', $slider_items, $image_size_slider, 'eut-vertical-style' );
			break;
		case 'video':
			fildisi_eutf_print_portfolio_video();
			break;
		case 'video-html5':
			fildisi_eutf_print_portfolio_video( 'html5' );
			break;
		case 'video-code':
			fildisi_eutf_print_portfolio_video( 'code' );
			break;
		case 'none':
			break;
		default:
			if( fildisi_eutf_option( 'has_sidebar' ) ) {
				$image_size = "medium_large";
				if ( 'yes' == $portfolio_media_fullwidth ) {
					$image_size = "large";
				}
			} else {
				$image_size = "fildisi-eutf-fullscreen";
			}

			$second_image = fildisi_eutf_post_meta( '_fildisi_eutf_second_featured_image' );
			if ( 'second-image' == $portfolio_media ) {
				if( !empty( $second_image ) ) {
					fildisi_eutf_print_portfolio_feature_image( $image_size, $second_image );
				}
			} else {
				fildisi_eutf_print_portfolio_feature_image( $image_size );
			}

			break;

	}
}


/**
 * Prints video of the portfolio media
 */
function fildisi_eutf_print_portfolio_video( $video_mode = '' ) {

	$video_webm = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_video_webm' );
	$video_mp4 = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_video_mp4' );
	$video_ogv = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_video_ogv' );
	$video_poster = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_video_poster' );
	$video_embed = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_video_embed' );

	if( 'code' == $video_mode ) {
		$video_embed = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_video_code' );
	}

	fildisi_eutf_print_media_video( $video_mode, $video_webm, $video_mp4, $video_ogv, $video_embed, $video_poster );
}

 /**
 * Prints portfolio like counter
 */
function fildisi_eutf_print_portfolio_like_counter( $counter_color = 'content' ) {

	$post_likes = fildisi_eutf_option( 'portfolio_social', '', 'eut-likes' );
	if ( !empty( $post_likes  ) ) {
		global $post;
		$post_id = $post->ID;
		$active = fildisi_eutf_likes( $post_id, 'status' );
		$icon = 'fa fa-heart-o';
		if( 'active' == $active ) {
			$icon = 'fa fa-heart';
		}
?>
		<div class="eut-like-counter eut-link-text eut-text-<?php echo esc_attr( $counter_color ); ?>"><i class="<?php echo esc_attr( $icon ); ?>"></i><span><?php echo fildisi_eutf_likes( $post_id ); ?></span></div>
<?php
	}

}


/**
 * Check Portfolio details if used
 */

function fildisi_eutf_check_portfolio_details() {
	global $post;
	$post_id = $post->ID;

	$eut_portfolio_details = fildisi_eutf_post_meta( '_fildisi_eutf_details', '' );
	$portfolio_fields = get_the_terms( $post_id, 'portfolio_field' );

	if ( !empty( $eut_portfolio_details ) || ! empty( $portfolio_fields ) ) {
		return true;
	}

	$eut_social_bar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_social_bar_layout', fildisi_eutf_option( 'portfolio_social_bar_layout', 'layout-1' ) );
	if( 'layout-2' == $eut_social_bar_layout && fildisi_eutf_social_bar ( 'portfolio', 'check' ) ) {
		return true;
	}
	return false;

}

/**
 * Prints Portfolio details
 */
if ( !function_exists('fildisi_eutf_print_portfolio_details') ) {
	function fildisi_eutf_print_portfolio_details() {
		global $post;
		$post_id = $post->ID;

		$heading_tag = fildisi_eutf_option( 'portfolio_details_heading_tag', 'div' );
		$eut_portfolio_details_title = fildisi_eutf_post_meta( '_fildisi_eutf_details_title', fildisi_eutf_option( 'portfolio_details_text' ) );
		$eut_portfolio_details = fildisi_eutf_post_meta( '_fildisi_eutf_details', '' );
		$portfolio_fields = get_the_terms( $post_id, 'portfolio_field' );

		$link_text = fildisi_eutf_post_meta( '_fildisi_eutf_details_link_text', fildisi_eutf_option( 'portfolio_details_link_text' ) );
		$link_url = fildisi_eutf_post_meta( '_fildisi_eutf_details_link_url' );
		$link_new_window = fildisi_eutf_post_meta( '_fildisi_eutf_details_link_new_window' );
		$link_extra_class = fildisi_eutf_post_meta( '_fildisi_eutf_details_link_extra_class' );

		$eut_portfolio_details_classes = array( 'eut-portfolio-description', 'eut-border' );
		if( empty( $link_url ) && !empty( $portfolio_fields ) ){
			array_push( $eut_portfolio_details_classes,  'eut-margin-bottom-1x' );
		}
		$eut_portfolio_details_class_string = implode( ' ', $eut_portfolio_details_classes );

		$link_classes = array( 'eut-portfolio-details-btn', 'eut-btn' );
		if( !empty( $link_extra_class ) ){
			array_push( $link_classes,  $link_extra_class );
		}
		if ( ! empty( $portfolio_fields ) ) {
			array_push( $link_classes,  'eut-margin-bottom-2x' );
		}
		$link_class_string = implode( ' ', $link_classes );

	?>

		<!-- Portfolio Info -->
		<div class="eut-portfolio-info eut-border">
			<?php
			if ( !empty( $eut_portfolio_details ) ) {
			?>
			<!-- Portfolio Description -->
			<div class="<?php echo esc_attr( $eut_portfolio_details_class_string ); ?>">
				<<?php echo tag_escape( $heading_tag ); ?> class="eut-h5"><?php echo wp_kses_post( $eut_portfolio_details_title ); ?></<?php echo tag_escape( $heading_tag ); ?>>
				<p><?php echo do_shortcode( wp_kses_post( $eut_portfolio_details ) ) ?></p>
				<?php
					// Portfolio Link
					if( !empty( $link_url )  ) {
						$link_target = "_self";
						if( !empty( $link_new_window )  ) {
							$link_target = "_blank";
						}
					?>
					<a href="<?php echo esc_url( $link_url ); ?>" class="<?php echo esc_attr( $link_class_string ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_text ); ?></a>
					<?php
					}
					?>
			</div>
			<!-- End Portfolio Description -->
			<?php
			}
			?>
			<?php
			if ( ! empty( $portfolio_fields ) ) {
			?>
			<!-- Fields -->
			<ul class="eut-portfolio-fields eut-border">
				<?php
					foreach( $portfolio_fields as $field ) {
						echo '<li class="eut-fields-title eut-heading-color">';
						if ( !empty( $field->description ) ) {
							echo '<span class="eut-fields-description eut-small-text">' . wp_kses_post( $field->description ) . '</span>';
						}
						echo '<span class="eut-link-text">' . esc_html( $field->name ) . '</span>';
						echo '</li>';
					}
				?>
			</ul>
			<!-- End Fields -->
			<?php
			}

			$eut_social_bar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_social_bar_layout', fildisi_eutf_option( 'portfolio_social_bar_layout', 'layout-1' ) );
			if( 'layout-2' == $eut_social_bar_layout ) {
				fildisi_eutf_social_bar ( 'portfolio' );
			}

			?>
		</div>
		<!-- End Portfolio Info -->
	<?php

	}
}

/**
 * Prints Portfolio Recents items. ( Classic Layout )
 */
function fildisi_eutf_print_recent_portfolio_items_classic() {

	$exclude_ids = array( get_the_ID() );
	$args = array(
		'post_type' => 'portfolio',
		'post_status'=>'publish',
		'post__not_in' => $exclude_ids ,
		'posts_per_page' => 3,
		'paged' => 1,
	);


	$query = new WP_Query( $args );

	$eut_portfolio_recent_title = fildisi_eutf_option( 'portfolio_recent_title' );

	if ( $query->have_posts() ) {
?>

	<!-- Related -->
	<div id="eut-portfolio-related" class="eut-related eut-padding-top-3x eut-padding-bottom-3x">
		<div class="eut-container">
			<div class="eut-wrapper">
				<?php if( !empty( $eut_portfolio_recent_title ) ) { ?>
				<div class="eut-related-title eut-link-text eut-heading-color eut-align-center"><?php echo esc_html( $eut_portfolio_recent_title); ?></div>
				<?php } ?>
				<div class="eut-row eut-columns-gap-30">
				<?php
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
						get_template_part( 'templates/portfolio', 'recent' );
					endwhile;
					else :
					endif;
				?>
				</div>
			</div>
		</div>
	</div>
	<!-- End Related -->
<?php
		wp_reset_postdata();
	}
}

/**
 * Prints Portfolio Recents items. ( Fildisi Layout )
 */
function fildisi_eutf_print_recent_portfolio_items_fildisi() {

	$exclude_ids = array( get_the_ID() );
	$args = array(
		'post_type' => 'portfolio',
		'post_status'=>'publish',
		'post__not_in' => $exclude_ids ,
		'posts_per_page' => 2,
		'paged' => 1,
	);


	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
?>

	<!-- Related -->
	<div class="eut-post-bar-item eut-post-related">
		<?php
			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
				get_template_part( 'templates/portfolio', 'recent' );
			endwhile;
			else :
			endif;
		?>
	</div>
	<!-- End Related -->
<?php
		wp_reset_postdata();
	}
}


/**
 * Prints Portfolio Feature Image
 */
function fildisi_eutf_print_portfolio_image( $image_size = 'fildisi-eutf-small-square', $mode = '', $atts = array() ) {

	if ( has_post_thumbnail() ) {
		$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
		$attachment_src = wp_get_attachment_image_src( $post_thumbnail_id, $image_size );
		$image_src = $attachment_src[0];
		if ( 'link' == $mode ){
			echo esc_url( $image_src );
		} else {
			if ( 'color' == $mode ){
				$image_src = get_template_directory_uri() . '/images/transparent/' . $image_size . '.png';
?>
				<img src="<?php echo esc_url( $image_src ); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" width="<?php echo esc_attr( $attachment_src[1] ); ?>" height="<?php echo esc_attr( $attachment_src[2] ); ?>"/>
<?php
			} else {
				echo wp_get_attachment_image( $post_thumbnail_id, $image_size, '', $atts );
			}

		}
	} else {
		$image_src = get_template_directory_uri() . '/images/empty/' . $image_size . '.jpg';
		if ( 'link' == $mode ){
			echo esc_url( $image_src );
		} else {
			if ( 'color' == $mode ){
				$image_src = get_template_directory_uri() . '/images/transparent/' . $image_size . '.png';
			}
?>
		<img src="<?php echo esc_url( $image_src ); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>"/>
<?php
		}
	}

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
