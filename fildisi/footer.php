			<?php
				$eut_sticky_footer = fildisi_eutf_visibility( 'sticky_footer' ) ? 'yes' : 'no';
			?>
				<footer id="eut-footer" data-sticky-footer="<?php echo esc_attr( $eut_sticky_footer ); ?>" class="eut-border eut-bookmark">
					<?php fildisi_eutf_print_bottom_bar(); ?>
					<div class="eut-footer-wrapper">
					<?php fildisi_eutf_print_footer_widgets(); ?>
					<?php fildisi_eutf_print_footer_bar(); ?>
					<?php fildisi_eutf_print_footer_bg_image(); ?>
					</div>

				</footer>
			<!-- SIDE AREA -->
			<?php
				$fildisi_eutf_sidearea_data = fildisi_eutf_get_sidearea_data();
				fildisi_eutf_print_side_area( $fildisi_eutf_sidearea_data );
				fildisi_eutf_print_cart_area();
			?>
			<!-- END SIDE AREA -->

			<!-- HIDDEN MENU -->
			<?php fildisi_eutf_print_hidden_menu(); ?>
			<!-- END HIDDEN MENU -->

			<?php fildisi_eutf_print_search_modal(); ?>
			<?php fildisi_eutf_print_form_modals(); ?>
			<?php fildisi_eutf_print_language_modal(); ?>
			<?php fildisi_eutf_print_login_modal(); ?>
			<?php fildisi_eutf_print_social_modal(); ?>

			<?php do_action( 'fildisi_eutf_footer_modal_container' ); ?>

			<?php fildisi_eutf_print_back_top(); ?>
			</div> <!-- end #eut-theme-content -->
		</div> <!-- end #eut-theme-wrapper -->

		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>