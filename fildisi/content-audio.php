<?php
/**
 * The Audio Post Type Template
 */
?>

<?php
if ( is_singular() ) {
	$fildisi_eutf_disable_media = fildisi_eutf_post_meta( '_fildisi_eutf_disable_media' );
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'eut-single-post' ); ?> itemscope itemType="http://schema.org/BlogPosting">
		<?php
			if ( 'yes' != $fildisi_eutf_disable_media ) {
		?>
		<div id="eut-single-media">
			<div class="eut-container">
			<?php fildisi_eutf_print_post_audio(); ?>
			</div>
		</div>
		<?php
			}
		?>
		<div id="eut-single-content">
			<?php fildisi_eutf_print_post_simple_title(); ?>
			<?php fildisi_eutf_print_post_structured_data(); ?>
			<div itemprop="articleBody">
				<?php the_content(); ?>
			</div>
		</div>

	</article>

<?php
} else {
	$blog_mode = fildisi_eutf_option( 'blog_mode', 'large' );
	$fildisi_eutf_post_class = fildisi_eutf_get_post_class();
?>

	<!-- Article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class( $fildisi_eutf_post_class ); ?> itemscope itemType="http://schema.org/BlogPosting">
		<?php do_action( 'fildisi_eutf_inner_post_loop_item_before' ); ?>
		<?php fildisi_eutf_print_post_feature_media( 'audio' ); ?>
		<div class="eut-post-content-wrapper">
			<div class="eut-post-content">
				<?php fildisi_eutf_print_post_meta_top(); ?>
				<?php fildisi_eutf_print_post_structured_data(); ?>
				<div itemprop="articleBody">
					<?php fildisi_eutf_print_post_excerpt(); ?>
				</div>
			</div>
		</div>
		<?php do_action( 'fildisi_eutf_inner_post_loop_item_after' ); ?>
	</article>
	<!-- End Article -->

<?php

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
