<!doctype html>
<!--[if lt IE 10]>
<html class="ie9 no-js" <?php language_attributes(); ?>>
<![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

	<head>
		<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) { ?>
		<!-- allow pinned sites -->
		<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
		<?php } ?>
		<?php wp_head(); ?>
	</head>

	<?php
		$fildisi_eutf_header_mode = fildisi_eutf_option( 'header_mode', 'default' );
		$header_sticky_type = fildisi_eutf_option( 'header_sticky_type', 'simple' );
		$fildisi_eutf_header_fullwidth = fildisi_eutf_option( 'header_fullwidth', '1' );
		$fildisi_eutf_header_data = fildisi_eutf_get_feature_header_data();
		$fildisi_eutf_header_style = $fildisi_eutf_header_data['header_style'];
		$fildisi_eutf_header_overlapping = $fildisi_eutf_header_data['data_overlap'];
		$fildisi_eutf_responsive_header_overlapping = fildisi_eutf_option( 'responsive_header_overlapping', 'no' );
		$fildisi_eutf_header_position = $fildisi_eutf_header_data['data_header_position'];
		$fildisi_eutf_menu_open_type = fildisi_eutf_option( 'header_menu_open_type', 'toggle' );

		// Theme Layout
		$fildisi_eutf_theme_layout = fildisi_eutf_option( 'theme_layout', 'stretched' );
		$fildisi_eutf_frame_size = fildisi_eutf_option( 'frame_size', 30 );

		//Sticky Header
		$fildisi_eutf_header_sticky_type = fildisi_eutf_option( 'header_sticky_type', 'simple' );

		if ( is_singular() ) {
			$fildisi_eutf_header_sticky_type = fildisi_eutf_post_meta( '_fildisi_eutf_sticky_header_type', $fildisi_eutf_header_sticky_type );
			$fildisi_eutf_responsive_header_overlapping = fildisi_eutf_post_meta( '_fildisi_eutf_responsive_header_overlapping', $fildisi_eutf_responsive_header_overlapping );
		} else if ( fildisi_eutf_is_woo_shop() ) {
			$fildisi_eutf_header_sticky_type = fildisi_eutf_post_meta_shop( '_fildisi_eutf_sticky_header_type', $fildisi_eutf_header_sticky_type );
			$fildisi_eutf_responsive_header_overlapping = fildisi_eutf_post_meta_shop( '_fildisi_eutf_responsive_header_overlapping', $fildisi_eutf_responsive_header_overlapping );
		}
		$fildisi_eutf_header_sticky_type = fildisi_eutf_visibility( 'header_sticky_enabled' ) ? $fildisi_eutf_header_sticky_type : 'none';

		$fildisi_eutf_header_sticky_height = fildisi_eutf_option( 'header_sticky_shrink_height', '0' );
		if( 'simple' == $fildisi_eutf_header_sticky_type && 'default' == $fildisi_eutf_header_mode) {
			$fildisi_eutf_header_sticky_height = fildisi_eutf_option( 'header_height' );
		}
		if( 'simple' == $fildisi_eutf_header_sticky_type && 'logo-top' == $fildisi_eutf_header_mode) {
			$fildisi_eutf_header_sticky_height = fildisi_eutf_option( 'header_bottom_height' );
		}
		$fildisi_eutf_responsive_header_height = fildisi_eutf_option( 'responsive_header_height' );
		$fildisi_eutf_header_menu_mode = fildisi_eutf_option( 'header_menu_mode', 'default' );

		$fildisi_eutf_menu_type = 'classic';

		if ( 'default' == $fildisi_eutf_header_mode ) {
			$fildisi_eutf_logo_align = 'left';
			if ( 'split' == $fildisi_eutf_header_menu_mode  ) {
				$fildisi_eutf_menu_align = 'center';
			} else {
				$fildisi_eutf_menu_align = fildisi_eutf_option( 'menu_align', 'right' );
			}
			$fildisi_eutf_menu_type = fildisi_eutf_option( 'menu_type', 'classic' );
			if ( is_singular() ) {
				$fildisi_eutf_menu_type = fildisi_eutf_post_meta( '_fildisi_eutf_menu_type', $fildisi_eutf_menu_type );
			} else if ( fildisi_eutf_is_woo_shop() ) {
				$fildisi_eutf_menu_type = fildisi_eutf_post_meta_shop( '_fildisi_eutf_menu_type', $fildisi_eutf_menu_type );
			}
		} else if ( 'logo-top' == $fildisi_eutf_header_mode ) {
			$fildisi_eutf_logo_align = fildisi_eutf_option( 'header_top_logo_align', 'center' );
			$fildisi_eutf_menu_align = fildisi_eutf_option( 'header_top_menu_align', 'center' );
			$fildisi_eutf_menu_type = fildisi_eutf_option( 'header_top_logo_menu_type', 'classic' );
			if ( is_singular() ) {
				$fildisi_eutf_menu_type = fildisi_eutf_post_meta( '_fildisi_eutf_menu_type', $fildisi_eutf_menu_type );
			} else if ( fildisi_eutf_is_woo_shop() ) {
				$fildisi_eutf_menu_type = fildisi_eutf_post_meta_shop( '_fildisi_eutf_menu_type', $fildisi_eutf_menu_type );
			}
		} else {
			$fildisi_eutf_header_fullwidth = 0;
			$fildisi_eutf_header_overlapping = 'no';
			$fildisi_eutf_header_sticky_type = 'none';
			$fildisi_eutf_menu_align = fildisi_eutf_option( 'header_side_menu_align', 'left' );
			$fildisi_eutf_logo_align = fildisi_eutf_option( 'header_side_logo_align', 'left' );
		}
		//Header Classes
		$fildisi_eutf_header_classes = array();
		if ( 1 == $fildisi_eutf_header_fullwidth ) {
			$fildisi_eutf_header_classes[] = 'eut-fullwidth';
		}
		if ( 'yes' == $fildisi_eutf_header_overlapping ) {
			$fildisi_eutf_header_classes[] = 'eut-overlapping';
		}
		if ( 'yes' == $fildisi_eutf_responsive_header_overlapping ) {
			$fildisi_eutf_header_classes[] = 'eut-responsive-overlapping';
		}
		if( 'below' == $fildisi_eutf_header_position ) {
			$fildisi_eutf_header_classes[] = 'eut-header-below';
		}
		if( 'advanced-hidden' == $fildisi_eutf_menu_type ){
			$fildisi_eutf_header_classes[] = 'eut-advanced-hidden-menu';
		}
		$fildisi_eutf_header_class_string = implode( ' ', $fildisi_eutf_header_classes );


		//Main Header Classes
		$fildisi_eutf_main_header_classes = array();
		$fildisi_eutf_main_header_classes[] = 'eut-header-' . $fildisi_eutf_header_mode;
		if ( 'side' == $fildisi_eutf_header_mode ) {
			$fildisi_eutf_main_header_classes[] = 'eut-' . $fildisi_eutf_menu_open_type . '-menu';
		} else {
			$fildisi_eutf_main_header_classes[] = 'eut-' . $fildisi_eutf_header_style;
		}
		if ( 'side' != $fildisi_eutf_header_mode || 'none' != $fildisi_eutf_header_sticky_type ) {
			$fildisi_eutf_main_header_classes[] = 'eut-' . $fildisi_eutf_header_sticky_type . '-sticky';
		}
		$fildisi_eutf_header_main_class_string = implode( ' ', $fildisi_eutf_main_header_classes );

		$fildisi_eutf_menu_arrows = fildisi_eutf_option( 'submenu_pointer', 'none' );

		// Main Menu Classes
		$fildisi_eutf_main_menu_classes = array();
		if ( 'side' != $fildisi_eutf_header_mode ) {
			$fildisi_eutf_main_menu_classes[] = 'eut-horizontal-menu';
			if ( 'default' == $fildisi_eutf_header_mode && 'split' == $fildisi_eutf_header_menu_mode  ) {
				$fildisi_eutf_main_menu_classes[] = 'eut-split-menu';
			}
			$fildisi_eutf_main_menu_classes[] = 'eut-position-' . $fildisi_eutf_menu_align;
			if( 'none' != $fildisi_eutf_menu_arrows ) {
				$fildisi_eutf_main_menu_classes[] = 'eut-' . $fildisi_eutf_menu_arrows;
			}
			if ( 'hidden' != $fildisi_eutf_menu_type ){
				$fildisi_eutf_main_menu_classes[] = 'eut-menu-type-' . $fildisi_eutf_menu_type;
			}
		} else {
			$fildisi_eutf_main_menu_classes[] = 'eut-vertical-menu';
			$fildisi_eutf_main_menu_classes[] = 'eut-align-' . $fildisi_eutf_menu_align;
		}
		$fildisi_eutf_main_menu_classes[] = 'eut-main-menu';
		$fildisi_eutf_main_menu_class_string = implode( ' ', $fildisi_eutf_main_menu_classes );

		$fildisi_eutf_main_menu = fildisi_eutf_get_header_nav();
		$fildisi_eutf_sidearea_data = fildisi_eutf_get_sidearea_data();


		$fildisi_eutf_header_sticky_devices_enabled = fildisi_eutf_option( 'header_sticky_devices_enabled' );
		$fildisi_eutf_header_sticky_devices = 'no';
		if ( '1' == $fildisi_eutf_header_sticky_devices_enabled ) {
			$fildisi_eutf_header_sticky_devices = 'yes';
		}

	?>

	<body <?php body_class(); ?>>
		<?php do_action( 'fildisi_eutf_body_top' ); ?>
		<?php if ( 'framed' == $fildisi_eutf_theme_layout ) { ?>
		<div id="eut-frames" data-frame-size="<?php echo esc_attr( $fildisi_eutf_frame_size ); ?>">
			<div class="eut-frame eut-top"></div>
			<div class="eut-frame eut-left"></div>
			<div class="eut-frame eut-right"></div>
			<div class="eut-frame eut-bottom"></div>
		</div>
		<?php } ?>

		<?php fildisi_eutf_print_theme_loader(); ?>

		<?php
			// Theme Wrapper Classes
			$fildisi_eutf_theme_wrapper_classes = array();
			if ( 'side' == $fildisi_eutf_header_mode ) {
				$fildisi_eutf_theme_wrapper_classes[] = 'eut-header-side';
			}
			if( 'below' == $fildisi_eutf_header_position && 'yes' == $fildisi_eutf_header_overlapping ) {
				$fildisi_eutf_theme_wrapper_classes[] = 'eut-feature-below';
			}
			$fildisi_eutf_theme_wrapper_class_string = implode( ' ', $fildisi_eutf_theme_wrapper_classes );

			$headedr_attributes = array();
			$header_attributes[] = 'class="' . esc_attr( $fildisi_eutf_header_class_string ) . '"';
			$header_attributes[] = 'data-sticky="' . esc_attr( $fildisi_eutf_header_sticky_type ) . '"';
			$header_attributes[] = 'data-sticky-height="' . esc_attr( $fildisi_eutf_header_sticky_height ) . '"';
			$header_attributes[] = 'data-devices-sticky="' . esc_attr( $fildisi_eutf_header_sticky_devices ) . '"';
			$header_attributes[] = 'data-devices-sticky-height="' . esc_attr( $fildisi_eutf_responsive_header_height ) . '"';
		?>

		<!-- Theme Wrapper -->
		<div id="eut-theme-wrapper" class="<?php echo esc_attr( $fildisi_eutf_theme_wrapper_class_string ); ?>" data-mask-layer="2">
			<div id="eut-theme-content">
			<?php
				//Top Bar
				fildisi_eutf_print_header_top_bar();
				//FEATURE Header Below
				if( 'below' == $fildisi_eutf_header_position ) {
					fildisi_eutf_print_header_feature();
				}
			?>

			<!-- HEADER -->
			<header id="eut-header" <?php echo implode( ' ', $header_attributes ); ?>>
				<div class="eut-wrapper clearfix">

					<!-- Header -->
					<div id="eut-main-header" class="<?php echo esc_attr( $fildisi_eutf_header_main_class_string ); ?>">
					<?php
						if ( 'side' == $fildisi_eutf_header_mode ) {
					?>
						<div class="eut-main-header-wrapper clearfix">
							<div class="eut-content">
								<?php do_action( 'fildisi_eutf_side_logo_before' ); ?>
								<?php fildisi_eutf_print_logo( 'side', $fildisi_eutf_logo_align ); ?>
								<?php do_action( 'fildisi_eutf_side_logo_after' ); ?>
								<?php if ( $fildisi_eutf_main_menu != 'disabled' ) { ?>
								<!-- Main Menu -->
								<nav id="eut-main-menu" class="<?php echo esc_attr( $fildisi_eutf_main_menu_class_string ); ?>">
									<div class="eut-wrapper">
										<?php fildisi_eutf_header_nav( $fildisi_eutf_main_menu ); ?>
									</div>
								</nav>
								<!-- End Main Menu -->
								<?php } ?>
							</div>
						</div>
						<div class="eut-header-elements-wrapper eut-align-<?php echo esc_attr( $fildisi_eutf_menu_align); ?>">
							<?php fildisi_eutf_print_header_elements( $fildisi_eutf_sidearea_data ); ?>
						</div>
						<?php fildisi_eutf_print_side_header_bg_image(); ?>
					<?php
						} else if ( 'logo-top' == $fildisi_eutf_header_mode ) {
						//Log on Top Header
					?>
						<div id="eut-top-header">
							<div class="eut-wrapper clearfix">
								<div class="eut-container">
									<?php do_action( 'fildisi_eutf_top_logo_before' ); ?>
									<?php fildisi_eutf_print_logo( 'logo-top', $fildisi_eutf_logo_align ); ?>
									<?php do_action( 'fildisi_eutf_top_logo_after' ); ?>
								</div>
							</div>
						</div>
						<div id="eut-bottom-header">
							<div class="eut-wrapper clearfix">
								<div class="eut-container">
									<div class="eut-header-elements-wrapper eut-position-right">
								<?php
									if ( 'hidden' == $fildisi_eutf_menu_type && 'disabled' != $fildisi_eutf_main_menu ) {
										fildisi_eutf_print_header_hiddenarea_button();
									}
									fildisi_eutf_print_header_elements();
									fildisi_eutf_print_header_sidearea_button( $fildisi_eutf_sidearea_data );
								?>
									</div>
								<?php
									if ( 'hidden' != $fildisi_eutf_menu_type && $fildisi_eutf_main_menu != 'disabled' ) {
								?>
										<!-- Main Menu -->
										<nav id="eut-main-menu" class="<?php echo esc_attr( $fildisi_eutf_main_menu_class_string ); ?>">
											<div class="eut-wrapper">
												<?php fildisi_eutf_header_nav( $fildisi_eutf_main_menu ); ?>
											</div>
										</nav>
										<!-- End Main Menu -->
								<?php
									}
								?>
								</div>
							</div>
						</div>
					<?php
						} else {
						//Default Header
					?>
						<div class="eut-wrapper clearfix">
							<div class="eut-container">
							<?php if ( 'default' == $fildisi_eutf_header_menu_mode || 'hidden' == $fildisi_eutf_menu_type || 'disabled' == $fildisi_eutf_main_menu  ) { ?>
								<?php do_action( 'fildisi_eutf_default_logo_before' ); ?>
								<?php fildisi_eutf_print_logo( 'default', $fildisi_eutf_logo_align ); ?>
								<?php do_action( 'fildisi_eutf_default_logo_after' ); ?>
							<?php } ?>
								<?php if ( 'advanced-hidden' == $fildisi_eutf_menu_type && 'disabled' != $fildisi_eutf_main_menu ) { ?>
									<div class="eut-hidden-menu-btn eut-position-right">
										<div class="eut-header-element">
											<a href="#">
												<span class="eut-item">
													<span></span>
													<span></span>
													<span></span>
												</span>
											</a>
										</div>
									</div>
								<?php } ?>
								<div class="eut-header-elements-wrapper eut-position-right">
							<?php
								if ( 'hidden' == $fildisi_eutf_menu_type && 'disabled' != $fildisi_eutf_main_menu ) {
									fildisi_eutf_print_header_hiddenarea_button();
								}
								fildisi_eutf_print_header_elements();
								fildisi_eutf_print_header_sidearea_button( $fildisi_eutf_sidearea_data );
							?>
								</div>
							<?php
								if ( 'hidden' != $fildisi_eutf_menu_type && $fildisi_eutf_main_menu != 'disabled' ) {
							?>
									<!-- Main Menu -->
									<nav id="eut-main-menu" class="<?php echo esc_attr( $fildisi_eutf_main_menu_class_string ); ?>">
										<div class="eut-wrapper">
											<?php fildisi_eutf_header_nav( $fildisi_eutf_main_menu, $fildisi_eutf_header_menu_mode ); ?>
										</div>
									</nav>
									<!-- End Main Menu -->
							<?php
								}
							?>
							</div>
						</div>
					<?php
						}
					?>

					</div>
					<!-- End Header -->

					<!-- Responsive Header -->
					<div id="eut-responsive-header">
						<div id="eut-main-responsive-header" class="eut-wrapper clearfix">
							<div class="eut-container">
							<?php do_action( 'fildisi_eutf_responsive_logo_before' ); ?>
							<?php fildisi_eutf_print_logo( 'responsive' , 'left' ); ?>
							<?php do_action( 'fildisi_eutf_responsive_logo_after' ); ?>
								<div class="eut-header-elements-wrapper eut-position-right">
								<?php do_action( 'fildisi_eutf_responsive_elements_first' ); ?>
								<!-- Hidden Menu & Side Area Button -->
								<?php
									if ( 'disabled' != $fildisi_eutf_main_menu || fildisi_eutf_check_header_elements_visibility_any() ){
										fildisi_eutf_print_header_hiddenarea_button();
									}
								?>
								<?php fildisi_eutf_print_login_responsive_button(); ?>
								<?php fildisi_eutf_print_cart_responsive_link(); ?>
								<?php
									$fildisi_eutf_responsive_sidearea_button = fildisi_eutf_option( 'responsive_sidearea_button_visibility', 'yes');
									if ( 'yes' == $fildisi_eutf_responsive_sidearea_button ) {
										fildisi_eutf_print_header_sidearea_button( $fildisi_eutf_sidearea_data );
									}
								?>
								<?php do_action( 'fildisi_eutf_responsive_elements_last' ); ?>
								</div>
							</div>
						</div>
					</div>
					<!-- End Responsive Header -->
				</div>

				<!-- Fildisi Sticky Header -->
			<?php
				if ( 'side' != $fildisi_eutf_header_mode && 'fildisi' == $fildisi_eutf_header_sticky_type ) {

				// Fildisi Sticky Menu Classes
				$fildisi_eutf_fildisi_sticky_menu_classes = array();

				$fildisi_eutf_fildisi_sticky_menu_classes[] = 'eut-horizontal-menu';
				$fildisi_eutf_fildisi_sticky_menu_classes[] = 'eut-position-' . $fildisi_eutf_menu_align;
				$fildisi_eutf_fildisi_sticky_menu_classes[] = 'eut-main-menu';
				if( 'none' != $fildisi_eutf_menu_arrows ) {
					$fildisi_eutf_fildisi_sticky_menu_classes[] = 'eut-' . $fildisi_eutf_menu_arrows;
				}
				if ( 'hidden' != $fildisi_eutf_menu_type ){
					$fildisi_eutf_fildisi_sticky_menu_classes[] = 'eut-menu-type-' . $fildisi_eutf_menu_type;
				}

				$fildisi_eutf_fildisi_sticky_menu_class_string = implode( ' ', $fildisi_eutf_fildisi_sticky_menu_classes );

			?>
				<div id="eut-fildisi-sticky-header" class="eut-fullwidth">
					<div class="eut-wrapper clearfix">
						<div class="eut-container">

						<?php fildisi_eutf_print_logo( 'fildisi-sticky' , 'left' ); ?>
						<div class="eut-header-elements-wrapper eut-position-right">
							<?php fildisi_eutf_print_header_elements(); ?>
							<?php fildisi_eutf_print_header_sidearea_button( $fildisi_eutf_sidearea_data ); ?>
						</div>
						<?php
							if ( 'hidden' != $fildisi_eutf_menu_type && $fildisi_eutf_main_menu != 'disabled' ) {
						?>
							<!-- Main Menu -->
							<nav id="eut-fildisi-sticky-menu" class="<?php echo esc_attr( $fildisi_eutf_fildisi_sticky_menu_class_string ); ?>">
								<div class="eut-wrapper">
									<?php fildisi_eutf_header_nav( $fildisi_eutf_main_menu ); ?>
								</div>
							</nav>
							<!-- End Main Menu -->
						<?php
							}
						?>

						</div>
					</div>

				</div>
			<?php
				}
			?>
				<!-- End Fildisi Sticky Header -->

			</header>
			<!-- END HEADER -->

			<?php
				//FEATURE Header Above
				if( 'above' == $fildisi_eutf_header_position ) {
					fildisi_eutf_print_header_feature();
				}

//Omit closing PHP tag to avoid accidental whitespace output errors.
