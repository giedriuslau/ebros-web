<?php
/*
*	Admin Page Welcome
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$docs_link = 'https://docs.euthemians.com/theme/fildisi/';
$videos_link = 'https://www.youtube.com/channel/UCNp1cNhcLQKJm81q9hAKAcw';
$support_link = 'https://euthemians.ticksy.com';

?>
	<div id="eut-welcome-wrap" class="wrap">
		<h2><?php esc_html_e( "Welcome", 'fildisi' ); ?></h2>
		<?php fildisi_eutf_print_admin_links('welcome'); ?>
		<div id="eut-welcome-panel" class="eut-admin-panel">
			<div class="eut-admin-panel-content">
				<h2><?php esc_html_e( "Welcome to Fildisi!", 'fildisi' ); ?></h2>
				<p class="about-description"><?php esc_html_e( "Thank you so much for this purchase. You are now ready to use another premium WordPress theme by Euthemians. Be sure that we'd be happy to support you all the way through and make Fildisi Theme a lasting experience.", 'fildisi' ); ?></p>
				<div class="eut-admin-panel-container">
					<div class="eut-admin-panel-row">
						<div class="eut-admin-panel-column eut-admin-panel-column-1-3">
							<div class="eut-admin-panel-column-content">
								<img class="eut-admin-panel-icon" src="<?php echo get_template_directory_uri() .'/includes/images/admin-icons/live-tutorial.jpg'?>" alt="<?php esc_attr_e( "Live Tutorial", 'fildisi' ); ?>">
								<h3><?php esc_html_e( "Live Tutorial", 'fildisi' ); ?></h3>
								<p><?php esc_html_e( "We hope that in our Live Knowledgebase you will find all required information to get your site running.", 'fildisi' ); ?></p>
								<a href="<?php echo esc_url( $docs_link ); ?>" class="eut-admin-panel-more" target="_blank" rel="noopener noreferrer"><?php esc_html_e( "Read More", 'fildisi' ); ?></a>
							</div>
						</div>
						<div class="eut-admin-panel-column eut-admin-panel-column-1-3">
							<div class="eut-admin-panel-column-content">
								<img class="eut-admin-panel-icon" src="<?php echo get_template_directory_uri() .'/includes/images/admin-icons/video-tutorial.jpg'?>" alt="<?php esc_attr_e( "Video Tutorials", 'fildisi' ); ?>">
								<h3><?php esc_html_e( "Video Tutorials", 'fildisi' ); ?></h3>
								<p><?php esc_html_e( "We also recommend to check out our Video Tutorials. The easiest way to discover the amazing features of Fildisi.", 'fildisi' ); ?></p>
								<a href="<?php echo esc_url( $videos_link ); ?>" class="eut-admin-panel-more" target="_blank" rel="noopener noreferrer"><?php esc_html_e( "Read More", 'fildisi' ); ?></a>
							</div>
						</div>
						<div class="eut-admin-panel-column eut-admin-panel-column-1-3">
							<div class="eut-admin-panel-column-content">
								<img class="eut-admin-panel-icon" src="<?php echo get_template_directory_uri() .'/includes/images/admin-icons/support-system.jpg'?>" alt="<?php esc_attr_e( "Support System", 'fildisi' ); ?>">
								<h3><?php esc_html_e( "Support System", 'fildisi' ); ?></h3>
								<p><?php esc_html_e( "Still no luck? No worries, we are here to help. Contact our support agents and they will get back to you as soon as possible.", 'fildisi' ); ?></p>
								<a href="<?php echo esc_url( $support_link ); ?>" class="eut-admin-panel-more" target="_blank" rel="noopener noreferrer"><?php esc_html_e( "Read More", 'fildisi' ); ?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php

//Omit closing PHP tag to avoid accidental whitespace output errors.
