<?php

/*
*	Main sidebar area
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

$fixed = "";
$fildisi_eutf_fixed_sidebar = "no";
$fildisi_eutf_sidebar_extra_content = false;

$sidebar_view = fildisi_eutf_get_current_view();
if ( 'search_page' == $sidebar_view ) {
	$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'search_page_sidebar' );
	$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'search_page_layout', 'none' );
	$fildisi_eutf_fixed_sidebar = fildisi_eutf_option( 'search_page_fixed_sidebar', 'no' );
} else if ( 'forum' == $sidebar_view ) {
	$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'forum_sidebar' );
	$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'forum_layout', 'none' );
	$fildisi_eutf_fixed_sidebar = fildisi_eutf_option( 'forum_fixed_sidebar', 'no' );
} else if ( 'shop' == $sidebar_view ) {
	if ( is_search() ) {
		$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'product_search_sidebar' );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'product_search_layout', 'none' );
		$fildisi_eutf_fixed_sidebar = fildisi_eutf_option( 'product_search_fixed_sidebar', 'no' );
	} else if ( is_shop() ) {
		$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta_shop( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'page_sidebar' ) );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta_shop( '_fildisi_eutf_layout', fildisi_eutf_option( 'page_layout', 'none' ) );
		$fildisi_eutf_fixed_sidebar =  fildisi_eutf_post_meta_shop( '_fildisi_eutf_fixed_sidebar' , fildisi_eutf_option( 'page_fixed_sidebar', 'no' ) );
	} else if( is_product() ) {
		$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'product_sidebar' ) );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'product_layout', 'none' ) );
		$fildisi_eutf_fixed_sidebar =  fildisi_eutf_post_meta( '_fildisi_eutf_fixed_sidebar' , fildisi_eutf_option( 'product_fixed_sidebar', 'no' ) );
	} else {
		$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'product_tax_sidebar' );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'product_tax_layout', 'none' );
		$fildisi_eutf_fixed_sidebar = fildisi_eutf_option( 'product_tax_fixed_sidebar', 'no' );
	}
}  else if ( 'event' == $sidebar_view ) {
	if ( is_singular( 'tribe_events' ) ) {
		$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'event_sidebar' ) );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'event_layout', 'none' ) );
		$fildisi_eutf_fixed_sidebar =  fildisi_eutf_post_meta( '_fildisi_eutf_fixed_sidebar' , fildisi_eutf_option( 'event_fixed_sidebar', 'no' ) );
	} else {
		$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'event_tax_sidebar' );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'event_tax_layout', 'none' );
		$fildisi_eutf_fixed_sidebar = fildisi_eutf_option( 'event_tax_fixed_sidebar', 'no' );
	}
} else if ( is_singular() ) {
	if ( is_singular( 'post' ) ) {
		$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'post_sidebar' ) );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'post_layout', 'none' ) );
		$fildisi_eutf_fixed_sidebar =  fildisi_eutf_post_meta( '_fildisi_eutf_fixed_sidebar' , fildisi_eutf_option( 'post_fixed_sidebar', 'no' ) );
	} else if ( is_singular( 'portfolio' ) ) {
		$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'portfolio_sidebar' ) );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'portfolio_layout', 'none' ) );
		$fildisi_eutf_fixed_sidebar =  fildisi_eutf_post_meta( '_fildisi_eutf_fixed_sidebar' , fildisi_eutf_option( 'portfolio_fixed_sidebar', 'no' ) );
		$fildisi_eutf_sidebar_extra_content = fildisi_eutf_check_portfolio_details();
		if( $fildisi_eutf_sidebar_extra_content && 'none' == $fildisi_eutf_sidebar_layout ) {
			$fildisi_eutf_sidebar_layout = 'right';
		}
	} else {
		$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'page_sidebar' ) );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'page_layout', 'none' ) );
		$fildisi_eutf_fixed_sidebar =  fildisi_eutf_post_meta( '_fildisi_eutf_fixed_sidebar' , fildisi_eutf_option( 'page_fixed_sidebar', 'no' ) );
	}
} else {
	$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'blog_sidebar' );
	$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'blog_layout', 'none' );
	$fildisi_eutf_fixed_sidebar = fildisi_eutf_option( 'blog_fixed_sidebar', 'no' );
}

if ( 'yes' == $fildisi_eutf_fixed_sidebar ) {
	$fixed = " eut-fixed-sidebar";
}

if ( 'none' != $fildisi_eutf_sidebar_layout && ( is_active_sidebar( $fildisi_eutf_sidebar_id ) || $fildisi_eutf_sidebar_extra_content ) ) {
	if ( 'left' == $fildisi_eutf_sidebar_layout || 'right' == $fildisi_eutf_sidebar_layout ) {

		$fildisi_eutf_sidebar_class = 'eut-sidebar' . $fixed;
?>
		<!-- Sidebar -->
		<aside id="eut-sidebar" class="<?php echo esc_attr( $fildisi_eutf_sidebar_class ); ?>">
			<div class="eut-wrapper clearfix">
				<?php
					if ( $fildisi_eutf_sidebar_extra_content ) {
						fildisi_eutf_print_portfolio_details();
					}
				?>
				<?php dynamic_sidebar( $fildisi_eutf_sidebar_id ); ?>
			</div>
		</aside>
		<!-- End Sidebar -->
<?php
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
