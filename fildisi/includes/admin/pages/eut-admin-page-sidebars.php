<?php
/*
*	Admin Custom Sidebars
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$fildisi_eutf_custom_sidebars = get_option( '_fildisi_eutf_custom_sidebars' );
?>
	<div id="eut-sidebar-wrap" class="wrap">
		<h2><?php esc_html_e( "Sidebars", 'fildisi' ); ?></h2>
		<?php fildisi_eutf_print_admin_links('sidebars'); ?>
		<br/>
		<?php if( isset( $_GET['sidebar-settings'] ) ) { ?>
		<div class="eut-sidebar-saved updated inline eut-notice-green">
			<p><strong><?php esc_html_e('Settings Saved!', 'fildisi' ); ?></strong></p>
		</div>
		<?php } ?>
		<div class="eut-sidebar-changed updated inline eut-notice-green">
			<p><strong><?php esc_html_e('Settings have changed, you should save them!', 'fildisi' ); ?></strong></p>
		</div>
		<form method="post" action="admin.php?page=fildisi-sidebars">
			<table class="eut-sidebar-table widefat" cellspacing="0">
				<thead>
					<tr>
						<th>
							<input type="button" id="eut-add-custom-sidebar-item" class="button button-primary" value="<?php esc_html_e('Add Sidebar', 'fildisi' ); ?>"/>
							<span class="eut-sidebar-spinner"></span>
						</th>
						<th>
							<input type="text" class="eut-sidebar-text" id="eut-custom-sidebar-item-name-new" value=""/>
							<div class="eut-sidebar-notice eut-notice-red">
								<strong><?php esc_html_e('Field must not be empty!', 'fildisi' ); ?></strong>
							</div>
							<div class="eut-sidebar-notice-exists eut-notice-red">
								<strong><?php esc_html_e('Sidebar with this name already exists!', 'fildisi' ); ?></strong>
							</div>
						</th>
					</tr>
				</thead>
				<tbody id="eut-custom-sidebar-container">
					<?php fildisi_eutf_print_admin_custom_sidebars( $fildisi_eutf_custom_sidebars ); ?>
				</tbody>
				<tfoot>
					<tr>
						<td><?php submit_button(); ?></td>
						<td>&nbsp;</td>
					</tr>
				</tfoot>
			</table>
			<?php wp_nonce_field( 'fildisi_eutf_nonce_sidebar_save', '_fildisi_eutf_nonce_sidebar_save' ); ?>

		</form>
	</div>
<?php


//Omit closing PHP tag to avoid accidental whitespace output errors.
