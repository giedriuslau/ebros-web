<?php
/*
*	'fildisi' Category Meta
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

	//Categories
	add_action('category_edit_form_fields','fildisi_eutf_category_edit_form_fields', 10);
	add_action('post_tag_edit_form_fields','fildisi_eutf_category_edit_form_fields', 10);
	add_action('product_cat_edit_form_fields','fildisi_eutf_category_edit_form_fields', 10);
	add_action('product_tag_edit_form_fields','fildisi_eutf_category_edit_form_fields', 10);
	add_action('edit_term','fildisi_eutf_save_category_fields', 10, 3);

	function fildisi_eutf_category_edit_form_fields( $term ) {
		$fildisi_eutf_term_meta = fildisi_eutf_get_term_meta( $term->term_id, '_fildisi_eutf_custom_title_options' );
		fildisi_eutf_print_category_fields( $fildisi_eutf_term_meta );
	}

	function fildisi_eutf_print_category_fields( $fildisi_eutf_custom_title_options = array() ) {
?>
		<tr class="form-field">
			<td colspan="2">
				<div id="eut-category-title" class="postbox">
<?php

			//Custom Title Option
			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select',
					'name' => 'fildisi_eutf_term_meta[custom]',
					'id' => 'eut-category-title-custom',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'custom' ),
					'options' => array(
						'' => esc_html__( '-- Inherit --', 'fildisi' ),
						'custom' => esc_html__( 'Custom', 'fildisi' ),

					),
					'label' => array(
						"title" => esc_html__( 'Title Options', 'fildisi' ),
					),
					'group_id' => 'eut-category-title',
					'highlight' => 'highlight',
				)
			);

			global $fildisi_eutf_area_height;
			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select',
					'options' => $fildisi_eutf_area_height,
					'name' => 'fildisi_eutf_term_meta[height]',
					'id' => 'eut-category-title-height',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'height', '40' ),
					'label' => array(
						"title" => esc_html__( 'Title Area Height', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);
			fildisi_eutf_print_admin_option(
				array(
					'type' => 'textfield',
					'name' => 'fildisi_eutf_term_meta[min_height]',
					'id' => 'eut-category-title-min-height',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'min_height', '200' ),
					'label' => array(
						"title" => esc_html__( 'Title Area Minimum Height in px', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);

			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select',
					'name' => 'fildisi_eutf_term_meta[container_size]',
					'id' => 'eut-category-title-container-size',
					'options' => array(
						'' => esc_html__( 'Default', 'fildisi' ),
						'large' => esc_html__( 'Large', 'fildisi' ),
					),
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'container_size' ),
					'label' => array(
						"title" => esc_html__( 'Container Size', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);

			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-colorpicker',
					'name' => 'fildisi_eutf_term_meta[bg_color]',
					'id' => 'eut-category-title-bg-color',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'bg_color', 'dark' ),
					'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'bg_color_custom', '#000000' ),
					'label' => array(
						"title" => esc_html__( 'Background Color', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
					'multiple' => 'multi',
				)
			);
			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-colorpicker',
					'name' => 'fildisi_eutf_term_meta[content_bg_color]',
					'id' => 'eut-category-title-content-bg-color',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_bg_color', 'none' ),
					'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_bg_color_custom', '#ffffff' ),
					'label' => array(
						"title" => esc_html__( 'Content Background Color', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
					'multiple' => 'multi',
					'type_usage' => 'title-content-bg',
				)
			);
			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-colorpicker',
					'name' => 'fildisi_eutf_term_meta[title_color]',
					'id' => 'eut-category-title-title-color',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'title_color', 'light' ),
					'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'title_color_custom', '#ffffff' ),
					'label' => array(
						"title" => esc_html__( 'Title Color', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
					'multiple' => 'multi',
				)
			);

			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-colorpicker',
					'name' => 'fildisi_eutf_term_meta[caption_color]',
					'id' => 'eut-category-title-caption_color',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'caption_color', 'light' ),
					'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'caption_color_custom', '#ffffff' ),
					'label' => array(
						"title" => esc_html__( 'Description Color', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
					'multiple' => 'multi',
				)
			);

			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select',
					'name' => 'fildisi_eutf_term_meta[content_size]',
					'id' => 'eut-category-title-content-size',
					'options' => array(
						'large' => esc_html__( 'Large', 'fildisi' ),
						'medium' => esc_html__( 'Medium', 'fildisi' ),
						'small' => esc_html__( 'Small', 'fildisi' ),
					),
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_size', 'large' ),
					'label' => array(
						"title" => esc_html__( 'Content Size', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);

			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-align',
					'name' => 'fildisi_eutf_term_meta[content_alignment]',
					'id' => 'eut-category-title-content-alignment',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_alignment', 'center' ),
					'label' => array(
						"title" => esc_html__( 'Content Alignment', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);

			global $fildisi_eutf_media_bg_position_selection;
			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select',
					'name' => 'fildisi_eutf_term_meta[content_position]',
					'id' => 'eut-category-title-content_position',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_position', 'center-center' ),
					'options' => $fildisi_eutf_media_bg_position_selection,
					'label' => array(
						"title" => esc_html__( 'Content Position', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);

			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-text-animation',
					'name' => 'fildisi_eutf_term_meta[content_animation]',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'content_animation', 'fade-in' ),
					'label' => esc_html__( 'Content Animation', 'fildisi' ),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);


			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select',
					'name' => 'fildisi_eutf_term_meta[bg_mode]',
					'id' => 'eut-category-title-bg-mode',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'bg_mode'),
					'options' => array(
						'' => esc_html__( 'Color Only', 'fildisi' ),
						'custom' => esc_html__( 'Custom Image', 'fildisi' ),

					),
					'label' => array(
						"title" => esc_html__( 'Background', 'fildisi' ),
					),
					'group_id' => 'eut-category-title',
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }

					]',
				)
			);
			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-image',
					'name' => 'fildisi_eutf_term_meta[bg_image_id]',
					'id' => 'eut-category-title-bg-image-id',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'bg_image_id'),
					'label' => array(
						"title" => esc_html__( 'Background Image', 'fildisi' ),
					),
					'width' => 'fullwidth',
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] },
						{ "id" : "eut-category-title-bg-mode", "values" : ["custom"] }

					]',
				)
			);
			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-bg-position',
					'name' => 'fildisi_eutf_term_meta[bg_position]',
					'id' => 'eut-category-title-bg-position',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'bg_position', 'center-center'),
					'label' => array(
						"title" => esc_html__( 'Background Position', 'fildisi' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] },
						{ "id" : "eut-category-title-bg-mode", "values" : ["custom"] }
					]',
				)
			);

			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-pattern-overlay',
					'name' => 'fildisi_eutf_term_meta[pattern_overlay]',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'pattern_overlay'),
					'label' => esc_html__( 'Pattern Overlay', 'fildisi' ),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] },
						{ "id" : "eut-category-title-bg-mode", "values" : ["custom"] }
					]',
				)
			);
			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-colorpicker',
					'name' => 'fildisi_eutf_term_meta[color_overlay]',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'color_overlay', 'dark' ),
					'value2' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'color_overlay_custom', '#000000' ),
					'label' => esc_html__( 'Color Overlay', 'fildisi' ),
					'multiple' => 'multi',
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] },
						{ "id" : "eut-category-title-bg-mode", "values" : ["custom"] }
					]',
				)
			);
			fildisi_eutf_print_admin_option(
				array(
					'type' => 'select-opacity',
					'name' => 'fildisi_eutf_term_meta[opacity_overlay]',
					'value' => fildisi_eutf_array_value( $fildisi_eutf_custom_title_options, 'opacity_overlay', '0' ),
					'label' => esc_html__( 'Opacity Overlay', 'fildisi' ),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] },
						{ "id" : "eut-category-title-bg-mode", "values" : ["custom"] }
					]',
				)
			);
?>
			</div>
		</td>
	</tr>
<?php
	}

	//Save Category Meta
	function fildisi_eutf_save_category_fields( $term_id, $tt_id = '', $taxonomy = '' ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$custom_meta_tax = array ( 'category', 'post_tag', 'product_cat', 'product_tag' );

		if ( isset( $_POST['fildisi_eutf_term_meta'] ) && in_array( $taxonomy, $custom_meta_tax ) ) {
			$fildisi_eutf_term_meta = fildisi_eutf_get_term_meta( $term_id, '_fildisi_eutf_custom_title_options' );
			$cat_keys = array_keys( $_POST['fildisi_eutf_term_meta'] );
			foreach ( $cat_keys as $key ) {
				if ( isset( $_POST['fildisi_eutf_term_meta'][$key] ) ) {
					$fildisi_eutf_term_meta[$key] = sanitize_text_field( $_POST['fildisi_eutf_term_meta'][$key] );
				}
			}
			fildisi_eutf_update_term_meta( $term_id , '_fildisi_eutf_custom_title_options', $fildisi_eutf_term_meta );
		}
	}

//Omit closing PHP tag to avoid accidental whitespace output errors.
