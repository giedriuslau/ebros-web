<?php

/*
*	Visual Composer Extension Plugin Hooks
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

/**
 * Translation function returning the theme translations
 */

/* All */
function fildisi_eutf_theme_vce_get_string_all() {
    return esc_html__( 'All', 'fildisi' );
}
/* Read more */
function fildisi_eutf_theme_vce_get_string_read_more() {
    return esc_html__( 'read more', 'fildisi' );
}
/* In Categories */
function fildisi_eutf_theme_vce_get_string_categories_in() {
    return esc_html__( 'in', 'fildisi' );
}

/* Author By */
function fildisi_eutf_theme_vce_get_string_by_author() {
    return esc_html__( 'By:', 'fildisi' );
}

/* E-mail */
function fildisi_eutf_theme_vce_get_string_email() {
    return esc_html__( 'E-mail', 'fildisi' );
}

/**
 * Hooks for portfolio translations
 */

add_filter( 'fildisi_eutf_vce_portfolio_string_all_categories', 'fildisi_eutf_theme_vce_get_string_all' );

 /**
 * Hooks for blog translations
 */

add_filter( 'fildisi_eutf_vce_string_read_more', 'fildisi_eutf_theme_vce_get_string_read_more' );
add_filter( 'fildisi_eutf_vce_blog_string_all_categories', 'fildisi_eutf_theme_vce_get_string_all' );
add_filter( 'fildisi_eutf_vce_blog_string_categories_in', 'fildisi_eutf_theme_vce_get_string_categories_in' );
add_filter( 'fildisi_eutf_vce_blog_string_by_author', 'fildisi_eutf_theme_vce_get_string_by_author' );




 /**
 * Hooks for general translations
 */
add_filter( 'fildisi_eutf_vce_gallery_string_all_categories', 'fildisi_eutf_theme_vce_get_string_all' );
add_filter( 'fildisi_eutf_vce_product_string_all_categories', 'fildisi_eutf_theme_vce_get_string_all' );
add_filter( 'fildisi_eutf_vce_event_string_all_categories', 'fildisi_eutf_theme_vce_get_string_all' );
add_filter( 'fildisi_eutf_vce_string_email', 'fildisi_eutf_theme_vce_get_string_email' );

//Omit closing PHP tag to avoid accidental whitespace output errors.
