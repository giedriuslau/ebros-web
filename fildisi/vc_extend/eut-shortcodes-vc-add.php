<?php
/*
 *	Euthemians Visual Composer Shortcode Extensions
 *
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
 */


if ( function_exists( 'vc_add_param' ) ) {

	//Generic css aniation for elements

	$fildisi_eutf_add_animation = array(
		"type" => "dropdown",
		"heading" => esc_html__("CSS Animation", 'fildisi' ),
		"param_name" => "animation",
		"admin_label" => true,
		"value" => array(
			esc_html__( "No", "fildisi" ) => '',
			esc_html__( "Fade In", "fildisi" ) => "eut-fade-in",
			esc_html__( "Fade In Up", "fildisi" ) => "eut-fade-in-up",
			esc_html__( "Fade In Up Big", "fildisi" ) => "eut-fade-in-up-big",
			esc_html__( "Fade In Down", "fildisi" ) => "eut-fade-in-down",
			esc_html__( "Fade In Down Big", "fildisi" ) => "eut-fade-in-down-big",
			esc_html__( "Fade In Left", "fildisi" ) => "eut-fade-in-left",
			esc_html__( "Fade In Left Big", "fildisi" ) => "eut-fade-in-left-big",
			esc_html__( "Fade In Right", "fildisi" ) => "eut-fade-in-right",
			esc_html__( "Fade In Right Big", "fildisi" ) => "eut-fade-in-right-big",
			esc_html__( "Zoom In", "fildisi" ) => "eut-zoom-in",
		),
		"description" => esc_html__("Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", 'fildisi' ),
	);

	$fildisi_eutf_add_animation_delay = array(
		"type" => "textfield",
		"heading" => esc_html__( 'Css Animation Delay', 'fildisi' ),
		"param_name" => "animation_delay",
		"value" => '200',
		"description" => esc_html__( "Add delay in milliseconds.", 'fildisi' ),
	);

	$fildisi_eutf_add_animation_duration = array(
		"type" => "dropdown",
		"heading" => esc_html__("CSS Animation Duration", 'fildisi' ),
		"param_name" => "animation_duration",
		"value" => array(
			esc_html__( "Very Fast", "fildisi" ) => "very-fast",
			esc_html__( "Fast", "fildisi" ) => "fast",
			esc_html__( "Normal", "fildisi" ) => "normal",
			esc_html__( "Slow", "fildisi" ) => "slow",
			esc_html__( "Very Slow", "fildisi" ) => "very-slow",
		),
		"std" => 'normal',
		"description" => esc_html__("Select the duration for your animated element.", 'fildisi' ),
	);

	$fildisi_eutf_add_margin_bottom = array(
		"type" => "textfield",
		"heading" => esc_html__( 'Bottom margin', 'fildisi' ),
		"param_name" => "margin_bottom",
		"description" => esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'fildisi' ),
	);

	$fildisi_eutf_add_el_class = array(
		"type" => "textfield",
		"heading" => esc_html__("Extra class name", 'fildisi' ),
		"param_name" => "el_class",
		"description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'fildisi' ),
	);
	$fildisi_eutf_add_el_wrapper_class = array(
		"type" => "textfield",
		"heading" => esc_html__("Wrapper class name", 'fildisi' ),
		"param_name" => "el_wrapper_class",
		"description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'fildisi' ),
	);

	$fildisi_eutf_column_width_list = array(
		esc_html__( '1 column - 1/12', 'fildisi' ) => '1/12',
		esc_html__( '2 columns - 1/6', 'fildisi' ) => '1/6',
		esc_html__( '3 columns - 1/4', 'fildisi' ) => '1/4',
		esc_html__( '4 columns - 1/3', 'fildisi' ) => '1/3',
		esc_html__( '5 columns - 5/12', 'fildisi' ) => '5/12',
		esc_html__( '6 columns - 1/2', 'fildisi' ) => '1/2',
		esc_html__( '7 columns - 7/12', 'fildisi' ) => '7/12',
		esc_html__( '8 columns - 2/3', 'fildisi' ) => '2/3',
		esc_html__( '9 columns - 3/4', 'fildisi' ) => '3/4',
		esc_html__( '10 columns - 5/6', 'fildisi' ) => '5/6',
		esc_html__( '11 columns - 11/12', 'fildisi' ) => '11/12',
		esc_html__( '12 columns - 1/1', 'fildisi' ) => '1/1'
	);

	$fildisi_eutf_column_desktop_hide_list = array(
		esc_html__( 'Default value from width attribute', 'fildisi') => '',
		esc_html__( 'Hide', 'fildisi' ) => 'hide',
	);

	$fildisi_eutf_column_width_tablet_list = array(
		esc_html__( 'Default value from width attribute', 'fildisi') => '',
		esc_html__( 'Hide', 'fildisi' ) => 'hide',
		esc_html__( '1 column - 1/12', 'fildisi' ) => '1-12',
		esc_html__( '2 columns - 1/6', 'fildisi' ) => '1-6',
		esc_html__( '3 columns - 1/4', 'fildisi' ) => '1-4',
		esc_html__( '4 columns - 1/3', 'fildisi' ) => '1-3',
		esc_html__( '5 columns - 5/12', 'fildisi' ) => '5-12',
		esc_html__( '6 columns - 1/2', 'fildisi' ) => '1-2',
		esc_html__( '7 columns - 7/12', 'fildisi' ) => '7-12',
		esc_html__( '8 columns - 2/3', 'fildisi' ) => '2-3',
		esc_html__( '9 columns - 3/4', 'fildisi' ) => '3-4',
		esc_html__( '10 columns - 5/6', 'fildisi' ) => '5-6',
		esc_html__( '11 columns - 11/12', 'fildisi' ) => '11-12',
		esc_html__( '12 columns - 1/1', 'fildisi' ) => '1',
	);

	$fildisi_eutf_column_width_tablet_sm_list = array(
		esc_html__( 'Inherit from Tablet Landscape', 'fildisi') => '',
		esc_html__( 'Hide', 'fildisi' ) => 'hide',
		esc_html__( '1 column - 1/12', 'fildisi' ) => '1-12',
		esc_html__( '2 columns - 1/6', 'fildisi' ) => '1-6',
		esc_html__( '3 columns - 1/4', 'fildisi' ) => '1-4',
		esc_html__( '4 columns - 1/3', 'fildisi' ) => '1-3',
		esc_html__( '5 columns - 5/12', 'fildisi' ) => '5-12',
		esc_html__( '6 columns - 1/2', 'fildisi' ) => '1-2',
		esc_html__( '7 columns - 7/12', 'fildisi' ) => '7-12',
		esc_html__( '8 columns - 2/3', 'fildisi' ) => '2-3',
		esc_html__( '9 columns - 3/4', 'fildisi' ) => '3-4',
		esc_html__( '10 columns - 5/6', 'fildisi' ) => '5-6',
		esc_html__( '11 columns - 11/12', 'fildisi' ) => '11-12',
		esc_html__( '12 columns - 1/1', 'fildisi' ) => '1',
	);
	$fildisi_eutf_column_mobile_width_list = array(
		esc_html__( 'Default value 12 columns - 1/1', 'fildisi' ) => '',
		esc_html__( 'Hide', 'fildisi' ) => 'hide',
		esc_html__( '3 columns - 1/4', 'fildisi' ) => '1-4',
		esc_html__( '4 columns - 1/3', 'fildisi' ) => '1-3',
		esc_html__( '6 columns - 1/2', 'fildisi' ) => '1-2',
		esc_html__( '12 columns - 1/1', 'fildisi' ) => '1',
	);

	//Add additional column options for Page Builder 5.5
	if ( defined( 'WPB_VC_VERSION' ) && version_compare( WPB_VC_VERSION, '5.5', '>=' ) ) {
		$fildisi_eutf_extra_list = array(
			esc_html__( '20% - 1/5', 'fildisi' ) => '1/5',
			esc_html__( '40% - 2/5', 'fildisi' ) => '2/5',
			esc_html__( '60% - 3/5', 'fildisi' ) => '3/5',
			esc_html__( '80% - 4/5', 'fildisi' ) => '4/5',
		);
		$fildisi_eutf_column_width_list = array_merge( $fildisi_eutf_column_width_list, $fildisi_eutf_extra_list);

		$fildisi_eutf_extra_list_simplified = array(
			esc_html__( '20% - 1/5', 'fildisi' ) => '1-5',
			esc_html__( '40% - 2/5', 'fildisi' ) => '2-5',
			esc_html__( '60% - 3/5', 'fildisi' ) => '3-5',
			esc_html__( '80% - 4/5', 'fildisi' ) => '4-5',
		);
		$fildisi_eutf_column_width_tablet_list = array_merge( $fildisi_eutf_column_width_tablet_list, $fildisi_eutf_extra_list_simplified );
		$fildisi_eutf_column_width_tablet_sm_list = array_merge( $fildisi_eutf_column_width_tablet_sm_list, $fildisi_eutf_extra_list_simplified );
		$fildisi_eutf_column_mobile_width_list = array_merge( $fildisi_eutf_column_mobile_width_list, $fildisi_eutf_extra_list_simplified );
	}

	$fildisi_eutf_column_gap_list = array(
		esc_html__( 'No Gap', 'fildisi' ) => 'none',
		esc_html__( '5px', 'fildisi' ) => '5',
		esc_html__( '10px', 'fildisi' ) => '10',
		esc_html__( '15px', 'fildisi' ) => '15',
		esc_html__( '20px', 'fildisi' ) => '20',
		esc_html__( '25px', 'fildisi' ) => '25',
		esc_html__( '30px', 'fildisi' ) => '30',
		esc_html__( '35px', 'fildisi' ) => '35',
		esc_html__( '40px', 'fildisi' ) => '40',
		esc_html__( '45px', 'fildisi' ) => '45',
		esc_html__( '50px', 'fildisi' ) => '50',
		esc_html__( '55px', 'fildisi' ) => '55',
		esc_html__( '60px', 'fildisi' ) => '60',
	);

	$fildisi_eutf_position_list = array(
		esc_html__( "None", 'fildisi' ) => '',
		esc_html__( "1x", 'fildisi' ) => '1x',
		esc_html__( "2x", 'fildisi' ) => '2x',
		esc_html__( "3x", 'fildisi' ) => '3x',
		esc_html__( "4x", 'fildisi' ) => '4x',
		esc_html__( "5x", 'fildisi' ) => '5x',
		esc_html__( "6x", 'fildisi' ) => '6x',
		esc_html__( "-1x", 'fildisi' ) => 'minus-1x',
		esc_html__( "-2x", 'fildisi' ) => 'minus-2x',
		esc_html__( "-3x", 'fildisi' ) => 'minus-3x',
		esc_html__( "-4x", 'fildisi' ) => 'minus-4x',
		esc_html__( "-5x", 'fildisi' ) => 'minus-5x',
		esc_html__( "-6x", 'fildisi' ) => 'minus-6x',
	);

	$fildisi_eutf_separator_list = array(
		esc_html__( "None", 'fildisi' ) => '',
		esc_html__( "Triangle", 'fildisi' ) => 'triangle-separator',
		esc_html__( "Large Triangle", 'fildisi' ) => 'large-triangle-separator',
		esc_html__( "Curve", 'fildisi' ) => 'curve-separator',
		esc_html__( "Curve Left", 'fildisi' ) => 'curve-left-separator',
		esc_html__( "Curve Right", 'fildisi' ) => 'curve-right-separator',
		esc_html__( "Tilt Left", 'fildisi' ) => 'tilt-left-separator',
		esc_html__( "Tilt Right", 'fildisi' ) => 'tilt-right-separator',
		esc_html__( "Round Split", 'fildisi' ) => 'round-split-separator',
		esc_html__( "Torn Paper", 'fildisi' ) => 'torn-paper-separator',
	);

	$fildisi_eutf_separator_size_list = array(
		esc_html__( "Small", 'fildisi' ) => '30px',
		esc_html__( "Medium", 'fildisi' ) => '60px',
		esc_html__( "Large", 'fildisi' ) => '90px',
		esc_html__( "Extra Large", 'fildisi' ) => '120px',
		esc_html__( "Section Height", 'fildisi' ) => '100%',
	);

	//Title Headings/Tags
	if( !function_exists( 'fildisi_eutf_get_heading_tag' ) ) {
		function fildisi_eutf_get_heading_tag( $std = '' ) {
			return	array(
				"type" => "dropdown",
				"heading" => esc_html__( "Title Tag", "fildisi" ),
				"param_name" => "heading_tag",
				"value" => array(
					esc_html__( "h1", "fildisi" ) => 'h1',
					esc_html__( "h2", "fildisi" ) => 'h2',
					esc_html__( "h3", "fildisi" ) => 'h3',
					esc_html__( "h4", "fildisi" ) => 'h4',
					esc_html__( "h5", "fildisi" ) => 'h5',
					esc_html__( "h6", "fildisi" ) => 'h6',
					esc_html__( "div", "fildisi" ) => 'div',
				),
				"description" => esc_html__( "Title Tag for SEO", "fildisi" ),
				"std" => $std,
				"group" => esc_html__( "Titles & Styles", "fildisi" ),
			);
		}
	}

	if( !function_exists( 'fildisi_eutf_get_heading' ) ) {
		function fildisi_eutf_get_heading( $std = '' ) {
			return	array(
				"type" => "dropdown",
				"heading" => esc_html__( "Title Size/Typography", "fildisi" ),
				"param_name" => "heading",
				"value" => array(
					esc_html__( "h1", "fildisi" ) => 'h1',
					esc_html__( "h2", "fildisi" ) => 'h2',
					esc_html__( "h3", "fildisi" ) => 'h3',
					esc_html__( "h4", "fildisi" ) => 'h4',
					esc_html__( "h5", "fildisi" ) => 'h5',
					esc_html__( "h6", "fildisi" ) => 'h6',
					esc_html__( "Leader Text", "fildisi" ) => 'leader-text',
					esc_html__( "Subtitle Text", "fildisi" ) => 'subtitle-text',
					esc_html__( "Small Text", "fildisi" ) => 'small-text',
					esc_html__( "Link Text", "fildisi" ) => 'link-text',
				),
				"description" => esc_html__( "Title size and typography, defined in Theme Options - Typography Options", "fildisi" ),
				"std" => $std,
				"group" => esc_html__( "Titles & Styles", "fildisi" ),
			);
		}
	}
	if( !function_exists( 'fildisi_eutf_get_custom_font_family' ) ) {
		function fildisi_eutf_get_custom_font_family( $std = '' ) {
			return	array(
				"type" => "dropdown",
				"heading" => esc_html__( "Custom Font Family", "fildisi" ),
				"param_name" => "custom_font_family",
				"value" => array(
					esc_html__( "Same as Typography", "fildisi" ) => '',
					esc_html__( "Custom Font Family 1", "fildisi" ) => 'custom-font-1',
					esc_html__( "Custom Font Family 2", "fildisi" ) => 'custom-font-2',
					esc_html__( "Custom Font Family 3", "fildisi" ) => 'custom-font-3',
					esc_html__( "Custom Font Family 4", "fildisi" ) => 'custom-font-4',

				),
				"description" => esc_html__( "Select a different font family, defined in Theme Options - Typography Options - Extras - Custom Font Family", "fildisi" ),
				"std" => $std,
				"group" => esc_html__( "Titles & Styles", "fildisi" ),
			);
		}
	}


	vc_add_param('vc_tta_tabs', fildisi_eutf_get_heading_tag('h3') );
	vc_add_param('vc_tta_tabs', fildisi_eutf_get_heading('h6') );
	vc_add_param('vc_tta_tabs', fildisi_eutf_get_custom_font_family() );
	vc_add_param('vc_tta_tour', fildisi_eutf_get_heading_tag('h3') );
	vc_add_param('vc_tta_tour', fildisi_eutf_get_heading('h6') );
	vc_add_param('vc_tta_tour', fildisi_eutf_get_custom_font_family() );
	vc_add_param('vc_tta_accordion', fildisi_eutf_get_heading_tag('h3') );
	vc_add_param('vc_tta_accordion', fildisi_eutf_get_heading('h6') );
	vc_add_param('vc_tta_accordion', fildisi_eutf_get_custom_font_family() );

	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__('Section ID', 'fildisi' ),
			"param_name" => "section_id",
			"description" => esc_html__("If you wish you can type an id to use it as bookmark.", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__('Font Color', 'fildisi' ),
			"param_name" => "font_color",
			"description" => esc_html__("Select font color", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Heading Color", 'fildisi' ),
			"param_name" => "heading_color",
			"value" => array(
				esc_html__( "Default", 'fildisi' ) => '',
				esc_html__( "Dark", 'fildisi' ) => 'dark',
				esc_html__( "Light", 'fildisi' ) => 'light',
				esc_html__( "Primary 1", 'fildisi' ) => 'primary-1',
				esc_html__( "Primary 2", 'fildisi' ) => 'primary-2',
				esc_html__( "Primary 3", 'fildisi' ) => 'primary-3',
				esc_html__( "Primary 4", 'fildisi' ) => 'primary-4',
				esc_html__( "Primary 5", 'fildisi' ) => 'primary-5',
				esc_html__( "Primary 6", 'fildisi' ) => 'primary-6',
				esc_html__( "Green", 'fildisi' ) => 'green',
				esc_html__( "Orange", 'fildisi' ) => 'orange',
				esc_html__( "Red", 'fildisi' ) => 'red',
				esc_html__( "Blue", 'fildisi' ) => 'blue',
				esc_html__( "Aqua", 'fildisi' ) => 'aqua',
				esc_html__( "Purple", 'fildisi' ) => 'purple',
				esc_html__( "Grey", 'fildisi' ) => 'grey',
			),
			"description" => esc_html__( "Select heading color", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Reverse columns in RTL', 'fildisi' ),
			'param_name' => 'rtl_reverse',
			'description' => esc_html__( 'If checked columns will be reversed in RTL.', 'fildisi' ),
			'value' => array( esc_html__( 'Yes', 'fildisi' ) => 'yes' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Disable row', 'fildisi' ),
			'param_name' => 'disable_element',
			'description' => esc_html__( 'If checked the row won\'t be visible on the public side of your website. You can switch it back any time.', 'fildisi' ),
			'value' => array( esc_html__( 'Yes', 'fildisi' ) => 'yes' ),
		)
	);
	vc_add_param( "vc_row", $fildisi_eutf_add_el_class );
	vc_add_param( "vc_row", $fildisi_eutf_add_el_wrapper_class );

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Section Type", 'fildisi' ),
			"param_name" => "section_type",
			"value" => array(
				esc_html__( "Full Width Background", 'fildisi' ) => 'fullwidth-background',
				esc_html__( "Full Width Element", 'fildisi' ) => 'fullwidth',
			),
			"description" => esc_html__( "Select section type", 'fildisi' ),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Section Window Height", 'fildisi' ),
			"param_name" => "section_full_height",
			"value" => array(
				esc_html__( "No", 'fildisi' ) => 'no',
				esc_html__( "Yes", 'fildisi' ) => 'fullheight',
			),
			"description" => esc_html__( "Select if you want your section height to be equal with the window height", 'fildisi' ),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => 'dropdown',
			"heading" => esc_html__( "Background Type", 'fildisi' ),
			"param_name" => "bg_type",
			"description" => esc_html__( "Select Background type", 'fildisi' ),
			"value" => array(
				esc_html__( "None", 'fildisi' ) => '',
				esc_html__( "Color", 'fildisi' ) => 'color',
				esc_html__( "Gradient Color", 'fildisi' ) => 'gradient',
				esc_html__( "Image", 'fildisi' ) => 'image',
				esc_html__( "Hosted Video", 'fildisi' ) => 'hosted_video',
				esc_html__( "YouTube Video", 'fildisi' ) => 'video',
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'YouTube link', 'fildisi' ),
			'param_name' => 'bg_video_url',
			'value' => 'https://www.youtube.com/watch?v=C9tsrkRSw-Q',
			// default video url
			'description' => esc_html__( 'Add YouTube link.', 'fildisi' ),
			'dependency' => array(
				'element' => 'bg_type',
				'value' => 'video',
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Video Popup Button", 'fildisi' ),
			"param_name" => "bg_video_button",
			"value" => array(
				esc_html__( 'None', 'fildisi' ) => '',
				esc_html__( 'Devices only', 'fildisi' ) => 'device',
				esc_html__( 'Always visible', 'fildisi' ) => 'all',
			),
			"description" => esc_html__( "Select video popup button behavior", 'fildisi' ),
			'dependency' => array(
				'element' => 'bg_type',
				'value' => 'video',
			),
			"std" => 'center-center',
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Video Button Position", 'fildisi' ),
			"param_name" => "bg_video_button_position",
			"value" => array(
				esc_html__( 'Left Top', 'fildisi' ) => 'left-top',
				esc_html__( 'Left Bottom', 'fildisi' ) => 'left-bottom',
				esc_html__( 'Center Center', 'fildisi' ) => 'center-center',
				esc_html__( 'Right Top', 'fildisi' ) => 'right-top',
				esc_html__( 'Right Bottom', 'fildisi' ) => 'right-bottom',
			),
			"description" => esc_html__( "Select position for video popup", 'fildisi' ),
			'dependency' => array(
				'element' => 'bg_video_button',
				'value_not_equal_to' => array( '' )
			),
			"std" => 'center-center',
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__( "Custom Background Color", 'fildisi' ),
			"param_name" => "bg_color",
			"description" => esc_html__( "Select background color for your row", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'color' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__( "Custom Color 1", 'fildisi' ),
			"param_name" => "bg_gradient_color_1",
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'gradient' )
			),
			"std" => 'rgba(3,78,144,0.9)',
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__( "Custom Color 2", 'fildisi' ),
			"param_name" => "bg_gradient_color_2",
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'gradient' )
			),
			"std" => 'rgba(25,180,215,0.9)',
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Gradient Direction", 'fildisi' ),
			"param_name" => "bg_gradient_direction",
			"value" => array(
				esc_html__( "Left to Right", 'fildisi' ) => '90',
				esc_html__( "Left Top to Right Bottom", 'fildisi' ) => '135',
				esc_html__( "Left Bottom to Right Top", 'fildisi' ) => '45',
				esc_html__( "Bottom to Top", 'fildisi' ) => '180',
			),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'gradient' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "attach_image",
			"heading" => esc_html__('Background Image', 'fildisi' ),
			"param_name" => "bg_image",
			"value" => '',
			"description" => esc_html__("Select background image for your row. Used also as fallback for video.", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image', 'hosted_video', 'video' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Background Image Type", 'fildisi' ),
			"param_name" => "bg_image_type",
			"value" => array(
				esc_html__( "Default", 'fildisi' ) => '',
				esc_html__( "Parallax", 'fildisi' ) => 'parallax',
				esc_html__( "Horizontal Parallax Left to Right", 'fildisi' ) => 'horizontal-parallax-lr',
				esc_html__( "Horizontal Parallax Right to Left", 'fildisi' ) => 'horizontal-parallax-rl',
				esc_html__( "Animated", 'fildisi' ) => 'animated',
				esc_html__( "Horizontal Animation", 'fildisi' ) => 'horizontal',
				esc_html__( "Fixed Image", 'fildisi' ) => 'fixed',
				esc_html__( "Image usage as Pattern", 'fildisi' ) => 'pattern'
			),
			"description" => esc_html__( "Select how a background image will be displayed", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Background Image Size", 'fildisi' ),
			"param_name" => "bg_image_size",
			"value" => array(
				esc_html__( "--Inherit--", 'fildisi' ) => '',
				esc_html__( "Responsive", 'fildisi' ) => 'responsive',
				esc_html__( "Extra Extra Large", 'fildisi' ) => 'extra-extra-large',
				esc_html__( "Full", 'fildisi' ) => 'full',
			),
			"description" => esc_html__( "Select the size of your background image", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Background Image Vertical Position", 'fildisi' ),
			"param_name" => "bg_image_vertical_position",
			"value" => array(
				esc_html__( "Top", 'fildisi' ) => 'top',
				esc_html__( "Center", 'fildisi' ) => 'center',
				esc_html__( "Bottom", 'fildisi' ) => 'bottom',
			),
			"description" => esc_html__( "Select vertical position for background image", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_image_type',
				'value' => array( 'horizontal-parallax-lr', 'horizontal-parallax-rl', 'horizontal' )
			),
			"std" => 'center',
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Background  Position", 'fildisi' ),
			"param_name" => "bg_position",
			"value" => array(
				esc_html__( 'Left Top', 'fildisi' ) => 'left-top',
				esc_html__( 'Left Center', 'fildisi' ) => 'left-center',
				esc_html__( 'Left Bottom', 'fildisi' ) => 'left-bottom',
				esc_html__( 'Center Top', 'fildisi' ) => 'center-top',
				esc_html__( 'Center Center', 'fildisi' ) => 'center-center',
				esc_html__( 'Center Bottom', 'fildisi' ) => 'center-bottom',
				esc_html__( 'Right Top', 'fildisi' ) => 'right-top',
				esc_html__( 'Right Center', 'fildisi' ) => 'right-center',
				esc_html__( 'Right Bottom', 'fildisi' ) => 'right-bottom',
			),
			"description" => esc_html__( "Select position for background image", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_image_type',
				'value' => array( '', 'animated' )
			),
			"std" => 'center-center',
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Background Position ( Tablet Portrait )", 'fildisi' ),
			"param_name" => "bg_tablet_sm_position",
			"value" => array(
				esc_html__( 'Inherit from above', 'fildisi' ) => '',
				esc_html__( 'Left Top', 'fildisi' ) => 'left-top',
				esc_html__( 'Left Center', 'fildisi' ) => 'left-center',
				esc_html__( 'Left Bottom', 'fildisi' ) => 'left-bottom',
				esc_html__( 'Center Top', 'fildisi' ) => 'center-top',
				esc_html__( 'Center Center', 'fildisi' ) => 'center-center',
				esc_html__( 'Center Bottom', 'fildisi' ) => 'center-bottom',
				esc_html__( 'Right Top', 'fildisi' ) => 'right-top',
				esc_html__( 'Right Center', 'fildisi' ) => 'right-center',
				esc_html__( 'Right Bottom', 'fildisi' ) => 'right-bottom',
			),
			"description" => esc_html__( "Tablet devices with portrait orientation and below.", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_image_type',
				'value' => array( '', 'animated' )
			),
			"std" => '',
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Parallax Sensor", 'fildisi' ),
			"param_name" => "parallax_threshold",
			"value" => array(
				esc_html__( "Low", 'fildisi' ) => '0.1',
				esc_html__( "Normal", 'fildisi' ) => '0.3',
				esc_html__( "High", 'fildisi' ) => '0.5',
				esc_html__( "Max", 'fildisi' ) => '0.8',
			),
			"description" => esc_html__( "Define the appearance for the parallax effect. Note that you get greater image zoom when you increase the parallax sensor.", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_image_type',
				'value' => array( 'parallax', 'horizontal-parallax-lr', 'horizontal-parallax-rl' )
			),
			"std" => '0.3',
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__("WebM File URL", 'fildisi'),
			"param_name" => "bg_video_webm",
			"description" => esc_html__( "Fill WebM and mp4 format for browser compatibility", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__( "MP4 File URL", 'fildisi' ),
			"param_name" => "bg_video_mp4",
			"description" => esc_html__( "Fill mp4 format URL", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__( "OGV File URL", 'fildisi' ),
			"param_name" => "bg_video_ogv",
			"description" => esc_html__( "Fill OGV format URL ( optional )", 'fildisi' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Loop", 'fildisi' ),
			"param_name" => "bg_video_loop",
			"value" => array(
				esc_html__( "Yes", 'fildisi' ) => 'yes',
				esc_html__( "No", 'fildisi' ) => 'no',
			),
			"std" => 'yes',
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Allow on devices", 'fildisi' ),
			"param_name" => "bg_video_device",
			"value" => array(
				esc_html__( "No", 'fildisi' ) => 'no',
				esc_html__( "Yes", 'fildisi' ) => 'yes',

			),
			"std" => 'no',
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => esc_html__( "Pattern overlay", 'fildisi'),
			"param_name" => "pattern_overlay",
			"description" => esc_html__( "If selected, a pattern will be added.", 'fildisi' ),
			"value" => Array(esc_html__( "Add pattern", 'fildisi' ) => 'yes'),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image', 'hosted_video', 'video' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Color overlay", 'fildisi' ),
			"param_name" => "color_overlay",
			"value" => array(
				esc_html__( "None", 'fildisi' ) => '',
				esc_html__( "Dark", 'fildisi' ) => 'dark',
				esc_html__( "Light", 'fildisi' ) => 'light',
				esc_html__( "Primary 1", 'fildisi' ) => 'primary-1',
				esc_html__( "Primary 2", 'fildisi' ) => 'primary-2',
				esc_html__( "Primary 3", 'fildisi' ) => 'primary-3',
				esc_html__( "Primary 4", 'fildisi' ) => 'primary-4',
				esc_html__( "Primary 5", 'fildisi' ) => 'primary-5',
				esc_html__( "Primary 6", 'fildisi' ) => 'primary-6',
				esc_html__( "Custom", 'fildisi' ) => 'custom',
				esc_html__( "Gradient", 'fildisi' ) => 'gradient',
			),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image', 'hosted_video', 'video' )
			),
			"description" => esc_html__( "A color overlay for the media", 'fildisi' ),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__('Custom Color Overlay', 'fildisi' ),
			"param_name" => "color_overlay_custom",
			"dependency" => array(
				'element' => 'color_overlay',
				'value' => array( 'custom' )
			),
			"std" => 'rgba(255,255,255,0.1)',
			"description" => esc_html__("Select custom color overlay", 'fildisi' ),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__('Gradient Color Overlay 1', 'fildisi' ),
			"param_name" => "gradient_overlay_custom_1",
			"dependency" => array(
				'element' => 'color_overlay',
				'value' => array( 'gradient' )
			),
			"std" => 'rgba(3,78,144,0.9)',
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__('Gradient Color Overlay 2', 'fildisi' ),
			"param_name" => "gradient_overlay_custom_2",
			"dependency" => array(
				'element' => 'color_overlay',
				'value' => array( 'gradient' )
			),
			"std" => 'rgba(25,180,215,0.9)',
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Gradient Direction", 'fildisi' ),
			"param_name" => "gradient_overlay_direction",
			"value" => array(
				esc_html__( "Left to Right", 'fildisi' ) => '90',
				esc_html__( "Left Top to Right Bottom", 'fildisi' ) => '135',
				esc_html__( "Left Bottom to Right Top", 'fildisi' ) => '45',
				esc_html__( "Bottom to Top", 'fildisi' ) => '180',
			),
			"dependency" => array(
				'element' => 'color_overlay',
				'value' => array( 'gradient' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Opacity overlay", 'fildisi' ),
			"param_name" => "opacity_overlay",
			"value" => array( '10', '20', '30' ,'40', '50', '60', '70', '80' ,'90' ),
			"description" => esc_html__( "Opacity of the overlay", 'fildisi' ),
			"dependency" => array(
				'element' => 'color_overlay',
				'value' => array( 'dark', 'light', 'primary-1', 'primary-2', 'primary-3', 'primary-4', 'primary-5', 'primary-6' )
			),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Top padding", 'fildisi' ),
			"param_name" => "padding_top_multiplier",
			"value" => array(
				esc_html__( "None", 'fildisi' ) => '',
				esc_html__( "1x", 'fildisi' ) => '1x',
				esc_html__( "2x", 'fildisi' ) => '2x',
				esc_html__( "3x", 'fildisi' ) => '3x',
				esc_html__( "4x", 'fildisi' ) => '4x',
				esc_html__( "5x", 'fildisi' ) => '5x',
				esc_html__( "6x", 'fildisi' ) => '6x',
				esc_html__( "Custom", 'fildisi' ) => 'custom',
			),
			"std" => '1x',
			"description" => esc_html__( "Select padding top for your section.", 'fildisi' ),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__( "Custom Top padding", 'fildisi' ),
			"param_name" => "padding_top",
			"dependency" => array(
				'element' => 'padding_top_multiplier',
				'value' => array( 'custom' )
			),
			"description" => esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'fildisi' ),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Bottom padding", 'fildisi' ),
			"param_name" => "padding_bottom_multiplier",
			"value" => array(
				esc_html__( "None", 'fildisi' ) => '',
				esc_html__( "1x", 'fildisi' ) => '1x',
				esc_html__( "2x", 'fildisi' ) => '2x',
				esc_html__( "3x", 'fildisi' ) => '3x',
				esc_html__( "4x", 'fildisi' ) => '4x',
				esc_html__( "5x", 'fildisi' ) => '5x',
				esc_html__( "6x", 'fildisi' ) => '6x',
				esc_html__( "Custom", 'fildisi' ) => 'custom',
			),
			"std" => '1x',
			"description" => esc_html__( "Select padding bottom for your section.", 'fildisi' ),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__( "Custom Bottom padding", 'fildisi' ),
			"param_name" => "padding_bottom",
			"dependency" => array(
				'element' => 'padding_bottom_multiplier',
				'value' => array( 'custom' )
			),
			"description" => esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'fildisi' ),
			"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
		"type" => "textfield",
		"heading" => esc_html__( 'Bottom margin', 'fildisi' ),
		"param_name" => "margin_bottom",
		"description" => esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'fildisi' ),
		"group" => esc_html__( "Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Columns Gap", 'fildisi' ),
			"param_name" => "columns_gap",
			'value' => $fildisi_eutf_column_gap_list,
			"description" => esc_html__( "Select gap between columns in row.", 'fildisi' ),
			"group" => esc_html__( "Inner Columns", 'fildisi' ),
			"std" => '30',
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Equal Column Height", 'fildisi' ),
			"param_name" => "equal_column_height",
			"value" => array(
				esc_html__( "None", 'fildisi' ) => 'none',
				esc_html__( "Equal Height Columns", 'fildisi' ) => 'equal-column',
				esc_html__( "Equal Height Columns and Middle Content", 'fildisi' ) => 'middle-content',
			),
			"description" => esc_html__( "Recommended for multiple columns with different background colors. Additionally you can set your columns content in middle. If you need some paddings in your columns, please place them only in the column with the largest content.", 'fildisi' ),
			"group" => esc_html__( "Inner Columns", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => esc_html__( "Desktop Visibility", 'fildisi'),
			"param_name" => "desktop_visibility",
			"description" => esc_html__( "If selected, row will be hidden on desktops/laptops.", 'fildisi' ),
			"value" => Array(esc_html__( "Hide", 'fildisi' ) => 'hide'),
			'group' => esc_html__( "Responsiveness", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => esc_html__( "Tablet Landscape Visibility", 'fildisi'),
			"param_name" => "tablet_visibility",
			"description" => esc_html__( "If selected, row will be hidden on tablet devices with landscape orientation.", 'fildisi' ),
			"value" => Array(esc_html__( "Hide", 'fildisi' ) => 'hide'),
			'group' => esc_html__( "Responsiveness", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => esc_html__( "Tablet Portrait Visibility", 'fildisi'),
			"param_name" => "tablet_sm_visibility",
			"description" => esc_html__( "If selected, row will be hidden on tablet devices with portrait orientation.", 'fildisi' ),
			"value" => Array(esc_html__( "Hide", 'fildisi' ) => 'hide'),
			'group' => esc_html__( "Responsiveness", 'fildisi' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => esc_html__( "Mobile Visibility", 'fildisi'),
			"param_name" => "mobile_visibility",
			"description" => esc_html__( "If selected, row will be hidden on mobile devices.", 'fildisi' ),
			"value" => Array(esc_html__( "Hide", 'fildisi' ) => 'hide'),
			'group' => esc_html__( "Responsiveness", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Landscape Equal Column Height", 'fildisi' ),
			"param_name" => "tablet_landscape_equal_column_height",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "None", 'fildisi' ) => 'false',
			),
			"dependency" => array(
				'element' => 'equal_column_height',
				'value_not_equal_to' => array( 'none' )
			),
			"description" => esc_html__( "Select if you wish to keep or disable the Equal Column Height.", 'fildisi' ),
			'group' => esc_html__( "Responsiveness", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Portrait Equal Column Height", 'fildisi' ),
			"param_name" => "tablet_portrait_equal_column_height",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "None", 'fildisi' ) => 'false',
			),
			"dependency" => array(
				'element' => 'equal_column_height',
				'value_not_equal_to' => array( 'none' )
			),
			"description" => esc_html__( "Select if you wish to keep or disable the Equal Column Height.", 'fildisi' ),
			'group' => esc_html__( "Responsiveness", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Mobile Equal Column Height", 'fildisi' ),
			"param_name" => "mobile_equal_column_height",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "None", 'fildisi' ) => 'false',
			),
			"dependency" => array(
				'element' => 'equal_column_height',
				'value_not_equal_to' => array( 'none' )
			),
			"description" => esc_html__( "Select if you wish to keep or disable the Equal Column Height.", 'fildisi' ),
			'group' => esc_html__( "Responsiveness", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Landscape Window Fullheight", 'fildisi' ),
			"param_name" => "tablet_landscape_full_height",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "No", 'fildisi' ) => 'false',
			),
			"dependency" => array(
				'element' => 'section_full_height',
				'value' => array( 'fullheight' )
			),
			"description" => esc_html__( "Select if you wish to keep or disable the Window Fullheight.", 'fildisi' ),
			'group' => esc_html__( "Responsiveness", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Portrait Window Fullheight", 'fildisi' ),
			"param_name" => "tablet_portrait_full_height",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "No", 'fildisi' ) => 'false',
			),
			"dependency" => array(
				'element' => 'section_full_height',
				'value' => array( 'fullheight' )
			),
			"description" => esc_html__( "Select if you wish to keep or disable the Window Fullheight.", 'fildisi' ),
			'group' => esc_html__( "Responsiveness", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Mobile Window Fullheight", 'fildisi' ),
			"param_name" => "mobile_full_height",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "No", 'fildisi' ) => 'false',
			),
			"dependency" => array(
				'element' => 'section_full_height',
				'value' => array( 'fullheight' )
			),
			"description" => esc_html__( "Select if you wish to keep or disable the Window Fullheight.", 'fildisi' ),
			'group' => esc_html__( "Responsiveness", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => 'dropdown',
			"heading" => esc_html__( "Top Separator", 'fildisi' ),
			"param_name" => "separator_top",
			"description" => esc_html__( "Select Top Separator type", 'fildisi' ),
			"value" => $fildisi_eutf_separator_list,
			"group" => esc_html__( "Separators", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => 'dropdown',
			"heading" => esc_html__( "Top Separator Size", 'fildisi' ),
			"param_name" => "separator_top_size",
			"description" => esc_html__( "Select Top Separator size", 'fildisi' ),
			"value" => $fildisi_eutf_separator_size_list,
			"std" => '90px',
			"dependency" => array(
				'element' => 'separator_top',
				'value_not_equal_to' => array( '' )
			),
			"group" => esc_html__( "Separators", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__('Top Separator Color', 'fildisi' ),
			"param_name" => "separator_top_color",
			"dependency" => array(
				'element' => 'separator_top',
				'value_not_equal_to' => array( '' )
			),
			"std" => '#ffffff',
			"description" => esc_html__("Select top separator color", 'fildisi' ),
			"group" => esc_html__( "Separators", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => 'dropdown',
			"heading" => esc_html__( "Bottom Separator", 'fildisi' ),
			"param_name" => "separator_bottom",
			"description" => esc_html__( "Select Bottom Separator type", 'fildisi' ),
			"value" => $fildisi_eutf_separator_list,
			"group" => esc_html__( "Separators", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => 'dropdown',
			"heading" => esc_html__( "Bottom Separator Size", 'fildisi' ),
			"param_name" => "separator_bottom_size",
			"description" => esc_html__( "Select Bottom Separator size", 'fildisi' ),
			"value" => $fildisi_eutf_separator_size_list,
			"std" => '90px',
			"dependency" => array(
				'element' => 'separator_bottom',
				'value_not_equal_to' => array( '' )
			),
			"group" => esc_html__( "Separators", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__('Bottom Separator Color', 'fildisi' ),
			"param_name" => "separator_bottom_color",
			"dependency" => array(
				'element' => 'separator_bottom',
				'value_not_equal_to' => array( '' )
			),
			"std" => '#ffffff',
			"description" => esc_html__("Select bottom separator color", 'fildisi' ),
			"group" => esc_html__( "Separators", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Header Style", 'fildisi' ),
			"param_name" => "scroll_header_style",
			"value" => array(
				esc_html__( "Dark", 'fildisi' ) => 'dark',
				esc_html__( "Light", 'fildisi' ) => 'light',
				esc_html__( "Default", 'fildisi' ) => 'default',
			),
			"std" => 'dark',
			"description" => esc_html__( "Select header style", 'fildisi' ),
			"group" => esc_html__( "Scrolling Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__('Scrolling Section Title', 'fildisi' ),
			"param_name" => "scroll_section_title",
			"description" => esc_html__("If you wish you can type a title for the side dot navigation.", 'fildisi' ),
			"group" => esc_html__( "Scrolling Section Options", 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Vertical Content Position", 'fildisi' ),
			"param_name" => "vertical_content_position",
			"value" => array(
				esc_html__( "Top", 'fildisi' ) => 'top',
				esc_html__( "Middle", 'fildisi' ) => 'middle',
				esc_html__( "Bottom", 'fildisi' ) => 'bottom',
			),
			"description" => esc_html__( "Select the vertical position of the content. Note this setting is affected only if you have set Equal Height Columns under: Row Settings > Inner Columns > Equal Column Height.", 'fildisi' ),
			'group' => esc_html__( 'Effect & Positions', 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Column Effect", 'fildisi' ),
			"param_name" => "column_effect",
			"value" => array(
				esc_html__( "None", 'fildisi' ) => 'none',
				esc_html__( "Vertical Parallax", 'fildisi' ) => 'vertical-parallax',
			),
			"description" => esc_html__( "Select column effect behaviour. Notice that the Mouse Move Effect does not affect on devices.", 'fildisi' ),
			'group' => esc_html__( 'Effect & Positions', 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Column Effect Total Range", 'fildisi' ),
			"param_name" => "column_effect_limit",
			"value" => array(
				esc_html__( "1x", 'fildisi' ) => '1x',
				esc_html__( "2x", 'fildisi' ) => '2x',
				esc_html__( "3x", 'fildisi' ) => '3x',
				esc_html__( "4x", 'fildisi' ) => '4x',
				esc_html__( "5x", 'fildisi' ) => '5x',
				esc_html__( "6x", 'fildisi' ) => '6x',
				esc_html__( "None", 'fildisi' ) => 'none',
			),
			"dependency" => array(
				'element' => 'column_effect',
				'value_not_equal_to' => array( 'none' )
			),
			"description" => esc_html__( "Select column effect total range of motion. None allows column to move with complete freedom.", 'fildisi' ),
			'group' => esc_html__( 'Effect & Positions', 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Column Effect Invert Motion", 'fildisi' ),
			"param_name" => "column_effect_invert",
			"value" => array(
				esc_html__( "No", 'fildisi' ) => 'false',
				esc_html__( "Yes", 'fildisi' ) => 'true',
			),
			"dependency" => array(
				'element' => 'column_effect',
				'value_not_equal_to' => array( 'none' )
			),
			'group' => esc_html__( 'Effect & Positions', 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Change column position", 'fildisi' ),
			"param_name" => "column_custom_position",
			"value" => array(
				esc_html__( "No", 'fildisi' ) => 'no',
				esc_html__( "Yes", 'fildisi' ) => 'yes',
			),
			'group' => esc_html__( 'Effect & Positions', 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( 'Top Position', 'fildisi' ),
			"param_name" => "position_top",
			"value" => $fildisi_eutf_position_list,
			"description" => esc_html__( "Select the top position of the column.", 'fildisi' ),
			'group' => esc_html__( 'Effect & Positions', 'fildisi' ),
			"dependency" => array(
				'element' => 'column_custom_position',
				'value' => array( 'yes' )
			),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( 'Bottom Position', 'fildisi' ),
			"param_name" => "position_bottom",
			"value" => $fildisi_eutf_position_list,
			"description" => esc_html__( "Select the bottom position of the column.", 'fildisi' ),
			'group' => esc_html__( 'Effect & Positions', 'fildisi' ),
			"dependency" => array(
				'element' => 'column_custom_position',
				'value' => array( 'yes' )
			),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Left Position", 'fildisi' ),
			"param_name" => "position_left",
			"value" => $fildisi_eutf_position_list,
			"description" => esc_html__( "Select the left position of the column.", 'fildisi' ),
			'group' => esc_html__( 'Effect & Positions', 'fildisi' ),
			"dependency" => array(
				'element' => 'column_custom_position',
				'value' => array( 'yes' )
			),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( 'Right Position', 'fildisi' ),
			"param_name" => "position_right",
			"value" => $fildisi_eutf_position_list,
			"description" => esc_html__( "Select the right position of the column.", 'fildisi' ),
			'group' => esc_html__( 'Effect & Positions', 'fildisi' ),
			"dependency" => array(
				'element' => 'column_custom_position',
				'value' => array( 'yes' )
			),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "textfield",
			"heading" => esc_html__( 'Z index', 'fildisi' ),
			"param_name" => "z_index",
			"description" => esc_html__( "Enter a number for column's z-index. Default value is 1, recommended to be larger than this.", 'fildisi' ),
			'group' => esc_html__( 'Effect & Positions', 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( "Width", 'fildisi' ),
			'param_name' => 'width',
			'value' => $fildisi_eutf_column_width_list,
			'description' => esc_html__( "Select column width.", 'fildisi' ),
			'std' => '1/1',
			'group' => esc_html__( "Width & Responsiveness", 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Desktop", 'fildisi' ),
			"param_name" => "desktop_hide",
			"value" => $fildisi_eutf_column_desktop_hide_list,
			"description" => esc_html__( "Responsive column on desktops/laptops.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-6',
		)
	);
	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Landscape", 'fildisi' ),
			"param_name" => "tablet_width",
			"value" => $fildisi_eutf_column_width_tablet_list,
			"description" => esc_html__( "Responsive column on tablet devices with landscape orientation.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-6',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Portrait", 'fildisi' ),
			"param_name" => "tablet_sm_width",
			"value" => $fildisi_eutf_column_width_tablet_sm_list,
			"description" => esc_html__( "Responsive column on tablet devices with portrait orientation.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-6',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Mobile", 'fildisi' ),
			"param_name" => "mobile_width",
			"value" => $fildisi_eutf_column_mobile_width_list,
			"description" => esc_html__( "Responsive column on mobile devices.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-6',
		)
	);

	vc_add_param( "vc_column",
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( "Window Height", 'fildisi' ),
			'param_name' => 'column_fullheight',
			"value" => array(
				esc_html__( "No", 'fildisi' ) => '',
				esc_html__( "Yes", 'fildisi' ) => 'fullheight',
			),
			"description" => esc_html__( "Select if you want your Column height to be equal with the window height", 'fildisi' ),
			'group' => esc_html__( "Width & Responsiveness", 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( "Tablet Landscape", 'fildisi' ),
			'param_name' => 'tablet_landscape_column_fullheight',
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "None", 'fildisi' ) => 'false',
			),
			"dependency" => array(
				'element' => 'column_fullheight',
				'value' => array( 'fullheight' )
			),
			"description" => esc_html__( "Select if you wish to keep or disable the Window Height.", 'fildisi' ),
			'group' => esc_html__( "Width & Responsiveness", 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( "Tablet Portrait", 'fildisi' ),
			'param_name' => 'tablet_portrait_column_fullheight',
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "None", 'fildisi' ) => 'false',
			),
			"dependency" => array(
				'element' => 'column_fullheight',
				'value' => array( 'fullheight' )
			),
			"description" => esc_html__( "Select if you wish to keep or disable the Window Height.", 'fildisi' ),
			'group' => esc_html__( "Width & Responsiveness", 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( "Mobile", 'fildisi' ),
			'param_name' => 'mobile_column_fullheight',
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "None", 'fildisi' ) => 'false',
			),
			"dependency" => array(
				'element' => 'column_fullheight',
				'value' => array( 'fullheight' )
			),
			"description" => esc_html__( "Select if you wish to keep or disable the Window Height.", 'fildisi' ),
			'group' => esc_html__( "Width & Responsiveness", 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => 'fildisi_param_label',
			"heading" => esc_html__( "Column Effect", 'fildisi'),
			"param_name" => "label_column_effect",
			'value' => '',
			'std' => '',
			"description" => esc_html__( "Define your column effect for devices. Default values are defined under Effect & Positions > Column Effect.", 'fildisi' ),
			"dependency" => array(
				'element' => 'column_effect',
				'value' => array( 'vertical-parallax' )
			),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Landscape", 'fildisi' ),
			"param_name" => "tablet_landscape_column_effect",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "Disable Effect", 'fildisi' ) => 'none',
			),
			"description" => esc_html__( "Define your column effect for Tablet Landscape.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			"dependency" => array(
				'element' => 'column_effect',
				'value' => array( 'vertical-parallax' )
			),
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Portrait", 'fildisi' ),
			"param_name" => "tablet_portrait_column_effect",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "Disable Effect", 'fildisi' ) => 'none',
			),
			"description" => esc_html__( "Define your column effect for Tablet Portrait.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			"dependency" => array(
				'element' => 'column_effect',
				'value' => array( 'vertical-parallax' )
			),
			"std" => 'none',
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Mobile", 'fildisi' ),
			"param_name" => "mobile_column_effect",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "Disable Effect", 'fildisi' ) => 'none',
			),
			"description" => esc_html__( "Define your column effect for Mobile.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			"dependency" => array(
				'element' => 'column_effect',
				'value' => array( 'vertical-parallax' )
			),
			"std" => 'none',
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => 'fildisi_param_label',
			"heading" => esc_html__( "Column Positions", 'fildisi'),
			"param_name" => "label_column_positions",
			'value' => '',
			'std' => '',
			"description" => esc_html__( "Define your column positions for devices. Default values are defined under Effect & Positions > Change Column Position.", 'fildisi' ),
			"dependency" => array(
				'element' => 'column_custom_position',
				'value' => array( 'yes' )
			),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Landscape", 'fildisi' ),
			"param_name" => "tablet_landscape_column_positions",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "Reset Positions", 'fildisi' ) => 'none',
			),
			"description" => esc_html__( "Define your column positions for Tablet Landscape.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			"dependency" => array(
				'element' => 'column_custom_position',
				'value' => array( 'yes' )
			),
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Portrait", 'fildisi' ),
			"param_name" => "tablet_portrait_column_positions",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "Reset Positions", 'fildisi' ) => 'none',
			),
			"description" => esc_html__( "Define your column positions for Tablet Portrait.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			"dependency" => array(
				'element' => 'column_custom_position',
				'value' => array( 'yes' )
			),
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Mobile", 'fildisi' ),
			"param_name" => "mobile_column_positions",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "Reset Positions", 'fildisi' ) => 'none',
			),
			"description" => esc_html__( "Define your column positions for Mobile.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			"dependency" => array(
				'element' => 'column_custom_position',
				'value' => array( 'yes' )
			),
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => 'fildisi_param_label',
			"heading" => esc_html__( "Text Align", 'fildisi'),
			"param_name" => "label_text_align",
			'value' => '',
			'std' => '',
			"description" => esc_html__( "Define your text align for devices. Default values are defined under General > Text Align.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Landscape", 'fildisi' ),
			"param_name" => "tablet_text_align",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "Left", 'fildisi' ) => 'left',
				esc_html__( "Center", 'fildisi' ) => 'center',
				esc_html__( "Right", 'fildisi' ) => 'right',
			),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-4',
			"description" => esc_html__( "Define your text align for Tablet Landscape.", 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Portrait", 'fildisi' ),
			"param_name" => "tablet_sm_text_align",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "Left", 'fildisi' ) => 'left',
				esc_html__( "Center", 'fildisi' ) => 'center',
				esc_html__( "Right", 'fildisi' ) => 'right',
			),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-4',
			"description" => esc_html__( "Define your text align for Tablet Portrait.", 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Mobile", 'fildisi' ),
			"param_name" => "mobile_text_align",
			"value" => array(
				esc_html__( "Default values", 'fildisi' ) => '',
				esc_html__( "Left", 'fildisi' ) => 'left',
				esc_html__( "Center", 'fildisi' ) => 'center',
				esc_html__( "Right", 'fildisi' ) => 'right',
			),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-4',
			"description" => esc_html__( "Define your text align for Tablet Mobile.", 'fildisi' ),
			'edit_field_class' => 'vc_col-sm-4',
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__('Font Color', 'fildisi' ),
			"param_name" => "font_color",
			"description" => esc_html__("Select font color", 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Heading Color", 'fildisi' ),
			"param_name" => "heading_color",
			"value" => array(
				esc_html__( "Default", 'fildisi' ) => '',
				esc_html__( "Dark", 'fildisi' ) => 'dark',
				esc_html__( "Light", 'fildisi' ) => 'light',
				esc_html__( "Primary 1", 'fildisi' ) => 'primary-1',
				esc_html__( "Primary 2", 'fildisi' ) => 'primary-2',
				esc_html__( "Primary 3", 'fildisi' ) => 'primary-3',
				esc_html__( "Primary 4", 'fildisi' ) => 'primary-4',
				esc_html__( "Primary 5", 'fildisi' ) => 'primary-5',
				esc_html__( "Primary 6", 'fildisi' ) => 'primary-6',
				esc_html__( "Green", 'fildisi' ) => 'green',
				esc_html__( "Orange", 'fildisi' ) => 'orange',
				esc_html__( "Red", 'fildisi' ) => 'red',
				esc_html__( "Blue", 'fildisi' ) => 'blue',
				esc_html__( "Aqua", 'fildisi' ) => 'aqua',
				esc_html__( "Purple", 'fildisi' ) => 'purple',
				esc_html__( "Grey", 'fildisi' ) => 'grey',
			),
			"description" => esc_html__( "Select heading color", 'fildisi' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Text Align", 'fildisi' ),
			"param_name" => "text_align",
			"value" => array(
				esc_html__( "Left", 'fildisi' ) => 'left',
				esc_html__( "Center", 'fildisi' ) => 'center',
				esc_html__( "Right", 'fildisi' ) => 'right',
			),
			"description" => esc_html__( "Select the text align of the content.", 'fildisi' ),
		)
	);

	vc_add_param( "vc_column", $fildisi_eutf_add_el_class );
	vc_add_param( "vc_column", $fildisi_eutf_add_el_wrapper_class );


	vc_add_param( "vc_column_inner", $fildisi_eutf_add_el_class );
	vc_add_param( "vc_column_inner", $fildisi_eutf_add_el_wrapper_class );

	vc_add_param( "vc_column_inner",
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( "Width", 'fildisi' ),
			'param_name' => 'width',
			'value' => $fildisi_eutf_column_width_list,
			'group' => esc_html__( "Width & Responsiveness", 'fildisi' ),
			'description' => esc_html__( "Select column width.", 'fildisi' ),
			'std' => '1/1'
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Desktop", 'fildisi' ),
			"param_name" => "desktop_hide",
			"value" => $fildisi_eutf_column_desktop_hide_list,
			"description" => esc_html__( "Responsive column on desktops/laptops.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Landscape", 'fildisi' ),
			"param_name" => "tablet_width",
			"value" => $fildisi_eutf_column_width_tablet_list,
			"description" => esc_html__( "Responsive column on tablet devices with landscape orientation.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Portrait", 'fildisi' ),
			"param_name" => "tablet_sm_width",
			"value" => $fildisi_eutf_column_width_tablet_sm_list,
			"description" => esc_html__( "Responsive column on tablet devices with portrait orientation.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Mobile", 'fildisi' ),
			"param_name" => "mobile_width",
			"value" => $fildisi_eutf_column_mobile_width_list,
			"description" => esc_html__( "Responsive column on mobile devices.", 'fildisi' ),
			'group' => esc_html__( 'Width & Responsiveness', 'fildisi' ),
		)
	);

	vc_add_param( "vc_widget_sidebar",
		array(
			'type' => 'hidden',
			'param_name' => 'title',
			'value' => '',
		)
	);

	if ( defined( 'WPB_VC_VERSION' ) && version_compare( WPB_VC_VERSION, '4.6', '>=') ) {

		vc_add_param( "vc_tta_tabs",
			array(
				'type' => 'hidden',
				'param_name' => 'no_fill_content_area',
				'value' => '',
			)
		);

		vc_add_param( "vc_tta_tabs",
			array(
				'type' => 'hidden',
				'param_name' => 'tab_position',
				'value' => 'top',
			)
		);

		vc_add_param( "vc_tta_accordion",
			array(
				'type' => 'hidden',
				'param_name' => 'no_fill',
				'value' => '',
			)
		);

		vc_add_param( "vc_tta_tour",
			array(
				'type' => 'hidden',
				'param_name' => 'no_fill_content_area',
				'value' => '',
			)
		);
	}

	vc_add_param( "vc_column_text",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Text Style", 'fildisi' ),
			"param_name" => "text_style",
			"value" => array(
				esc_html__( "None", 'fildisi' ) => '',
				esc_html__( "Leader", 'fildisi' ) => 'leader-text',
				esc_html__( "Subtitle", 'fildisi' ) => 'subtitle',
			),
			"description" => esc_html__( "Select your text style", 'fildisi' ),
		)
	);
	vc_add_param( "vc_column_text", $fildisi_eutf_add_animation );
	vc_add_param( "vc_column_text", $fildisi_eutf_add_animation_delay );
	vc_add_param( "vc_column_text", $fildisi_eutf_add_animation_duration );


}

//Omit closing PHP tag to avoid accidental whitespace output errors.
