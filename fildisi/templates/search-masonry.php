<?php
/*
*	Template Search Masonry
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/
?>

<?php
	$title_tag = fildisi_eutf_option( 'search_page_heading_tag', 'h4' );
	$title_class = fildisi_eutf_option( 'search_page_heading', 'h4' );
	$excerpt_length = fildisi_eutf_option( 'search_page_excerpt_length_small' );
	$excerpt_more = fildisi_eutf_option( 'search_page_excerpt_more' );
	$search_page_show_image = fildisi_eutf_option( 'search_page_show_image', 'yes' );
	$search_page_mode = fildisi_eutf_option( 'search_page_mode', 'masonry' );

	if ( 'yes' == $search_page_show_image ) {
		$search_image_mode = fildisi_eutf_option( 'search_image_mode', 'landscape' );
		$search_masonry_image_mode = fildisi_eutf_option( 'search_masonry_image_mode', 'medium' );

		if ( 'masonry' == $search_page_mode) {
			$search_image_mode = $search_masonry_image_mode;
		}
		$image_size = fildisi_eutf_get_image_size( $search_image_mode );
	}

?>

<article id="eut-search-<?php the_ID(); ?><?php echo uniqid('-'); ?>" <?php post_class( 'eut-blog-item eut-isotope-item' ); ?>>
	<div class="eut-blog-item-inner eut-isotope-item-inner">
	<?php if ( 'yes' == $search_page_show_image && has_post_thumbnail() ) { ?>
		<div class="eut-media eut-image-hover clearfix">
			<a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_post_thumbnail( $image_size ); ?></a>
		</div>
	<?php } ?>
		<div class="eut-post-content-wrapper">
			<div class="eut-post-content">
				<?php the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><' . tag_escape( $title_tag ) . ' class="eut-post-title eut-text-hover-primary-1 eut-' . esc_attr( $title_class ) . '">', '</' . tag_escape( $title_tag ) . '></a>' ); ?>
				<div itemprop="articleBody">
					<?php echo fildisi_eutf_excerpt( $excerpt_length, $excerpt_more  ); ?>
				</div>
			</div>
		</div>
	</div>
</article>