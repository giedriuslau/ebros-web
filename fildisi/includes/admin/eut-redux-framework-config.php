<?php
/**
*	Theme Options Config File
*	@version	1.0
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

if ( !class_exists( "ReduxFramework" ) ) {
	return;
}

if (!class_exists("Fildisi_Eutf_Redux_Framework_config")) {

	class Fildisi_Eutf_Redux_Framework_config {

		public $args = array();
		public $sections = array();
		public $theme;
		public $ReduxFramework;

		public function __construct() {
			// Set the default arguments
			$this->setArguments();

			// Create the sections and fields
			$this->setSections();

			// No errors please
			if ( !isset( $this->args['opt_name'] ) ) {
				return;
			}
			$this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
		}

		public function fildisi_eutf_redux_customizer_visibility() {
			$visibility = apply_filters( 'fildisi_eutf_redux_customizer_visibility', false );
			return $visibility;
		}

		public function setSections() {

			/**
			 * Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
			 * */
			global $fildisi_eutf_social_list, $fildisi_eutf_button_type_selection, $fildisi_eutf_button_shape_selection, $fildisi_eutf_button_color_selection, $fildisi_eutf_area_height;

			//Gravity Forms
			$fildisi_eutf_gravity_options = array();
			if ( class_exists( 'RGFormsModel' ) ) {
				$gravity_forms = RGFormsModel::get_forms( 1, 'title' );
				if ( $gravity_forms ) {
					$fildisi_eutf_gravity_options = array( esc_html__( 'Select a form to display.', 'fildisi' ) => '' );
					foreach ( $gravity_forms as $gravity_form ) {
						$fildisi_eutf_gravity_options[ $gravity_form->id ] = $gravity_form->title;
					}
				}
			}

			$fildisi_eutf_social_options = $fildisi_eutf_social_list;

			$fildisi_eutf_portfolio_social_options = array(
				'email' => esc_html__( 'E-mail', 'fildisi' ),
				'twitter' => 'Twitter',
				'facebook' => 'Facebook',
				'linkedin' => 'LinkedIn',
				'pinterest' => 'Pinterest',
				'reddit' => 'reddit',
				'tumblr' => 'Tumblr',
				'eut-likes' => '(Euthemians) Likes',
			);

			$fildisi_eutf_portfolio_term_selection = array(
				'none' => esc_html__( 'None', 'fildisi' ),
				'portfolio_category' => esc_html__( 'Portfolio Category', 'fildisi' ),
				'portfolio_field' => esc_html__( 'Portfolio Field', 'fildisi' ),
			);

			$fildisi_eutf_post_social_options = array(
				'email' => esc_html__( 'E-mail', 'fildisi' ),
				'twitter' => 'Twitter',
				'facebook' => 'Facebook',
				'linkedin' => 'LinkedIn',
				'reddit' => 'reddit',
				'tumblr' => 'Tumblr',
				'eut-likes' => '(Euthemians) Likes',
			);

			$fildisi_eutf_product_social_options = array(
				'email' => esc_html__( 'E-mail', 'fildisi' ),
				'twitter' => 'Twitter',
				'facebook' => 'Facebook',
				'linkedin' => 'LinkedIn',
				'reddit' => 'reddit',
				'tumblr' => 'Tumblr',
				'eut-likes' => '(Euthemians) Likes',
			);

			$fildisi_eutf_event_social_options = array(
				'email' => esc_html__( 'E-mail', 'fildisi' ),
				'twitter' => 'Twitter',
				'facebook' => 'Facebook',
				'linkedin' => 'LinkedIn',
				'reddit' => 'reddit',
				'tumblr' => 'Tumblr',
				'eut-likes' => '(Euthemians) Likes',
			);

			$fildisi_eutf_event_mode_selection = array(
				'small' => esc_html__( 'Small Media', 'fildisi' ),
				'grid' => esc_html__( 'Grid' , 'fildisi' ),
			);

			$fildisi_eutf_blog_mode_selection = array(
				'large' => esc_html__( 'Large Media', 'fildisi' ),
				'small' => esc_html__( 'Small Media', 'fildisi' ),
				'masonry' => esc_html__( 'Masonry' , 'fildisi' ),
				'grid' => esc_html__( 'Grid' , 'fildisi' ),
			);

			$fildisi_eutf_blog_image_mode_selection = array(
				'landscape-large-wide' => esc_html__( 'Landscape Large Wide Crop', 'fildisi' ),
				'landscape-medium' => esc_html__( 'Landscape Medium Crop', 'fildisi' ),
				'large' => esc_html__( 'Resize ( Large )', 'fildisi' ),
				'medium_large' => esc_html__( 'Resize ( Medium Large )', 'fildisi' ),
				'medium' => esc_html__( 'Resize ( Medium )', 'fildisi' ),
			);

			$fildisi_eutf_blog_grid_image_mode_selection = array(
				'square' => esc_html__( 'Square Small Crop', 'fildisi' ),
				'landscape' => esc_html__( 'Landscape Small Crop', 'fildisi' ),
				'portrait' => esc_html__( 'Portrait Small Crop', 'fildisi' ),
				'medium_large' => esc_html__( 'Resize ( Medium Large )', 'fildisi' ),
				'medium' => esc_html__( 'Resize ( Medium )', 'fildisi' ),
			);

			$fildisi_eutf_blog_masonry_image_mode_selection = array(
				'large' => esc_html__( 'Resize ( Large )', 'fildisi' ),
				'medium_large' => esc_html__( 'Resize ( Medium Large )', 'fildisi' ),
				'medium' => esc_html__( 'Resize ( Medium )', 'fildisi' ),
			);

			$fildisi_eutf_search_grid_image_mode_selection = array(
				'square' => esc_html__( 'Square Small Crop', 'fildisi' ),
				'landscape' => esc_html__( 'Landscape Small Crop', 'fildisi' ),
				'portrait' => esc_html__( 'Portrait Small Crop', 'fildisi' ),
				'medium_large' => esc_html__( 'Resize ( Medium Large )', 'fildisi' ),
				'medium' => esc_html__( 'Resize ( Medium )', 'fildisi' ),
			);

			$fildisi_eutf_search_masonry_image_mode_selection = array(
				'large' => esc_html__( 'Resize ( Large )', 'fildisi' ),
				'medium_large' => esc_html__( 'Resize ( Medium Large )', 'fildisi' ),
				'medium' => esc_html__( 'Resize ( Medium )', 'fildisi' ),
			);

			$fildisi_eutf_blog_columns_selection = array(
				'2' => '2',
				'3' => '3',
				'4' => '4',
				'5' => '5',
			);
			$fildisi_eutf_blog_columns_selection_tablet = array(
				'2' => '2',
				'3' => '3',
				'4' => '4',
			);
			$fildisi_eutf_blog_columns_selection_mobile = array(
				'1' => '1',
				'2' => '2',
			);
			$fildisi_eutf_blog_headings_tag_selection = array(
				'auto' => esc_html__( 'Auto', 'fildisi' ),
				'h2'  => 'h2',
				'h3'  => 'h3',
				'h4'  => 'h4',
				'h5'  => 'h5',
				'h6'  => 'h6',
				'div'  => 'div',
			);

			$fildisi_eutf_blog_headings_selection = array(
				'auto' => esc_html__( 'Auto', 'fildisi' ),
				'h2'  => 'h2',
				'h3'  => 'h3',
				'h4'  => 'h4',
				'h5'  => 'h5',
				'h6'  => 'h6',
				'leader-text' => esc_html__( 'Leader Text', 'fildisi' ),
				'subtitle-text' => esc_html__( 'Subtitle Text', 'fildisi' ),
				'small-text' => esc_html__( 'Small Text', 'fildisi' ),
				'link-text' => esc_html__( 'Link Text', 'fildisi' ),
			);

			$fildisi_eutf_event_headings_selection = array(
				'h2'  => 'h2',
				'h3'  => 'h3',
				'h4'  => 'h4',
				'h5'  => 'h5',
				'h6'  => 'h6',
			);

			$fildisi_eutf_theme_layout_selection = array(
				'stretched' => esc_html__( 'Stretched', 'fildisi' ),
				'boxed' => esc_html__( 'Boxed', 'fildisi' ),
				'framed' => esc_html__( 'Framed', 'fildisi' ),
			);

			$fildisi_eutf_layout_selection = array(
				'none' => array('alt' => esc_html__( 'Full Width', 'fildisi' ), 'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
				'left' => array('alt' => esc_html__( 'Left Sidebar', 'fildisi' ), 'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
				'right' => array('alt' => esc_html__( 'Right Sidebar', 'fildisi' ), 'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
			);

			$fildisi_eutf_align_selection = array(
				'left' => esc_html__( 'Left', 'fildisi' ),
				'right' => esc_html__( 'Right', 'fildisi' ),
			);

			$fildisi_eutf_align_selection_extra = array(
				'left' => esc_html__( 'Left', 'fildisi' ),
				'center' => esc_html__( 'Center', 'fildisi' ),
				'right' => esc_html__( 'Right', 'fildisi' ),
			);

			$fildisi_eutf_align_selection_full = array(
				'left-top' => esc_html__( 'Left Top', 'fildisi' ),
				'left-center' => esc_html__( 'Left Center', 'fildisi' ),
				'left-bottom' => esc_html__( 'Left Bottom', 'fildisi' ),
				'center-top' => esc_html__( 'Center Top', 'fildisi' ),
				'center-center' => esc_html__( 'Center Center', 'fildisi' ),
				'center-bottom' => esc_html__( 'Center Bottom', 'fildisi' ),
				'right-top' => esc_html__( 'Right Top', 'fildisi' ),
				'right-center' => esc_html__( 'Right Center', 'fildisi' ),
				'right-bottom' => esc_html__( 'Right Bottom', 'fildisi' ),
			);

			$fildisi_eutf_animation_selection = array(
				'fade-in' => esc_html__( 'Default', 'fildisi' ),
				'none' => esc_html__( 'None', 'fildisi' ),
				'fade-in-up' => esc_html__( 'Fade In Up', 'fildisi' ),
				'fade-in-down' => esc_html__( 'Fade In Down', 'fildisi' ),
				'fade-in-left' => esc_html__( 'Fade In Left', 'fildisi' ),
				'fade-in-right' => esc_html__( 'Fade In Right', 'fildisi' ),
				'zoom-in' => esc_html__( 'Zoom In', 'fildisi' ),
				'zoom-out' => esc_html__( 'Zoom Out', 'fildisi' ),
			);

			$fildisi_eutf_title_bg_mode = array(
				'color' => esc_html__( 'Color Only', 'fildisi' ),
				'featured' => esc_html__( 'Featured Image', 'fildisi' ),
				'custom' => esc_html__( 'Custom Image', 'fildisi' ),
			);

			$fildisi_eutf_title_bg_mode_limited = array(
				'color' => esc_html__( 'Color Only', 'fildisi' ),
				'custom' => esc_html__( 'Custom Image', 'fildisi' ),

			);

			$fildisi_eutf_background_type = array(
				'transparent' => esc_html__( 'None', 'fildisi' ),
				'colored' => esc_html__( 'Background', 'fildisi' ),
				'advanced' => esc_html__( 'Stretched Background', 'fildisi' ),
			);

			$fildisi_eutf_header_menu_options = array(
				'search' => esc_html__( 'Search', 'fildisi' ),
				'form' => esc_html__( 'Contact Form (Contact Form 7 or Gravity Forms Required)', 'fildisi' ),
				'language' => esc_html__( 'Language selector (WPML or Polylang Required)', 'fildisi' ),
				//'login' => esc_html__( 'Login', 'fildisi' ),
				'cart' => esc_html__( 'Shopping Cart (WooCommerce Required)', 'fildisi' ),
				'social' => esc_html__( 'Social Icons', 'fildisi' ),
			);
			$fildisi_eutf_header_menu_options_default = array(
				'search' => false,
				'form' => false,
				'language' => false,
				//'login' => false,
				'cart' => false,
				'social' => false,
			);

			$fildisi_eutf_header_menu_selection = array(
				'default' => esc_html__( 'Default', 'fildisi' ),
				'disabled' => esc_html__( 'Disabled', 'fildisi' ),
			);

			$fildisi_eutf_top_bar_options = array(
				'menu' => esc_html__( 'Menu (Assigned: Menus - Manage Locations)', 'fildisi' ),
				'text' => esc_html__( 'Text', 'fildisi' ),
				'search' => esc_html__( 'Search', 'fildisi' ),
				'language' => esc_html__( 'Language selector (WPML or Polylang Required)', 'fildisi' ),
				'form' => esc_html__( 'Contact Form (Contact Form 7 or Gravity Forms Required)', 'fildisi' ),
				//'login' => esc_html__( 'Login', 'fildisi' ),
				'social' => esc_html__( 'Social Icons', 'fildisi' ),
			);
			$fildisi_eutf_top_bar_options_default = array(
				'menu' => false,
				'text' => false,
				'search' => false,
				'language' => false,
				'form' => false,
				//'login' => false,
				'social' => false,
			);

			$fildisi_eutf_menu_animations = array(
				'none' => esc_html__( 'None', 'fildisi' ),
				'fade-in' => esc_html__( 'Fade in', 'fildisi' ),
				'fade-in-up' => esc_html__( 'Fade in Up', 'fildisi' ),
				'fade-in-down' => esc_html__( 'Fade in Down', 'fildisi' ),
				'fade-in-left' => esc_html__( 'Fade in Left', 'fildisi' ),
				'fade-in-right' => esc_html__( 'Fade in Right', 'fildisi' ),
			);
			$fildisi_eutf_menu_pointers = array(
				'none' => esc_html__( 'None', 'fildisi' ),
				'arrow' => esc_html__( 'Arrow', 'fildisi' ),
				'arrow-not-first-level' => esc_html__( 'Arrow (exclude first level items)', 'fildisi' ),
			);

			$fildisi_eutf_color_selection = array(
				'dark' => esc_html__( 'Dark', 'fildisi' ),
				'light' => esc_html__( 'Light', 'fildisi' ),
				'primary-1' => esc_html__( 'Primary 1', 'fildisi' ),
				'primary-2' => esc_html__( 'Primary 2', 'fildisi' ),
				'primary-3' => esc_html__( 'Primary 3', 'fildisi' ),
				'primary-4' => esc_html__( 'Primary 4', 'fildisi' ),
				'primary-5' => esc_html__( 'Primary 5', 'fildisi' ),
				'primary-6' => esc_html__( 'Primary 6', 'fildisi' ),
			);
			$fildisi_eutf_color_selection_extra = array(
				'dark' => esc_html__( 'Dark', 'fildisi' ),
				'light' => esc_html__( 'Light', 'fildisi' ),
				'primary-1' => esc_html__( 'Primary 1', 'fildisi' ),
				'primary-2' => esc_html__( 'Primary 2', 'fildisi' ),
				'primary-3' => esc_html__( 'Primary 3', 'fildisi' ),
				'primary-4' => esc_html__( 'Primary 4', 'fildisi' ),
				'primary-5' => esc_html__( 'Primary 5', 'fildisi' ),
				'primary-6' => esc_html__( 'Primary 6', 'fildisi' ),
				'custom' => esc_html__( 'Custom', 'fildisi' ),
			);

			$fildisi_eutf_bg_color_selection = array(
				'none' => esc_html__( 'None', 'fildisi' ),
				'dark' => esc_html__( 'Dark', 'fildisi' ),
				'light' => esc_html__( 'Light', 'fildisi' ),
				'primary-1' => esc_html__( 'Primary 1', 'fildisi' ),
				'primary-2' => esc_html__( 'Primary 2', 'fildisi' ),
				'primary-3' => esc_html__( 'Primary 3', 'fildisi' ),
				'primary-4' => esc_html__( 'Primary 4', 'fildisi' ),
				'primary-5' => esc_html__( 'Primary 5', 'fildisi' ),
				'primary-6' => esc_html__( 'Primary 6', 'fildisi' ),
			);

			$fildisi_eutf_bg_color_selection_extra = array(
				'none' => esc_html__( 'None', 'fildisi' ),
				'dark' => esc_html__( 'Dark', 'fildisi' ),
				'light' => esc_html__( 'Light', 'fildisi' ),
				'primary-1' => esc_html__( 'Primary 1', 'fildisi' ),
				'primary-2' => esc_html__( 'Primary 2', 'fildisi' ),
				'primary-3' => esc_html__( 'Primary 3', 'fildisi' ),
				'primary-4' => esc_html__( 'Primary 4', 'fildisi' ),
				'primary-5' => esc_html__( 'Primary 5', 'fildisi' ),
				'primary-6' => esc_html__( 'Primary 6', 'fildisi' ),
				'custom' => esc_html__( 'Custom', 'fildisi' ),
			);

			$fildisi_eutf_header_mode_selection = array(
				'default' => array('alt' => esc_html__( 'Default', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/header/header-mode-default.png' ),
				'logo-top' => array('alt' => esc_html__( 'Logo on Top', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/header/header-mode-logo-top.png' ),
				'side' => array('alt' => esc_html__( 'Side', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/header/header-mode-side.png' ),
			);
			$fildisi_eutf_header_menu_mode_selection = array(
				'default' => array('alt' => esc_html__( 'Default', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/menu/default-menu.png' ),
				'split' => array('alt' => esc_html__( 'Split', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/menu/split-menu.png' ),
			);

			$fildisi_eutf_header_style = array(
				'default' => esc_html__( 'Default', 'fildisi' ),
				'dark' => esc_html__( 'Dark', 'fildisi' ),
				'light' => esc_html__( 'Light', 'fildisi' ),
			);

			$fildisi_eutf_title_style = array(
				'advanced' => esc_html__( 'Advanced Title', 'fildisi' ),
				'simple' => esc_html__( 'Simple Title', 'fildisi' ),
			);

			$fildisi_eutf_enable_selection = array(
				'no' => esc_html__( 'No', 'fildisi' ),
				'yes' => esc_html__( 'Yes', 'fildisi' ),
			);

			$fildisi_eutf_menu_responsibe_style_selection = array(
				'1' => esc_html__( 'Style 1', 'fildisi' ),
				'2' => esc_html__( 'Style 2', 'fildisi' ),
			);

			$fildisi_eutf_menu_responsibe_toggle_selection = array(
				'icon' => esc_html__( 'Icon', 'fildisi' ),
				'text' => esc_html__( 'Text', 'fildisi' ),
			);

			$fildisi_eutf_footer_column_selection = array(
				'footer-1' => array('alt' => esc_html__( 'Footer 1', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/footer/footer-1.png' ),
				'footer-2' => array('alt' => esc_html__( 'Footer 2', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/footer/footer-2.png' ),
				'footer-3' => array('alt' => esc_html__( 'Footer 3', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/footer/footer-3.png' ),
				'footer-4' => array('alt' => esc_html__( 'Footer 4', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/footer/footer-4.png' ),
				'footer-5' => array('alt' => esc_html__( 'Footer 5', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/footer/footer-5.png' ),
				'footer-6' => array('alt' => esc_html__( 'Footer 6', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/footer/footer-6.png' ),
				'footer-7' => array('alt' => esc_html__( 'Footer 7', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/footer/footer-7.png' ),
				'footer-8' => array('alt' => esc_html__( 'Footer 8', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/footer/footer-8.png' ),
				'footer-9' => array('alt' => esc_html__( 'Footer 9', 'fildisi' ), 'img' => get_template_directory_uri() . '/includes/images/footer/footer-9.png' ),
			);


			$fildisi_eutf_opacity_selection_simple = array(
				'0'  => '0%',
				'10' => '10%',
				'20' => '20%',
				'30' => '30%',
				'40' => '40%',
				'50' => '50%',
				'60' => '60%',
				'70' => '70%',
				'80' => '80%',
				'90' => '90%',
				'100'=> '100%',
			);

			$fildisi_eutf_opacity_selection = array(
				'0'    => '0%',
				'0.05' => '5%',
				'0.10' => '10%',
				'0.15' => '15%',
				'0.20' => '20%',
				'0.25' => '25%',
				'0.30' => '30%',
				'0.35' => '35%',
				'0.40' => '40%',
				'0.45' => '45%',
				'0.50' => '50%',
				'0.55' => '55%',
				'0.60' => '60%',
				'0.65' => '65%',
				'0.70' => '70%',
				'0.75' => '75%',
				'0.80' => '80%',
				'0.85' => '85%',
				'0.90' => '90%',
				'0.95' => '95%',
				'1'    => '100%',
			);
			$fildisi_eutf_ratio_selection = array(
				'0'    => '0%',
				'0.05' => '5%',
				'0.1'  => '10%',
				'0.15' => '15%',
				'0.2'  => '20%',
				'0.25' => '25%',
				'0.3'  => '30%',
				'0.35' => '35%',
				'0.4'  => '40%',
				'0.45' => '45%',
				'0.5'  => '50%',
				'0.55' => '55%',
				'0.6'  => '60%',
				'0.65' => '65%',
				'0.7'  => '70%',
				'0.75' => '75%',
				'0.8'  => '80%',
				'0.85'  => '85%',
				'0.9'  => '90%',
				'0.95' => '95%',
				'1'    => '100%',
			);

			$fildisi_eutf_headings_tag_selection = array(
				'div' => 'div',
				'h2'  => 'h2',
				'h3'  => 'h3',
				'h4'  => 'h4',
				'h5'  => 'h5',
				'h6'  => 'h6',
			);

			$fildisi_eutf_headings_selection = array(
				'h2'  => 'h2',
				'h3'  => 'h3',
				'h4'  => 'h4',
				'h5'  => 'h5',
				'h6'  => 'h6',
				'leader-text' => esc_html__( 'Leader Text', 'fildisi' ),
				'subtitle-text' => esc_html__( 'Subtitle Text', 'fildisi' ),
				'small-text' => esc_html__( 'Small Text', 'fildisi' ),
				'link-text' => esc_html__( 'Link Text', 'fildisi' ),
			);

			$fildisi_eutf_pattern_selection = array(
				'none' => esc_html__( 'No', 'fildisi' ),
				'default' => esc_html__( 'Yes', 'fildisi' ),
			);

			$fildisi_eutf_padding_selection = array(
				'none' => esc_html__( 'None', 'fildisi' ),
				'1x' => esc_html__( '1x', 'fildisi' ),
				'2x' => esc_html__( '2x', 'fildisi' ),
				'3x' => esc_html__( '3x', 'fildisi' ),
				'4x' => esc_html__( '4x', 'fildisi' ),
				'5x' => esc_html__( '5x', 'fildisi' ),
				'6x' => esc_html__( '6x', 'fildisi' ),
			);
			$fildisi_eutf_padding_selection_extra = array(
				'none' => esc_html__( 'None', 'fildisi' ),
				'1x' => esc_html__( '1x', 'fildisi' ),
				'2x' => esc_html__( '2x', 'fildisi' ),
				'3x' => esc_html__( '3x', 'fildisi' ),
				'4x' => esc_html__( '4x', 'fildisi' ),
				'5x' => esc_html__( '5x', 'fildisi' ),
				'6x' => esc_html__( '6x', 'fildisi' ),
				'custom' => esc_html__( 'Custom', 'fildisi' ),
			);

			$fildisi_eutf_content_size_selection = array(
				'large' => esc_html__( 'Large', 'fildisi' ),
				'medium' => esc_html__( 'Medium', 'fildisi' ),
				'small' => esc_html__( 'Small', 'fildisi' ),
			);

			$fildisi_eutf_container_size_selection = array(
				'default' => esc_html__( 'Default', 'fildisi' ),
				'large' => esc_html__( 'Large', 'fildisi' ),
			);

			//Standard Fonts
			$fildisi_eutf_std_fonts = array(
				"Arial, Helvetica, sans-serif"                         => "Arial, Helvetica, sans-serif",
				"'Arial Black', Gadget, sans-serif"                    => "'Arial Black', Gadget, sans-serif",
				"'Bookman Old Style', serif"                           => "'Bookman Old Style', serif",
				"'Comic Sans MS', cursive"                             => "'Comic Sans MS', cursive",
				"Courier, monospace"                                   => "Courier, monospace",
				"Garamond, serif"                                      => "Garamond, serif",
				"Georgia, serif"                                       => "Georgia, serif",
				"Impact, Charcoal, sans-serif"                         => "Impact, Charcoal, sans-serif",
				"'Lucida Console', Monaco, monospace"                  => "'Lucida Console', Monaco, monospace",
				"'Lucida Sans Unicode', 'Lucida Grande', sans-serif"   => "'Lucida Sans Unicode', 'Lucida Grande', sans-serif",
				"'MS Sans Serif', Geneva, sans-serif"                  => "'MS Sans Serif', Geneva, sans-serif",
				"'MS Serif', 'New York', sans-serif"                   => "'MS Serif', 'New York', sans-serif",
				"'Palatino Linotype', 'Book Antiqua', Palatino, serif" => "'Palatino Linotype', 'Book Antiqua', Palatino, serif",
				"Tahoma,Geneva, sans-serif"                            => "Tahoma, Geneva, sans-serif",
				"'Times New Roman', Times,serif"                       => "'Times New Roman', Times, serif",
				"'Trebuchet MS', Helvetica, sans-serif"                => "'Trebuchet MS', Helvetica, sans-serif",
				"Verdana, Geneva, sans-serif"                          => "Verdana, Geneva, sans-serif",
			);
			$fildisi_eutf_std_fonts = apply_filters( 'fildisi_eutf_std_fonts', $fildisi_eutf_std_fonts );

			$fildisi_eutf_feature_section_post_types_selection = array(
				'page'   => esc_html__( 'Pages', 'fildisi' ),
				'portfolio'   => esc_html__( 'Portfolio Items', 'fildisi' ),
				'post'   => esc_html__( 'Posts', 'fildisi' ),
				'product'   => esc_html__( 'Products', 'fildisi' ),
				'tribe_events'   => esc_html__( 'Events', 'fildisi' ),
			);

			$fildisi_eutf_container_size_selector = array(
				'container' => esc_html__( 'Container Size' , 'fildisi' ),
				'1170' => esc_html__( 'Large' , 'fildisi' ),
				'970' => esc_html__( 'Medium' , 'fildisi' ),
				'770' => esc_html__( 'Small' , 'fildisi' ),
			);

			//Skin Presets
			$fildisi_eutf_skin_palette_1 = '{"top_bar_bg_color":"#ffffff","top_bar_font_color":"#777777","top_bar_link_color":"#777777","top_bar_hover_color":"#F95F51","default_header_background_color":"#ffffff","default_header_background_color_opacity":"1","default_header_border_color":"#000000","default_header_border_color_opacity":"0.10","default_header_menu_text_color":"#000000","default_header_menu_text_hover_color":"#F95F51","default_header_menu_type_color":"#eef1f6","default_header_menu_type_color_hover":"#F95F51","default_header_submenu_bg_color":"#171a1d","default_header_submenu_text_color":"#777777","default_header_submenu_text_hover_color":"#ffffff","default_header_submenu_text_bg_hover_color":"#171a1d","default_header_submenu_column_text_color":"#ffffff","default_header_submenu_column_text_hover_color":"#ffffff","default_header_submenu_border_color":"#333638","logo_top_header_logo_area_background_color":"#ffffff","logo_top_header_logo_area_background_color_opacity":"1","logo_top_header_border_color":"#000000","logo_top_header_border_color_opacity":"0.10","logo_top_header_menu_area_background_color":"#ffffff","logo_top_header_menu_area_background_color_opacity":"1","logo_top_header_menu_text_color":"#000000","logo_top_header_menu_text_hover_color":"#F95F51","logo_top_header_menu_type_color":"#eef1f6","logo_top_header_menu_type_color_hover":"#F95F51","logo_top_header_submenu_bg_color":"#ffffff","logo_top_header_submenu_text_color":"#777777","logo_top_header_submenu_text_hover_color":"#F95F51","logo_top_header_submenu_text_bg_hover_color":"#ffffff","logo_top_header_submenu_column_text_color":"#000000","logo_top_header_submenu_column_text_hover_color":"#F95F51","logo_top_header_submenu_border_color":"#eef1f6","side_header_background_color":"#f7f7f7","side_header_background_color_opacity":"1","side_header_menu_text_color":"#000000","side_header_menu_text_hover_color":"#F95F51","side_header_submenu_text_color":"#000000","side_header_submenu_text_hover_color":"#F95F51","side_header_border_color":"#323232","side_header_border_opacity":"0.10","header_sticky_background_color":"#101215","header_sticky_background_color_opacity":"1","header_sticky_border_color":"#eef1f6","header_sticky_border_color_opacity":"0.30","sticky_menu_text_color":"#9c9c9c","sticky_menu_text_hover_color":"#ffffff","header_sticky_menu_type_color":"#eef1f6","header_sticky_menu_type_color_hover":"#eef1f6","light_menu_text_hover_color":"#ffffff","light_menu_type_color_hover":"#eef1f6","light_header_border_color":"#ffffff","light_header_border_color_opacity":"0.30","dark_menu_text_hover_color":"#000000","dark_menu_type_color_hover":"#eef1f6","dark_header_border_color":"#000000","dark_header_border_color_opacity":"0.10","responsive_header_background_color":"#000000","responsive_header_background_opacity":"1","responsive_header_elements_color":"#bfbfbf","responsive_header_elements_hover_color":"#ffffff","responsive_menu_background_color":"#ffffff","responsive_menu_link_color":"#777777","responsive_menu_link_hover_color":"#F95F51","responsive_menu_close_btn_color":"#777777","responsive_menu_border_color":"#eef1f6","responsive_menu_overflow_background_color":"#000000","responsive_menu_overflow_background_color_opacity":"0.90","page_anchor_menu_background_color":"#171a1d","page_anchor_menu_text_color":"#ffffff","page_anchor_menu_text_hover_color":"#F95F51","page_anchor_menu_background_hover_color":"#171a1d","page_anchor_menu_border_color":"#333638","main_content_background_color":"#ffffff","body_heading_color":"#000000","body_text_color":"#676767","body_text_link_color":"#000000","body_text_link_hover_color":"#F95F51","body_border_color":"#e9e9e9","widget_title_color":"#000000","body_primary_1_color":"#F95F51","body_primary_2_color":"#003a40","body_primary_3_color":"#487F84","body_primary_4_color":"#5F597E","body_primary_5_color":"#d6ccad","body_primary_6_color":"#db5111","blog_title_bg_color":"dark","blog_title_bg_color_custom":"#000000","blog_title_content_bg_color":"none","blog_title_content_bg_color_custom":"#ffffff","blog_title_color":"light","blog_title_color_custom":"#ffffff","blog_description_color":"light","blog_description_color_custom":"#ffffff","post_title_bg_color":"dark","post_title_bg_color_custom":"#000000","post_title_content_bg_color":"none","post_title_content_bg_color_custom":"#ffffff","post_subheading_color":"light","post_subheading_color_custom":"#ffffff","post_title_color":"light","post_title_color_custom":"#ffffff","post_description_color":"light","post_description_color_custom":"#ffffff","post_bar_background_color":"#ffffff","post_bar_socials_color":"#d3d3d3","post_bar_socials_color_hover":"#000000","post_bar_nav_title_color":"#000000","post_bar_arrow_color":"#d3d3d3","post_bar_border_color":"#e9e9e9","page_title_bg_color":"primary-2","page_title_bg_color_custom":"#000000","page_title_content_bg_color":"none","page_title_content_bg_color_custom":"#ffffff","page_title_color":"light","page_title_color_custom":"#ffffff","page_description_color":"light","page_description_color_custom":"#ffffff","search_page_title_bg_color":"dark","search_page_title_bg_color_custom":"#000000","search_page_title_content_bg_color":"none","search_page_title_content_bg_color_custom":"#ffffff","search_page_title_color":"light","search_page_title_color_custom":"#ffffff","search_page_description_color":"light","search_page_description_color_custom":"#ffffff","portfolio_title_bg_color":"dark","portfolio_title_bg_color_custom":"#000000","portfolio_title_content_bg_color":"none","portfolio_title_content_bg_color_custom":"#ffffff","portfolio_title_color":"light","portfolio_title_color_custom":"#ffffff","portfolio_description_color":"light","portfolio_description_color_custom":"#ffffff","portfolio_bar_background_color":"#ffffff","portfolio_bar_socials_color":"#d3d3d3","portfolio_bar_socials_color_hover":"#000000","portfolio_bar_nav_title_color":"#000000","portfolio_bar_arrow_color":"#d3d3d3","portfolio_bar_border_color":"#e9e9e9","product_tax_title_bg_color":"dark","product_tax_title_bg_color_custom":"#000000","product_tax_title_content_bg_color":"none","product_tax_title_content_bg_color_custom":"#ffffff","product_tax_title_color":"light","product_tax_title_color_custom":"#ffffff","product_tax_description_color":"light","product_tax_description_color_custom":"#ffffff","product_title_bg_color":"dark","product_title_bg_color_custom":"#000000","product_title_content_bg_color":"none","product_title_content_bg_color_custom":"#ffffff","product_title_color":"light","product_title_color_custom":"#ffffff","product_description_color":"light","product_description_color_custom":"#ffffff","product_area_bg_color":"#eeeeee","product_area_headings_color":"#000000","product_area_font_color":"#999999","product_area_link_color":"#FF7D88","product_area_hover_color":"#000000","product_area_border_color":"#e0e0e0","product_area_button_color":"primary-1","product_area_button_hover_color":"black","product_bar_background_color":"#ffffff","product_bar_socials_color":"#d3d3d3","product_bar_socials_color_hover":"#000000","product_bar_nav_title_color":"#000000","product_bar_arrow_color":"#d3d3d3","product_bar_border_color":"#e9e9e9","footer_widgets_bg_color":"#0D0F10","footer_widgets_headings_color":"#ffffff","footer_widgets_font_color":"#ffffff","footer_widgets_link_color":"#ffffff","footer_widgets_hover_color":"#909090","footer_widgets_border_color":"#3E3F3F","footer_bar_bg_color":"#0D0F10","footer_bar_bg_color_opacity":"1","footer_bar_font_color":"#ffffff","footer_bar_link_color":"#ffffff","footer_bar_hover_color":"#909090","page_breadcrumbs_background_color":"#ffffff","page_breadcrumbs_text_color":"#6e6e6e","page_breadcrumbs_text_hover_color":"#F95F51","page_breadcrumbs_divider_color":"#b2b2b2","page_breadcrumbs_border_color":"#e0e0e0","safebutton_layer_1_color":"#F95F51","safebutton_layer_2_color":"#000000","safebutton_layer_3_color":"#ffffff","sliding_area_background_color":"#232323","sliding_area_title_color":"#ffffff","sliding_area_text_color":"#777777","sliding_area_link_color":"#777777","sliding_area_link_hover_color":"#F95F51","sliding_area_close_btn_color":"#777777","sliding_area_border_color":"#3e3e3e","sliding_area_overflow_background_color":"#ffffff","sliding_area_overflow_background_color_opacity":"0.90","modal_title_color":"#000000","modal_text_color":"#777777","modal_cursor_color_color":"dark","modal_border_color":"#eef1f6","modal_overflow_background_color":"#ffffff","modal_overflow_background_color_opacity":"1","back_to_top_icon_color":"#ffffff","back_to_top_shape_color":"#262829","redux-backup":1}';
			$fildisi_eutf_skin_palette_2 = '{"top_bar_bg_color":"#ffffff","top_bar_font_color":"#777777","top_bar_link_color":"#777777","top_bar_hover_color":"#6ECA09","default_header_background_color":"#ffffff","default_header_background_color_opacity":"1","default_header_border_color":"#000000","default_header_border_color_opacity":"0.10","default_header_menu_text_color":"#000000","default_header_menu_text_hover_color":"#6ECA09","default_header_menu_type_color":"#eef1f6","default_header_menu_type_color_hover":"#6ECA09","default_header_submenu_bg_color":"#171a1d","default_header_submenu_text_color":"#777777","default_header_submenu_text_hover_color":"#ffffff","default_header_submenu_text_bg_hover_color":"#171a1d","default_header_submenu_column_text_color":"#ffffff","default_header_submenu_column_text_hover_color":"#ffffff","default_header_submenu_border_color":"#333638","logo_top_header_logo_area_background_color":"#ffffff","logo_top_header_logo_area_background_color_opacity":"1","logo_top_header_border_color":"#000000","logo_top_header_border_color_opacity":"0.10","logo_top_header_menu_area_background_color":"#ffffff","logo_top_header_menu_area_background_color_opacity":"1","logo_top_header_menu_text_color":"#000000","logo_top_header_menu_text_hover_color":"#6ECA09","logo_top_header_menu_type_color":"#eef1f6","logo_top_header_menu_type_color_hover":"#6ECA09","logo_top_header_submenu_bg_color":"#ffffff","logo_top_header_submenu_text_color":"#777777","logo_top_header_submenu_text_hover_color":"#6ECA09","logo_top_header_submenu_text_bg_hover_color":"#ffffff","logo_top_header_submenu_column_text_color":"#000000","logo_top_header_submenu_column_text_hover_color":"#6ECA09","logo_top_header_submenu_border_color":"#eef1f6","side_header_background_color":"#f7f7f7","side_header_background_color_opacity":"1","side_header_menu_text_color":"#000000","side_header_menu_text_hover_color":"#6ECA09","side_header_submenu_text_color":"#000000","side_header_submenu_text_hover_color":"#6ECA09","side_header_border_color":"#323232","side_header_border_opacity":"0.10","header_sticky_background_color":"#101215","header_sticky_background_color_opacity":"1","header_sticky_border_color":"#eef1f6","header_sticky_border_color_opacity":"0.30","sticky_menu_text_color":"#9c9c9c","sticky_menu_text_hover_color":"#ffffff","header_sticky_menu_type_color":"#eef1f6","header_sticky_menu_type_color_hover":"#eef1f6","light_menu_text_hover_color":"#ffffff","light_menu_type_color_hover":"#eef1f6","light_header_border_color":"#ffffff","light_header_border_color_opacity":"0.30","dark_menu_text_hover_color":"#000000","dark_menu_type_color_hover":"#eef1f6","dark_header_border_color":"#000000","dark_header_border_color_opacity":"0.10","responsive_header_background_color":"#000000","responsive_header_background_opacity":"1","responsive_header_elements_color":"#bfbfbf","responsive_header_elements_hover_color":"#ffffff","responsive_menu_background_color":"#ffffff","responsive_menu_link_color":"#777777","responsive_menu_link_hover_color":"#6ECA09","responsive_menu_close_btn_color":"#777777","responsive_menu_border_color":"#eef1f6","responsive_menu_overflow_background_color":"#000000","responsive_menu_overflow_background_color_opacity":"0.90","page_anchor_menu_background_color":"#171a1d","page_anchor_menu_text_color":"#ffffff","page_anchor_menu_text_hover_color":"#6ECA09","page_anchor_menu_background_hover_color":"#171a1d","page_anchor_menu_border_color":"#333638","main_content_background_color":"#ffffff","body_heading_color":"#000000","body_text_color":"#676767","body_text_link_color":"#000000","body_text_link_hover_color":"#6ECA09","body_border_color":"#e9e9e9","widget_title_color":"#000000","body_primary_1_color":"#6ECA09","body_primary_2_color":"#003a40","body_primary_3_color":"#487F84","body_primary_4_color":"#5F597E","body_primary_5_color":"#d6ccad","body_primary_6_color":"#db5111","blog_title_bg_color":"dark","blog_title_bg_color_custom":"#000000","blog_title_content_bg_color":"none","blog_title_content_bg_color_custom":"#ffffff","blog_title_color":"light","blog_title_color_custom":"#ffffff","blog_description_color":"light","blog_description_color_custom":"#ffffff","post_title_bg_color":"dark","post_title_bg_color_custom":"#000000","post_title_content_bg_color":"none","post_title_content_bg_color_custom":"#ffffff","post_subheading_color":"light","post_subheading_color_custom":"#ffffff","post_title_color":"light","post_title_color_custom":"#ffffff","post_description_color":"light","post_description_color_custom":"#ffffff","post_bar_background_color":"#ffffff","post_bar_socials_color":"#d3d3d3","post_bar_socials_color_hover":"#000000","post_bar_nav_title_color":"#000000","post_bar_arrow_color":"#d3d3d3","post_bar_border_color":"#e9e9e9","page_title_bg_color":"primary-2","page_title_bg_color_custom":"#000000","page_title_content_bg_color":"none","page_title_content_bg_color_custom":"#ffffff","page_title_color":"light","page_title_color_custom":"#ffffff","page_description_color":"light","page_description_color_custom":"#ffffff","search_page_title_bg_color":"dark","search_page_title_bg_color_custom":"#000000","search_page_title_content_bg_color":"none","search_page_title_content_bg_color_custom":"#ffffff","search_page_title_color":"light","search_page_title_color_custom":"#ffffff","search_page_description_color":"light","search_page_description_color_custom":"#ffffff","portfolio_title_bg_color":"dark","portfolio_title_bg_color_custom":"#000000","portfolio_title_content_bg_color":"none","portfolio_title_content_bg_color_custom":"#ffffff","portfolio_title_color":"light","portfolio_title_color_custom":"#ffffff","portfolio_description_color":"light","portfolio_description_color_custom":"#ffffff","portfolio_bar_background_color":"#ffffff","portfolio_bar_socials_color":"#d3d3d3","portfolio_bar_socials_color_hover":"#000000","portfolio_bar_nav_title_color":"#000000","portfolio_bar_arrow_color":"#d3d3d3","portfolio_bar_border_color":"#e9e9e9","product_tax_title_bg_color":"dark","product_tax_title_bg_color_custom":"#000000","product_tax_title_content_bg_color":"none","product_tax_title_content_bg_color_custom":"#ffffff","product_tax_title_color":"light","product_tax_title_color_custom":"#ffffff","product_tax_description_color":"light","product_tax_description_color_custom":"#ffffff","product_title_bg_color":"dark","product_title_bg_color_custom":"#000000","product_title_content_bg_color":"none","product_title_content_bg_color_custom":"#ffffff","product_title_color":"light","product_title_color_custom":"#ffffff","product_description_color":"light","product_description_color_custom":"#ffffff","product_area_bg_color":"#eeeeee","product_area_headings_color":"#000000","product_area_font_color":"#999999","product_area_link_color":"#FF7D88","product_area_hover_color":"#000000","product_area_border_color":"#e0e0e0","product_area_button_color":"primary-1","product_area_button_hover_color":"black","product_bar_background_color":"#ffffff","product_bar_socials_color":"#d3d3d3","product_bar_socials_color_hover":"#000000","product_bar_nav_title_color":"#000000","product_bar_arrow_color":"#d3d3d3","product_bar_border_color":"#e9e9e9","footer_widgets_bg_color":"#0D0F10","footer_widgets_headings_color":"#ffffff","footer_widgets_font_color":"#ffffff","footer_widgets_link_color":"#ffffff","footer_widgets_hover_color":"#909090","footer_widgets_border_color":"#3E3F3F","footer_bar_bg_color":"#0D0F10","footer_bar_bg_color_opacity":"1","footer_bar_font_color":"#ffffff","footer_bar_link_color":"#ffffff","footer_bar_hover_color":"#909090","page_breadcrumbs_background_color":"#ffffff","page_breadcrumbs_text_color":"#6e6e6e","page_breadcrumbs_text_hover_color":"#6ECA09","page_breadcrumbs_divider_color":"#b2b2b2","page_breadcrumbs_border_color":"#e0e0e0","safebutton_layer_1_color":"#6ECA09","safebutton_layer_2_color":"#000000","safebutton_layer_3_color":"#ffffff","sliding_area_background_color":"#232323","sliding_area_title_color":"#ffffff","sliding_area_text_color":"#777777","sliding_area_link_color":"#777777","sliding_area_link_hover_color":"#6ECA09","sliding_area_close_btn_color":"#777777","sliding_area_border_color":"#3e3e3e","sliding_area_overflow_background_color":"#ffffff","sliding_area_overflow_background_color_opacity":"0.90","modal_title_color":"#000000","modal_text_color":"#777777","modal_cursor_color_color":"dark","modal_border_color":"#eef1f6","modal_overflow_background_color":"#ffffff","modal_overflow_background_color_opacity":"1","back_to_top_icon_color":"#ffffff","back_to_top_shape_color":"#262829","redux-backup":1}';
			$fildisi_eutf_skin_palette_3 = '{"top_bar_bg_color":"#ffffff","top_bar_font_color":"#777777","top_bar_link_color":"#777777","top_bar_hover_color":"#E01076","default_header_background_color":"#ffffff","default_header_background_color_opacity":"1","default_header_border_color":"#000000","default_header_border_color_opacity":"0.10","default_header_menu_text_color":"#000000","default_header_menu_text_hover_color":"#E01076","default_header_menu_type_color":"#eef1f6","default_header_menu_type_color_hover":"#E01076","default_header_submenu_bg_color":"#171a1d","default_header_submenu_text_color":"#777777","default_header_submenu_text_hover_color":"#ffffff","default_header_submenu_text_bg_hover_color":"#171a1d","default_header_submenu_column_text_color":"#ffffff","default_header_submenu_column_text_hover_color":"#ffffff","default_header_submenu_border_color":"#333638","logo_top_header_logo_area_background_color":"#ffffff","logo_top_header_logo_area_background_color_opacity":"1","logo_top_header_border_color":"#000000","logo_top_header_border_color_opacity":"0.10","logo_top_header_menu_area_background_color":"#ffffff","logo_top_header_menu_area_background_color_opacity":"1","logo_top_header_menu_text_color":"#000000","logo_top_header_menu_text_hover_color":"#E01076","logo_top_header_menu_type_color":"#eef1f6","logo_top_header_menu_type_color_hover":"#E01076","logo_top_header_submenu_bg_color":"#ffffff","logo_top_header_submenu_text_color":"#777777","logo_top_header_submenu_text_hover_color":"#E01076","logo_top_header_submenu_text_bg_hover_color":"#ffffff","logo_top_header_submenu_column_text_color":"#000000","logo_top_header_submenu_column_text_hover_color":"#E01076","logo_top_header_submenu_border_color":"#eef1f6","side_header_background_color":"#f7f7f7","side_header_background_color_opacity":"1","side_header_menu_text_color":"#000000","side_header_menu_text_hover_color":"#E01076","side_header_submenu_text_color":"#000000","side_header_submenu_text_hover_color":"#E01076","side_header_border_color":"#323232","side_header_border_opacity":"0.10","header_sticky_background_color":"#101215","header_sticky_background_color_opacity":"1","header_sticky_border_color":"#eef1f6","header_sticky_border_color_opacity":"0.30","sticky_menu_text_color":"#9c9c9c","sticky_menu_text_hover_color":"#ffffff","header_sticky_menu_type_color":"#eef1f6","header_sticky_menu_type_color_hover":"#eef1f6","light_menu_text_hover_color":"#ffffff","light_menu_type_color_hover":"#eef1f6","light_header_border_color":"#ffffff","light_header_border_color_opacity":"0.30","dark_menu_text_hover_color":"#000000","dark_menu_type_color_hover":"#eef1f6","dark_header_border_color":"#000000","dark_header_border_color_opacity":"0.10","responsive_header_background_color":"#000000","responsive_header_background_opacity":"1","responsive_header_elements_color":"#bfbfbf","responsive_header_elements_hover_color":"#ffffff","responsive_menu_background_color":"#ffffff","responsive_menu_link_color":"#777777","responsive_menu_link_hover_color":"#E01076","responsive_menu_close_btn_color":"#777777","responsive_menu_border_color":"#eef1f6","responsive_menu_overflow_background_color":"#000000","responsive_menu_overflow_background_color_opacity":"0.90","page_anchor_menu_background_color":"#171a1d","page_anchor_menu_text_color":"#ffffff","page_anchor_menu_text_hover_color":"#E01076","page_anchor_menu_background_hover_color":"#171a1d","page_anchor_menu_border_color":"#333638","main_content_background_color":"#ffffff","body_heading_color":"#000000","body_text_color":"#676767","body_text_link_color":"#000000","body_text_link_hover_color":"#E01076","body_border_color":"#e9e9e9","widget_title_color":"#000000","body_primary_1_color":"#E01076","body_primary_2_color":"#003a40","body_primary_3_color":"#487F84","body_primary_4_color":"#5F597E","body_primary_5_color":"#d6ccad","body_primary_6_color":"#db5111","blog_title_bg_color":"dark","blog_title_bg_color_custom":"#000000","blog_title_content_bg_color":"none","blog_title_content_bg_color_custom":"#ffffff","blog_title_color":"light","blog_title_color_custom":"#ffffff","blog_description_color":"light","blog_description_color_custom":"#ffffff","post_title_bg_color":"dark","post_title_bg_color_custom":"#000000","post_title_content_bg_color":"none","post_title_content_bg_color_custom":"#ffffff","post_subheading_color":"light","post_subheading_color_custom":"#ffffff","post_title_color":"light","post_title_color_custom":"#ffffff","post_description_color":"light","post_description_color_custom":"#ffffff","post_bar_background_color":"#ffffff","post_bar_socials_color":"#d3d3d3","post_bar_socials_color_hover":"#000000","post_bar_nav_title_color":"#000000","post_bar_arrow_color":"#d3d3d3","post_bar_border_color":"#e9e9e9","page_title_bg_color":"primary-2","page_title_bg_color_custom":"#000000","page_title_content_bg_color":"none","page_title_content_bg_color_custom":"#ffffff","page_title_color":"light","page_title_color_custom":"#ffffff","page_description_color":"light","page_description_color_custom":"#ffffff","search_page_title_bg_color":"dark","search_page_title_bg_color_custom":"#000000","search_page_title_content_bg_color":"none","search_page_title_content_bg_color_custom":"#ffffff","search_page_title_color":"light","search_page_title_color_custom":"#ffffff","search_page_description_color":"light","search_page_description_color_custom":"#ffffff","portfolio_title_bg_color":"dark","portfolio_title_bg_color_custom":"#000000","portfolio_title_content_bg_color":"none","portfolio_title_content_bg_color_custom":"#ffffff","portfolio_title_color":"light","portfolio_title_color_custom":"#ffffff","portfolio_description_color":"light","portfolio_description_color_custom":"#ffffff","portfolio_bar_background_color":"#ffffff","portfolio_bar_socials_color":"#d3d3d3","portfolio_bar_socials_color_hover":"#000000","portfolio_bar_nav_title_color":"#000000","portfolio_bar_arrow_color":"#d3d3d3","portfolio_bar_border_color":"#e9e9e9","product_tax_title_bg_color":"dark","product_tax_title_bg_color_custom":"#000000","product_tax_title_content_bg_color":"none","product_tax_title_content_bg_color_custom":"#ffffff","product_tax_title_color":"light","product_tax_title_color_custom":"#ffffff","product_tax_description_color":"light","product_tax_description_color_custom":"#ffffff","product_title_bg_color":"dark","product_title_bg_color_custom":"#000000","product_title_content_bg_color":"none","product_title_content_bg_color_custom":"#ffffff","product_title_color":"light","product_title_color_custom":"#ffffff","product_description_color":"light","product_description_color_custom":"#ffffff","product_area_bg_color":"#eeeeee","product_area_headings_color":"#000000","product_area_font_color":"#999999","product_area_link_color":"#FF7D88","product_area_hover_color":"#000000","product_area_border_color":"#e0e0e0","product_area_button_color":"primary-1","product_area_button_hover_color":"black","product_bar_background_color":"#ffffff","product_bar_socials_color":"#d3d3d3","product_bar_socials_color_hover":"#000000","product_bar_nav_title_color":"#000000","product_bar_arrow_color":"#d3d3d3","product_bar_border_color":"#e9e9e9","footer_widgets_bg_color":"#0D0F10","footer_widgets_headings_color":"#ffffff","footer_widgets_font_color":"#ffffff","footer_widgets_link_color":"#ffffff","footer_widgets_hover_color":"#909090","footer_widgets_border_color":"#3E3F3F","footer_bar_bg_color":"#0D0F10","footer_bar_bg_color_opacity":"1","footer_bar_font_color":"#ffffff","footer_bar_link_color":"#ffffff","footer_bar_hover_color":"#909090","page_breadcrumbs_background_color":"#ffffff","page_breadcrumbs_text_color":"#6e6e6e","page_breadcrumbs_text_hover_color":"#E01076","page_breadcrumbs_divider_color":"#b2b2b2","page_breadcrumbs_border_color":"#e0e0e0","safebutton_layer_1_color":"#E01076","safebutton_layer_2_color":"#000000","safebutton_layer_3_color":"#ffffff","sliding_area_background_color":"#232323","sliding_area_title_color":"#ffffff","sliding_area_text_color":"#777777","sliding_area_link_color":"#777777","sliding_area_link_hover_color":"#E01076","sliding_area_close_btn_color":"#777777","sliding_area_border_color":"#3e3e3e","sliding_area_overflow_background_color":"#ffffff","sliding_area_overflow_background_color_opacity":"0.90","modal_title_color":"#000000","modal_text_color":"#777777","modal_cursor_color_color":"dark","modal_border_color":"#eef1f6","modal_overflow_background_color":"#ffffff","modal_overflow_background_color_opacity":"1","back_to_top_icon_color":"#ffffff","back_to_top_shape_color":"#262829","redux-backup":1}';
			$fildisi_eutf_skin_palette_4 = '{"top_bar_bg_color":"#ffffff","top_bar_font_color":"#777777","top_bar_link_color":"#777777","top_bar_hover_color":"#15C7FF","default_header_background_color":"#ffffff","default_header_background_color_opacity":"1","default_header_border_color":"#000000","default_header_border_color_opacity":"0.10","default_header_menu_text_color":"#000000","default_header_menu_text_hover_color":"#15C7FF","default_header_menu_type_color":"#eef1f6","default_header_menu_type_color_hover":"#15C7FF","default_header_submenu_bg_color":"#171a1d","default_header_submenu_text_color":"#777777","default_header_submenu_text_hover_color":"#ffffff","default_header_submenu_text_bg_hover_color":"#171a1d","default_header_submenu_column_text_color":"#ffffff","default_header_submenu_column_text_hover_color":"#ffffff","default_header_submenu_border_color":"#333638","logo_top_header_logo_area_background_color":"#ffffff","logo_top_header_logo_area_background_color_opacity":"1","logo_top_header_border_color":"#000000","logo_top_header_border_color_opacity":"0.10","logo_top_header_menu_area_background_color":"#ffffff","logo_top_header_menu_area_background_color_opacity":"1","logo_top_header_menu_text_color":"#000000","logo_top_header_menu_text_hover_color":"#15C7FF","logo_top_header_menu_type_color":"#eef1f6","logo_top_header_menu_type_color_hover":"#15C7FF","logo_top_header_submenu_bg_color":"#ffffff","logo_top_header_submenu_text_color":"#777777","logo_top_header_submenu_text_hover_color":"#15C7FF","logo_top_header_submenu_text_bg_hover_color":"#ffffff","logo_top_header_submenu_column_text_color":"#000000","logo_top_header_submenu_column_text_hover_color":"#15C7FF","logo_top_header_submenu_border_color":"#eef1f6","side_header_background_color":"#f7f7f7","side_header_background_color_opacity":"1","side_header_menu_text_color":"#000000","side_header_menu_text_hover_color":"#15C7FF","side_header_submenu_text_color":"#000000","side_header_submenu_text_hover_color":"#15C7FF","side_header_border_color":"#323232","side_header_border_opacity":"0.10","header_sticky_background_color":"#101215","header_sticky_background_color_opacity":"1","header_sticky_border_color":"#eef1f6","header_sticky_border_color_opacity":"0.30","sticky_menu_text_color":"#9c9c9c","sticky_menu_text_hover_color":"#ffffff","header_sticky_menu_type_color":"#eef1f6","header_sticky_menu_type_color_hover":"#eef1f6","light_menu_text_hover_color":"#ffffff","light_menu_type_color_hover":"#eef1f6","light_header_border_color":"#ffffff","light_header_border_color_opacity":"0.30","dark_menu_text_hover_color":"#000000","dark_menu_type_color_hover":"#eef1f6","dark_header_border_color":"#000000","dark_header_border_color_opacity":"0.10","responsive_header_background_color":"#000000","responsive_header_background_opacity":"1","responsive_header_elements_color":"#bfbfbf","responsive_header_elements_hover_color":"#ffffff","responsive_menu_background_color":"#ffffff","responsive_menu_link_color":"#777777","responsive_menu_link_hover_color":"#15C7FF","responsive_menu_close_btn_color":"#777777","responsive_menu_border_color":"#eef1f6","responsive_menu_overflow_background_color":"#000000","responsive_menu_overflow_background_color_opacity":"0.90","page_anchor_menu_background_color":"#171a1d","page_anchor_menu_text_color":"#ffffff","page_anchor_menu_text_hover_color":"#15C7FF","page_anchor_menu_background_hover_color":"#171a1d","page_anchor_menu_border_color":"#333638","main_content_background_color":"#ffffff","body_heading_color":"#000000","body_text_color":"#676767","body_text_link_color":"#000000","body_text_link_hover_color":"#15C7FF","body_border_color":"#e9e9e9","widget_title_color":"#000000","body_primary_1_color":"#15C7FF","body_primary_2_color":"#003a40","body_primary_3_color":"#487F84","body_primary_4_color":"#5F597E","body_primary_5_color":"#d6ccad","body_primary_6_color":"#db5111","blog_title_bg_color":"dark","blog_title_bg_color_custom":"#000000","blog_title_content_bg_color":"none","blog_title_content_bg_color_custom":"#ffffff","blog_title_color":"light","blog_title_color_custom":"#ffffff","blog_description_color":"light","blog_description_color_custom":"#ffffff","post_title_bg_color":"dark","post_title_bg_color_custom":"#000000","post_title_content_bg_color":"none","post_title_content_bg_color_custom":"#ffffff","post_subheading_color":"light","post_subheading_color_custom":"#ffffff","post_title_color":"light","post_title_color_custom":"#ffffff","post_description_color":"light","post_description_color_custom":"#ffffff","post_bar_background_color":"#ffffff","post_bar_socials_color":"#d3d3d3","post_bar_socials_color_hover":"#000000","post_bar_nav_title_color":"#000000","post_bar_arrow_color":"#d3d3d3","post_bar_border_color":"#e9e9e9","page_title_bg_color":"primary-2","page_title_bg_color_custom":"#000000","page_title_content_bg_color":"none","page_title_content_bg_color_custom":"#ffffff","page_title_color":"light","page_title_color_custom":"#ffffff","page_description_color":"light","page_description_color_custom":"#ffffff","search_page_title_bg_color":"dark","search_page_title_bg_color_custom":"#000000","search_page_title_content_bg_color":"none","search_page_title_content_bg_color_custom":"#ffffff","search_page_title_color":"light","search_page_title_color_custom":"#ffffff","search_page_description_color":"light","search_page_description_color_custom":"#ffffff","portfolio_title_bg_color":"dark","portfolio_title_bg_color_custom":"#000000","portfolio_title_content_bg_color":"none","portfolio_title_content_bg_color_custom":"#ffffff","portfolio_title_color":"light","portfolio_title_color_custom":"#ffffff","portfolio_description_color":"light","portfolio_description_color_custom":"#ffffff","portfolio_bar_background_color":"#ffffff","portfolio_bar_socials_color":"#d3d3d3","portfolio_bar_socials_color_hover":"#000000","portfolio_bar_nav_title_color":"#000000","portfolio_bar_arrow_color":"#d3d3d3","portfolio_bar_border_color":"#e9e9e9","product_tax_title_bg_color":"dark","product_tax_title_bg_color_custom":"#000000","product_tax_title_content_bg_color":"none","product_tax_title_content_bg_color_custom":"#ffffff","product_tax_title_color":"light","product_tax_title_color_custom":"#ffffff","product_tax_description_color":"light","product_tax_description_color_custom":"#ffffff","product_title_bg_color":"dark","product_title_bg_color_custom":"#000000","product_title_content_bg_color":"none","product_title_content_bg_color_custom":"#ffffff","product_title_color":"light","product_title_color_custom":"#ffffff","product_description_color":"light","product_description_color_custom":"#ffffff","product_area_bg_color":"#eeeeee","product_area_headings_color":"#000000","product_area_font_color":"#999999","product_area_link_color":"#FF7D88","product_area_hover_color":"#000000","product_area_border_color":"#e0e0e0","product_area_button_color":"primary-1","product_area_button_hover_color":"black","product_bar_background_color":"#ffffff","product_bar_socials_color":"#d3d3d3","product_bar_socials_color_hover":"#000000","product_bar_nav_title_color":"#000000","product_bar_arrow_color":"#d3d3d3","product_bar_border_color":"#e9e9e9","footer_widgets_bg_color":"#0D0F10","footer_widgets_headings_color":"#ffffff","footer_widgets_font_color":"#ffffff","footer_widgets_link_color":"#ffffff","footer_widgets_hover_color":"#909090","footer_widgets_border_color":"#3E3F3F","footer_bar_bg_color":"#0D0F10","footer_bar_bg_color_opacity":"1","footer_bar_font_color":"#ffffff","footer_bar_link_color":"#ffffff","footer_bar_hover_color":"#909090","page_breadcrumbs_background_color":"#ffffff","page_breadcrumbs_text_color":"#6e6e6e","page_breadcrumbs_text_hover_color":"#15C7FF","page_breadcrumbs_divider_color":"#b2b2b2","page_breadcrumbs_border_color":"#e0e0e0","safebutton_layer_1_color":"#15C7FF","safebutton_layer_2_color":"#000000","safebutton_layer_3_color":"#ffffff","sliding_area_background_color":"#232323","sliding_area_title_color":"#ffffff","sliding_area_text_color":"#777777","sliding_area_link_color":"#777777","sliding_area_link_hover_color":"#15C7FF","sliding_area_close_btn_color":"#777777","sliding_area_border_color":"#3e3e3e","sliding_area_overflow_background_color":"#ffffff","sliding_area_overflow_background_color_opacity":"0.90","modal_title_color":"#000000","modal_text_color":"#777777","modal_cursor_color_color":"dark","modal_border_color":"#eef1f6","modal_overflow_background_color":"#ffffff","modal_overflow_background_color_opacity":"1","back_to_top_icon_color":"#ffffff","back_to_top_shape_color":"#262829","redux-backup":1}';
			$fildisi_eutf_skin_palette_5 = '{"top_bar_bg_color":"#ffffff","top_bar_font_color":"#777777","top_bar_link_color":"#777777","top_bar_hover_color":"#9013FE","default_header_background_color":"#ffffff","default_header_background_color_opacity":"1","default_header_border_color":"#000000","default_header_border_color_opacity":"0.10","default_header_menu_text_color":"#000000","default_header_menu_text_hover_color":"#9013FE","default_header_menu_type_color":"#eef1f6","default_header_menu_type_color_hover":"#9013FE","default_header_submenu_bg_color":"#171a1d","default_header_submenu_text_color":"#777777","default_header_submenu_text_hover_color":"#ffffff","default_header_submenu_text_bg_hover_color":"#171a1d","default_header_submenu_column_text_color":"#ffffff","default_header_submenu_column_text_hover_color":"#ffffff","default_header_submenu_border_color":"#333638","logo_top_header_logo_area_background_color":"#ffffff","logo_top_header_logo_area_background_color_opacity":"1","logo_top_header_border_color":"#000000","logo_top_header_border_color_opacity":"0.10","logo_top_header_menu_area_background_color":"#ffffff","logo_top_header_menu_area_background_color_opacity":"1","logo_top_header_menu_text_color":"#000000","logo_top_header_menu_text_hover_color":"#9013FE","logo_top_header_menu_type_color":"#eef1f6","logo_top_header_menu_type_color_hover":"#9013FE","logo_top_header_submenu_bg_color":"#ffffff","logo_top_header_submenu_text_color":"#777777","logo_top_header_submenu_text_hover_color":"#9013FE","logo_top_header_submenu_text_bg_hover_color":"#ffffff","logo_top_header_submenu_column_text_color":"#000000","logo_top_header_submenu_column_text_hover_color":"#9013FE","logo_top_header_submenu_border_color":"#eef1f6","side_header_background_color":"#f7f7f7","side_header_background_color_opacity":"1","side_header_menu_text_color":"#000000","side_header_menu_text_hover_color":"#9013FE","side_header_submenu_text_color":"#000000","side_header_submenu_text_hover_color":"#9013FE","side_header_border_color":"#323232","side_header_border_opacity":"0.10","header_sticky_background_color":"#101215","header_sticky_background_color_opacity":"1","header_sticky_border_color":"#eef1f6","header_sticky_border_color_opacity":"0.30","sticky_menu_text_color":"#9c9c9c","sticky_menu_text_hover_color":"#ffffff","header_sticky_menu_type_color":"#eef1f6","header_sticky_menu_type_color_hover":"#eef1f6","light_menu_text_hover_color":"#ffffff","light_menu_type_color_hover":"#eef1f6","light_header_border_color":"#ffffff","light_header_border_color_opacity":"0.30","dark_menu_text_hover_color":"#000000","dark_menu_type_color_hover":"#eef1f6","dark_header_border_color":"#000000","dark_header_border_color_opacity":"0.10","responsive_header_background_color":"#000000","responsive_header_background_opacity":"1","responsive_header_elements_color":"#bfbfbf","responsive_header_elements_hover_color":"#ffffff","responsive_menu_background_color":"#ffffff","responsive_menu_link_color":"#777777","responsive_menu_link_hover_color":"#9013FE","responsive_menu_close_btn_color":"#777777","responsive_menu_border_color":"#eef1f6","responsive_menu_overflow_background_color":"#000000","responsive_menu_overflow_background_color_opacity":"0.90","page_anchor_menu_background_color":"#171a1d","page_anchor_menu_text_color":"#ffffff","page_anchor_menu_text_hover_color":"#9013FE","page_anchor_menu_background_hover_color":"#171a1d","page_anchor_menu_border_color":"#333638","main_content_background_color":"#ffffff","body_heading_color":"#000000","body_text_color":"#676767","body_text_link_color":"#000000","body_text_link_hover_color":"#9013FE","body_border_color":"#e9e9e9","widget_title_color":"#000000","body_primary_1_color":"#9013FE","body_primary_2_color":"#003a40","body_primary_3_color":"#487F84","body_primary_4_color":"#5F597E","body_primary_5_color":"#d6ccad","body_primary_6_color":"#db5111","blog_title_bg_color":"dark","blog_title_bg_color_custom":"#000000","blog_title_content_bg_color":"none","blog_title_content_bg_color_custom":"#ffffff","blog_title_color":"light","blog_title_color_custom":"#ffffff","blog_description_color":"light","blog_description_color_custom":"#ffffff","post_title_bg_color":"dark","post_title_bg_color_custom":"#000000","post_title_content_bg_color":"none","post_title_content_bg_color_custom":"#ffffff","post_subheading_color":"light","post_subheading_color_custom":"#ffffff","post_title_color":"light","post_title_color_custom":"#ffffff","post_description_color":"light","post_description_color_custom":"#ffffff","post_bar_background_color":"#ffffff","post_bar_socials_color":"#d3d3d3","post_bar_socials_color_hover":"#000000","post_bar_nav_title_color":"#000000","post_bar_arrow_color":"#d3d3d3","post_bar_border_color":"#e9e9e9","page_title_bg_color":"primary-2","page_title_bg_color_custom":"#000000","page_title_content_bg_color":"none","page_title_content_bg_color_custom":"#ffffff","page_title_color":"light","page_title_color_custom":"#ffffff","page_description_color":"light","page_description_color_custom":"#ffffff","search_page_title_bg_color":"dark","search_page_title_bg_color_custom":"#000000","search_page_title_content_bg_color":"none","search_page_title_content_bg_color_custom":"#ffffff","search_page_title_color":"light","search_page_title_color_custom":"#ffffff","search_page_description_color":"light","search_page_description_color_custom":"#ffffff","portfolio_title_bg_color":"dark","portfolio_title_bg_color_custom":"#000000","portfolio_title_content_bg_color":"none","portfolio_title_content_bg_color_custom":"#ffffff","portfolio_title_color":"light","portfolio_title_color_custom":"#ffffff","portfolio_description_color":"light","portfolio_description_color_custom":"#ffffff","portfolio_bar_background_color":"#ffffff","portfolio_bar_socials_color":"#d3d3d3","portfolio_bar_socials_color_hover":"#000000","portfolio_bar_nav_title_color":"#000000","portfolio_bar_arrow_color":"#d3d3d3","portfolio_bar_border_color":"#e9e9e9","product_tax_title_bg_color":"dark","product_tax_title_bg_color_custom":"#000000","product_tax_title_content_bg_color":"none","product_tax_title_content_bg_color_custom":"#ffffff","product_tax_title_color":"light","product_tax_title_color_custom":"#ffffff","product_tax_description_color":"light","product_tax_description_color_custom":"#ffffff","product_title_bg_color":"dark","product_title_bg_color_custom":"#000000","product_title_content_bg_color":"none","product_title_content_bg_color_custom":"#ffffff","product_title_color":"light","product_title_color_custom":"#ffffff","product_description_color":"light","product_description_color_custom":"#ffffff","product_area_bg_color":"#eeeeee","product_area_headings_color":"#000000","product_area_font_color":"#999999","product_area_link_color":"#FF7D88","product_area_hover_color":"#000000","product_area_border_color":"#e0e0e0","product_area_button_color":"primary-1","product_area_button_hover_color":"black","product_bar_background_color":"#ffffff","product_bar_socials_color":"#d3d3d3","product_bar_socials_color_hover":"#000000","product_bar_nav_title_color":"#000000","product_bar_arrow_color":"#d3d3d3","product_bar_border_color":"#e9e9e9","footer_widgets_bg_color":"#0D0F10","footer_widgets_headings_color":"#ffffff","footer_widgets_font_color":"#ffffff","footer_widgets_link_color":"#ffffff","footer_widgets_hover_color":"#909090","footer_widgets_border_color":"#3E3F3F","footer_bar_bg_color":"#0D0F10","footer_bar_bg_color_opacity":"1","footer_bar_font_color":"#ffffff","footer_bar_link_color":"#ffffff","footer_bar_hover_color":"#909090","page_breadcrumbs_background_color":"#ffffff","page_breadcrumbs_text_color":"#6e6e6e","page_breadcrumbs_text_hover_color":"#9013FE","page_breadcrumbs_divider_color":"#b2b2b2","page_breadcrumbs_border_color":"#e0e0e0","safebutton_layer_1_color":"#9013FE","safebutton_layer_2_color":"#000000","safebutton_layer_3_color":"#ffffff","sliding_area_background_color":"#232323","sliding_area_title_color":"#ffffff","sliding_area_text_color":"#777777","sliding_area_link_color":"#777777","sliding_area_link_hover_color":"#9013FE","sliding_area_close_btn_color":"#777777","sliding_area_border_color":"#3e3e3e","sliding_area_overflow_background_color":"#ffffff","sliding_area_overflow_background_color_opacity":"0.90","modal_title_color":"#000000","modal_text_color":"#777777","modal_cursor_color_color":"dark","modal_border_color":"#eef1f6","modal_overflow_background_color":"#ffffff","modal_overflow_background_color_opacity":"1","back_to_top_icon_color":"#ffffff","back_to_top_shape_color":"#262829","redux-backup":1}';
			$fildisi_eutf_skin_palette_6 = '{"top_bar_bg_color":"#ffffff","top_bar_font_color":"#777777","top_bar_link_color":"#777777","top_bar_hover_color":"#F5A623","default_header_background_color":"#ffffff","default_header_background_color_opacity":"1","default_header_border_color":"#000000","default_header_border_color_opacity":"0.10","default_header_menu_text_color":"#000000","default_header_menu_text_hover_color":"#F5A623","default_header_menu_type_color":"#eef1f6","default_header_menu_type_color_hover":"#F5A623","default_header_submenu_bg_color":"#171a1d","default_header_submenu_text_color":"#777777","default_header_submenu_text_hover_color":"#ffffff","default_header_submenu_text_bg_hover_color":"#171a1d","default_header_submenu_column_text_color":"#ffffff","default_header_submenu_column_text_hover_color":"#ffffff","default_header_submenu_border_color":"#333638","logo_top_header_logo_area_background_color":"#ffffff","logo_top_header_logo_area_background_color_opacity":"1","logo_top_header_border_color":"#000000","logo_top_header_border_color_opacity":"0.10","logo_top_header_menu_area_background_color":"#ffffff","logo_top_header_menu_area_background_color_opacity":"1","logo_top_header_menu_text_color":"#000000","logo_top_header_menu_text_hover_color":"#F5A623","logo_top_header_menu_type_color":"#eef1f6","logo_top_header_menu_type_color_hover":"#F5A623","logo_top_header_submenu_bg_color":"#ffffff","logo_top_header_submenu_text_color":"#777777","logo_top_header_submenu_text_hover_color":"#F5A623","logo_top_header_submenu_text_bg_hover_color":"#ffffff","logo_top_header_submenu_column_text_color":"#000000","logo_top_header_submenu_column_text_hover_color":"#F5A623","logo_top_header_submenu_border_color":"#eef1f6","side_header_background_color":"#f7f7f7","side_header_background_color_opacity":"1","side_header_menu_text_color":"#000000","side_header_menu_text_hover_color":"#F5A623","side_header_submenu_text_color":"#000000","side_header_submenu_text_hover_color":"#F5A623","side_header_border_color":"#323232","side_header_border_opacity":"0.10","header_sticky_background_color":"#101215","header_sticky_background_color_opacity":"1","header_sticky_border_color":"#eef1f6","header_sticky_border_color_opacity":"0.30","sticky_menu_text_color":"#9c9c9c","sticky_menu_text_hover_color":"#ffffff","header_sticky_menu_type_color":"#eef1f6","header_sticky_menu_type_color_hover":"#eef1f6","light_menu_text_hover_color":"#ffffff","light_menu_type_color_hover":"#eef1f6","light_header_border_color":"#ffffff","light_header_border_color_opacity":"0.30","dark_menu_text_hover_color":"#000000","dark_menu_type_color_hover":"#eef1f6","dark_header_border_color":"#000000","dark_header_border_color_opacity":"0.10","responsive_header_background_color":"#000000","responsive_header_background_opacity":"1","responsive_header_elements_color":"#bfbfbf","responsive_header_elements_hover_color":"#ffffff","responsive_menu_background_color":"#ffffff","responsive_menu_link_color":"#777777","responsive_menu_link_hover_color":"#F5A623","responsive_menu_close_btn_color":"#777777","responsive_menu_border_color":"#eef1f6","responsive_menu_overflow_background_color":"#000000","responsive_menu_overflow_background_color_opacity":"0.90","page_anchor_menu_background_color":"#171a1d","page_anchor_menu_text_color":"#ffffff","page_anchor_menu_text_hover_color":"#F5A623","page_anchor_menu_background_hover_color":"#171a1d","page_anchor_menu_border_color":"#333638","main_content_background_color":"#ffffff","body_heading_color":"#000000","body_text_color":"#676767","body_text_link_color":"#000000","body_text_link_hover_color":"#F5A623","body_border_color":"#e9e9e9","widget_title_color":"#000000","body_primary_1_color":"#F5A623","body_primary_2_color":"#003a40","body_primary_3_color":"#487F84","body_primary_4_color":"#5F597E","body_primary_5_color":"#d6ccad","body_primary_6_color":"#db5111","blog_title_bg_color":"dark","blog_title_bg_color_custom":"#000000","blog_title_content_bg_color":"none","blog_title_content_bg_color_custom":"#ffffff","blog_title_color":"light","blog_title_color_custom":"#ffffff","blog_description_color":"light","blog_description_color_custom":"#ffffff","post_title_bg_color":"dark","post_title_bg_color_custom":"#000000","post_title_content_bg_color":"none","post_title_content_bg_color_custom":"#ffffff","post_subheading_color":"light","post_subheading_color_custom":"#ffffff","post_title_color":"light","post_title_color_custom":"#ffffff","post_description_color":"light","post_description_color_custom":"#ffffff","post_bar_background_color":"#ffffff","post_bar_socials_color":"#d3d3d3","post_bar_socials_color_hover":"#000000","post_bar_nav_title_color":"#000000","post_bar_arrow_color":"#d3d3d3","post_bar_border_color":"#e9e9e9","page_title_bg_color":"primary-2","page_title_bg_color_custom":"#000000","page_title_content_bg_color":"none","page_title_content_bg_color_custom":"#ffffff","page_title_color":"light","page_title_color_custom":"#ffffff","page_description_color":"light","page_description_color_custom":"#ffffff","search_page_title_bg_color":"dark","search_page_title_bg_color_custom":"#000000","search_page_title_content_bg_color":"none","search_page_title_content_bg_color_custom":"#ffffff","search_page_title_color":"light","search_page_title_color_custom":"#ffffff","search_page_description_color":"light","search_page_description_color_custom":"#ffffff","portfolio_title_bg_color":"dark","portfolio_title_bg_color_custom":"#000000","portfolio_title_content_bg_color":"none","portfolio_title_content_bg_color_custom":"#ffffff","portfolio_title_color":"light","portfolio_title_color_custom":"#ffffff","portfolio_description_color":"light","portfolio_description_color_custom":"#ffffff","portfolio_bar_background_color":"#ffffff","portfolio_bar_socials_color":"#d3d3d3","portfolio_bar_socials_color_hover":"#000000","portfolio_bar_nav_title_color":"#000000","portfolio_bar_arrow_color":"#d3d3d3","portfolio_bar_border_color":"#e9e9e9","product_tax_title_bg_color":"dark","product_tax_title_bg_color_custom":"#000000","product_tax_title_content_bg_color":"none","product_tax_title_content_bg_color_custom":"#ffffff","product_tax_title_color":"light","product_tax_title_color_custom":"#ffffff","product_tax_description_color":"light","product_tax_description_color_custom":"#ffffff","product_title_bg_color":"dark","product_title_bg_color_custom":"#000000","product_title_content_bg_color":"none","product_title_content_bg_color_custom":"#ffffff","product_title_color":"light","product_title_color_custom":"#ffffff","product_description_color":"light","product_description_color_custom":"#ffffff","product_area_bg_color":"#eeeeee","product_area_headings_color":"#000000","product_area_font_color":"#999999","product_area_link_color":"#FF7D88","product_area_hover_color":"#000000","product_area_border_color":"#e0e0e0","product_area_button_color":"primary-1","product_area_button_hover_color":"black","product_bar_background_color":"#ffffff","product_bar_socials_color":"#d3d3d3","product_bar_socials_color_hover":"#000000","product_bar_nav_title_color":"#000000","product_bar_arrow_color":"#d3d3d3","product_bar_border_color":"#e9e9e9","footer_widgets_bg_color":"#0D0F10","footer_widgets_headings_color":"#ffffff","footer_widgets_font_color":"#ffffff","footer_widgets_link_color":"#ffffff","footer_widgets_hover_color":"#909090","footer_widgets_border_color":"#3E3F3F","footer_bar_bg_color":"#0D0F10","footer_bar_bg_color_opacity":"1","footer_bar_font_color":"#ffffff","footer_bar_link_color":"#ffffff","footer_bar_hover_color":"#909090","page_breadcrumbs_background_color":"#ffffff","page_breadcrumbs_text_color":"#6e6e6e","page_breadcrumbs_text_hover_color":"#F5A623","page_breadcrumbs_divider_color":"#b2b2b2","page_breadcrumbs_border_color":"#e0e0e0","safebutton_layer_1_color":"#F5A623","safebutton_layer_2_color":"#000000","safebutton_layer_3_color":"#ffffff","sliding_area_background_color":"#232323","sliding_area_title_color":"#ffffff","sliding_area_text_color":"#777777","sliding_area_link_color":"#777777","sliding_area_link_hover_color":"#F5A623","sliding_area_close_btn_color":"#777777","sliding_area_border_color":"#3e3e3e","sliding_area_overflow_background_color":"#ffffff","sliding_area_overflow_background_color_opacity":"0.90","modal_title_color":"#000000","modal_text_color":"#777777","modal_cursor_color_color":"dark","modal_border_color":"#eef1f6","modal_overflow_background_color":"#ffffff","modal_overflow_background_color_opacity":"1","back_to_top_icon_color":"#ffffff","back_to_top_shape_color":"#262829","redux-backup":1}';

			$fildisi_gmap_api_key_link = '<a href="//developers.google.com/maps/documentation/javascript/get-api-key" target="_blank" rel="noopener noreferrer">' . esc_html__( 'Generate Google Map API Key', 'fildisi' ) . '</a>';
			$fildisi_gmap_style_link = '<a href="//mapstyle.withgoogle.com/" target="_blank" rel="noopener noreferrer">' . esc_html__( 'Styling Wizard Google Maps API.', 'fildisi' ) . '</a>';

			$fildisi_is_google = apply_filters( 'fildisi_eutf_gfonts_visibility', true );
			// ACTUAL DECLARATION OF SECTIONS

			$this->sections[] = array(
				'icon' => 'el-icon-cogs',
				'title' => esc_html__( 'General Settings', 'fildisi' ),
				'id' => 'eut_redux_section_general_settings',
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'theme_layout',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Theme Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select between Streched or Boxed for the theme basic Layout.', 'fildisi' ),
						'options' => $fildisi_eutf_theme_layout_selection,
						'default' => 'stretched',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'boxed_size',
						'type' => 'text',
						'default' => '1220',
						'title' => esc_html__( 'Theme Wrapper Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Theme Wrapper width in px (Default is 1220).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'theme_layout', 'equals', 'boxed' ),
					),
					array(
						'id' => 'frame_size',
						'type' => 'text',
						'default' => '30',
						'title' => esc_html__( 'Frame Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter frame size in px (Default is 30).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'theme_layout', 'equals', 'framed' ),
					),
					array(
						'id'          => 'frame_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Frame Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select a color for the framed layout.', 'fildisi' ),
						'default'     => '#ffffff',
						'required' => array( 'theme_layout', 'equals', 'framed' ),
						'transparent' => false,
					),
					array(
						'id' => 'container_size',
						'type' => 'text',
						'default' => '1170',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Container width in px (Default is 1170).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id'       => 'body_background',
						'type'     => 'background',
						'title'    => esc_html__( 'Theme Background Image / Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image or color for the theme.', 'fildisi' ),
						'background-color' => true,
						'transparent' => false,
						'background-repeat' => true,
						'background-attachment' => true,
						'background-clip' => false,
						'background-size' => true,
						'output'    => '.eut-body',
						'default' => array (
							'background-color' => '#171A1D',
						),
					),
					array(
						'id'=>'theme_loader',
						'type' => 'switch',
						'title' => esc_html__( 'Theme Loader', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable Theme Loader.', 'fildisi' ),
						'default' => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'=>'show_spinner',
						'type' => 'switch',
						'title' => esc_html__( 'Enable Spinner', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable Theme Loader Spinner.', 'fildisi' ),
						'default' => '1',
						'1' => esc_html__( 'On', 'fildisi' ),
						'0' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'theme_loader', 'equals', '1' ),
					),
					array(
						'id' => 'page_transition',
						'type' => 'select',
						'title' => esc_html__( 'Page Transition', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the page transition when navigating from one page to another. It is worth noting that it maybe causes problems with plugins which use ajax requests.', 'fildisi' ),
						'options' => array(
							'none' => esc_html__( 'None', 'fildisi' ),
							'fade-in' => esc_html__( 'Fade In', 'fildisi' ),
							'left-to-right' => esc_html__( 'Left to Right', 'fildisi' ),
							'right-to-left' => esc_html__( 'Right to Left', 'fildisi' ),
							'top-to-bottom' => esc_html__( 'Top to Bottom', 'fildisi' ),
							'bottom-to-top' => esc_html__( 'Bottom to Top', 'fildisi' ),
							'zoom-out' => esc_html__( 'Zoom Out', 'fildisi' ),
						),
						'default' => 'none',
						'validate' => 'not_empty',
						'required' => array( 'theme_loader', 'equals', '1' ),
					),
					array(
						'id' => 'replace_admin_logo',
						'type' => 'switch',
						'title' => esc_html__( 'Replace Admin Logo', 'fildisi' ),
						'subtitle'=> esc_html__( 'Replace the backend admin logo with your company logo.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'       => 'admin_logo',
						'type'     => 'media',
						'title' => esc_html__( 'Admin Logo', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the image for your company logo. ( If empty Logo Default will be used instead )', 'fildisi' ),
						'required' => array( 'replace_admin_logo', 'equals', '1' ),
					),
					array(
						'id' => 'admin_logo_height',
						'type' => 'text',
						'default' => '84',
						'title' => esc_html__( 'Admin Logo Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the company logo height in px (Default is 84).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'replace_admin_logo', 'equals', '1' ),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Logos', 'fildisi' ),
				'id' => 'eut_redux_section_logos',
				'header' => '',
				'desc' => '',
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-photo',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'logo',
						'url' => true,
						'type' => 'media',
						'title' => esc_html__( 'Logo Default Header', 'fildisi' ),
						'read-only' => false,
						'default' => array( 'url' => get_template_directory_uri() .'/images/logos/logo-default.png', 'width' => '216', 'height' => '100' ),
						'subtitle' => esc_html__( 'Upload the logo for the Default Header.', 'fildisi' ),
					),
					array(
						'id' => 'logo_light',
						'url'=> true,
						'type' => 'media',
						'title' => esc_html__( 'Logo Light Header', 'fildisi' ),
						'read-only' => false,
						'default' => array( 'url' => get_template_directory_uri() .'/images/logos/logo-light.png', 'width' => '216', 'height' => '100' ),
						'subtitle' => esc_html__( 'Upload the logo for the Light Header.', 'fildisi' ),
					),
					array(
						'id' => 'logo_dark',
						'url' => true,
						'type' => 'media',
						'title' => esc_html__( 'Logo Dark Header', 'fildisi' ),
						'read-only' => false,
						'default' => array( 'url' => get_template_directory_uri() .'/images/logos/logo-default.png', 'width' => '216', 'height' => '100' ),
						'subtitle' => esc_html__( 'Upload the logo for the Dark Header.', 'fildisi' ),
					),
					array(
						'id' => 'logo_side',
						'url' => true,
						'type' => 'media',
						'title' => esc_html__( 'Logo Side Header', 'fildisi' ),
						'read-only' => false,
						'default' => array( 'url' => get_template_directory_uri() .'/images/logos/logo-side.png', 'width' => '182', 'height' => '200' ),
						'subtitle' => esc_html__( 'Upload the logo for the Side Header.', 'fildisi' ),
					),
					array(
						'id' => 'logo_sticky',
						'url'=> true,
						'type' => 'media',
						'title' => esc_html__( 'Logo Sticky Header', 'fildisi' ),
						'read-only' => false,
						'default' => array( 'url' => get_template_directory_uri() .'/images/logos/logo-sticky.png', 'width' => '80', 'height' => '88' ),
						'subtitle' => esc_html__( 'Upload the logo for the Sticky Header.', 'fildisi' ),
					),
					array(
						'id' => 'logo_responsive',
						'url' => true,
						'type' => 'media',
						'title' => esc_html__( 'Logo Responsive Header', 'fildisi' ),
						'read-only' => false,
						'default' => array( 'url' => get_template_directory_uri() .'/images/logos/logo-responsive.png', 'width' => '46', 'height' => '50' ),
						'subtitle' => esc_html__( 'Upload the logo for the Responsive Header.', 'fildisi' ),
					),
				)
			);
			$this->sections[] = array(
				'title' => esc_html__( 'Top Bar Options', 'fildisi' ),
				'id' => 'eut_redux_section_top_bar_options',
				'header' => '',
				'desc' => esc_html__( 'You can enable the TopBar area, just above the Header, in order to add various elements in two different positions, left and right.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-arrow-up',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id'=>'top_bar_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Top Bar Area', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the TopBar Area, the area just above your header.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'             => 'top_bar_spacing',
						'type'           => 'spacing',
						'mode'           => 'padding',
						'units'          => 'px',
						'units_extended' => 'false',
						'left'           => 'false',
						'right'          => 'false',
						'title'          => esc_html__( 'Top Bar Spacing', 'fildisi' ),
						'subtitle'       => esc_html__( 'Set the spacing, Top and Bottom, of the TopBar Area.', 'fildisi' ),
						'desc'           => esc_html__( 'Set spacing Top, Bottom in px.', 'fildisi'),
						'default'        => array(
							'padding-top'     => '15px',
							'padding-bottom'  => '15px',
							'units'           => 'px',
						),
						'required' => array( 'top_bar_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'top_bar_section_type',
						'type' => 'select',
						'title' => esc_html__( 'Top Bar Full Width', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you prefer a full-width Top Bar Area.', 'fildisi' ),
						'options' => array(
							'fullwidth-background' => esc_html__( 'No', 'fildisi' ),
							'fullwidth-element' => esc_html__( 'Yes', 'fildisi' ),
						),
						'default' => 'fullwidth-background',
						'validate' => 'not_empty',
						'required' => array( 'top_bar_enabled', 'equals', '1' ),
					),
					array(
						'id'   => 'info_top_bar_left',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Left Top Bar Area', 'fildisi' ),
						'desc' => esc_html__( 'In side navigation mode, Left Top Bar Area is shown first.', 'fildisi' ),
						'required' => array( 'top_bar_enabled', 'equals', '1' ),
					),
					array(
						'id'=>'top_bar_left_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Left Area', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the Left TopBar Area.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'top_bar_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'top_bar_left_options',
						'type' => 'sortable',
						'mode' => 'checkbox',
						'title' => esc_html__( 'Left Area Elements', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable or Disable the elements you like to show in the Left TopBar Area.', 'fildisi' ),
						'options' => $fildisi_eutf_top_bar_options,
						'default' => $fildisi_eutf_top_bar_options_default,
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_left_enabled', 'equals', '1' ),
						),
					),
					array(
						'id' => 'top_bar_left_text',
						'type' => 'text',
						'title' => esc_html__( 'Left Area Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Place the text you wish for your Left TopBar Area.', 'fildisi' ),
						'default' => '',
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_left_enabled', 'equals', '1' ),
						),
					),
					array(
						'id'=>'top_bar_left_type_form',
						'type' => 'button_set',
						'title' => esc_html__( 'Left Form Type', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the type of your form.', 'fildisi' ),
						'options' => array(
							'contact-form' => esc_html__( 'Contact Form 7', 'fildisi' ),
							'gravity-form' => esc_html__( 'Gravity Form', 'fildisi' ),
						),
						'default' => 'contact-form',
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_left_enabled', 'equals', '1' ),
						),
					),
					array(
						'id' => 'top_bar_left_form',
						'type' => 'select',
						'title' => esc_html__( 'Left Area Form', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the form for your Contact Form.', 'fildisi' ),
						'data' => 'posts',
						'args' => array( 'post_type' => 'wpcf7_contact_form', 'numberposts' => -1 ),
						'default' => '',
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_left_enabled', 'equals', '1' ),
							array( 'top_bar_left_type_form', 'equals', 'contact-form' ),
						),
					),
					array(
						'id' => 'top_bar_left_gravity_form',
						'type' => 'select',
						'title' => esc_html__( 'Left Area Form', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the form for your Gravity Form.', 'fildisi' ),
						'options' => $fildisi_eutf_gravity_options,
						'default' => '',
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_left_enabled', 'equals', '1' ),
							array( 'top_bar_left_type_form', 'equals', 'gravity-form' ),
						),
					),
					array(
						'id' => 'top_bar_left_social_options',
						'type' => 'checkbox',
						'title' => esc_html__( 'Left Area Social Icons', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your social icons. Social URLs are configured from Theme Options - Social Media', 'fildisi' ),
						'desc' => '',
						'class' => 'eut-redux-columns',
						'label' => true,
						'options' => $fildisi_eutf_social_options,
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_left_enabled', 'equals', '1' ),
						),
					),
					array(
						'id'   => 'info_top_bar_right',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Right Top Bar Area', 'fildisi' ),
						'desc' => esc_html__( 'In side navigation mode, Right Top Bar Area is shown second.', 'fildisi' ),
						'required' => array( 'top_bar_enabled', 'equals', '1' ),
					),
					array(
						'id'=>'top_bar_right_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Right Area', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the Right TopBar Area.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'top_bar_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'top_bar_right_options',
						'type' => 'sortable',
						'mode' => 'checkbox',
						'title' => esc_html__( 'Right Area Elements', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable or Disable the elements you like to show in the Right TopBar Area.', 'fildisi' ),
						'options' => $fildisi_eutf_top_bar_options,
						'default' => $fildisi_eutf_top_bar_options_default,
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_right_enabled', 'equals', '1' ),
						),
					),
					array(
						'id' => 'top_bar_right_text',
						'type' => 'text',
						'title' => esc_html__( 'Right Area Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Place the text you wish for your Right TopBar Area.', 'fildisi' ),
						'default' => '',
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_right_enabled', 'equals', '1' ),
						),
					),
					array(
						'id'=>'top_bar_right_type_form',
						'type' => 'button_set',
						'title' => esc_html__( 'Right Form Type', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the type of your form.', 'fildisi' ),
						'options' => array(
							'contact-form' => esc_html__( 'Contact Form 7', 'fildisi' ),
							'gravity-form' => esc_html__( 'Gravity Form', 'fildisi' ),
						),
						'default' => 'contact-form',
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_right_enabled', 'equals', '1' ),
						),
					),
					array(
						'id' => 'top_bar_right_form',
						'type' => 'select',
						'title' => esc_html__( 'Right Area Form', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the form for your Contact Form.', 'fildisi' ),
						'data' => 'posts',
						'args' => array( 'post_type' => 'wpcf7_contact_form', 'numberposts' => -1 ),
						'default' => '',
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_right_enabled', 'equals', '1' ),
							array( 'top_bar_right_type_form', 'equals', 'contact-form' ),
						),
					),
					array(
						'id' => 'top_bar_right_gravity_form',
						'type' => 'select',
						'title' => esc_html__( 'Right Area Form', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the form for your Gravity Form.', 'fildisi' ),
						'options' => $fildisi_eutf_gravity_options,
						'default' => '',
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_right_enabled', 'equals', '1' ),
							array( 'top_bar_right_type_form', 'equals', 'gravity-form' ),
						),
					),
					array(
						'id' => 'top_bar_right_social_options',
						'type' => 'checkbox',
						'title' => esc_html__( 'Right Area Social Icons', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your social icons. Social URLs are configured from Theme Options - Social Media', 'fildisi' ),
						'class' => 'eut-redux-columns',
						'label' => true,
						'options' => $fildisi_eutf_social_options,
						'required' => array(
							array( 'top_bar_enabled', 'equals', '1' ),
							array( 'top_bar_right_enabled', 'equals', '1' ),
						),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Header Options', 'fildisi' ),
				'id' => 'eut_redux_section_header_options',
				'header' => '',
				'desc' => esc_html__( 'Here you can set your preferences for the Theme Header(Logo, Menu and Menu Elements). Notice that most of the options below depend on your first selection where you have to select among Default, Logo on Top or Side Navigation Header.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-screen',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id'=>'header_mode',
						'type' => 'image_select',
						'title' => esc_html__( 'Header Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your Header Mode. keep in mind that this is one of the most important options for the appearance of your site. Your selection here will determine the following section options.', 'fildisi' ),
						'options' => $fildisi_eutf_header_mode_selection,
						'default' => 'default',
					),
					array(
						'id'=>'header_menu_mode',
						'type' => 'image_select',
						'title' => esc_html__( 'Main Menu Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your main menu mode. Select between default and split menu', 'fildisi' ),
						'options' => $fildisi_eutf_header_menu_mode_selection,
						'default' => 'default',
						'required' => array( 'header_mode', 'equals', 'default' ),
					),
					array(
						'id'=>'split_menu_item_position',
						'type' => 'button_set',
						'title' => esc_html__( 'Extra Menu Item Position ', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select where you want to place the extra menu item in case of even menu items.', 'fildisi' ),
						'options' => array(
							'left' => esc_html__( 'Left', 'fildisi' ),
							'right' => esc_html__( 'Right', 'fildisi' ),
						),
						'default' => 'left',
						'required' => array(
							array( 'header_mode', 'equals', 'default' ),
							array( 'header_menu_mode', 'equals', 'split' ),
						),
					),
					array(
						'id' => 'header_height',
						'type' => 'text',
						'default' => '90',
						'title' => esc_html__( 'Header Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Header height in px (Default is 90).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'header_mode', 'equals', 'default' ),
					),
					array(
						'id' => 'header_side_width',
						'type' => 'text',
						'default' => '300',
						'title' => esc_html__( 'Header Width', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Header Width in px (Default is 300).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'header_mode', 'equals', 'side' ),
					),
					array(
						'id'             => 'header_side_spacing',
						'type'           => 'spacing',
						'mode'           => 'padding',
						'units'          => 'px',
						'units_extended' => 'false',
						'top'           => 'false',
						'bottom'          => 'false',
						'title'          => esc_html__( 'Spacing', 'fildisi' ),
						'subtitle'       => esc_html__( 'Set the spacings for the content of the Header.', 'fildisi' ),
						'desc'           => esc_html__( 'Set spacing Right, Left in px.', 'fildisi'),
						'default'        => array(
							'padding-left'     => '30px',
							'padding-right'  => '30px',
							'units'           => 'px',
						),
						'required' => array( 'header_mode', 'equals', 'side' ),
					),
					array(
						'id'       => 'header_side_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Header Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode_limited,
						'default'  => 'color',
						'required' => array( 'header_mode', 'equals', 'side' ),
					),
					array(
						'id'       => 'header_side_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the header.', 'fildisi' ),
						'required' => array(
							array( 'header_mode', 'equals', 'side' ),
							array( 'header_side_bg_mode', 'equals', 'custom' ),
						),
					),
					array(
						'id' => 'header_side_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array(
							array( 'header_mode', 'equals', 'side' ),
							array( 'header_side_bg_mode', 'equals', 'custom' ),
						),
					),
					array(
						'id' => 'header_side_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array(
							array( 'header_mode', 'equals', 'side' ),
							array( 'header_side_bg_mode', 'equals', 'custom' ),
						),
					),
					array(
						'id' => 'header_side_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array(
							array( 'header_mode', 'equals', 'side' ),
							array( 'header_side_bg_mode', 'equals', 'custom' ),
						),
					),
					array(
						'id' => 'header_side_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array(
							array( 'header_mode', 'equals', 'side' ),
							array( 'header_side_bg_mode', 'equals', 'custom' ),
						),
					),
					array(
						'id' => 'header_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Full Width', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable/Disable full width Header.', 'fildisi' ),
						'default' => 1,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'header_mode', '!=', 'side' ),
					),
					array(
						'id'   => 'info_logo_default_options',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Logo Options for the Default Header', 'fildisi' ),
						'required' => array( 'header_mode', 'equals', 'default' ),
					),
					array(
						'id' => 'logo_height',
						'type' => 'text',
						'default' => '50',
						'title' => esc_html__( 'Logo Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Logo height in px (Default is 50).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'header_mode', 'equals', 'default' ),
					),
					array(
						'id'   => 'info_logo_ontop_options',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Logo Options for the Logo on Top Header', 'fildisi' ),
						'required' => array( 'header_mode', 'equals', 'logo-top' ),
					),
					array(
						'id' => 'header_top_logo_height',
						'type' => 'text',
						'default' => '70',
						'title' => esc_html__( 'Logo Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Logo height in px (Default is 30).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'header_mode', 'equals', 'logo-top' ),
					),
					array(
						'id' => 'header_top_height',
						'type' => 'text',
						'default' => '180',
						'title' => esc_html__( 'Logo Area Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Logo Area Height in px (Default is 120).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'header_mode', 'equals', 'logo-top' ),
					),
					array(
						'id' => 'header_top_logo_align',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Logo Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Align your Logo as you wish.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
						'required' => array( 'header_mode', 'equals', 'logo-top' ),
					),
					array(
						'id'   => 'info_logo_side_options',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Logo Options for the Side Header', 'fildisi' ),
						'required' => array( 'header_mode', 'equals', 'side' ),
					),
					array(
						'id' => 'header_side_logo_height',
						'type' => 'text',
						'default' => '90',
						'title' => esc_html__( 'Logo Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Logo Height in px (Default is 90).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'header_mode', 'equals', 'side' ),
					),
					array(
						'id' => 'header_side_logo_align',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Logo Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Align your Logo as you wish.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
						'required' => array( 'header_mode', 'equals', 'side' ),
					),
					array(
						'id'             => 'header_side_logo_spacing',
						'type'           => 'spacing',
						'mode'           => 'padding',
						'units'          => array('em', 'px', '%'),
						'units_extended' => 'false',
						'left'           => 'false',
						'right'          => 'false',
						'title'          => esc_html__( 'Spacing', 'fildisi' ),
						'subtitle'       => esc_html__( 'Set the spacings of the Logo.', 'fildisi' ),
						'desc'           => esc_html__( 'Set spacing Top, Bottom in px.', 'fildisi'),
						'default'        => array(
							'padding-top'     => '100px',
							'padding-bottom'  => '100px',
							'units'           => 'px',
						),
						'required' => array( 'header_mode', 'equals', 'side' ),
					),
					array(
						'id'   => 'info_menu_options',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Menu Options', 'fildisi' ),
					),
					array(
						'id' => 'menu_type',
						'type' => 'select',
						'title' => esc_html__( 'Menu Type', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the type of the Main Menu.', 'fildisi' ),
						'options' => array(
							'classic' => esc_html__( 'Classic', 'fildisi' ),
							'button' => esc_html__( 'Button Style', 'fildisi' ),
							'underline' => esc_html__( 'Underline', 'fildisi' ),
							'hidden' => esc_html__( 'Hidden', 'fildisi' ),
							'advanced-hidden' => esc_html__( 'Advanced Hidden', 'fildisi' ),
						),
						'default' => 'classic',
						'validate' => 'not_empty',
						'required' => array( 'header_mode', 'equals', 'default' ),
					),
					array(
						'id' => 'menu_align',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Menu Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Align your Menu as you wish.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'right',
						'validate' => 'not_empty',
						'required' => array(
							array( 'header_mode', 'equals', 'default' ),
							array( 'header_menu_mode', 'equals', 'default' ),
							array( 'menu_type', '!=', 'hidden' ),
						),
					),
					array(
						'id' => 'header_bottom_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Menu Area Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Menu Area Height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'header_mode', 'equals', 'logo-top' ),
					),
					array(
						'id' => 'header_top_logo_menu_type',
						'type' => 'select',
						'title' => esc_html__( 'Menu Type', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the type of the Main Menu.', 'fildisi' ),
						'options' => array(
							'classic' => esc_html__( 'Classic', 'fildisi' ),
							'button' => esc_html__( 'Button Style', 'fildisi' ),
							'underline' => esc_html__( 'Underline', 'fildisi' ),
							'hidden' => esc_html__( 'Hidden', 'fildisi' ),
						),
						'default' => 'classic',
						'validate' => 'not_empty',
						'required' => array( 'header_mode', 'equals', 'logo-top' ),
					),
					array(
						'id' => 'header_top_menu_align',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Menu Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Align your Menu as you wish.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
						'required' => array(
							array( 'header_mode', 'equals', 'logo-top' ),
							array( 'header_top_logo_menu_type', '!=', 'hidden' ),
						),
					),
					array(
						'id' => 'submenu_pointer',
						'type' => 'select',
						'title' => esc_html__( 'Sub Menu Pointer', 'fildisi' ),
						'subtitle'=> esc_html__( 'Choose pointer for the submenu.', 'fildisi' ),
						'desc' => esc_html__( "Note: This setting will not affect hidden menu.", 'fildisi' ),
						'options' => $fildisi_eutf_menu_pointers,
						'default' => 'none',
						'validate' => 'not_empty',
						'required' => array( 'header_mode', '!=', 'side' ),
					),
					array(
						'id' => 'submenu_top_position',
						'type' => 'text',
						'default' => '0',
						'title' => esc_html__( 'Sub Menu Position', 'fildisi' ),
						'subtitle' => esc_html__( 'Define the distance between the top position of the Submenu and the bottom of the Header in px. (Default is 0 - Submenu starts at the bottom of the Header).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'header_mode', '!=', 'side' ),
					),
					array(
						'id' => 'header_menu_open_type',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Menu Type', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your menu type.', 'fildisi' ),
						'options' => array(
							'toggle' => esc_html__( 'Toggle', 'fildisi' ),
							'slide' => esc_html__( 'Slide', 'fildisi' ),
						),
						'default' => 'toggle',
						'validate' => 'not_empty',
						'required' => array( 'header_mode', 'equals', 'side' ),
					),
					array(
						'id' => 'header_side_menu_align',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Menu Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Align your Menu as you wish.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
						'required' => array(
							array( 'header_mode', 'equals', 'side' ),
						),

					),
					array(
						'id'   => 'info_menu_elements',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Menu Elements Options', 'fildisi' ),
					),
					array(
						'id'=>'header_menu_options_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Menu Elements', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or disable the use of various elements in your header like socials, search, language selector and Contact Form.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'header_menu_options',
						'type' => 'sortable',
						'mode' => 'checkbox',
						'title' => esc_html__( 'Menu Elements Options', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable / Disable various menu elements options.', 'fildisi' ),
						'options' => $fildisi_eutf_header_menu_options,
						'default' => $fildisi_eutf_header_menu_options_default,
						'required' => array( 'header_menu_options_enabled', 'equals', '1' ),
					),
					array(
						'id'=>'header_menu_type_form',
						'type' => 'button_set',
						'title' => esc_html__( 'Menu Form Type', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the type of your form.', 'fildisi' ),
						'options' => array(
							'contact-form' => esc_html__( 'Contact Form 7', 'fildisi' ),
							'gravity-form' => esc_html__( 'Gravity Form', 'fildisi' ),
						),
						'default' => 'contact-form',
						'required' => array( 'header_menu_options_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'header_menu_form',
						'type' => 'select',
						'title' => esc_html__( 'Menu Form', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the form for your Contact Form.', 'fildisi' ),
						'data' => 'posts',
						'args' => array( 'post_type' => 'wpcf7_contact_form', 'numberposts' => -1 ),
						'default' => '',
						'required' => array(
							array( 'header_menu_options_enabled', 'equals', '1' ),
							array( 'header_menu_type_form', 'equals', 'contact-form' ),
						),
					),
					array(
						'id' => 'header_menu_gravity_form',
						'type' => 'select',
						'title' => esc_html__( 'Menu Form', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the form for your Gravity Form.', 'fildisi' ),
						'options' => $fildisi_eutf_gravity_options,
						'default' => '',
						'required' => array(
							array( 'header_menu_options_enabled', 'equals', '1' ),
							array( 'header_menu_type_form', 'equals', 'gravity-form' ),
						),
					),
					array(
						'id'=>'header_menu_social_mode',
						'type' => 'select',
						'title' => esc_html__( 'Menu Social Icons Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select how you want to display your social icons single or modal.', 'fildisi' ),
						'options' => array(
							'modal' => esc_html__( 'Modal Popup', 'fildisi' ),
							'single' => esc_html__( 'Single Social Icons', 'fildisi' ),
						),
						"default" => 'modal',
						'required' => array( 'header_menu_options_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'header_menu_social_options',
						'type' => 'checkbox',
						'title' => esc_html__( 'Menu Social Icons', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the social icons.', 'fildisi' ),
						'class' => 'eut-redux-columns',
						'desc' => '',
						'label' => true,
						'options' => $fildisi_eutf_social_options,
						'required' => array( 'header_menu_options_enabled', 'equals', '1' ),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Sticky Header Options', 'fildisi' ),
				'id' => 'eut_redux_section_sticky_header_options',
				'header' => '',
				'desc' => esc_html__( 'You can enable the Sticky Header option if you want to provide constant access to the Header elements while visitors scroll down the page. These settings will not affect if you select the Side Header Mode in Theme Options > Header Options.', 'fildisi' ),
				'submenu' => true,
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-screen',
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'=>'header_sticky_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Sticky Header', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable the Sticky Header option.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array(
							array( 'header_mode', '!=', 'side' ),
						),
					),
					array(
						'id' => 'header_sticky_type',
						'type' => 'select',
						'title' => esc_html__( 'Sticky Header Type', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the type of the Sticky Header.', 'fildisi' ),
						'options' => array(
							'simple' => esc_html__( 'Simple', 'fildisi' ),
							'shrink' => esc_html__( 'Shrink', 'fildisi' ),
							'advanced' => esc_html__( 'Scroll Up', 'fildisi' ),
						),
						'default' => 'simple',
						'validate' => 'not_empty',
						'required' => array( 'header_sticky_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'header_sticky_shrink_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Header Shrink Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the shrink height for the Header in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array(
							array( 'header_sticky_enabled', 'equals', '1' ),
							array( 'header_sticky_type', '!=', 'simple' ),
						),
					),
					array(
						'id' => 'header_sticky_shrink_logo_height',
						'type' => 'text',
						'default' => '40',
						'title' => esc_html__( 'Logo Shrink Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the shrink height for the Logo in px (Default is 40).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array(
							array( 'header_sticky_enabled', 'equals', '1' ),
							array( 'header_sticky_type', '!=', 'simple' ),
						),
					),
					array(
						'id'=>'header_sticky_devices_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Devices Sticky Header', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable the Sticky Header on responsive header mode.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Responsive Header Options / Hidden Menu Options', 'fildisi' ),
				'id' => 'eut_redux_section_responsive_header_options',
				'header' => '',
				'desc' => esc_html__( 'Define your preferences for the responsive header.', 'fildisi' ),
				'submenu' => true,
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-screen',
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'responsive_header_threshold',
						'type' => 'text',
						'default' => '1024',
						'title' => esc_html__( 'Responsive Header Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the screensize you want to activate the responsive header in px, based on the menu items you have (Default is 1024).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'responsive_header_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Responsive Header Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Header height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'responsive_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Responsive Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Responsive Header overlaps the content.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_responsive_logo_options',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Logo Options for the Responsive Header', 'fildisi' ),
					),
					array(
						'id' => 'responsive_logo_height',
						'type' => 'text',
						'default' => '25',
						'title' => esc_html__( 'Responsive Logo Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Logo height in px (Default is 25).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id'   => 'info_responsive_menu_options',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Responsive / Hidden Menu Options', 'fildisi' ),
					),
					array(
						'id' => 'menu_responsive_toggle_selection',
						'type' => 'select',
						'title' => esc_html__( 'Responsive Menu Toggle Button Selection', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the toggle button content for your responsive menu.', 'fildisi' ),
						'options' => $fildisi_eutf_menu_responsibe_toggle_selection,
						'default' => 'icon',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'menu_responsive_toggle_text',
						'type' => 'text',
						'title' => esc_html__( 'Responsive Menu Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the text for your responsive menu.', 'fildisi' ),
						'default' => 'Menu',
						'required' => array( 'menu_responsive_toggle_selection', 'equals', 'text' ),
					),
					array(
						'id' => 'menu_responsive_open_type',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Responsive / Hidden Menu Type', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your responsive menu type.', 'fildisi' ),
						'options' => array(
							'toggle' => esc_html__( 'Toggle', 'fildisi' ),
							'slide' => esc_html__( 'Slide', 'fildisi' ),
						),
						'default' => 'slide',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'menu_responsive_align',
						'type' => 'select',
						'title' => esc_html__( 'Responsive Menu Align', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your responsive menu align.', 'fildisi' ),
						'options' => array(
							'left' => esc_html__( 'Left', 'fildisi' ),
							'center' => esc_html__( 'Center', 'fildisi' ),
						),
						'default' => 'left',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'menu_responsive_width',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Responsive / Hidden Menu Width', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the width for your hidden menu.', 'fildisi' ),
						'options' => array(
							'small' => esc_html__( 'Small', 'fildisi' ),
							'medium' => esc_html__( 'Medium', 'fildisi' ),
							'large' => esc_html__( 'Large', 'fildisi' ),
						),
						'default' => 'small',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'menu_responsive_text',
						'type' => 'editor',
						'args' => array ( 'wpautop' => false ),
						'title' => esc_html__( 'Responsive / Hidden Menu Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Type your text or anything else you want.', 'fildisi' ),
						'default' => '',
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-arrow-down',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Bottom / Footer Areas', 'fildisi' ),
				'id' => 'eut_redux_section_footer_options',
				'desc' => esc_html__( 'These areas appear at the bottom of your pages. Define your preferences.', 'fildisi' ),
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id'=>'sticky_footer',
						'type' => 'switch',
						'title' => esc_html__( 'Sticky Areas', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to make sticky these areas.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'bottom_bar_area',
						'type' => 'select',
						'title' => esc_html__( 'Bottom Bar Area Item', 'fildisi' ),
						'subtitle' => esc_html__( 'Select an area item for your Bottom Bar Area.', 'fildisi' ),
						'data' => 'posts',
						'args' => array( 'post_type' => 'area-item', 'numberposts' => -1 ),
						'default' => '',
					),
					array(
						'id'   => 'info_footer_widgets',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Footer Widgets Settings', 'fildisi' ),
					),
					array(
						'id'       => 'footer_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Footer Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode_limited,
						'default'  => 'color',
					),
					array(
						'id'       => 'footer_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the footer.', 'fildisi' ),
						'required' => array( 'footer_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'footer_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'footer_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'footer_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array( 'footer_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'footer_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array( 'footer_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'footer_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array( 'footer_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id'=>'footer_widgets_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Footer Widgets Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable the Footer Area to show the widget areas of the footer.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'footer_widgets_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Footer Column Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the Footer column layout.', 'fildisi' ),
						'options' => $fildisi_eutf_footer_column_selection,
						'default' => 'footer-1',
						'required' => array( 'footer_widgets_visibility', 'equals', '1' ),
					),
					array(
						'id' => 'footer_section_type',
						'type' => 'select',
						'title' => esc_html__( 'Footer Full Width', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you prefer a full-width Footer Area.', 'fildisi' ),
						'options' => array(
							'fullwidth-background' => esc_html__( 'No', 'fildisi' ),
							'fullwidth-element' => esc_html__( 'Yes', 'fildisi' ),
						),
						'default' => 'fullwidth-background',
						'validate' => 'not_empty',
						'required' => array( 'footer_widgets_visibility', 'equals', '1' ),
					),
					array(
						'id' => 'footer_padding_top_multiplier',
						'type' => 'select',
						'title' => esc_html__( 'Footer Top Padding', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the space above the footer area.', 'fildisi' ),
						'options' => $fildisi_eutf_padding_selection,
						'default' => '3x',
						'validate' => 'not_empty',
						'required' => array( 'footer_widgets_visibility', 'equals', '1' ),
					),
					array(
						'id' => 'footer_padding_bottom_multiplier',
						'type' => 'select',
						'title' => esc_html__( 'Footer Bottom Padding', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the space below the footer area.', 'fildisi' ),
						'options' => $fildisi_eutf_padding_selection,
						'default' => '3x',
						'validate' => 'not_empty',
						'required' => array( 'footer_widgets_visibility', 'equals', '1' ),
					),
					array(
						'id'   => 'info_footer_bar',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Footer Bar Settings', 'fildisi' ),
					),
					array(
						'id'=>'footer_bar_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Footer Bar Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable the Footer Bar Area to add text (copyright), bottom menu and socials.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'footer_bar_section_type',
						'type' => 'select',
						'title' => esc_html__( 'Footer Bar Full Width', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you prefer a full-width Footer Bar Area.', 'fildisi' ),
						'options' => array(
							'fullwidth-background' => esc_html__( 'No', 'fildisi' ),
							'fullwidth-element' => esc_html__( 'Yes', 'fildisi' ),
						),
						'default' => 'fullwidth-background',
						'validate' => 'not_empty',
						'required' => array( 'footer_bar_visibility', 'equals', '1' ),
					),
					array(
						'id' => 'footer_bar_align_center',
						'type' => 'select',
						'title' => esc_html__( 'Footer Bar Center', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if the Footer Bar elements will be centered.', 'fildisi' ),
						'options' => array(
							'no' => esc_html__( 'No', 'fildisi' ),
							'yes' => esc_html__( 'Yes', 'fildisi' ),
						),
						'default' => 'yes',
						'validate' => 'not_empty',
						'required' => array( 'footer_bar_visibility', 'equals', '1' ),
					),
					array(
						'id'=>'footer_copyright_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Footer Copyright Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable the Footer Copyright Area. Edit it as you wish.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'footer_bar_visibility', 'equals', '1' ),
					),
					array(
						'id' => 'footer_copyright_text',
						'type' => 'editor',
						'args' => array ( 'wpautop' => false ),
						'title' => esc_html__( 'Copyright Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Type your copyright text or anything else you want.', 'fildisi' ),
						'default' => '<p>With <i class="eut-text-primary-1 fa fa-heart"></i> by <a href="//themeforest.net/user/euthemians/portfolio" target="_blank" rel="noopener noreferrer">Euthemians</a> - Premium Themes</p>',
						'required' => array(
							array( 'footer_bar_visibility', 'equals', '1' ),
							array( 'footer_copyright_visibility', 'equals', '1' ),
						),
					),
					array(
						'id'=>'second_area_visibility',
						'type' => 'button_set',
						'title' => esc_html__( 'Second Footer Area', 'fildisi' ),
						'subtitle'=> esc_html__( 'This is the second position in the Footer Bar Area. You can easily add the Bottom Menu (assigned in Appearance > Menus) or socials.', 'fildisi' ),
						'options' => array(
							'1' => esc_html__( 'Hide', 'fildisi' ),
							'2' => esc_html__( 'Menu', 'fildisi' ),
							'3' => esc_html__( 'Socials', 'fildisi' ),
							'4' => esc_html__( 'Text', 'fildisi' ),
						),
						'default' => '1',
						'required' => array( 'footer_bar_visibility', 'equals', '1' ),
					),
					array(
						'id' => 'footer_second_copyright_text',
						'type' => 'editor',
						'args' => array ( 'wpautop' => false ),
						'title' => esc_html__( 'Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Type your text or anything else you want.', 'fildisi' ),
						'default' => '',
						'required' => array(
							array( 'footer_bar_visibility', 'equals', '1' ),
							array( 'second_area_visibility', 'equals', '4' ),
						),
					),
					array(
						'id' => 'footer_social_display',
						'type' => 'select',
						'title' => esc_html__( 'Footer Social Display', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select how you want to display your social items.', 'fildisi' ),
						'options' => array(
							'text' => esc_html__( 'Text', 'fildisi' ),
							'icon' => esc_html__( 'Icons', 'fildisi' ),
						),
						'default' => 'text',
						'validate' => 'not_empty',
						'required' => array(
							array( 'footer_bar_visibility', 'equals', '1' ),
							array( 'second_area_visibility', 'equals', '3' ),
						),
					),
					array(
						'id' => 'footer_social_options',
						'type' => 'checkbox',
						'title' => esc_html__( 'Footer Social items', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your social icons.', 'fildisi' ),
						'desc' => '',
						'class' => 'eut-redux-columns',
						'label' => true,
						'options' => $fildisi_eutf_social_options,
						'required' => array(
							array( 'footer_bar_visibility', 'equals', '1' ),
							array( 'second_area_visibility', 'equals', '3' ),
						),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-edit',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Blog Options', 'fildisi' ),
				'id' => 'eut_redux_section_blog_options',
				'desc' => esc_html__( 'Changes here will affect the assigned blog page in case you have set a Static Page as blog page in Settings > Reading > Front Page Displays and not when you use the shortcodes to create a blog page. Also these settings will also affect on Archives / Categories / Tags overview pages.', 'fildisi' ),
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'blog_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Blog Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the layout for the assigned blog page. Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'right',
					),
					array(
						'id' => 'blog_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Blog Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar for the assigned blog page.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'blog_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Blog Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'blog_sidearea_visibility',
						'type' => 'select',
						'title' => esc_html__( 'Blog Sliding Area Visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable the Blog Sliding Area for the assigned blog page.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'blog_sidearea_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Blog Sliding Area Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar widget area for the sliding area.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id'   => 'info_style_blog_settings',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Blog Style and Basic Blog Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style and basic settings for the default blog.', 'fildisi' ),
					),
					array(
						'id' => 'blog_mode',
						'type' => 'select',
						'title' => esc_html__( 'Blog Mode', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the blog mode', 'fildisi' ),
						'options' => $fildisi_eutf_blog_mode_selection,
						'default' => 'large',
					),
					array(
						'id' => 'blog_shadow_style',
						'type' => 'select',
						'title' => esc_html__( 'Blog Style', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the blog style', 'fildisi' ),
						'options' =>array(
							'shadow-mode' => esc_html__( 'With Shadow', 'fildisi' ),
							'no-shadow-mode' => esc_html__( 'Without Shadow', 'fildisi' ),
						),
						'default' => 'shadow-mode',
						'required' => array(
							array( 'blog_mode','!=', 'large' ),
							array( 'blog_mode','!=', 'small' ),
						),
					),
					array(
						'id' => 'blog_media_area',
						'type' => 'switch',
						'title' => esc_html__( 'Blog Media Area visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Select if you want to enable media area', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'blog_image_mode',
						'type' => 'select',
						'title' => esc_html__( 'Blog Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your Blog Image Size', 'fildisi' ),
						'options' => $fildisi_eutf_blog_image_mode_selection,
						'default' => 'landscape-large-wide',
						'required' => array(
							array( 'blog_mode','equals', 'large' ),
							array( 'blog_media_area', 'equals', '1' ),
						),
					),
					array(
						'id' => 'blog_grid_image_mode',
						'type' => 'select',
						'title' => esc_html__( 'Blog Grid Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your Blog Image Size', 'fildisi' ),
						'options' => $fildisi_eutf_blog_grid_image_mode_selection,
						'default' => 'landscape',
						'required' => array(
							array( 'blog_mode','!=', 'large' ),
							array( 'blog_mode','!=', 'masonry' ),
							array( 'blog_media_area', 'equals', '1' ),
						),
					),
					array(
						'id' => 'blog_masonry_image_mode',
						'type' => 'select',
						'title' => esc_html__( 'Blog Masonry_Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your Blog Masonry Image Size', 'fildisi' ),
						'options' => $fildisi_eutf_blog_masonry_image_mode_selection,
						'default' => 'medium',
						'required' => array(
							array( 'blog_mode','equals', 'masonry' ),
							array( 'blog_media_area', 'equals', '1' ),
						),
					),
					array(
						'id' => 'blog_image_prio',
						'type' => 'select',
						'title' => esc_html__( 'Blog Featured Image Priority', 'fildisi' ),
						'subtitle' => esc_html__( 'If enabled, Featured image is displayed instead of media element', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
						'required' => array( 'blog_media_area', 'equals', '1' ),
					),
					array(
						'id' => 'blog_columns_large_screen',
						'type' => 'select',
						'title' => esc_html__( 'Blog Columns Large Screens', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the Blog Columns for large screens', 'fildisi' ),
						'options' => $fildisi_eutf_blog_columns_selection,
						'default' => '3',
						'required' => array(
							array( 'blog_mode','!=', 'large' ),
							array( 'blog_mode','!=', 'small' ),
						),
					),
					array(
						'id' => 'blog_columns',
						'type' => 'select',
						'title' => esc_html__( 'Blog Columns', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the Blog Columns', 'fildisi' ),
						'options' => $fildisi_eutf_blog_columns_selection,
						'default' => '3',
						'required' => array(
							array( 'blog_mode','!=', 'large' ),
							array( 'blog_mode','!=', 'small' ),
						),
					),
					array(
						'id' => 'blog_columns_tablet_landscape',
						'type' => 'select',
						'title' => esc_html__( 'Blog Tablet Landscape Columns', 'fildisi' ),
						'subtitle' => esc_html__( 'Select responsive column on tablet devices, landscape orientation.', 'fildisi' ),
						'options' => $fildisi_eutf_blog_columns_selection_tablet,
						'default' => '2',
						'required' => array(
							array( 'blog_mode','!=', 'large' ),
							array( 'blog_mode','!=', 'small' ),
						),
					),
					array(
						'id' => 'blog_columns_tablet_portrait',
						'type' => 'select',
						'title' => esc_html__( 'Blog Tablet Portrait Columns', 'fildisi' ),
						'subtitle' => esc_html__( 'Select responsive column on tablet devices, portrait orientation.', 'fildisi' ),
						'options' => $fildisi_eutf_blog_columns_selection_tablet,
						'default' => '2',
						'required' => array(
							array( 'blog_mode','!=', 'large' ),
							array( 'blog_mode','!=', 'small' ),
						),
					),
					array(
						'id' => 'blog_columns_mobile',
						'type' => 'select',
						'title' => esc_html__( 'Blog Mobile Columns', 'fildisi' ),
						'subtitle' => esc_html__( 'Select responsive column on mobile devices.', 'fildisi' ),
						'options' => $fildisi_eutf_blog_columns_selection_mobile,
						'default' => '1',
						'required' => array(
							array( 'blog_mode','!=', 'large' ),
							array( 'blog_mode','!=', 'small' ),
						),
					),
					array(
						'id' => 'blog_gutter',
						"type" => "select",
						"title" => esc_html__( "Gutter between items", "fildisi" ),
						'subtitle' => esc_html__( 'Add gutter among items.', 'fildisi' ),
						"options" => array(
							'yes' => esc_html__( "Yes", "fildisi" ),
							'no' => esc_html__( "No", "fildisi" ),
						),
						'default' => 'yes',
						'required' => array(
							array( 'blog_mode','!=', 'large' ),
							array( 'blog_mode','!=', 'small' ),
						),
					),
					array(
						'id'=>'blog_gutter_size',
						'type' => 'text',
						'default' => '30',
						'title' => esc_html__( 'Gutter Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Define the gutter size.', 'fildisi' ),
						'required' => array(
							array( 'blog_mode','!=', 'large' ),
							array( 'blog_mode','!=', 'small' ),
							array( 'blog_gutter','equals', 'yes' ),
						),
					),
					array(
						"id" => "blog_animation",
						"type" => "select",
						"title" => esc_html__( "Blog CSS Animation", "fildisi"),
						"subtitle" => esc_html__("Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", "fildisi" ),
						"options" => array(
							'none' => esc_html__( "No", "fildisi" ),
							"eut-fade-in" => esc_html__( "Fade In", "fildisi" ),
							"eut-fade-in-up" => esc_html__( "Fade In Up", "fildisi" ),
							"eut-fade-in-down" => esc_html__( "Fade In Down", "fildisi" ),
							"eut-fade-in-left" => esc_html__( "Fade In Left", "fildisi" ),
							"eut-fade-in-right" => esc_html__( "Fade In Right", "fildisi" ),
							"eut-zoom-in" => esc_html__( "Zoom In", "fildisi" ),
						),
						'default' => 'none',
					),
					array(
						'id' => 'blog_heading_tag',
						'type' => 'select',
						'title' => esc_html__( 'Blog Title Tag', 'fildisi' ),
						'subtitle' => esc_html__( 'Select tag for your blog title.', 'fildisi' ),
						'options' => $fildisi_eutf_blog_headings_tag_selection,
						'default' => 'auto',
					),
					array(
						'id' => 'blog_heading',
						'type' => 'select',
						'title' => esc_html__( 'Blog Title Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select size and typography for your blog title.', 'fildisi' ),
						'options' => $fildisi_eutf_blog_headings_selection,
						'default' => 'auto',
					),
					array(
						'id' => 'blog_item_spinner',
						'type' => 'select',
						'title' => esc_html__( 'Blog Loader', 'fildisi' ),
						'subtitle' => esc_html__( 'If selected, this will enable a graphic spinner before load.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'=>'blog_excerpt_length_small',
						'type' => 'text',
						'default' => '30',
						'title' => esc_html__( 'Excerpt Length', 'fildisi' ),
						'subtitle' => esc_html__( 'Type how many words you want to display in your post excerpts (Default is 30).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'blog_mode', '!=', 'large' ),
					),
					array(
						'id' => 'blog_auto_excerpt',
						'type' => 'switch',
						'title' => esc_html__( 'Auto Excerpt', 'fildisi' ),
						'subtitle'=> esc_html__( "Adds automatic excerpt to all posts. If auto excerpt is off, blog will show all content, a desired 'cut-off' can be inserted in each post with more quicktag.", 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'blog_mode', 'equals', 'large' ),
					),

					array(
						'id'=>'blog_excerpt_length',
						'type' => 'text',
						'default' => '55',
						'title' => esc_html__( 'Excerpt Length', 'fildisi' ),
						'subtitle' => esc_html__( 'Type how many words you want to display in your post excerpts (Default is 55).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array(
							array( 'blog_auto_excerpt', 'equals', '1' ),
							array( 'blog_mode', 'equals', 'large' ),
						),
					),
					array(
						'id' => 'blog_excerpt_more',
						'type' => 'switch',
						'title' => esc_html__( 'Read More', 'fildisi' ),
						'subtitle'=> esc_html__( "Adds a read more button after the excerpt or more quicktag.", 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'=>'blog_comments_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Comments Field Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the comments field of your blog overview.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'=>'blog_author_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Author Field Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the author field of your blog overview.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'=>'blog_date_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Date Field Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the date field of your blog overview.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'=>'blog_like_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Like Field Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the like field of your blog overview.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'=>'blog_categories_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Categories Field Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the categories field of your blog overview.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'   => 'info_style_blog_header',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Blog Header Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default blog header.', 'fildisi' ),
					),
					array(
						'id' => 'blog_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Blog Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your blog header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
					),
					array(
						'id' => 'blog_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Blog Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Blog Header overlaps the content. Combine this option with the Light or Dark Header.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'=>'blog_title',
						'type' => 'select',
						'title' => esc_html__( 'Main Blog Title', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to use the site name and tagline as the main blog title or hide it.', 'fildisi' ),
						'options' => array(
							'sitetitle' => esc_html__( 'Site Title / Tagline', 'fildisi' ),
							'custom' => esc_html__( 'Custom Title / Description', 'fildisi' ),
							'none' => esc_html__( 'None', 'fildisi' ),
						),
						'default' => 'sitetitle',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'blog_custom_title', //translatable
						'type' => 'text',
						'default' => '',
						'title' => esc_html__( 'Custom Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter blog custom title.', 'fildisi' ),
						'required' => array( 'blog_title', 'equals', 'custom' ),
					),
					array(
						'id' => 'blog_custom_description', //translatable
						'type' => 'text',
						'default' => '',
						'title' => esc_html__( 'Custom Description', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter blog custom description.', 'fildisi' ),
						'required' => array( 'blog_title', 'equals', 'custom' ),
					),
					array(
						'id' => 'blog_title_height',
						'type' => 'select',
						'default' => '45',
						'title' => esc_html__( 'Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter blog title height (Default is 45%).', 'fildisi' ),
						'options' => $fildisi_eutf_area_height,
						'validate' => 'not_empty',
					),
					array(
						'id' => 'blog_title_min_height',
						'type' => 'text',
						'default' => '200',
						'title' => esc_html__( 'Minimum Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter blog title minimum height in px (Default is 200).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'blog_title_container_size',
						'type' => 'select',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the container.', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selection,
						'default' => 'default',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'blog_title_content_size',
						'type' => 'select',
						'title' => esc_html__( 'Content Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_content_size_selection,
						'default' => 'large',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'blog_title_content_alignment',
						'type' => 'select',
						'title' => esc_html__( 'Content Alignment', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the alignemt of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'blog_title_content_position',
						'type' => 'select',
						'title' => esc_html__( 'Content Position', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your position for the default blog title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'blog_title_content_animation',
						'type' => 'select',
						'title' => esc_html__( 'Content Animation', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your animation for the default blog title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_animation_selection,
						'default' => 'fade-in-up',
						'validate' => 'not_empty',
					),
					array(
						'id'       => 'blog_title_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Background for the blog page, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode_limited,
						'default'  => 'color',
					),
					array(
						'id'       => 'blog_title_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the default blog title.', 'fildisi' ),
						'required' => array( 'blog_title_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'blog_title_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'blog_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'blog_title_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array( 'blog_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'blog_title_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array( 'blog_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'blog_title_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array( 'blog_title_bg_mode', '!=', 'color' ),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Single Post Settings', 'fildisi' ),
				'id' => 'eut_redux_section_single_post_settings',
				'header' => '',
				'desc' => esc_html__( 'Define your preferences for the Single Posts. Notice that most of them can be overridden when you create a single post.', 'fildisi' ),
				'submenu' => true,
				'icon' => 'el-icon-edit',
				'icon_class' => 'el-icon-large',
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'post_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Post Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the layout for the Single Posts. Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'right',
					),
					array(
						'id' => 'post_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Post Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar for the Single Posts.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'post_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Post Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'post_content_width',
						'type' => 'select',
						'title' => esc_html__( 'Post Content Width', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the Single Post Content width (only for Full Width Post Layout)', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selector,
						'default' => '770',
					),
					array(
						'id' => 'post_sidearea_visibility',
						'type' => 'select',
						'title' => esc_html__( 'Post Sliding Area Visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable the Post Sliding Area if you want.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'post_sidearea_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Post Sliding Area Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar widget area for the sliding area.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'post_feature_image_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Featured Image Visibility (Standard Post)', 'fildisi' ),
						'subtitle'=> esc_html__( 'Toggle Featured Image on or off.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'post_tag_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Post Tags Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the post tags.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'post_category_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Post Categories Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the post categories.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'post_author_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Author Info Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the Author Info field.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'post_author_info_link_text',
						'type' => 'text',
						'title' => esc_html__( 'Author Info Link Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the text for the author info link.', 'fildisi' ),
						"default" => 'All stories by :',
						'required' => array( 'post_author_visibility', 'equals', '1' ),
					),
					array(
						'id'=>'post_comments_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Comments Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the comments of your single posts.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'   => 'info_style_post_header',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Post Header Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default post header.', 'fildisi' ),
					),
					array(
						'id' => 'post_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Post Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your post header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
					),
					array(
						'id' => 'post_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Post Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Post Header overlaps the content. Combine this option with the Light or Dark Header. You can still override this when you edit each single post.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_style_post_title',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Single Post Title Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default single post title.', 'fildisi' ),
					),
					array(
						'id' => 'post_title_style',
						'type' => 'select',
						'default' => 'advanced',
						'title' => esc_html__( 'Title Style', 'fildisi' ),
						'subtitle' => esc_html__( 'Select Post Title Style', 'fildisi' ),
						'options' => $fildisi_eutf_title_style,
						'validate' => 'not_empty',
					),
					array(
						'id' => 'post_title_height',
						'type' => 'select',
						'default' => '45',
						'title' => esc_html__( 'Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter post title height(Default is 45%).', 'fildisi' ),
						'options' => $fildisi_eutf_area_height,
						'validate' => 'not_empty',
						'required' => array( 'post_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'post_title_min_height',
						'type' => 'text',
						'default' => '200',
						'title' => esc_html__( 'Minimum Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter post title minimum height in px (Default is 200).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'post_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'post_title_container_size',
						'type' => 'select',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the container.', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selection,
						'default' => 'default',
						'validate' => 'not_empty',
						'required' => array( 'post_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'post_title_content_size',
						'type' => 'select',
						'title' => esc_html__( 'Content Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_content_size_selection,
						'default' => 'large',
						'validate' => 'not_empty',
						'required' => array( 'post_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'post_title_content_alignment',
						'type' => 'select',
						'title' => esc_html__( 'Content Alignment', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the alignemt of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
						'required' => array( 'post_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'post_title_content_position',
						'type' => 'select',
						'title' => esc_html__( 'Content Position', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your position for the default post title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'post_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'post_title_content_animation',
						'type' => 'select',
						'title' => esc_html__( 'Content Animation', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your animation for the default post title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_animation_selection,
						'default' => 'fade-in-up',
						'validate' => 'not_empty',
						'required' => array( 'post_title_style', 'equals', 'advanced' ),
					),
					array(
						'id'       => 'post_title_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Post Title Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode,
						'default'  => 'color',
						'required' => array( 'post_title_style', 'equals', 'advanced' ),
					),
					array(
						'id'       => 'post_title_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the default post title.', 'fildisi' ),
						'required' => array(
							array( 'post_title_style', 'equals', 'advanced' ),
							array( 'post_title_bg_mode', 'equals', 'custom' ),
						),
					),
					array(
						'id' => 'post_title_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array(
							array( 'post_title_style', 'equals', 'advanced' ),
							array( 'post_title_bg_mode', '!=', 'color' ),
						),
					),
					array(
						'id' => 'post_title_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array(
							array( 'post_title_style', 'equals', 'advanced' ),
							array( 'post_title_bg_mode', '!=', 'color' ),
						),
					),
					array(
						'id' => 'post_title_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array(
							array( 'post_title_style', 'equals', 'advanced' ),
							array( 'post_title_bg_mode', '!=', 'color' ),
						),
					),
					array(
						'id' => 'post_title_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array(
							array( 'post_title_style', 'equals', 'advanced' ),
							array( 'post_title_bg_mode', '!=', 'color' ),
						),
					),
					array(
						'id'   => 'info_style_post_anchor_menu',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Anchor Menu Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Anchor Menu Bar where you can place a custom sticky menu per post.', 'fildisi' ),
					),
					array(
						'id' => 'post_anchor_menu_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Anchor Menu Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Anchor Menu height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'post_anchor_menu_alignment',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Anchor Menu Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the alignment of your Anchor Menu in posts.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'left',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'post_anchor_menu_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Fullwidth', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to have full width Anchor Menu.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'   => 'info_style_post_breadcrumbs',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Breadcrumbs Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Breadcrumbs Bar.', 'fildisi' ),
					),
					array(
						'id'=>'post_breadcrumbs_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Breadcrumbs', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the Breadcrumbs for Single Post / Archives / Categories / Tags.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'post_breadcrumbs_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Breadcrumbs Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Breadcrumbs height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'post_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'post_breadcrumbs_alignment',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Breadcrumbs Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the alignment of your breadcrumbs.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'left',
						'validate' => 'not_empty',
						'required' => array( 'post_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'post_breadcrumbs_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Fullwidth', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show your breadcrumbs full width.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'post_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id'   => 'info_style_post_social',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Socials Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the social.', 'fildisi' ),
					),
					array(
						'id' => 'post_social',
						'type' => 'checkbox',
						'title' => esc_html__( 'Social Share', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable or Disable post social shares for the Single Posts.', 'fildisi' ),
						'options' => $fildisi_eutf_post_social_options,
						'default' => 0,
					),
					array(
						'id'   => 'info_style_post_related',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Related Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the related posts.', 'fildisi' ),
					),
					array(
						'id'=>'post_related_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Related Posts Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the visibility of the related posts.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'post_related_title_first',
						'type' => 'text',
						'title' => esc_html__( 'Related Posts First Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the text for the first posts related title.', 'fildisi' ),
						"default" => 'You might also like',
						'required' => array( 'post_related_visibility', 'equals', '1' ),
					),
					array(
						'id' => 'post_related_title_second',
						'type' => 'text',
						'title' => esc_html__( 'Related Posts Second Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the text for the second posts related title.', 'fildisi' ),
						"default" => '',
						'required' => array( 'post_related_visibility', 'equals', '1' ),
					),
					array(
						'id'   => 'info_style_post_nav',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Navigation Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Navigation.', 'fildisi' ),
					),
					array(
						'id'=>'post_nav_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Posts Navigation Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the visibility of the posts navigation.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'post_nav_same_term',
						'type' => 'checkbox',
						'title' => esc_html__( 'Post Navigation Same Term', 'fildisi' ),
						'subtitle'=> esc_html__( 'If selected, only navigation items from the current taxonomy term will be displayed.', 'fildisi' ),
						'default' => 0,
						'required' => array( 'post_nav_visibility', 'equals', '1' ),
					),
					array(
						'id' => 'post_backlink_id',
						'type' => 'select',
						'title' => esc_html__( 'Post Backlink', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the overview page for your post backlink.', 'fildisi' ),
						'data' => 'page',
						'default' => '',
						'required' => array( 'post_nav_visibility', 'equals', '1' ),
					),
					array(
						'id'             => 'post_nav_spacing',
						'type'           => 'spacing',
						'mode'           => 'padding',
						'units'          => 'px',
						'units_extended' => 'false',
						'left'           => 'false',
						'right'          => 'false',
						'title'          => esc_html__( 'Navigation Bar Spacing', 'fildisi' ),
						'subtitle'       => esc_html__( 'Set the spacing, Top and Bottom, of the Navigation Area.', 'fildisi' ),
						'desc'           => esc_html__( 'Set spacing Top, Bottom in px.', 'fildisi'),
						'default'        => array(
							'padding-top'     => '30px',
							'padding-bottom'  => '30px',
							'units'           => 'px',
						),
						'required' => array( 'post_nav_visibility', 'equals', '1' ),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-pencil',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Page Options', 'fildisi'),
				'id' => 'eut_redux_section_page_options',
				'subtitle' => esc_html__( 'You can find the basic settings for the pages here. Almost everything can be overridden when you edit a single page.', 'fildisi' ),
				'desc' => esc_html__( 'You can find the basic settings for the pages here. Almost everything can be overridden when you edit a single page.', 'fildisi' ),
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'page_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Page Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the layout for the pages. Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'none',
					),
					array(
						'id' => 'page_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Page Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default sidebar for the pages in case you do not use full width layout.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'page_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Page Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'page_sidearea_visibility',
						'type' => 'select',
						'title' => esc_html__( 'Page Sliding Area Visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable the Sliding Area for the pages.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'page_sidearea_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Page Sliding Area Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar for the sliding area.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id'=>'page_comments_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Comments Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Easily disable the comments for all pages.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'   => 'info_style_page_header',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Page Header Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default page header.', 'fildisi' ),
					),
					array(
						'id' => 'page_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Page Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your page header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
					),
					array(
						'id' => 'page_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Page Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Page Header overlaps the content. Combine this option with the Light or Dark Header. You can still override this when you edit each single page.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_style_page_title',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Page Title Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default page title.', 'fildisi' ),
					),
					array(
						'id' => 'page_title_height',
						'type' => 'select',
						'default' => '45',
						'title' => esc_html__( 'Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Page title height (Default is 45%).', 'fildisi' ),
						'options' => $fildisi_eutf_area_height,
						'validate' => 'not_empty',
					),
					array(
						'id' => 'page_title_min_height',
						'type' => 'text',
						'default' => '200',
						'title' => esc_html__( 'Minimum Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter page title minimum height in px (Default is 200).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'page_title_container_size',
						'type' => 'select',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the container.', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selection,
						'default' => 'default',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'page_title_content_size',
						'type' => 'select',
						'title' => esc_html__( 'Content Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_content_size_selection,
						'default' => 'large',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'page_title_content_alignment',
						'type' => 'select',
						'title' => esc_html__( 'Content Alignment', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the alignemt of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'page_title_content_position',
						'type' => 'select',
						'title' => esc_html__( 'Content Position', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your position for the default page title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'page_title_content_animation',
						'type' => 'select',
						'title' => esc_html__( 'Content Animation', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your animation for the default page title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_animation_selection,
						'default' => 'fade-in-up',
						'validate' => 'not_empty',
					),
					array(
						'id'       => 'page_title_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Page Title Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode,
						'default'  => 'color',
					),
					array(
						'id'       => 'page_title_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the default page title.', 'fildisi' ),
						'required' => array( 'page_title_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'page_title_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'page_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'page_title_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array( 'page_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'page_title_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array( 'page_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'page_title_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array( 'page_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id'   => 'info_style_page_anchor_menu',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Anchor Menu Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Anchor Menu Bar where you can place a custom sticky menu per page.', 'fildisi' ),
					),
					array(
						'id' => 'page_anchor_menu_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Anchor Menu Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Anchor Menu height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'page_anchor_menu_alignment',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Anchor Menu Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the alignment of your anchor menu.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'left',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'page_anchor_menu_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Fullwidth', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show your anchor menu full width.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'   => 'info_style_page_breadcrumbs',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Breadcrumbs Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Breadcrumbs Bar.', 'fildisi' ),
					),
					array(
						'id'=>'page_breadcrumbs_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Breadcrumbs', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the Breadcrumbs for Single Page.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'page_breadcrumbs_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Breadcrumbs Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Breadcrumbs height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'page_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'page_breadcrumbs_alignment',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Breadcrumbs Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the alignment of your breadcrumbs.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'left',
						'validate' => 'not_empty',
						'required' => array( 'page_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'page_breadcrumbs_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Fullwidth', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show your breadcrumbs full width.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'page_breadcrumbs_enabled', 'equals', '1' ),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-search',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Search Options', 'fildisi'),
				'id' => 'eut_redux_section_search_page_options',
				'subtitle' => esc_html__( 'You can find the basic settings for the search page here.', 'fildisi' ),
				'desc' => esc_html__( 'You can find the basic settings for the search page here.', 'fildisi' ),
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'search_page_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Search Page Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the layout for the search page. Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'none',
					),
					array(
						'id' => 'search_page_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default sidebar for the search page in case you do not use full width layout.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
					),
					array(
						'id' => 'search_page_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_style_search_page_header',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Search Page Header Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the search page header.', 'fildisi' ),
					),
					array(
						'id' => 'search_page_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Page Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your page header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
					),
					array(
						'id' => 'search_page_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Page Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Page Header overlaps the content. Combine this option with the Light or Dark Header.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_style_search_page_title',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Search Page Title Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the search page title.', 'fildisi' ),
					),
					array(
						'id' => 'search_page_title_height',
						'type' => 'select',
						'default' => '45',
						'title' => esc_html__( 'Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter page title height (Default is 45%).', 'fildisi' ),
						'options' => $fildisi_eutf_area_height,
						'validate' => 'not_empty',
					),
					array(
						'id' => 'search_page_title_min_height',
						'type' => 'text',
						'default' => '200',
						'title' => esc_html__( 'Minimum Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter page title minimum height in px (Default is 200).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'search_page_title_container_size',
						'type' => 'select',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the container.', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selection,
						'default' => 'default',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'search_page_title_content_size',
						'type' => 'select',
						'title' => esc_html__( 'Content Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_content_size_selection,
						'default' => 'large',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'search_page_title_content_alignment',
						'type' => 'select',
						'title' => esc_html__( 'Content Alignment', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the alignemt of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'search_page_title_content_position',
						'type' => 'select',
						'title' => esc_html__( 'Content Position', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your position for the search page title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'search_page_title_content_animation',
						'type' => 'select',
						'title' => esc_html__( 'Content Animation', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your animation for the search page title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_animation_selection,
						'default' => 'fade-in-up',
						'validate' => 'not_empty',
					),
					array(
						'id'       => 'search_page_title_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Page Title Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode_limited,
						'default'  => 'color',
					),
					array(
						'id'       => 'search_page_title_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the search page title.', 'fildisi' ),
						'required' => array( 'search_page_title_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'search_page_title_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'search_page_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'search_page_title_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array( 'search_page_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'search_page_title_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array( 'search_page_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'search_page_title_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array( 'search_page_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id'   => 'info_style_search_page_settings',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Search Page Style and Basic Search Page Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style and basic settings for the search page.', 'fildisi' ),
					),
					array(
						'id' => 'search_page_mode',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Mode', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the search page mode', 'fildisi' ),
						'options' => array(
							'masonry' => esc_html__( 'Masonry' , 'fildisi' ),
							'grid' => esc_html__( 'Grid' , 'fildisi' ),
							'small' => esc_html__( 'Small Media', 'fildisi' ),
						),
						'default' => 'masonry',
					),
					array(
						'id' => 'search_page_shadow_style',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Style', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the search style', 'fildisi' ),
						'options' =>array(
							'shadow-mode' => esc_html__( 'With Shadow', 'fildisi' ),
							'no-shadow-mode' => esc_html__( 'Without Shadow', 'fildisi' ),
						),
						'default' => 'shadow-mode',
						'required' => array( 'search_page_mode', '!=', 'small' ),
					),
					array(
						'id' => 'search_page_show_image',
						'type' => 'select',
						'title' => esc_html__( 'Show Search Page Featured Image', 'fildisi' ),
						'subtitle' => esc_html__( 'If enabled, Featured image is displayed', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'yes',
					),
					array(
						'id' => 'search_image_mode',
						'type' => 'select',
						'title' => esc_html__( 'Search Grid Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your Search Grid Image Size', 'fildisi' ),
						'options' => $fildisi_eutf_search_grid_image_mode_selection,
						'default' => 'landscape',
						'required' => array(
							array( 'search_page_mode', '!=', 'masonry' ),
							array( 'search_page_show_image', 'equals', 'yes' ),
						),
					),
					array(
						'id' => 'search_masonry_image_mode',
						'type' => 'select',
						'title' => esc_html__( 'Search Masonry_Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your Search Masonry Image Size', 'fildisi' ),
						'options' => $fildisi_eutf_search_masonry_image_mode_selection,
						'default' => 'medium',
						'required' => array(
							array( 'search_page_mode','equals', 'masonry' ),
							array( 'search_page_show_image', 'equals', 'yes' ),
						),
					),
					array(
						'id' => 'search_page_columns_large_screen',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Large Screen Columns', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the Search Page Columns on large screen devices', 'fildisi' ),
						'options' => $fildisi_eutf_blog_columns_selection,
						'default' => '3',
						'required' => array( 'search_page_mode', '!=', 'small' ),
					),
					array(
						'id' => 'search_page_columns',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Columns', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the Search Page Columns', 'fildisi' ),
						'options' => $fildisi_eutf_blog_columns_selection,
						'default' => '3',
						'required' => array( 'search_page_mode', '!=', 'small' ),
					),
					array(
						'id' => 'search_page_columns_tablet_landscape',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Tablet Landscape Columns', 'fildisi' ),
						'subtitle' => esc_html__( 'Select responsive column on tablet devices, landscape orientation.', 'fildisi' ),
						'options' => $fildisi_eutf_blog_columns_selection_tablet,
						'default' => '2',
						'required' => array( 'search_page_mode', '!=', 'small' ),
					),
					array(
						'id' => 'search_page_columns_tablet_portrait',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Tablet Portrait Columns', 'fildisi' ),
						'subtitle' => esc_html__( 'Select responsive column on tablet devices, portrait orientation.', 'fildisi' ),
						'options' => $fildisi_eutf_blog_columns_selection_tablet,
						'default' => '2',
						'required' => array( 'search_page_mode', '!=', 'small' ),
					),
					array(
						'id' => 'search_page_columns_mobile',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Mobile Columns', 'fildisi' ),
						'subtitle' => esc_html__( 'Select responsive column on mobile devices.', 'fildisi' ),
						'options' => $fildisi_eutf_blog_columns_selection_mobile,
						'default' => '1',
						'required' => array( 'search_page_mode', '!=', 'small' ),
					),
					array(
						'id' => 'search_page_heading_tag',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Result Title Tag', 'fildisi' ),
						'subtitle' => esc_html__( 'Select tag for your search result title.', 'fildisi' ),
						'options' => $fildisi_eutf_headings_tag_selection,
						'default' => 'h4',
					),
					array(
						'id' => 'search_page_heading',
						'type' => 'select',
						'title' => esc_html__( 'Search Page Result Title Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select size and typography for your search result title.', 'fildisi' ),
						'options' => $fildisi_eutf_headings_selection,
						'default' => 'h4',
					),
					array(
						'id'=>'search_page_excerpt_length_small',
						'type' => 'text',
						'default' => '30',
						'title' => esc_html__( 'Excerpt Length', 'fildisi' ),
						'subtitle' => esc_html__( 'Type how many words you want to display in your post excerpts (Default is 30).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'search_page_excerpt_more',
						'type' => 'switch',
						'title' => esc_html__( 'Read More', 'fildisi' ),
						'subtitle'=> esc_html__( "Adds a read more button after the excerpt or more quicktag.", 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'search_modal_text',
						'type' => 'text',
						'title' => esc_html__( 'Search Modal Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the search text of the modal.', 'fildisi' ),
						'default' => 'Start Typing',
					),
					array(
						'id' => 'search_modal_button_text',
						'type' => 'text',
						'title' => esc_html__( 'Search Modal Button Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the search text of the modal button.', 'fildisi' ),
						'default' => 'Start Searching',
					),
					array(
						'id' => 'search_page_not_found_text',
						'type' => 'editor',
						'args' => array ( 'wpautop' => false ),
						'title' => esc_html__( 'Search Page Not Found Content', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the content of your search page when no results are found.', 'fildisi' ),
						'default' => '<h3 class="eut-align-center eut-subtitle-text">Maybe you should check again your spelling.</h3><h3 class="eut-align-center">This is not the page you are looking for...</h3>',
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-briefcase',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Portfolio Options', 'fildisi' ),
				'id' => 'eut_redux_section_portfolio_options',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id'=>'portfolio_slug',
						'type' => 'text',
						'default' => 'portfolio',
						'title' => esc_html__( 'Slug', 'fildisi' ),
						'subtitle' => esc_html__( "Enter Portfolio Slug (Default is 'portfolio'). If you change it, go to Settings - Permalinks and click on Save Changes.", 'fildisi' ),
						'desc' => esc_html__( "Slug must not be used anywhere else (e.g: category, page, post).", 'fildisi' ),
						'validate' => 'not_empty',
					),
					array(
						'id' => 'portfolio_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Portfolio Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default layout for the Portfolio (single portfolio items). Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'none',
					),
					array(
						'id' => 'portfolio_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Portfolio Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default sidebar for the single portfolio items.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'portfolio_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Portfolio Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'portfolio_sidearea_visibility',
						'type' => 'select',
						'title' => esc_html__( 'Portfolio Sliding Area Visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable the Sliding Area for the single portfolio items.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'portfolio_sidearea_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Portfolio Sliding Area Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar widget area for the sliding area.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'portfolio_details_text',
						'type' => 'text',
						'title' => esc_html__( 'Portfolio Details Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the text for the Portfolio Details title.', 'fildisi' ),
						"default" => 'Project Details',
					),
					array(
						'id' => 'portfolio_details_heading_tag',
						'type' => 'select',
						'title' => esc_html__( 'Portfolio Details Text Heading Tag', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the headings tag for your Portfolio Details Text.', 'fildisi' ),
						'options' => $fildisi_eutf_headings_tag_selection,
						'default' => 'div',
					),
					array(
						'id' => 'portfolio_details_link_text',
						'type' => 'text',
						'title' => esc_html__( 'Portfolio Details Link Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the text for the Portfolio Details link text.', 'fildisi' ),
						"default" => 'View Project',
					),
					array(
						'id'=>'portfolio_comments_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Comments Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the comments of your portfolio.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'   => 'info_style_portfolio_header',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Portfolio Header Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default portfolio header.', 'fildisi' ),
					),
					array(
						'id' => 'portfolio_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Portfolio Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your portfolio header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
					),
					array(
						'id' => 'portfolio_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Portfolio Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Portfolio Header overlaps the content. Combine this option with the Light or Dark Header. You can still override this when you edit each single portfolio.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_style_portfolio_title',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Portfolio Title Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the single default portfolio title.', 'fildisi' ),
					),
					array(
						'id' => 'portfolio_title_height',
						'type' => 'select',
						'default' => '45',
						'title' => esc_html__( 'Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter portfolio title height (Default is 45%).', 'fildisi' ),
						'options' => $fildisi_eutf_area_height,
						'validate' => 'not_empty',
					),
					array(
						'id' => 'portfolio_title_min_height',
						'type' => 'text',
						'default' => '200',
						'title' => esc_html__( 'Min Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter portfolio title minimum height in px (Default is 200).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'portfolio_title_container_size',
						'type' => 'select',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the container.', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selection,
						'default' => 'default',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'portfolio_title_content_size',
						'type' => 'select',
						'title' => esc_html__( 'Content Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_content_size_selection,
						'default' => 'large',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'portfolio_title_content_alignment',
						'type' => 'select',
						'title' => esc_html__( 'Content Alignment', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the alignemt of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'portfolio_title_content_position',
						'type' => 'select',
						'title' => esc_html__( 'Content Position', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your position for the default portfolio title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'portfolio_title_content_animation',
						'type' => 'select',
						'title' => esc_html__( 'Content Animation', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your animation for the default portfolio title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_animation_selection,
						'default' => 'fade-in',
						'validate' => 'not_empty',
					),
					array(
						'id'       => 'portfolio_title_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Portfolio Title Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode,
						'default'  => 'color',
					),
					array(
						'id'       => 'portfolio_title_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the default portfolio title.', 'fildisi' ),
						'required' => array( 'portfolio_title_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'portfolio_title_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'portfolio_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'portfolio_title_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array( 'portfolio_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'portfolio_title_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array( 'portfolio_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'portfolio_title_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array( 'portfolio_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id'   => 'info_style_portfolio_anchor_menu',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Anchor Menu Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Anchor Menu Bar where you can place a custom sticky menu per portfolio item.', 'fildisi' ),
					),
					array(
						'id' => 'portfolio_anchor_menu_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Anchor Menu Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Anchor Menu height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'portfolio_anchor_menu_alignment',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Anchor Menu Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the alignment of your anchor menu.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'left',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'portfolio_anchor_menu_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Fullwidth', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show your anchor menu full width.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'   => 'info_style_portfolio_breadcrumbs',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Breadcrumbs Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Breadcrumbs Bar.', 'fildisi' ),
					),
					array(
						'id'=>'portfolio_breadcrumbs_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Breadcrumbs', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the Breadcrumbs for Single Portfolio.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'portfolio_breadcrumbs_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Breadcrumbs Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Breadcrumbs height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'portfolio_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'portfolio_breadcrumbs_alignment',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Breadcrumbs Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the alignment of your breadcrumbs.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'left',
						'validate' => 'not_empty',
						'required' => array( 'portfolio_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'portfolio_breadcrumbs_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Fullwidth', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show your breadcrumbs full width.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'portfolio_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id'   => 'info_style_portfolio_socials',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Socials Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Socials.', 'fildisi' ),
					),
					array(
						'id' => 'portfolio_social_bar_layout',
						'type' => 'select',
						'title' => esc_html__( 'Socials Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the Layout for Socials', 'fildisi' ),
						'options' => array(
							'layout-1' => esc_html__( 'Bottom Layout', 'fildisi' ),
							'layout-2' => esc_html__( 'Side Layout', 'fildisi' ),
						),
						'default' => 'layout-1',
					),
					array(
						'id' => 'portfolio_social',
						'type' => 'checkbox',
						'title' => esc_html__( 'Social Share', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable or Disable the social shares you like in the single portfolio items.', 'fildisi' ),
						'options' => $fildisi_eutf_portfolio_social_options,
						'default' => 0,
					),
					array(
						'id'   => 'info_style_portfolio_recent',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Recent Items Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Recent Items.', 'fildisi' ),
					),
					array(
						'id'=>'portfolio_recents_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Recent Items Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the recent items carousel.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'portfolio_recent_title',
						'type' => 'text',
						'title' => esc_html__( 'Portfolio Recent Items Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the text for the portfolio recent items title.', 'fildisi' ),
						"default" => 'More Projects',
						'required' => array( 'portfolio_recents_visibility', 'equals', '1' ),
					),
					array(
						'id'   => 'info_style_portfolio_nav',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Navigation Bar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Navigation Bar.', 'fildisi' ),
					),
					array(
						'id'=>'portfolio_nav_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Portfolio Navigation Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the visibility of the portfolio navigation.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'portfolio_nav_term',
						'type' => 'select',
						'title' => esc_html__( 'Portfolio Navigation Same Term', 'fildisi' ),
						'subtitle' => esc_html__( 'If selected, only navigation items with the same selected taxonomy term will be displayed.', 'fildisi' ),
						'options' => $fildisi_eutf_portfolio_term_selection,
						'default' => 'none',
						'required' => array( 'portfolio_nav_visibility', 'equals', '1' ),
					),
					array(
						'id' => 'portfolio_backlink_id',
						'type' => 'select',
						'title' => esc_html__( 'Portfolio Backlink', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the overview page for your portfolio backlink.', 'fildisi' ),
						'data' => 'page',
						'default' => '',
						'required' => array( 'portfolio_nav_visibility', 'equals', '1' ),
					),
					array(
						'id'             => 'portfolio_nav_spacing',
						'type'           => 'spacing',
						'mode'           => 'padding',
						'units'          => 'px',
						'units_extended' => 'false',
						'left'           => 'false',
						'right'          => 'false',
						'title'          => esc_html__( 'Navigation Bar Spacing', 'fildisi' ),
						'subtitle'       => esc_html__( 'Set the spacing, Top and Bottom, of the Navigation Area.', 'fildisi' ),
						'desc'           => esc_html__( 'Set spacing Top, Bottom in px.', 'fildisi'),
						'default'        => array(
							'padding-top'     => '30px',
							'padding-bottom'  => '30px',
							'units'           => 'px',
						),
						'required' => array( 'portfolio_nav_visibility', 'equals', '1' ),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-shopping-cart',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'WooCommerce Options', 'fildisi'),
				'id' => 'eut_redux_section_woocommerce_options',
				'subtitle' => esc_html__( 'You can find the Theme settings for the WooCommerce here.', 'fildisi' ),
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'product_tax_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Product Taxonomy Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default layout for the Product Taxonomy. Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'none',
					),
					array(
						'id' => 'product_tax_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Product Taxonomy Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default sidebar for the single Product Taxonomy.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_tax_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Product Taxonomy Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'product_tax_sidearea_visibility',
						'type' => 'select',
						'title' => esc_html__( 'Product Taxonomy Sliding Area Visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable the Sliding Area for the Product Taxonomy.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'product_tax_sidearea_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Product Taxonomy Sliding Area Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar widget area for the sliding area.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id'   => 'info_style_product_tax_header',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Product Taxonomy Header Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default Product Taxonomy header.', 'fildisi' ),
					),
					array(
						'id' => 'product_tax_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Product Taxonomy Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your Product Taxonomy header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
					),
					array(
						'id' => 'product_tax_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Product Taxonomy Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Product Taxonomy Header overlaps the content. Combine this option with the Light or Dark Header.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_style_product_tax_title',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Product Taxonomy Title Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the single default Product Taxonomy title.', 'fildisi' ),
					),
					array(
						'id' => 'product_tax_title_height',
						'type' => 'select',
						'default' => '60',
						'title' => esc_html__( 'Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Product Taxonomy title height (Default is 60%).', 'fildisi' ),
						'options' => $fildisi_eutf_area_height,
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_tax_title_min_height',
						'type' => 'text',
						'default' => '320',
						'title' => esc_html__( 'Min Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Product Taxonomy title minimum height in px (Default is 320).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'product_tax_title_container_size',
						'type' => 'select',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the container.', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selection,
						'default' => 'default',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_tax_title_content_size',
						'type' => 'select',
						'title' => esc_html__( 'Content Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_content_size_selection,
						'default' => 'large',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_tax_title_content_alignment',
						'type' => 'select',
						'title' => esc_html__( 'Content Alignment', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the alignemt of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_tax_title_content_position',
						'type' => 'select',
						'title' => esc_html__( 'Content Position', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your position for the default Product Taxonomy title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_tax_title_content_animation',
						'type' => 'select',
						'title' => esc_html__( 'Content Animation', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your animation for the default Product Taxonomy title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_animation_selection,
						'default' => 'fade-in',
						'validate' => 'not_empty',
					),
					array(
						'id'       => 'product_tax_title_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Product Taxonomy Title Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode_limited,
						'default'  => 'color',
					),
					array(
						'id'       => 'product_tax_title_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the default Product Taxonomy title.', 'fildisi' ),
						'required' => array( 'product_tax_title_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'product_tax_title_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'product_tax_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'product_tax_title_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array( 'product_tax_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'product_tax_title_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array( 'product_tax_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'product_tax_title_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array( 'product_tax_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id'   => 'info_style_product_overview',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Shop Overview Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the shop overview.', 'fildisi' ),
					),
					array(
						'id' => 'product_overview_image_effect',
						'type' => 'select',
						'title' => esc_html__( 'Loop Product Image effect', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the effect for the overview product image', 'fildisi' ),
						'options' => array(
							'none' => esc_html__( 'None', 'fildisi' ),
							'second' => esc_html__( 'Second Image on Hover', 'fildisi' ),
						),
						'default' => 'second',
					),
					array(
						'id' => 'product_overview_image_zoom_effect',
						'type' => 'select',
						'options' => array(
							'in' => esc_html__( 'Zoom In', 'fildisi' ),
							'out' => esc_html__( 'Zoom Out', 'fildisi' ),
							'none' => esc_html__( 'None', 'fildisi' ),
						),
						'title' => esc_html__( 'Loop Image Zoom Effect', 'fildisi' ),
						'subtitle' => esc_html__( 'Choose the image zoom effect.', 'fildisi' ),
						'default' => 'none',
					),
					array(
						'id' => 'product_overview_image_size',
						'type' => 'select',
						'options' => array(
							'default' => esc_html__( 'Default', 'fildisi' ),
							'square' => esc_html__( 'Square Small Crop', 'fildisi' ),
							'landscape' => esc_html__( 'Landscape Small Crop', 'fildisi' ),
							'portrait' => esc_html__( 'Portrait Small Crop', 'fildisi' ),
							'large' => esc_html__( 'Resize ( Large )', 'fildisi' ),
							'medium_large' => esc_html__( 'Resize ( Medium Large )', 'fildisi' ),
							'medium' => esc_html__( 'Resize ( Medium )', 'fildisi' ),
						),
						'title' => esc_html__( 'Loop Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Choose the overview image size.', 'fildisi' ),
						'default' => 'default',
					),
					array(
						'id' => 'product_loop_columns',
						'type' => 'select',
						'options' => array(
							'2' => '2',
							'3' => '3',
							'4' => '4',
							'5' => '5',
						),
						'default' => '4',
						'title' => esc_html__( 'Loop Columns', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the number of columns (Default is 4).', 'fildisi' ),
					),
					array(
						'id' => 'product_loop_shop_per_page',
						'type' => 'text',
						'default' => '12',
						'title' => esc_html__( 'Loop Products per Page', 'fildisi' ),
						'subtitle' => esc_html__( 'Select how many products per page you want to show (Default is 12).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'product_overview_heading_tag',
						'type' => 'select',
						'title' => esc_html__( 'Loop Product Title Tag', 'fildisi' ),
						'subtitle' => esc_html__( 'Select tag for your loop product title.', 'fildisi' ),
						'options' => $fildisi_eutf_headings_tag_selection,
						'default' => 'h3',
					),
					array(
						'id' => 'product_overview_heading',
						'type' => 'select',
						'title' => esc_html__( 'Loop Product Title Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select size and typography for your loop product title.', 'fildisi' ),
						'options' => $fildisi_eutf_headings_selection,
						'default' => 'h6',
					),
					array(
						'id' => 'product_overview_overlay_color',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'light',
					),
					array(
						'id' => 'product_overview_overlay_opacity',
						'type' => 'select',
						'options' => $fildisi_eutf_opacity_selection_simple,
						'title' => esc_html__( 'Overlay Opacity', 'fildisi' ),
						'subtitle' => esc_html__( 'Choose the opacity for the overlay.', 'fildisi' ),
						'default' => '90',
					),


				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Single Product Settings', 'fildisi' ),
				'id' => 'eut_redux_section_single_product_settings',
				'header' => '',
				'desc' => esc_html__( 'Define your preferences for the Single Products. Notice that most of them can be overridden when you create a single product.', 'fildisi' ),
				'submenu' => true,
				'icon' => 'el-icon-shopping-cart',
				'icon_class' => 'el-icon-large',
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'product_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Product Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the layout for the Single Products. Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'right',
					),
					array(
						'id' => 'product_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Product Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar for the Single Products.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Product Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'product_sidearea_visibility',
						'type' => 'select',
						'title' => esc_html__( 'Product Sliding Area Visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable the Product Sliding Area if you want.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'product_sidearea_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Product Sliding Area Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar widget area for the sliding area.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id'   => 'info_style_product_header',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Product Header Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default post header.', 'fildisi' ),
					),
					array(
						'id' => 'product_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Product Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your product header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
					),
					array(
						'id' => 'product_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Product Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Product Header overlaps the content. Combine this option with the Light or Dark Header. You can still override this when you edit each single product.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_style_product_title',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Single Product Title Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default single product title.', 'fildisi' ),
					),
					array(
						'id' => 'product_title_style',
						'type' => 'select',
						'default' => 'simple',
						'title' => esc_html__( 'Title Style', 'fildisi' ),
						'subtitle' => esc_html__( 'Select Product Title Style', 'fildisi' ),
						'options' => $fildisi_eutf_title_style,
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_title_height',
						'type' => 'select',
						'default' => '60',
						'title' => esc_html__( 'Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter product title height (Default is 60%).', 'fildisi' ),
						'options' => $fildisi_eutf_area_height,
						'validate' => 'not_empty',
						'required' => array( 'product_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'product_title_min_height',
						'type' => 'text',
						'default' => '200',
						'title' => esc_html__( 'Min Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter product title minimum height in px (Default is 200).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'product_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'product_title_container_size',
						'type' => 'select',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the container.', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selection,
						'default' => 'default',
						'validate' => 'not_empty',
						'required' => array( 'product_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'product_title_content_size',
						'type' => 'select',
						'title' => esc_html__( 'Content Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_content_size_selection,
						'default' => 'large',
						'validate' => 'not_empty',
						'required' => array( 'product_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'product_title_content_alignment',
						'type' => 'select',
						'title' => esc_html__( 'Content Alignment', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the alignemt of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
						'required' => array( 'product_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'product_title_content_position',
						'type' => 'select',
						'title' => esc_html__( 'Content Position', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your position for the default product title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'product_title_style', 'equals', 'advanced' ),
					),
					array(
						'id' => 'product_title_content_animation',
						'type' => 'select',
						'title' => esc_html__( 'Content Animation', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your animation for the default product title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_animation_selection,
						'default' => 'fade-in',
						'validate' => 'not_empty',
						'required' => array( 'product_title_style', 'equals', 'advanced' ),
					),
					array(
						'id'       => 'product_title_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Product Title Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode,
						'default'  => 'color',
						'required' => array( 'product_title_style', 'equals', 'advanced' ),
					),
					array(
						'id'       => 'product_title_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the default product title.', 'fildisi' ),
						'required' => array(
							array( 'product_title_style', 'equals', 'advanced' ),
							array( 'product_title_bg_mode', 'equals', 'custom' ),
						),
					),
					array(
						'id' => 'product_title_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array(
							array( 'product_title_style', 'equals', 'advanced' ),
							array( 'product_title_bg_mode', '!=', 'color' ),
						),
					),
					array(
						'id' => 'product_title_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array(
							array( 'product_title_style', 'equals', 'advanced' ),
							array( 'product_title_bg_mode', '!=', 'color' ),
						),
					),
					array(
						'id' => 'product_title_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array(
							array( 'product_title_style', 'equals', 'advanced' ),
							array( 'product_title_bg_mode', '!=', 'color' ),
						),
					),
					array(
						'id' => 'product_title_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array(
							array( 'product_title_style', 'equals', 'advanced' ),
							array( 'product_title_bg_mode', '!=', 'color' ),
						),
					),
					array(
						'id'   => 'info_style_product_content_area',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Product Area/Product Content Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the product area and product content (Tabs).', 'fildisi' ),
					),
					array(
						'id' => 'product_area_section_type',
						'type' => 'select',
						'title' => esc_html__( 'Product Area Full Width', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you prefer a full-width Product Content Area.', 'fildisi' ),
						'options' => array(
							'fullwidth-background' => esc_html__( 'No', 'fildisi' ),
							'fullwidth-element' => esc_html__( 'Yes', 'fildisi' ),
						),
						'default' => 'fullwidth-background',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_area_padding_top_multiplier',
						'type' => 'select',
						'title' => esc_html__( 'Product Area Top Padding', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the space above the area.', 'fildisi' ),
						'options' => $fildisi_eutf_padding_selection,
						'default' => '3x',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_area_padding_bottom_multiplier',
						'type' => 'select',
						'title' => esc_html__( 'Product Area bottom Padding', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the space below the product area.', 'fildisi' ),
						'options' => $fildisi_eutf_padding_selection,
						'default' => '3x',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_content_width',
						'type' => 'select',
						'title' => esc_html__( 'Product Content Width', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the Single Product Content width (only for Full Width Product Layout)', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selector,
						'default' => '770',
					),
					array(
						'id'   => 'info_style_product_anchor_menu',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Anchor Menu Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Anchor Menu Bar where you can place a custom sticky menu per product.', 'fildisi' ),
					),
					array(
						'id' => 'product_anchor_menu_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Anchor Menu Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Anchor Menu height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'product_anchor_menu_alignment',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Anchor Menu Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the alignment of your Anchor Menu in products.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'left',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'product_anchor_menu_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Fullwidth', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to have full width Anchor Menu.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'   => 'info_style_product_breadcrumbs',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Breadcrumbs Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Breadcrumbs Bar.', 'fildisi' ),
					),
					array(
						'id'=>'product_breadcrumbs_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Breadcrumbs', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the Breadcrumbs for Single Product / Product Categories / Product Tags.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'product_breadcrumbs_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Breadcrumbs Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Breadcrumbs height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'product_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'product_breadcrumbs_alignment',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Breadcrumbs Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the alignment of your breadcrumbs.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'left',
						'validate' => 'not_empty',
						'required' => array( 'product_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'product_breadcrumbs_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Fullwidth', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show your breadcrumbs full width.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'product_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id'   => 'info_style_product_image',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Single Product Image Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the single product image.', 'fildisi' ),
					),
					array(
						'id' => 'product_image_thumb_style',
						'type' => 'select',
						'title' => esc_html__( 'Product Image / Thumbnails Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the style for the single product image and thumbnails. Note: some external plugins might not be compatible with Theme style.', 'fildisi' ),
						'options' => array(
							'theme' => esc_html__( 'Theme Style', 'fildisi' ),
							'woo' => esc_html__( 'Default WooCommerce Style', 'fildisi' ),
						),
						'default' => 'theme',
					),
					array(
						'id' => 'product_image_effect',
						'type' => 'select',
						'title' => esc_html__( 'Product Image effect', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the effect for the main product image', 'fildisi' ),
						'options' => array(
							'none' => esc_html__( 'None', 'fildisi' ),
							'zoom' => esc_html__( 'Zoom', 'fildisi' ),
						),
						'default' => 'none',
						'required' => array( 'product_image_thumb_style', 'equals', 'theme' ),
					),
					array(
						'id' => 'product_image_size',
						'type' => 'select',
						'options' => array(
							'default' => esc_html__( 'Default', 'fildisi' ),
							'square' => esc_html__( 'Square Small Crop', 'fildisi' ),
							'landscape' => esc_html__( 'Landscape Small Crop', 'fildisi' ),
							'portrait' => esc_html__( 'Portrait Small Crop', 'fildisi' ),
							'large' => esc_html__( 'Resize ( Large )', 'fildisi' ),
							'medium_large' => esc_html__( 'Resize ( Medium Large )', 'fildisi' ),
							'medium' => esc_html__( 'Resize ( Medium )', 'fildisi' ),
						),
						'title' => esc_html__( 'Product Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Choose the product image size.', 'fildisi' ),
						'default' => 'default',
						'required' => array( 'product_image_thumb_style', 'equals', 'theme' ),
					),
					array(
						'id' => 'product_gallery_mode',
						'type' => 'select',
						'title' => esc_html__( 'Product Gallery Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the mode for the single product gallery', 'fildisi' ),
						'options' => array(
							'none' => esc_html__( 'None', 'fildisi' ),
							'popup' => esc_html__( 'Magnific Popup', 'fildisi' ),
						),
						'default' => 'popup',
						'required' => array( 'product_image_thumb_style', 'equals', 'theme' ),
					),
					array(
						'id' => 'product_gallery_title_caption',
						"type" => "select",
						"title" => esc_html__( "Product Gallery Image Title & Caption Visibility", "fildisi" ),
						"subtitle" => esc_html__( "Define the visibility for your image title - caption.", "fildisi" ),
						'options' => array(
							'none' => esc_html__( 'None' , 'fildisi' ),
							'title-caption' => esc_html__( 'Title and Caption' , 'fildisi' ),
							'title-only' => esc_html__( 'Title Only' , 'fildisi' ),
							'caption-only' => esc_html__( 'Caption Only' , 'fildisi' ),
						),
						'default' => 'none',
						'required' => array( 'product_image_thumb_style', 'equals', 'theme' ),
					),
					array(
						'id' => 'product_image_middle',
						'type' => 'switch',
						'title' => esc_html__( 'Product Image Middle Content', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable/Disable middle content for product image/thumbs and product summary.', 'fildisi' ),
						'default' => 1,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'product_image_thumb_style', 'equals', 'theme' ),
					),
					array(
						'id'=>'product_gallery_woo_zoom',
						'type' => 'switch',
						'title' => esc_html__( 'Zoom Effect', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable image zoom effect.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'product_image_thumb_style', 'equals', 'woo' ),
					),
					array(
						'id'=>'product_gallery_woo_lightbox',
						'type' => 'switch',
						'title' => esc_html__( 'Gallery Lightbox', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable gallery lightbox.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'product_image_thumb_style', 'equals', 'woo' ),
					),
					array(
						'id'=>'product_gallery_woo_slider',
						'type' => 'switch',
						'title' => esc_html__( 'Gallery Slider', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable gallery slider.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'product_image_thumb_style', 'equals', 'woo' ),
					),
					array(
						'id'   => 'info_style_product_social',
						'type' => 'info',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Socials Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Socials.', 'fildisi' ),
					),
					array(
						'id' => 'product_social',
						'type' => 'checkbox',
						'title' => esc_html__( 'Social Share', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable or Disable post social shares for the Single Product.', 'fildisi' ),
						'options' => $fildisi_eutf_product_social_options,
						'default' => 0,
					),
					array(
						'id'   => 'info_style_product_nav',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Navigation Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Navigation.', 'fildisi' ),
					),
					array(
						'id'=>'product_nav_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Product Navigation Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the visibility of the products navigation.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'product_nav_same_term',
						'type' => 'checkbox',
						'title' => esc_html__( 'Product Navigation Same Term', 'fildisi' ),
						'subtitle'=> esc_html__( 'If selected, only navigation items from the current taxonomy term will be displayed.', 'fildisi' ),
						'default' => 0,
						'required' => array( 'product_nav_visibility', 'equals', '1' ),
					),
					array(
						'id' => 'product_backlink_id',
						'type' => 'select',
						'title' => esc_html__( 'Product Backlink', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the overview page for your product backlink.', 'fildisi' ),
						'data' => 'page',
						'default' => '',
						'required' => array( 'product_nav_visibility', 'equals', '1' ),
					),
					array(
						'id'             => 'product_nav_spacing',
						'type'           => 'spacing',
						'mode'           => 'padding',
						'units'          => 'px',
						'units_extended' => 'false',
						'left'           => 'false',
						'right'          => 'false',
						'title'          => esc_html__( 'Navigation Bar Spacing', 'fildisi' ),
						'subtitle'       => esc_html__( 'Set the spacing, Top and Bottom, of the Navigation Area.', 'fildisi' ),
						'desc'           => esc_html__( 'Set spacing Top, Bottom in px.', 'fildisi'),
						'default'        => array(
							'padding-top'     => '30px',
							'padding-bottom'  => '30px',
							'units'           => 'px',
						),
						'required' => array( 'product_nav_visibility', 'equals', '1' ),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Product Search Settings', 'fildisi' ),
				'id' => 'eut_redux_section_product_search_settings',
				'header' => '',
				'desc' => esc_html__( 'Define your preferences for the Product Search.', 'fildisi' ),
				'submenu' => true,
				'icon' => 'el-icon-shopping-cart',
				'icon_class' => 'el-icon-large',
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'product_search_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Product Search Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the layout for the product search page. Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'none',
					),
					array(
						'id' => 'product_search_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Product Search Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default sidebar for the product search page in case you do not use full width layout.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
					),
					array(
						'id' => 'product_search_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Product Search Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-calendar',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Events Calendar Options', 'fildisi'),
				'id' => 'eut_redux_section_event_calendar_options',
				'subtitle' => esc_html__( 'You can find the Theme settings for the Events Calendar here.', 'fildisi' ),
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'event_tax_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Event Taxonomy Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default layout for the Event Taxonomy. Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'none',
					),
					array(
						'id' => 'event_tax_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Event Taxonomy Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default sidebar for the single Event Taxonomy.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_tax_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Event Taxonomy Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'event_tax_sidearea_visibility',
						'type' => 'select',
						'title' => esc_html__( 'Event Taxonomy Sliding Area Visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable the Sliding Area for the Event Taxonomy.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'event_tax_sidearea_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Event Taxonomy Sliding Area Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar widget area for the sliding area.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id'   => 'info_style_event_tax_header',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Event Taxonomy Header Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default Event Taxonomy header.', 'fildisi' ),
					),
					array(
						'id' => 'event_tax_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Event Taxonomy Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your Event Taxonomy header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
					),
					array(
						'id' => 'event_tax_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Event Taxonomy Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Event Taxonomy Header overlaps the content. Combine this option with the Light or Dark Header.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_style_event_tax_title',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Event Taxonomy Title Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the Event Taxonomy title.', 'fildisi' ),
					),
					array(
						'id' => 'event_tax_title_height',
						'type' => 'select',
						'default' => '60',
						'title' => esc_html__( 'Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Event Taxonomy title height (Default is 60%).', 'fildisi' ),
						'options' => $fildisi_eutf_area_height,
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_tax_title_min_height',
						'type' => 'text',
						'default' => '320',
						'title' => esc_html__( 'Min Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Event Taxonomy title minimum height in px (Default is 320).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'event_tax_title_container_size',
						'type' => 'select',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the container.', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selection,
						'default' => 'default',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_tax_title_content_size',
						'type' => 'select',
						'title' => esc_html__( 'Content Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_content_size_selection,
						'default' => 'large',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_tax_title_content_alignment',
						'type' => 'select',
						'title' => esc_html__( 'Content Alignment', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the alignemt of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_tax_title_content_position',
						'type' => 'select',
						'title' => esc_html__( 'Content Position', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your position for the default Event Taxonomy title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_tax_title_content_animation',
						'type' => 'select',
						'title' => esc_html__( 'Content Animation', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your animation for the default Event Taxonomy title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_animation_selection,
						'default' => 'fade-in',
						'validate' => 'not_empty',
					),
					array(
						'id'       => 'event_tax_title_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Event Taxonomy Title Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode_limited,
						'default'  => 'color',
					),
					array(
						'id'       => 'event_tax_title_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the default Event Taxonomy title.', 'fildisi' ),
						'required' => array( 'event_tax_title_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'event_tax_title_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'event_tax_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'event_tax_title_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array( 'event_tax_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'event_tax_title_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array( 'event_tax_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'event_tax_title_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array( 'event_tax_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id'   => 'info_style_event_overview',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Event Overview Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the event overview.', 'fildisi' ),
					),
					array(
						'id' => 'event_overview_image_size',
						'type' => 'select',
						'options' => array(
							'default' => esc_html__( 'Default', 'fildisi' ),
							'square' => esc_html__( 'Square Small Crop', 'fildisi' ),
							'landscape' => esc_html__( 'Landscape Small Crop', 'fildisi' ),
							'portrait' => esc_html__( 'Portrait Small Crop', 'fildisi' ),
							'large' => esc_html__( 'Resize ( Large )', 'fildisi' ),
							'medium_large' => esc_html__( 'Resize ( Medium Large )', 'fildisi' ),
							'medium' => esc_html__( 'Resize ( Medium )', 'fildisi' ),
						),
						'title' => esc_html__( 'Event Overview Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Choose the event overview image size.', 'fildisi' ),
						'default' => 'default',
					),
					array(
						'id' => 'event_overview_heading',
						'type' => 'select',
						'title' => esc_html__( 'Event Overview Title Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select size and typography for your event overview title.', 'fildisi' ),
						'options' => $fildisi_eutf_event_headings_selection,
						'default' => 'h2',
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Single Event Settings', 'fildisi' ),
				'id' => 'eut_redux_section_single_event_settings',
				'header' => '',
				'desc' => esc_html__( 'Define your preferences for the Single Events. Notice that most of them can be overridden when you create a single event.', 'fildisi' ),
				'submenu' => true,
				'icon' => 'el-icon-calendar',
				'icon_class' => 'el-icon-large',
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'event_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Event Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the layout for the Single Events. Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'right',
					),
					array(
						'id' => 'event_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Event Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar for the Single Events.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Event Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'event_sidearea_visibility',
						'type' => 'select',
						'title' => esc_html__( 'Event Sliding Area Visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable the Event Sliding Area if you want.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'event_sidearea_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Event Sliding Area Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar widget area for the sliding area.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_content_width',
						'type' => 'select',
						'title' => esc_html__( 'Event Content Width', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the Event Content width (only for Full Width Post Layout)', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selector,
						'default' => '1170',
					),
					array(
						'id'   => 'info_style_event_header',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Event Header Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default post header.', 'fildisi' ),
					),
					array(
						'id' => 'event_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Event Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your event header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
					),
					array(
						'id' => 'event_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Event Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Event Header overlaps the content. Combine this option with the Light or Dark Header. You can still override this when you edit each single event.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_style_event_title',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Single Event Title Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default single event title.', 'fildisi' ),
					),
					array(
						'id' => 'event_title_height',
						'type' => 'select',
						'default' => '60',
						'title' => esc_html__( 'Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter event title height (Default is 60%).', 'fildisi' ),
						'options' => $fildisi_eutf_area_height,
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_title_min_height',
						'type' => 'text',
						'default' => '200',
						'title' => esc_html__( 'Min Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter event title minimum height in px (Default is 200).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'event_title_container_size',
						'type' => 'select',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the container.', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selection,
						'default' => 'default',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_title_content_size',
						'type' => 'select',
						'title' => esc_html__( 'Content Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_content_size_selection,
						'default' => 'large',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_title_content_alignment',
						'type' => 'select',
						'title' => esc_html__( 'Content Alignment', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the alignemt of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_title_content_position',
						'type' => 'select',
						'title' => esc_html__( 'Content Position', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your position for the default event title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_title_content_animation',
						'type' => 'select',
						'title' => esc_html__( 'Content Animation', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your animation for the default event title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_animation_selection,
						'default' => 'fade-in',
						'validate' => 'not_empty',
					),
					array(
						'id'       => 'event_title_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Event Title Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode,
						'default'  => 'color',
					),
					array(
						'id'       => 'event_title_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the default event title.', 'fildisi' ),
						'required' => array( 'event_title_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'event_title_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'event_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'event_title_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array( 'event_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'event_title_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array( 'event_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'event_title_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array( 'event_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id'   => 'info_style_event_anchor_menu',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Anchor Menu Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Anchor Menu Bar where you can place a custom sticky menu per event.', 'fildisi' ),
					),
					array(
						'id' => 'event_anchor_menu_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Anchor Menu Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Anchor Menu height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'event_anchor_menu_alignment',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Anchor Menu Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the alignment of your Anchor Menu in events.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'left',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'event_anchor_menu_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Fullwidth', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to have full width Anchor Menu.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'   => 'info_style_event_breadcrumbs',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Breadcrumbs Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Breadcrumbs Bar.', 'fildisi' ),
					),
					array(
						'id'=>'event_breadcrumbs_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Breadcrumbs', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the Breadcrumbs for Single Event / Event Categories / Event Tags.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'event_breadcrumbs_height',
						'type' => 'text',
						'default' => '60',
						'title' => esc_html__( 'Breadcrumbs Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Breadcrumbs height in px (Default is 60).', 'fildisi' ),
						'validate' => 'numeric',
						'required' => array( 'event_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'event_breadcrumbs_alignment',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Breadcrumbs Alignment', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the alignment of your breadcrumbs.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'left',
						'validate' => 'not_empty',
						'required' => array( 'event_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'event_breadcrumbs_fullwidth',
						'type' => 'switch',
						'title' => esc_html__( 'Fullwidth', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show your breadcrumbs full width.', 'fildisi' ),
						'default' => 0,
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
						'required' => array( 'event_breadcrumbs_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'event_image_size',
						'type' => 'select',
						'options' => array(
							'default' => esc_html__( 'Default', 'fildisi' ),
							'square' => esc_html__( 'Square Small Crop', 'fildisi' ),
							'landscape' => esc_html__( 'Landscape Small Crop', 'fildisi' ),
							'landscape-large-wide' => esc_html__( 'Landscape Large Wide Crop', 'fildisi' ),
							'portrait' => esc_html__( 'Portrait Small Crop', 'fildisi' ),
							'extra-extra-large' => esc_html__( 'Resize ( Extra Extra Large )', 'fildisi' ),
							'large' => esc_html__( 'Resize ( Large )', 'fildisi' ),
							'medium_large' => esc_html__( 'Resize ( Medium Large )', 'fildisi' ),
							'medium' => esc_html__( 'Resize ( Medium )', 'fildisi' ),
						),
						'title' => esc_html__( 'Event Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Choose the event image size.', 'fildisi' ),
						'default' => 'default',
					),
					array(
						'id'   => 'info_style_event_nav',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Navigation & Socials Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define your preferences for the Navigation.', 'fildisi' ),
					),
					array(
						'id'=>'event_nav_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Event Navigation Visibility', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the visibility of the events navigation.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id' => 'event_nav_same_term',
						'type' => 'checkbox',
						'title' => esc_html__( 'Event Navigation Same Term', 'fildisi' ),
						'subtitle'=> esc_html__( 'If selected, only navigation items from the current taxonomy term will be displayed.', 'fildisi' ),
						'default' => 0,
						'required' => array( 'event_nav_visibility', 'equals', '1' ),
					),
					array(
						'id'             => 'event_nav_spacing',
						'type'           => 'spacing',
						'mode'           => 'padding',
						'units'          => 'px',
						'units_extended' => 'false',
						'left'           => 'false',
						'right'          => 'false',
						'title'          => esc_html__( 'Navigation Bar Spacing', 'fildisi' ),
						'subtitle'       => esc_html__( 'Set the spacing, Top and Bottom, of the Navigation Area.', 'fildisi' ),
						'desc'           => esc_html__( 'Set spacing Top, Bottom in px.', 'fildisi'),
						'default'        => array(
							'padding-top'     => '40px',
							'padding-bottom'  => '40px',
							'units'           => 'px',
						),
					),
					array(
						'id' => 'event_social',
						'type' => 'checkbox',
						'title' => esc_html__( 'Social Share', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable or Disable social shares for the Single Event.', 'fildisi' ),
						'options' => $fildisi_eutf_event_social_options,
						'default' => 0,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-comment-alt',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'bbPress Forum Options', 'fildisi'),
				'id' => 'eut_redux_section_bbpress_options',
				'subtitle' => esc_html__( 'You can find the Theme settings for the bbPress here.', 'fildisi' ),
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'forum_layout',
						'type' => 'image_select',
						'compiler' => true,
						'title' => esc_html__( 'Forum Layout', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default layout for the Forum. Choose among Full Width, Left Sidebar or Right Sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_layout_selection,
						'default' => 'none',
					),
					array(
						'id' => 'forum_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Forum Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the default sidebar for the single Forum.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'forum_fixed_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Forum Fixed Sidebar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want a fixed sidebar.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'forum_sidearea_visibility',
						'type' => 'select',
						'title' => esc_html__( 'Forum Sliding Area Visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable the Sliding Area for the Forum.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id' => 'forum_sidearea_sidebar',
						'type' => 'select',
						'title' => esc_html__( 'Forum Sliding Area Sidebar', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the sidebar widget area for the sliding area.', 'fildisi' ),
						'data' => 'sidebar',
						'default' => 'eut-default-sidebar',
						'validate' => 'not_empty',
					),
					array(
						'id'   => 'info_style_forum_header',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Forum Header Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the default Forum header.', 'fildisi' ),
					),
					array(
						'id' => 'forum_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Forum Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your Forum header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
					),
					array(
						'id' => 'forum_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Forum Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Forum Header overlaps the content. Combine this option with the Light or Dark Header.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
					),
					array(
						'id'   => 'info_style_forum_title',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Forum Title Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Set the style for the single default Forum title.', 'fildisi' ),
					),
					array(
						'id' => 'forum_title_height',
						'type' => 'select',
						'default' => '60',
						'title' => esc_html__( 'Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Forum title height (Default is 60%).', 'fildisi' ),
						'options' => $fildisi_eutf_area_height,
						'validate' => 'not_empty',
					),
					array(
						'id' => 'forum_title_min_height',
						'type' => 'text',
						'default' => '320',
						'title' => esc_html__( 'Min Height', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter Forum title minimum height in px (Default is 320).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'forum_title_container_size',
						'type' => 'select',
						'title' => esc_html__( 'Container Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the container.', 'fildisi' ),
						'options' => $fildisi_eutf_container_size_selection,
						'default' => 'default',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'forum_title_content_size',
						'type' => 'select',
						'title' => esc_html__( 'Content Size', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the size of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_content_size_selection,
						'default' => 'large',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'forum_title_content_alignment',
						'type' => 'select',
						'title' => esc_html__( 'Content Alignment', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the alignemt of the content.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_extra,
						'default' => 'center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'forum_title_content_position',
						'type' => 'select',
						'title' => esc_html__( 'Content Position', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your position for the default Forum title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'forum_title_content_animation',
						'type' => 'select',
						'title' => esc_html__( 'Content Animation', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your animation for the default Forum title/description.', 'fildisi' ),
						'options' => $fildisi_eutf_animation_selection,
						'default' => 'fade-in',
						'validate' => 'not_empty',
					),
					array(
						'id'       => 'forum_title_bg_mode',
						'type'     => 'select',
						'title' => esc_html__( 'Background Mode', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the Forum Title Background, Color or Image.', 'fildisi' ),
						'options' => $fildisi_eutf_title_bg_mode_limited,
						'default'  => 'color',
					),
					array(
						'id'       => 'forum_title_bg_image',
						'type'     => 'media',
						'title' => esc_html__( 'Background Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Select a background image for the default Forum title.', 'fildisi' ),
						'required' => array( 'forum_title_bg_mode', 'equals', 'custom' ),
					),
					array(
						'id' => 'forum_title_bg_position',
						'type' => 'select',
						'title' => esc_html__( 'Background Position', 'fildisi' ),
						'options' => $fildisi_eutf_align_selection_full,
						'default' => 'center-center',
						'validate' => 'not_empty',
						'required' => array( 'forum_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'forum_title_pattern_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Pattern Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_pattern_selection,
						'default' => 'none',
						'required' => array( 'forum_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'forum_title_color_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Color Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection,
						'default' => 'dark',
						'required' => array( 'forum_title_bg_mode', '!=', 'color' ),
					),
					array(
						'id' => 'forum_title_opacity_overlay',
						'type' => 'select',
						'title' => esc_html__( 'Opacity Overlay', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0',
						'required' => array( 'forum_title_bg_mode', '!=', 'color' ),
					),
				)
			);


			$this->sections[] = array(
				'title' => esc_html__( 'Typography Options', 'fildisi' ),
				'id' => 'eut_redux_section_typography_options',
				'header' => '',
				'desc' => '',
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-font',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(),
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Header/Body', 'fildisi' ),
				'id' => 'eut_redux_section_typography_header',
				'header' => '',
				'desc' => esc_html__( 'Define your main typography settings.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-font',
				'submenu' => true,
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'   => 'info_body_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Main Body Fonts', 'fildisi' ),
					),
					array(
						'id' => 'body_font',
						'type' => 'typography',
						'title' => esc_html__( 'Body Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the body font properties.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height'=> true,
						'text-align'=> false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'default' => array(
							'font-size' => '14px',
							'line-height' => '28px',
							'font-family' => 'Hind',
							'font-weight' => '400',
							'letter-spacing' => '0px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'single_post_font',
						'type' => 'typography',
						'title' => esc_html__( 'Single Post & Product Body Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the single post & single product body font properties.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height'=> true,
						'text-align'=> false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'default' => array(
							'font-size' => '16px',
							'line-height' => '30px',
							'font-family' => 'Hind',
							'font-weight' => '300',
							'letter-spacing' => '0px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id'   => 'info_logo_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Logo as text Fonts', 'fildisi' ),
					),
					array(
						'id' => 'logo_font',
						'type' => 'typography',
						'title' => esc_html__( 'Logo Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the logo font properties.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height'=> false,
						'text-align'=> false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '36px',
							'font-weight' => '700',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id'   => 'info_menu_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Main Menu Fonts', 'fildisi' ),
					),
					array(
						'id' => 'main_menu_font',
						'type' => 'typography',
						'title' => esc_html__( 'Menu Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the menu font properties.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '11px',
							'font-weight' => '600',
							'text-transform' => 'uppercase',
							'letter-spacing' => '1.5px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'sub_menu_font',
						'type' => 'typography',
						'title' => esc_html__( 'Submenu Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the submenu font properties.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '11px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'description_menu_font',
						'type' => 'typography',
						'title' => esc_html__( 'Menu Description Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the Menu description font properties.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind',
							'font-size' => '11px',
							'font-weight' => '300',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id'   => 'info_hidden_menu_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Hidden Menu Fonts', 'fildisi' ),
					),
					array(
						'id' => 'hidden_menu_font',
						'type' => 'typography',
						'title' => esc_html__( 'Hidden Menu Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the hidden menu font properties.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'capitalize',
							'font-weight' => '600',
							'font-size' => '30px',
							'letter-spacing' => '-1px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'sub_hidden_menu_font',
						'type' => 'typography',
						'title' => esc_html__( 'Hidden Submenu Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the hidden submenu font properties.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '12px',
							'font-weight' => '500',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'description_hidden_menu_font',
						'type' => 'typography',
						'title' => esc_html__( 'Hidden Menu Description Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the hidden menu description font properties.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '11px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Headings (H1-H6)', 'fildisi' ),
				'id' => 'eut_redux_section_typography_headings',
				'header' => '',
				'desc' => esc_html__( 'Define your Typography Headings ( H1-H6 ).', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-font',
				'submenu' => true,
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'   => 'info_headers_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Headings Fonts', 'fildisi' ),
					),
					array(
						'id' => 'h1_font',
						'type' => 'typography',
						'title' => esc_html__( 'H1 Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the H1 font.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '60px',
							'line-height' => '72px',
							'letter-spacing' => '-2.5px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'h2_font',
						'type' => 'typography',
						'title' => esc_html__( 'H2 Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the H2 font.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '48px',
							'line-height' => '64px',
							'letter-spacing' => '-2px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'h3_font',
						'type' => 'typography',
						'title' => esc_html__( 'H3 Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the H3 font.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '38px',
							'line-height' => '56px',
							'letter-spacing' => '-1.5px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'h4_font',
						'type' => 'typography',
						'title' => esc_html__( 'H4 Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the H4 font.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '30px',
							'line-height' => '48px',
							'letter-spacing' => '-1px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'h5_font',
						'type' => 'typography',
						'title' => esc_html__( 'H5 Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the H5 font.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '20px',
							'line-height' => '38px',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'h6_font',
						'type' => 'typography',
						'title' => esc_html__( 'H6 Font', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the H6 font.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '14px',
							'line-height' => '32px',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Titles/Descriptions', 'fildisi' ),
				'id' => 'eut_redux_section_typography_titles',
				'header' => '',
				'desc' => esc_html__( 'Define your typography for titles/descriptions.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-font',
				'submenu' => true,
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'   => 'info_page_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Page/Blog Typography', 'fildisi' ),
					),
					array(
						'id' => 'page_title',
						'type' => 'typography',
						'title' => esc_html__( 'Page Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the default page titles and for the assigned blog page in case you have set a Static Page as blog page in Settings > Reading > Front Page Displays. (Archives / Categories / Tags overview pages)', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '48px',
							'line-height' => '64px',
							'letter-spacing' => '-2px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'page_description',
						'type' => 'typography',
						'title' => esc_html__( 'Page Description', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify font for the page description and for the description of the assigned blog page in case you have set a Static Page as blog page in Settings > Reading > Front Page Displays. (Archives / Categories / Tags overview pages)', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind',
							'font-size' => '24px',
							'line-height' => '36px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id'   => 'info_post_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Single Post Typography', 'fildisi' ),
					),
					array(
						'id' => 'post_simple_title',
						'type' => 'typography',
						'title' => esc_html__( 'Post Simple Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the single post simple title.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '36px',
							'line-height' => '56px',
							'font-weight' => '300',
							'text-transform' => 'none',
							'letter-spacing' => '0px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'post_title',
						'type' => 'typography',
						'title' => esc_html__( 'Post Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the single post titles.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '48px',
							'line-height' => '64px',
							'letter-spacing' => '-2px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'post_title_desc',
						'type' => 'typography',
						'title' => esc_html__( 'Post Description', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the single post description.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind',
							'font-size' => '24px',
							'line-height' => '36px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '1px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'post_title_meta',
						'type' => 'typography',
						'title' => esc_html__( 'Post Meta', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for meta ( categories ) in the post title section.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '12px',
							'line-height' => '24px',
							'font-weight' => '300',
							'text-transform' => 'none',
							'letter-spacing' => '1px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'post_title_extra_meta',
						'type' => 'typography',
						'title' => esc_html__( 'Post Extra Meta', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for extra meta ( Author/Date/Comments ) in the post title section.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '12px',
							'line-height' => '24px',
							'font-weight' => '400',
							'text-transform' => 'capitalize',
							'letter-spacing' => '1px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id'   => 'info_portfolio_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Portfolio Typography', 'fildisi' ),
					),
					array(
						'id' => 'portfolio_title',
						'type' => 'typography',
						'title' => esc_html__( 'Portfolio Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the default single portfolio titles.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=>false,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '48px',
							'line-height' => '64px',
							'letter-spacing' => '-2px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'portfolio_description',
						'type' => 'typography',
						'title' => esc_html__( 'Portfolio Description', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the default single portfolio description.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=>false,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind',
							'font-size' => '24px',
							'line-height' => '36px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id'   => 'info_forum_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'bbPress Forum Typography', 'fildisi' ),
					),
					array(
						'id' => 'forum_title',
						'type' => 'typography',
						'title' => esc_html__( 'Forum Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the forum titles.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=>false,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '48px',
							'line-height' => '60px',
							'font-weight' => '700',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id'   => 'info_woocommerce_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'WooCommerce Typography', 'fildisi' ),
					),
					array(
						'id' => 'product_simple_title',
						'type' => 'typography',
						'title' => esc_html__( 'WooCommerce Product Simple Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the product simple title', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=>false,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '38px',
							'line-height' => '48px',
							'letter-spacing' => '-2px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'product_short_description',
						'type' => 'typography',
						'title' => esc_html__( 'WooCommerce Product Short Description', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the product short description', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=>false,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind',
							'font-size' => '16px',
							'line-height' => '32px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'product_tax_title',
						'type' => 'typography',
						'title' => esc_html__( 'WooCommerce Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the product titles', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=>false,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '48px',
							'line-height' => '64px',
							'letter-spacing' => '-2px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'product_tax_description',
						'type' => 'typography',
						'title' => esc_html__( 'WooCommerce Title Description', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify font for the product title description', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=>false,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind',
							'font-size' => '24px',
							'line-height' => '36px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
										array(
						'id'   => 'info_events_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Events Calendar Typography', 'fildisi' ),
					),
					array(
						'id' => 'event_tax_title',
						'type' => 'typography',
						'title' => esc_html__( 'Events Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the event titles', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=>false,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '60px',
							'line-height' => '72px',
							'font-weight' => '700',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'event_tax_description',
						'type' => 'typography',
						'title' => esc_html__( 'Events Description', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify font for the events description', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=>false,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind Siliguri',
							'font-size' => '20px',
							'line-height' => '30px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '0px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Feature Section', 'fildisi' ),
				'id' => 'eut_redux_section_typography_feature_section',
				'header' => '',
				'desc' => esc_html__( 'Define your typography for feature section.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-font',
				'submenu' => true,
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'   => 'info_feature_typography_custom_height',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Feature Section Typography with Custom Height', 'fildisi' ),
					),
					array(
						'id' => 'feature_subheading_custom_font',
						'type' => 'typography',
						'title' => esc_html__( 'Sub Heading', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the sub-heading in the feature section.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '16px',
							'line-height' => '18px',
							'font-weight' => '400',
							'text-transform' => 'uppercase',
							'letter-spacing' => '1.5px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'feature_title_custom_font',
						'type' => 'typography',
						'title' => esc_html__( 'Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the title in the feature section.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'text-transform' => 'none',
							'font-weight' => '600',
							'font-size' => '48px',
							'line-height' => '64px',
							'letter-spacing' => '-2px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'feature_desc_custom_font',
						'type' => 'typography',
						'title' => esc_html__( 'Description', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the description in the feature section.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind',
							'font-size' => '24px',
							'line-height' => '36px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '0px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id'   => 'info_feature_typography_full',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Feature Section Typography with Fullscreen mode', 'fildisi' ),
					),
					array(
						'id' => 'feature_subheading_full_font',
						'type' => 'typography',
						'title' => esc_html__( 'Sub Heading', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the sub-heading in the feature section.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '16px',
							'line-height' => '18px',
							'font-weight' => '400',
							'text-transform' => 'uppercase',
							'letter-spacing' => '0px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'feature_title_full_font',
						'type' => 'typography',
						'title' => esc_html__( 'Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the title in the feature section.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '72px',
							'line-height' => '96px',
							'font-weight' => '300',
							'text-transform' => 'none',
							'letter-spacing' => '0px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'feature_desc_full_font',
						'type' => 'typography',
						'title' => esc_html__( 'Description', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the font for the  description in the feature section.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind',
							'font-size' => '24px',
							'line-height' => '36px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '0px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Extras', 'fildisi' ),
				'id' => 'eut_redux_section_typography_extras',
				'header' => '',
				'desc' => esc_html__( 'Define your typography for extra fields ( widgets, special texts, custom fonts .etc ).', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-font',
				'submenu' => true,
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'   => 'widgets_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Widgets Typography', 'fildisi' ),
					),
					array(
						'id' => 'widget_title',
						'type' => 'typography',
						'title' => esc_html__( 'Widgets Title', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the style for the widget titles.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '14px',
							'line-height' => '32px',
							'font-weight' => '600',
							'text-transform' => 'capitalize',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'widget_text',
						'type' => 'typography',
						'title' => esc_html__( 'Widgets Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the style for the widget texts.', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind',
							'font-size' => '14px',
							'line-height' => '28px',
							'font-weight' => '400',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id'   => 'info_special_typography',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Special Text Typography', 'fildisi' ),
					),
					array(
						'id' => 'leader_text',
						'type' => 'typography',
						'title' => esc_html__( 'Leader Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the style for the leader text. This is used in various elements (Text block, Testimonial...)', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Hind',
							'font-size' => '20px',
							'line-height' => '36px',
							'font-weight' => '300',
							'text-transform' => 'none',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'subtitle_text',
						'type' => 'typography',
						'title' => esc_html__( 'Subtitle Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the style for the subtitle text. This is used in various elements (Slogan Subtitle...)', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => true,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '16px',
							'line-height' => '24px',
							'font-weight' => '300',
							'text-transform' => 'uppercase',
							'letter-spacing' => '2px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'small_text',
						'type' => 'typography',
						'title' => esc_html__( 'Small Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the style for the small text. This is used in various elements (Tags, Post Meta...)', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '10px',
							'font-weight' => '400',
							'text-transform' => 'uppercase',
							'letter-spacing' => '1px',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'link_text',
						'type' => 'typography',
						'title' => esc_html__( 'Link Text', 'fildisi' ),
						'subtitle' => esc_html__( 'Specify the style for the link text. This is used in various elements (Buttons, Read More...)', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'font-family' => 'Poppins',
							'font-size' => '12px',
							'font-weight' => '600',
							'text-transform' => 'uppercase',
							'letter-spacing' => '',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id'   => 'info_custom_font_family',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Custom Font Family', 'fildisi' ),
					),
					array(
						'id' => 'custom_font_family_1',
						'type' => 'typography',
						'title' => esc_html__( 'Custom Font Family 1', 'fildisi' ),
						'subtitle' => esc_html__( 'This is used in various elements (Title, Slogan, Callout...)', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'font-size' => false,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'text-transform' => 'none',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'custom_font_family_2',
						'type' => 'typography',
						'title' => esc_html__( 'Custom Font Family 2', 'fildisi' ),
						'subtitle' => esc_html__( 'This is used in various elements (Title, Slogan, Callout...)', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'font-size' => false,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'text-transform' => 'none',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'custom_font_family_3',
						'type' => 'typography',
						'title' => esc_html__( 'Custom Font Family 3', 'fildisi' ),
						'subtitle' => esc_html__( 'This is used in various elements (Title, Slogan, Callout...)', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'font-size' => false,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'text-transform' => 'none',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
					array(
						'id' => 'custom_font_family_4',
						'type' => 'typography',
						'title' => esc_html__( 'Custom Font Family 4', 'fildisi' ),
						'subtitle' => esc_html__( 'This is used in various elements (Title, Slogan, Callout...)', 'fildisi' ),
						'google' => $fildisi_is_google,
						'line-height' => false,
						'text-align' => false,
						'letter-spacing' => true,
						'font-size' => false,
						'color'=> false,
						'preview'=> true,
						'text-transform' => true,
						'default' => array(
							'text-transform' => 'none',
						),
						'fonts' => $fildisi_eutf_std_fonts,
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Responsive Typography', 'fildisi' ),
				'id' => 'eut_redux_section_responsive_typography',
				'header' => '',
				'desc' => '',
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-fontsize',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(),
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Responsive Headings', 'fildisi' ),
				'id' => 'eut_redux_section_responsive_typography_headings',
				'header' => '',
				'desc' => esc_html__( 'Define your responsiveness for the Typography Headings ( H1-H6 ).', 'fildisi' ),
				'submenu' => true,
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-fontsize',
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'   => 'info_typography_small_desktop_headings',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Small Desktop', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (min-width: 1201px) and (max-width: 1440px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_small_desktop_threshold_headings',
						'type' => 'text',
						'default' => '20',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 20).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_small_desktop_ratio_headings',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '1',
					),
					array(
						'id'   => 'info_typography_tablet_landscape_headings',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Tablet Landscape', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (min-width: 960px) and (max-width: 1200px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_tablet_landscape_threshold_headings',
						'type' => 'text',
						'default' => '20',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 20).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_tablet_landscape_ratio_headings',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '0.9',
					),
					array(
						'id'   => 'info_typography_tablet_portrait_headings',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Tablet Portrait', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (min-width: 768px) and (max-width: 959px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_tablet_portrait_threshold_headings',
						'type' => 'text',
						'default' => '20',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 20).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_tablet_portrait_ratio_headings',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '0.8',
					),
					array(
						'id'   => 'info_typography_mobile_headings',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Mobile', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (max-width: 767px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_mobile_threshold_headings',
						'type' => 'text',
						'default' => '20',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 20).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_mobile_ratio_headings',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '0.7',
					),
				),
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Responsive First Group', 'fildisi' ),
				'id' => 'eut_redux_section_responsive_typography_first_group',
				'header' => '',
				'desc' => esc_html__( 'Define your responsiveness Typography for the following elements: Page Title, Post Title, Portfolio Title, Product Title, Feature Section Title, Feature Section Description with Fullscreen mode.', 'fildisi' ),
				'submenu' => true,
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-fontsize',
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'   => 'info_typography_small_desktop',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Small Desktop', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (min-width: 1201px) and (max-width: 1440px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_small_desktop_threshold',
						'type' => 'text',
						'default' => '20',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 20).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_small_desktop_ratio',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '1',
					),
					array(
						'id'   => 'info_typography_tablet_landscape',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Tablet Landscape', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (min-width: 960px) and (max-width: 1200px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_tablet_landscape_threshold',
						'type' => 'text',
						'default' => '20',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 20).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_tablet_landscape_ratio',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '0.9',
					),
					array(
						'id'   => 'info_typography_tablet_portrait',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Tablet Portrait', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (min-width: 768px) and (max-width: 959px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_tablet_portrait_threshold',
						'type' => 'text',
						'default' => '20',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 20).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_tablet_portrait_ratio',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '0.85',
					),
					array(
						'id'   => 'info_typography_mobile',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Mobile', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (max-width: 767px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_mobile_threshold',
						'type' => 'text',
						'default' => '18',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 18).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_mobile_ratio',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '0.6',
					),
				),
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Responsive Second Group', 'fildisi' ),
				'id' => 'eut_redux_section_responsive_typography_second_group',
				'header' => '',
				'desc' => esc_html__( 'Define your responsiveness Typography for the following elements: Page Description, Post Description, Portfolio Description, Product Description, Feature Section Description with Custom Height, Leader Text, Subtitle Text, Link Texts, Menus and Buttons.', 'fildisi' ),
				'submenu' => true,
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-fontsize',
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'   => 'info_typography_small_desktop2',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Small Desktop', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (min-width: 1201px) and (max-width: 1440px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_small_desktop_threshold2',
						'type' => 'text',
						'default' => '14',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 14).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_small_desktop_ratio2',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '1',
					),
					array(
						'id'   => 'info_typography_tablet_landscape2',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Tablet Landscape', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (min-width: 960px) and (max-width: 1200px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_tablet_landscape_threshold2',
						'type' => 'text',
						'default' => '14',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 14).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_tablet_landscape_ratio2',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '0.9',
					),
					array(
						'id'   => 'info_typography_tablet_portrait2',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Tablet Portrait', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (min-width: 768px) and (max-width: 959px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_tablet_portrait_threshold2',
						'type' => 'text',
						'default' => '14',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 14).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_tablet_portrait_ratio2',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '0.8',
					),
					array(
						'id'   => 'info_typography_mobile2',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Mobile', 'fildisi' ),
						'subtitle'=> esc_html__( 'Screen (max-width: 767px)', 'fildisi' ),
					),
					array(
						'id' => 'typography_mobile_threshold2',
						'type' => 'text',
						'default' => '13',
						'title' => esc_html__( 'Size Threshold', 'fildisi' ),
						'subtitle' => esc_html__( 'Size threshold in px (Default is 13).', 'fildisi' ),
						'validate' => 'numeric',
					),
					array(
						'id' => 'typography_mobile_ratio2',
						'type' => 'select',
						'title' => esc_html__( 'Size Ratio', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select percentage of the original size.', 'fildisi' ),
						'options' => $fildisi_eutf_ratio_selection,
						'default' => '0.8',
					),
				),
			);

			$this->sections[] = array(
				'icon' => 'el-icon-file-edit',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'CSS / JS Options', 'fildisi' ),
				'id' => 'eut_redux_section_css_js_options',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'css_code',
						'type' => 'ace_editor',
						'title' => esc_html__( 'CSS Code', 'fildisi' ),
						'subtitle' => esc_html__( 'Paste your CSS code here.', 'fildisi' ),
						'mode' => 'css',
						'theme' => 'monokai',
						'desc' => '',
						'default' => ''
					),
					array(
						'id' => 'custom_js',
						'type' => 'ace_editor',
						'mode' => 'javascript',
						'theme' => 'chrome',
						'title' => esc_html__( 'JS Code', 'fildisi' ),
						'subtitle' => esc_html__( 'Add your custom JavaScript code here. Please do not include any script tags.', 'fildisi' ),
						'desc' => '',
						'default' => ''
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Style Options', 'fildisi' ),
				'id' => 'eut_redux_section_style_options',
				'desc' => sprintf( wp_kses( __( 'To customize the color scheme, please use the <a href="%s">Live Color Customizer</a>.', 'fildisi' ), array( 'br' => array(), 'a' => array( 'href' => true, 'target' => true ) ) ), esc_url( admin_url('/customize.php') ) ),
				'customizer' => false,
				'fields' => array(
					array(
						'id'    => 'info_style_color_preset',
						'type'  => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Skin Presets', 'fildisi' ),
					),
					array(
						'id'        => 'skin_preset',
						'type'      => 'image_select',
						'presets'   => true,
						'title'     => esc_html__( 'Select your Skin', 'fildisi' ),
						'default'   => 0,
						'subtitle'  => esc_html__( 'Use the preset as you wish in order to customize your color scheme.', 'fildisi' ),
						'options'   => array(
							'palette-1'  => array('img' => get_template_directory_uri() . '/includes/images/skins/palette-1.png', 'presets' => $fildisi_eutf_skin_palette_1 ),
							'palette-2'  => array('img' => get_template_directory_uri() . '/includes/images/skins/palette-2.png', 'presets' => $fildisi_eutf_skin_palette_2 ),
							'palette-3'  => array('img' => get_template_directory_uri() . '/includes/images/skins/palette-3.png', 'presets' => $fildisi_eutf_skin_palette_3 ),
							'palette-4'  => array('img' => get_template_directory_uri() . '/includes/images/skins/palette-4.png', 'presets' => $fildisi_eutf_skin_palette_4 ),
							'palette-5'  => array('img' => get_template_directory_uri() . '/includes/images/skins/palette-5.png', 'presets' => $fildisi_eutf_skin_palette_5 ),
							'palette-6'  => array('img' => get_template_directory_uri() . '/includes/images/skins/palette-6.png', 'presets' => $fildisi_eutf_skin_palette_6 ),
						),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Top Bar', 'fildisi' ),
				'id' => 'eut_redux_section_colors_top_bar',
				'desc' => esc_html__( 'Set your color preferences for the TopBar (you will see the changes in the live preview only if TopBar is enabled).', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
					//Top Bar Color Settings
					array(
						'id'          => 'top_bar_bg_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Top Bar Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your Top Bar.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'top_bar_font_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Top Bar Font Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Font color for your Top Bar.', 'fildisi' ),
						'default'     => '#777777',
						'transparent' => false,
					),
					array(
						'id'          => 'top_bar_link_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Top Bar Link Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Link color for your Top Bar.', 'fildisi' ),
						'default'     => '#777777',
						'transparent' => false,
					),
					array(
						'id'          => 'top_bar_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Top Bar Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Hover color for your Top Bar.', 'fildisi' ),
						'default'     => '#F95F51',
						'transparent' => false,
					),
				)
			);
			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Header / Menu', 'fildisi' ),
				'id' => 'eut_redux_section_colors_header_menu',
				'desc' => esc_html__( 'Set your color preferences for the Header and Menu.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
				),
			);
			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Default Header', 'fildisi' ),
				'id' => 'eut_redux_section_colors_default_header',
				'desc' => esc_html__( 'Set your color preferences for the Default Header. Keep in mind that the basic settings for the Default Header are in Theme Options > Header Options.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					//Default Header Color Settings
					array(
						'id'       => 'default_header_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a background color for the header.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id' => 'default_header_background_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Background Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the background of the header.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '1',
					),
					array(
						'id'       => 'default_header_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a border color for the header.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id' => 'default_header_border_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Border Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the border of the header.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '0',
					),
					//Menu Color Settings
					array(
						'id'          => 'default_header_menu_text_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Text Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the menu text.', 'fildisi' ),
						'default'     => '#000000',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_menu_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Text Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover menu text.', 'fildisi' ),
						'default'     => '#F95F51',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_menu_type_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Type Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the menu type. This will affect if you select the button or underline type.', 'fildisi' ),
						'default'     => '#eef1f6',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_menu_type_color_hover',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Type Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover menu type. This will affect if you select the button or underline type.', 'fildisi' ),
						'default'     => '#F95F51',
						'transparent' => false,
					),
					//Sub Menu Color Settings
					array(
						'id'          => 'default_header_submenu_bg_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a background color for the sub menu.', 'fildisi' ),
						'default'     => '#171a1d',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_submenu_text_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Text Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the sub menu text.', 'fildisi' ),
						'default'     => '#777777',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_submenu_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Text Hover/Active Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover or active sub menu text.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_submenu_text_bg_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Hover/Active Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a background color for the hover or active sub menu.', 'fildisi' ),
						'default'     => '#171a1d',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_submenu_column_text_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Column Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the sub menu column titles ( Used in mega menu ).', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_submenu_column_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Column Titles Hover/Active Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover or active sub menu column titles ( Used in mega menu ).', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_submenu_border_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Border Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a border color for the sub menu.', 'fildisi' ),
						'default'     => '#333638',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_label_bg_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Label Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a background color for the label.', 'fildisi' ),
						'default'     => '#454545',
						'transparent' => false,
					),
					array(
						'id'          => 'default_header_label_text_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Label Text Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a text color for the label.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Logo on Top Header', 'fildisi' ),
				'id' => 'eut_redux_section_colors_logotop_header',
				'desc' => esc_html__( 'Set your color preferences for the Logo on top Header. Keep in mind that the basic settings for the Logo on top Header are in Theme Options > Header Options.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					//Logo on top Header Color Settings
					array(
						'id'       => 'logo_top_header_logo_area_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Logo Area Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a background color for the logo area.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id' => 'logo_top_header_logo_area_background_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Logo Area Background Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the background of the logo area.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '1',
					),
					array(
						'id'       => 'logo_top_header_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a border color for the header.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id' => 'logo_top_header_border_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Border Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the border of the header.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '0.10',
					),
					array(
						'id'       => 'logo_top_header_menu_area_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Menu Area Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a background color for the menu area.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id' => 'logo_top_header_menu_area_background_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Menu Area Background Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the background of the menu area.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '1',
					),
					//Menu Color Settings
					array(
						'id'          => 'logo_top_header_menu_text_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Text Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the menu text.', 'fildisi' ),
						'default'     => '#000000',
						'transparent' => false,
					),
					array(
						'id'          => 'logo_top_header_menu_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Text Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover menu text.', 'fildisi' ),
						'default'     => '#F95F51',
						'transparent' => false,
					),
					array(
						'id'          => 'logo_top_header_menu_type_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Type Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the menu type. This will affect if you select the button or underline type.', 'fildisi' ),
						'default'     => '#eef1f6',
						'transparent' => false,
					),
					array(
						'id'          => 'logo_top_header_menu_type_color_hover',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Type Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover menu type. This will affect if you select the button or underline type.', 'fildisi' ),
						'default'     => '#F95F51',
						'transparent' => false,
					),
					//Sub Menu Color Settings
					array(
						'id'          => 'logo_top_header_submenu_bg_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a background color for the sub menu.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'logo_top_header_submenu_text_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Text Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the sub menu text.', 'fildisi' ),
						'default'     => '#777777',
						'transparent' => false,
					),
					array(
						'id'          => 'logo_top_header_submenu_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Text Hover/Active Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover or active sub menu text.', 'fildisi' ),
						'default'     => '#F95F51',
						'transparent' => false,
					),
					array(
						'id'          => 'logo_top_header_submenu_text_bg_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Hover/Active Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a background color for the hover or active sub menu.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'logo_top_header_submenu_column_text_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Column Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the sub menu column titles ( Used in mega menu ).', 'fildisi' ),
						'default'     => '#000000',
						'transparent' => false,
					),
					array(
						'id'          => 'logo_top_header_submenu_column_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Column Titles Hover/Active Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover or active sub menu column titles ( Used in mega menu ).', 'fildisi' ),
						'default'     => '#F95F51',
						'transparent' => false,
					),
					array(
						'id'          => 'logo_top_header_submenu_border_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Border Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a border color for the sub menu.', 'fildisi' ),
						'default'     => '#eef1f6',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Side Header', 'fildisi' ),
				'id' => 'eut_redux_section_colors_side_header',
				'desc' => esc_html__( 'Set your color preferences for the Side Header. Keep in mind that the basic settings for the Side Header are in Theme Options > Header Options.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					//Logo on top Header Color Settings
					array(
						'id'       => 'side_header_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a background color for the header.', 'fildisi' ),
						'default'  => '#f7f7f7',
						'transparent' => false,
					),
					array(
						'id' => 'side_header_background_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Background Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the background of the header.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '1',
					),
					//Menu Color Settings
					array(
						'id'          => 'side_header_menu_text_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Text Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the menu text.', 'fildisi' ),
						'default'     => '#000000',
						'transparent' => false,
					),
					array(
						'id'          => 'side_header_menu_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Text Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover menu text.', 'fildisi' ),
						'default'     => '#F95F51',
						'transparent' => false,
					),
					//Sub Menu Color Settings
					array(
						'id'          => 'side_header_submenu_text_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Text Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the sub menu text.', 'fildisi' ),
						'default'     => '#000000',
						'transparent' => false,
					),
					array(
						'id'          => 'side_header_submenu_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Sub Menu Text Hover/Active Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover or active sub menu text.', 'fildisi' ),
						'default'     => '#F95F51',
						'transparent' => false,
					),
					array(
						'id'          => 'side_header_border_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a border color.', 'fildisi' ),
						'default'     => '#323232',
						'transparent' => false,
					),
					array(
						'id' => 'side_header_border_opacity',
						'type' => 'select',
						'title' => esc_html__('Border Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the borders.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '0.10',
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Sticky Header', 'fildisi' ),
				'id' => 'eut_redux_section_colors_sticky_header',
				'desc' => esc_html__( 'Set your color preferences for the Sticky Header. You can enable/disable, select the type and logo for the sticky header in Theme Options > Header Options > Sticky Header Options.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					//Sticky Header Color Settings
					array(
						'id'       => 'header_sticky_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a background color for the header.', 'fildisi' ),
						'default'  => '#101215',
						'transparent' => false,
					),
					array(
						'id' => 'header_sticky_background_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Background Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the background of the header.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '1',
					),
					array(
						'id'       => 'header_sticky_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a border color for the sticky header.', 'fildisi' ),
						'default'  => '#eef1f6',
						'transparent' => false,
					),
					array(
						'id' => 'header_sticky_border_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Border Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the border of the sticky header.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '0',
					),
					//Menu Color Settings
					array(
						'id'          => 'sticky_menu_text_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Text Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the menu text.', 'fildisi' ),
						'default'     => '#9c9c9c',
						'transparent' => false,
					),
					array(
						'id'          => 'sticky_menu_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Text Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover menu text.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'header_sticky_menu_type_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Type Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the menu type. This will affect if you select the button or underline type.', 'fildisi' ),
						'default'     => '#eef1f6',
						'transparent' => false,
					),
					array(
						'id'          => 'header_sticky_menu_type_color_hover',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Type Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the menu type. This will affect if you select the button or underline type.', 'fildisi' ),
						'default'     => '#eef1f6',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Light Header', 'fildisi' ),
				'id' => 'eut_redux_section_colors_light_header',
				'desc' => esc_html__( 'Notice that the Light Header is the transparent header with light fonts that theme provides in order to combine with the overlapping option everywhere in your site.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					//Menu Color Settings
					array(
						'id'          => 'light_menu_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Text Hover/Active Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover/active menu text.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'light_menu_type_color_hover',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Type Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the menu type. This will affect if you select the button or underline type.', 'fildisi' ),
						'default'     => '#eef1f6',
						'transparent' => false,
					),
					array(
						'id'       => 'light_header_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a border color for the light header.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id' => 'light_header_border_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Border Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the border of the light header.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '0',
					),
				)
			);
			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Dark Header', 'fildisi' ),
				'id' => 'eut_redux_section_colors_dark_header',
				'desc' => esc_html__( 'Notice that the Dark Header is the transparent header with dark fonts that theme provides in order to combine with the overlapping option everywhere in your site.', 'fildisi' ),
				'submenu' => false,
				'eut_colors' => true,
				'panel' => false,
				'subsection' => true,
				'fields' => array(
					//Menu Color Settings
					array(
						'id'          => 'dark_menu_text_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Text Hover/Active Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the hover/active menu text.', 'fildisi' ),
						'default'     => '#000000',
						'transparent' => false,
					),
					array(
						'id'          => 'dark_menu_type_color_hover',
						'type'        => 'color',
						'title'       => esc_html__( 'Menu Type Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the menu type. This will affect if you select the button or underline type.', 'fildisi' ),
						'default'     => '#eef1f6',
						'transparent' => false,
					),
					array(
						'id'       => 'dark_header_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a border color for the dark header.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id' => 'dark_header_border_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Border Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the border of the dark header.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '0',
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Responsive Header', 'fildisi' ),
				'id' => 'eut_redux_section_colors_responsive_header',
				'desc' => esc_html__( 'Set your color preferences for the Responsive Header. These settings will affect when the responsive header appears, below 1024px.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id'       => 'responsive_header_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a background color for the header.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id' => 'responsive_header_background_opacity',
						'type' => 'select',
						'title' => esc_html__('Background Color Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the background of the responsive header.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '1',
					),
					array(
						'id'          => 'responsive_header_elements_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Header Elements Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the header elements.', 'fildisi' ),
						'default'     => '#838383',
						'transparent' => false,
					),
					array(
						'id'          => 'responsive_header_elements_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Header Elements Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for the header elements hover.', 'fildisi' ),
						'default'     => '#000000',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Responsive / Hidden Menu', 'fildisi' ),
				'id' => 'eut_redux_section_colors_responsive_menu',
				'desc' => esc_html__( 'Set your color preferences for the Responsive Menu. These settings will also affect for the hidden menu.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id'       => 'responsive_menu_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a background color for the responsive menu.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'       => 'responsive_menu_link_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Link Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the links of the responsive menu.', 'fildisi' ),
						'default'  => '#777777',
						'transparent' => false,
					),
					array(
						'id'       => 'responsive_menu_link_hover_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Hover Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the hovers of the responsive menu.', 'fildisi' ),
						'default'  => '#F95F51',
						'transparent' => false,
					),
					array(
						'id'       => 'responsive_menu_close_btn_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Close Button Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the close button of the responsive menu.', 'fildisi' ),
						'default'  => '#777777',
						'transparent' => false,
					),
					array(
						'id'       => 'responsive_menu_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the borders of the responsive menu.', 'fildisi' ),
						'default'  => '#eef1f6',
						'transparent' => false,
					),
					array(
						'id'       => 'responsive_menu_overflow_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Overflow Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a background color for the overflow area, when responsive menu opens.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id' => 'responsive_menu_overflow_background_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Overflow Background Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the overflow area, when responsive menu opens.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '0.90',
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Anchor Menu', 'fildisi' ),
				'id' => 'eut_redux_section_colors_anchor_menu',
				'header' => '',
				'desc' => esc_html__( 'Set your color preferences for the Anchor Menu in case you use one in any of your pages, posts items.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
					array(
						'id'       => 'page_anchor_menu_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Background color for the Anchor Menu.', 'fildisi' ),
						'default'  => '#171a1d',
						'transparent' => false,
					),
					array(
						'id'       => 'page_anchor_menu_text_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Font Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Font color for the Anchor Menu.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'       => 'page_anchor_menu_text_hover_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Hover Text Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Hover color for the Anchor Menu.', 'fildisi' ),
						'default'  => '#F95F51',
						'transparent' => false,
					),
					array(
						'id'       => 'page_anchor_menu_background_hover_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Hover Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Hover background color for the Anchor Menu.', 'fildisi' ),
						'default'  => '#171a1d',
						'transparent' => false,
					),
					array(
						'id'       => 'page_anchor_menu_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Border color for the Anchor Menu.', 'fildisi' ),
						'default'  => '#333638',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Main Content', 'fildisi' ),
				'id' => 'eut_redux_section_colors_main_content',
				'header' => '',
				'desc' => esc_html__( 'Set your color preferences for the main content area of your site.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
					array(
						'id'       => 'main_content_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a background color.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,

					),
					array(
						'id'       => 'body_heading_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Headings Text Color (h1-h6)', 'fildisi' ),
						'subtitle'    => esc_html__( 'Pick a color for headings text.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id'       => 'body_text_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Text Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the text.', 'fildisi' ),
						'default'  => '#888888',
						'transparent' => false,
					),
					array(
						'id'       => 'body_text_link_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Link Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the links.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id'       => 'body_text_link_hover_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Hover Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for hover text.', 'fildisi' ),
						'default'  => '#F95F51',
						'transparent' => false,
					),
					array(
						'id'       => 'body_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a border color.', 'fildisi' ),
						'default'  => '#e9e9e9',
						'transparent' => false,
					),
					array(
						'id'       => 'widget_title_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Widget Title Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for widget titles.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id'       => 'body_primary_1_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Primary 1 Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for primary 1.', 'fildisi' ),
						'default'  => '#F95F51',
						'transparent' => false,
					),
					array(
						'id'       => 'body_primary_2_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Primary 2 Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for primary 2.', 'fildisi' ),
						'default'  => '#02C4F7',
						'transparent' => false,
					),
					array(
						'id'       => 'body_primary_3_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Primary 3 Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for primary 3.', 'fildisi' ),
						'default'  => '#91DF43',
						'transparent' => false,
					),
					array(
						'id'       => 'body_primary_4_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Primary 4 Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for primary 4.', 'fildisi' ),
						'default'  => '#411992',
						'transparent' => false,
					),
					array(
						'id'       => 'body_primary_5_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Primary 5 Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for primary 5.', 'fildisi' ),
						'default'  => '#355880',
						'transparent' => false,
					),
					array(
						'id'       => 'body_primary_6_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Primary 6 Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for primary 6.', 'fildisi' ),
						'default'  => '#db5111',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Blog /Post', 'fildisi' ),
				'id' => 'eut_redux_section_colors_blog_post',
				'desc' => esc_html__( 'Set your color preferences for your Blog / Post.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
				),
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Blog Title', 'fildisi' ),
				'id' => 'eut_redux_section_colors_blog_title',
				'header' => '',
				'desc' => esc_html__( 'Set the background color for the blog title area and the color of the blog title. These settings will only affect in case you have set a Static Page as blog page in Settings > Reading > Front Page Displays. Also these settings will also affect on Archives / Categories / Tags overview pages.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'blog_title_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your blog title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'blog_title_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#f7f7f7',
						'required' => array( 'blog_title_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'blog_title_content_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Content Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background Color for your title content.', 'fildisi' ),
						'options' => $fildisi_eutf_bg_color_selection_extra,
						'default' => 'none',
					),
					array(
						'id'       => 'blog_title_content_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Content Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'blog_title_content_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'blog_title_color',
						'type' => 'select',
						'title' => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your blog title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'blog_title_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#000000',
						'required' => array( 'blog_title_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'blog_description_color',
						'type' => 'select',
						'title' => esc_html__( 'Description Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your blog description.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'blog_description_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Description Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#9B9B9B',
						'required' => array( 'blog_description_color', 'equals', 'custom' ),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Post Title', 'fildisi' ),
				'id' => 'eut_redux_section_colors_post_title',
				'header' => '',
				'desc' => esc_html__( 'Set the background color for the single post title area and the color of the post title. Notice that you can disable it, override it or even use the feature section when you edit your single posts.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'post_title_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your post title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'post_title_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#f7f7f7',
						'required' => array( 'post_title_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'post_title_content_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Content Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background Color for your title content.', 'fildisi' ),
						'options' => $fildisi_eutf_bg_color_selection_extra,
						'default' => 'none',
					),
					array(
						'id'       => 'post_title_content_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Content Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'post_title_content_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'post_subheading_color',
						'type' => 'select',
						'title' => esc_html__( 'Categories/Meta Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your post categories/meta.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'post_subheading_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Meta Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'post_subheading_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'post_title_color',
						'type' => 'select',
						'title' => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your post title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'post_title_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#000000',
						'required' => array( 'post_title_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'post_description_color',
						'type' => 'select',
						'title' => esc_html__( 'Description Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your post description.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'post_description_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Description Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#9B9B9B',
						'required' => array( 'post_description_color', 'equals', 'custom' ),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Post Bar', 'fildisi' ),
				'id' => 'eut_redux_section_colors_post_bar',
				'header' => '',
				'desc' => esc_html__( 'Set the colors for the navigation - socials area in the single post.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id'       => 'post_bar_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Background color for the post bar.', 'fildisi' ),
						'default'  => '#f7f7f7',
						'transparent' => false,
					),
					array(
						'id'       => 'post_bar_nav_title_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Navigation Titles Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Color for the Navigation Titles.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id'       => 'post_bar_arrow_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Navigation Arrows Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Color for the Navigation Arrows.', 'fildisi' ),
						'default'  => '#d3d3d3',
						'transparent' => false,
					),
					array(
						'id'       => 'post_bar_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Border color for the post bar.', 'fildisi' ),
						'default'  => '#f7f7f7',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Page', 'fildisi' ),
				'id' => 'eut_redux_section_colors_page',
				'desc' => esc_html__( 'Set your color preferences for your page.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
				),
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Page Title', 'fildisi' ),
				'id' => 'eut_redux_section_colors_page_title',
				'header' => '',
				'desc' => esc_html__( 'Set the background color for the predefined page title area and the color of the page title. Notice that you can disable it, override it or even use the feature section when you edit your single pages.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'page_title_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your page title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'page_title_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#f7f7f7',
						'required' => array( 'page_title_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'page_title_content_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Content Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background Color for your title content.', 'fildisi' ),
						'options' => $fildisi_eutf_bg_color_selection_extra,
						'default' => 'none',
					),
					array(
						'id'       => 'page_title_content_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Content Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'page_title_content_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'page_title_color',
						'type' => 'select',
						'title' => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your page title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'page_title_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#000000',
						'required' => array( 'page_title_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'page_description_color',
						'type' => 'select',
						'title' => esc_html__( 'Description Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your page description.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'page_description_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Description Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#9B9B9B',
						'required' => array( 'page_description_color', 'equals', 'custom' ),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Search Page', 'fildisi' ),
				'id' => 'eut_redux_section_colors_search_page',
				'desc' => esc_html__( 'Set your color preferences for your search page.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
				),
			);
			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Search Page Title', 'fildisi' ),
				'id' => 'eut_redux_section_colors_search_page_title',
				'header' => '',
				'desc' => esc_html__( 'Set the background color for the search page title area and the color of the page title.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'search_page_title_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your page title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'search_page_title_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#f7f7f7',
						'required' => array( 'search_page_title_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'search_page_title_content_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Content Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background Color for your title content.', 'fildisi' ),
						'options' => $fildisi_eutf_bg_color_selection_extra,
						'default' => 'none',
					),
					array(
						'id'       => 'search_page_title_content_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Content Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'search_page_title_content_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'search_page_title_color',
						'type' => 'select',
						'title' => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your page title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'search_page_title_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#000000',
						'required' => array( 'search_page_title_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'search_page_description_color',
						'type' => 'select',
						'title' => esc_html__( 'Description Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your page description.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'search_page_description_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Description Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#9B9B9B',
						'required' => array( 'search_page_description_color', 'equals', 'custom' ),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Portfolio', 'fildisi' ),
				'id' => 'eut_redux_section_colors_portfolio',
				'desc' => esc_html__( 'Set your color preferences for your portfolio.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
				),
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Portfolio Title', 'fildisi' ),
				'id' => 'eut_redux_section_colors_portfolio_title',
				'header' => '',
				'desc' => esc_html__( 'Set the background color for the single portfolio title area and the color of the portfolio title and description. Notice that you can disable it, override it or even use the feature section when you edit your single portfolio items.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'portfolio_title_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your portfolio title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'portfolio_title_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#f7f7f7',
						'required' => array( 'portfolio_title_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'portfolio_title_content_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Content Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background Color for your title content.', 'fildisi' ),
						'options' => $fildisi_eutf_bg_color_selection_extra,
						'default' => 'none',
					),
					array(
						'id'       => 'portfolio_title_content_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Content Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'portfolio_title_content_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'portfolio_title_color',
						'type' => 'select',
						'title' => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your portfolio title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'portfolio_title_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#000000',
						'required' => array( 'portfolio_title_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'portfolio_description_color',
						'type' => 'select',
						'title' => esc_html__( 'Description Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your portfolio description.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'portfolio_description_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Description Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#9B9B9B',
						'required' => array( 'portfolio_description_color', 'equals', 'custom' ),
					),
				)
			);
			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Portfolio Bar', 'fildisi' ),
				'id' => 'eut_redux_section_colors_portfolio_bar',
				'header' => '',
				'desc' => esc_html__( 'Set the colors for the navigation - socials area in the single portfolio.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id'       => 'portfolio_bar_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Background color for the portfolio bar.', 'fildisi' ),
						'default'  => '#f7f7f7',
						'transparent' => false,
					),
					array(
						'id'       => 'portfolio_bar_nav_title_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Navigation Titles Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Color for the Navigation Titles.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id'       => 'portfolio_bar_arrow_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Navigation Arrows Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Color for the Navigation Arrows.', 'fildisi' ),
						'default'  => '#d3d3d3',
						'transparent' => false,
					),
					array(
						'id'       => 'portfolio_bar_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Portfolio Bar Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Border color for the portfolio Bar.', 'fildisi' ),
						'default'  => '#f7f7f7',
						'transparent' => false,
					),
				)
			);
			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Shop/Product', 'fildisi' ),
				'id' => 'eut_redux_section_colors_shop_product',
				'desc' => esc_html__( 'Set your color preferences for your Shop/Product.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
				),
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Product Taxonomy Title', 'fildisi' ),
				'id' => 'eut_redux_section_colors_product_tax_title',
				'header' => '',
				'desc' => esc_html__( 'Set the background color for the product taxonomies title area and the color of the product taxonomies title and description.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'product_tax_title_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'product_tax_title_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#000000',
						'required' => array( 'product_tax_title_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'product_tax_title_content_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Content Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background Color for your title content.', 'fildisi' ),
						'options' => $fildisi_eutf_bg_color_selection_extra,
						'default' => 'none',
					),
					array(
						'id'       => 'product_tax_title_content_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Content Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'product_tax_title_content_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'product_tax_title_color',
						'type' => 'select',
						'title' => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'light',
					),
					array(
						'id'       => 'product_tax_title_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'product_tax_title_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'product_tax_description_color',
						'type' => 'select',
						'title' => esc_html__( 'Description Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your description.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'product_tax_description_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Description Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'product_tax_description_color', 'equals', 'custom' ),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Product Title', 'fildisi' ),
				'id' => 'eut_redux_section_colors_product_title',
				'header' => '',
				'desc' => esc_html__( 'Set the background color for the single product title area and the color of the product title and description. Notice that you can disable it, override it or even use the feature section when you edit your single product items.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'product_title_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'product_title_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#f7f7f7',
						'required' => array( 'product_title_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'product_title_content_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Content Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background Color for your title content.', 'fildisi' ),
						'options' => $fildisi_eutf_bg_color_selection_extra,
						'default' => 'none',
					),
					array(
						'id'       => 'product_title_content_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Content Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'product_title_content_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'product_title_color',
						'type' => 'select',
						'title' => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'product_title_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'product_title_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'product_description_color',
						'type' => 'select',
						'title' => esc_html__( 'Description Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your description.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'product_description_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Description Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#9B9B9B',
						'required' => array( 'product_description_color', 'equals', 'custom' ),
					),
				)
			);
			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Product Area', 'fildisi' ),
				'id' => 'eut_redux_section_colors_product_area',
				'header' => '',
				'desc' => esc_html__( 'Set the colors for the single product content area. Notice that you can override it when you edit your single product items.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id'          => 'product_area_bg_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your Product Area.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'product_area_headings_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Headings Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Headings color for your Product Area.', 'fildisi' ),
						'default'     => '#000000',
						'transparent' => false,
					),
					array(
						'id'          => 'product_area_font_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Font Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Font color for your Product Area.', 'fildisi' ),
						'default'     => '#999999',
						'transparent' => false,
					),
					array(
						'id'          => 'product_area_link_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Link Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Link color for your Product Area.', 'fildisi' ),
						'default'     => '#FF7D88',
						'transparent' => false,
					),
					array(
						'id'          => 'product_area_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Hover color for your Product Area.', 'fildisi' ),
						'default'     => '#000000',
						'transparent' => false,
					),
					array(
						'id'          => 'product_area_border_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Border color for your Product Area.', 'fildisi' ),
						'default'     => '#e0e0e0',
						'transparent' => false,
					),
					array(
						'id'=>'product_area_button_color',
						'type' => 'select',
						'title' => esc_html__( 'Button Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the color of your Product Area Buttons.', 'fildisi' ),
						'options' => $fildisi_eutf_button_color_selection,
						'default' => 'primary-1',
						'validate' => 'not_empty',
					),
					array(
						'id'=>'product_area_button_hover_color',
						'type' => 'select',
						'title' => esc_html__( 'Button Hover Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the hover color of your Product Area Buttons.', 'fildisi' ),
						'options' => $fildisi_eutf_button_color_selection,
						'default' => 'black',
						'validate' => 'not_empty',
					),
				)
			);
			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Product Bar', 'fildisi' ),
				'id' => 'eut_redux_section_colors_product_bar',
				'header' => '',
				'desc' => esc_html__( 'Set the colors for the navigation - socials area in the single product.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id'       => 'product_bar_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Background color for the product bar.', 'fildisi' ),
						'default'  => '#f7f7f7',
						'transparent' => false,
					),
					array(
						'id'       => 'product_bar_nav_title_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Navigation Titles Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Color for the Navigation Titles.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id'       => 'product_bar_arrow_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Navigation Arrows Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Color for the Navigation Arrows.', 'fildisi' ),
						'default'  => '#d3d3d3',
						'transparent' => false,
					),
					array(
						'id'       => 'product_bar_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Product Bar Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Border color for the product Bar.', 'fildisi' ),
						'default'  => '#f7f7f7',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Events Calendar', 'fildisi' ),
				'id' => 'eut_redux_section_colors_events_calendar',
				'desc' => esc_html__( 'Set your color preferences for your Events.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
				),
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Event Taxonomy Title', 'fildisi' ),
				'id' => 'eut_redux_section_colors_event_tax_title',
				'header' => '',
				'desc' => esc_html__( 'Set the background color for the event taxonomies title area and the color of the event taxonomies title and description.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'event_tax_title_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'event_tax_title_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#f7f7f7',
						'required' => array( 'event_tax_title_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'event_tax_title_content_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Content Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background Color for your title content.', 'fildisi' ),
						'options' => $fildisi_eutf_bg_color_selection_extra,
						'default' => 'none',
					),
					array(
						'id'       => 'event_tax_title_content_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Content Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'event_tax_title_content_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'event_tax_title_color',
						'type' => 'select',
						'title' => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'event_tax_title_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#000000',
						'required' => array( 'event_tax_title_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'event_tax_description_color',
						'type' => 'select',
						'title' => esc_html__( 'Description Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your description.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'light',
					),
					array(
						'id'       => 'event_tax_description_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Description Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#9B9B9B',
						'required' => array( 'event_tax_description_color', 'equals', 'custom' ),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Event Title', 'fildisi' ),
				'id' => 'eut_redux_section_colors_event_title',
				'header' => '',
				'desc' => esc_html__( 'Set the background color for the single event title area and the color of the event title and description. Notice that you can disable it, override it or even use the feature section when you edit your single event items.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'event_title_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'event_title_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#f7f7f7',
						'required' => array( 'event_title_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'event_title_content_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Content Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background Color for your title content.', 'fildisi' ),
						'options' => $fildisi_eutf_bg_color_selection_extra,
						'default' => 'none',
					),
					array(
						'id'       => 'event_title_content_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Content Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'event_title_content_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'event_title_color',
						'type' => 'select',
						'title' => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'event_title_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#000000',
						'required' => array( 'event_title_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'event_description_color',
						'type' => 'select',
						'title' => esc_html__( 'Description Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your description.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'custom',
					),
					array(
						'id'       => 'event_description_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Description Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#9B9B9B',
						'required' => array( 'event_description_color', 'equals', 'custom' ),
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Event Bar', 'fildisi' ),
				'id' => 'eut_redux_section_colors_event_bar',
				'header' => '',
				'desc' => esc_html__( 'Set the colors for the navigation - socials area in the single event.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id'       => 'event_bar_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Background color for the event bar.', 'fildisi' ),
						'default'  => '#f7f7f7',
						'transparent' => false,
					),
					array(
						'id'       => 'event_bar_nav_title_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Navigation Titles Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Color for the Navigation Titles.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id'       => 'event_bar_arrow_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Navigation Arrows Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Color for the Navigation Arrows.', 'fildisi' ),
						'default'  => '#d3d3d3',
						'transparent' => false,
					),
					array(
						'id'       => 'event_bar_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Event Bar Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Border color for the Event Bar.', 'fildisi' ),
						'default'  => '#f7f7f7',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - bbPress Forum', 'fildisi' ),
				'id' => 'eut_redux_section_colors_bbpress',
				'desc' => esc_html__( 'Set your color preferences for your Forum.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
				),
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Forum Title', 'fildisi' ),
				'id' => 'eut_redux_section_colors_forum_title',
				'header' => '',
				'desc' => esc_html__( 'Set the background color for the forum title area.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					array(
						'id' => 'forum_title_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'dark',
					),
					array(
						'id'       => 'forum_title_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#000000',
						'required' => array( 'forum_title_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'forum_title_content_bg_color',
						'type' => 'select',
						'title' => esc_html__( 'Content Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background Color for your title content.', 'fildisi' ),
						'options' => $fildisi_eutf_bg_color_selection_extra,
						'default' => 'none',
					),
					array(
						'id'       => 'forum_title_content_bg_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Content Background Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'forum_title_content_bg_color', 'equals', 'custom' ),
					),
					array(
						'id' => 'forum_title_color',
						'type' => 'select',
						'title' => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Color for your title.', 'fildisi' ),
						'options' => $fildisi_eutf_color_selection_extra,
						'default' => 'light',
					),
					array(
						'id'       => 'forum_title_color_custom',
						'type'     => 'color',
						'title'    => esc_html__( 'Custom Title Color', 'fildisi' ),
						'transparent' => false,
						'default'  => '#ffffff',
						'required' => array( 'forum_title_color', 'equals', 'custom' ),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Footer', 'fildisi' ),
				'id' => 'eut_redux_section_colors_footer',
				'desc' => esc_html__( 'Set your color preferences for your Footer.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
				),
			);
			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Footer Area', 'fildisi' ),
				'id' => 'eut_redux_section_colors_footer_area',
				'header' => '',
				'desc' => esc_html__( 'Set your color preferences for the Footer Area. Define the Footer Area in Theme Options > Footer Options.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					//Footer Area Color Settings
					array(
						'id'          => 'footer_widgets_bg_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your Footer Area.', 'fildisi' ),
						'default'     => '#1E1E1E',
						'transparent' => false,
					),
					array(
						'id'          => 'footer_widgets_headings_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Headings Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Headings color for your Footer Area.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'footer_widgets_font_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Font Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Font color for your Footer Area.', 'fildisi' ),
						'default'     => '#808080',
						'transparent' => false,
					),
					array(
						'id'          => 'footer_widgets_link_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Link Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Link color for your Footer Area.', 'fildisi' ),
						'default'     => '#808080',
						'transparent' => false,
					),
					array(
						'id'          => 'footer_widgets_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Hover color for your Footer Area.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'          => 'footer_widgets_border_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Border color for your Footer Area.', 'fildisi' ),
						'default'     => '#1E1E1E',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Footer Bar Area', 'fildisi' ),
				'id' => 'eut_redux_section_colors_footer_bar_area',
				'header' => '',
				'desc' => esc_html__( 'Set your color preferences for the Footer Bar Area(copyright area). Define the Footer Bar Area in Theme Options > Footer Options.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => true,
				'fields' => array(
					//Footer Bar Color Settings
					array(
						'id'          => 'footer_bar_bg_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Background color for your Footer Bar Area.', 'fildisi' ),
						'default'     => '#1E1E1E',
						'transparent' => false,
					),
					array(
						'id'          => 'footer_bar_bg_color_opacity',
						'type'        => 'select',
						'title'       => esc_html__('Background Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for your Footer Bar Area.', 'fildisi' ),
						'options'     => $fildisi_eutf_opacity_selection,
						"default"     => '1',
					),
					array(
						'id'          => 'footer_bar_font_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Font Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Font color for your Footer Bar Area.', 'fildisi' ),
						'default'     => '#808080',
						'transparent' => false,
					),
					array(
						'id'          => 'footer_bar_link_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Link Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Link color for your Footer Bar Area.', 'fildisi' ),
						'default'     => '#808080',
						'transparent' => false,
					),
					array(
						'id'          => 'footer_bar_hover_color',
						'type'        => 'color',
						'title'       => esc_html__( 'Hover Color', 'fildisi' ),
						'subtitle'    => esc_html__( 'Hover color for your Footer Bar Area.', 'fildisi' ),
						'default'     => '#ffffff',
						'transparent' => false,
					),
				)
			);
			$this->sections[] = array(
				'title' => esc_html__( 'Colors - Breadcrumbs', 'fildisi' ),
				'id' => 'eut_redux_section_colors_breadcrumbs',
				'header' => '',
				'desc' => esc_html__( 'Set your color preferences for the Breadcrumbs in case you use then in any of your pages, posts or items.', 'fildisi' ),
				'icon_class' => 'el-icon-large',
				'icon' => 'el-icon-brush',
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
					array(
						'id'       => 'page_breadcrumbs_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Background color for the Breadcrumbs.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'       => 'page_breadcrumbs_text_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Font Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Font color for the Breadcrumbs.', 'fildisi' ),
						'default'  => '#6e6e6e',
						'transparent' => false,
					),
					array(
						'id'       => 'page_breadcrumbs_text_hover_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Hover Text Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Hover color for the Breadcrumbs.', 'fildisi' ),
						'default'  => '#F95F51',
						'transparent' => false,
					),
					array(
						'id'       => 'page_breadcrumbs_divider_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Divider Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Divider color for the Breadcrumbs.', 'fildisi' ),
						'default'  => '#b2b2b2',
						'transparent' => false,
					),
					array(
						'id'       => 'page_breadcrumbs_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Border color for the Breadcrumbs.', 'fildisi' ),
						'default'  => '#e0e0e0',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Sliding Area', 'fildisi' ),
				'id' => 'eut_redux_section_colors_sliding_area',
				'desc' => esc_html__( 'Set your color preferences for the Sliding Area.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
					array(
						'id'       => 'sliding_area_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a background color for the sliding area.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'       => 'sliding_area_title_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the titles - headings (h1-h6) of the sliding area.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id'       => 'sliding_area_text_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Text Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the text of the sliding area.', 'fildisi' ),
						'default'  => '#808080',
						'transparent' => false,
					),
					array(
						'id'       => 'sliding_area_link_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Link Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the links of the sliding area.', 'fildisi' ),
						'default'  => '#808080',
						'transparent' => false,
					),
					array(
						'id'       => 'sliding_area_link_hover_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Hover Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the hovers of the sliding area.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id'       => 'sliding_area_close_btn_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Close Button Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the close button of the sliding area.', 'fildisi' ),
						'default'  => '#808080',
						'transparent' => false,
					),
					array(
						'id'       => 'sliding_area_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the borders of the sliding area.', 'fildisi' ),
						'default'  => '#e9e9e9',
						'transparent' => false,
					),
					array(
						'id'       => 'sliding_area_overflow_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Overflow Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a background color for the overflow area, when sliding area opens.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id' => 'sliding_area_overflow_background_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Overflow Background Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the overflow area, when sliding area opens.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '0',
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Modals', 'fildisi' ),
				'id' => 'eut_redux_section_colors_modals',
				'desc' => esc_html__( 'Set your color preferences for the Modals.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
					array(
						'id'       => 'modal_title_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Title Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the titles - headings (h1-h6) of the modal.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id'       => 'modal_text_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Text Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the text of the modal.', 'fildisi' ),
						'default'  => '#777777',
						'transparent' => false,
					),
					array(
						'id' => 'modal_cursor_color_color',
						'type' => 'select',
						'title' => esc_html__( 'Navigation - Close Buttons Color', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select a color for the navigation and close buttons of the modal.', 'fildisi' ),
						'options' => array(
							'dark' => esc_html__( 'Dark', 'fildisi' ),
							'light' => esc_html__( 'Light', 'fildisi' ),
						),
						'default' => 'dark',
						'validate' => 'not_empty',
					),
					array(
						'id'       => 'modal_border_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Border Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for the borders of the modal.', 'fildisi' ),
						'default'  => '#e9e9e9',
						'transparent' => false,
					),
					array(
						'id'       => 'modal_overflow_background_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Overflow Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a background color for the overflow area, when modal opens.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id' => 'modal_overflow_background_color_opacity',
						'type' => 'select',
						'title' => esc_html__('Overflow Background Opacity', 'fildisi' ),
						'subtitle'    => esc_html__( 'Select opacity for the overflow area, when modal opens.', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						"default" => '1',
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Back to Top', 'fildisi' ),
				'id' => 'eut_redux_section_colors_back_to_top',
				'desc' => esc_html__( 'Set your color preferences for Back to Top.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
					array(
						'id'=>'back_to_top_icon_color',
						'type' => 'color',
						'title' => esc_html__( 'Back to Top Icon Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the color of Back to Top icon.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'=>'back_to_top_shape_color',
						'type' => 'color',
						'title' => esc_html__( 'Back to Top Icon Shape Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the color of Back to Top shape.', 'fildisi' ),
						'default' => '#262829',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Colors - Privacy / Cookies', 'fildisi' ),
				'id' => 'eut_redux_section_colors_privacy_consent_bar',
				'desc' => esc_html__( 'Set your color preferences for Privacy / Cookies feature.', 'fildisi' ),
				'submenu' => false,
				'panel' => false,
				'eut_colors' => true,
				'subsection' => false,
				'fields' => array(
					array(
						'id'=>'privacy_bar_bg_color',
						'type' => 'color',
						'title' => esc_html__( 'Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the background color of Privacy Consent Info Bar.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
					),
					array(
						'id' => 'privacy_bar_bg_opacity',
						'type' => 'select',
						'title' => esc_html__( 'Background Opacity', 'fildisi' ),
						'options' => $fildisi_eutf_opacity_selection,
						'default' => '0.90',
					),
					array(
						'id'=>'privacy_bar_text_color',
						'type' => 'color',
						'title' => esc_html__( 'Text Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the color of Privacy Consent Info Bar Text.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'=>'privacy_bar_button_text_color',
						'type' => 'color',
						'title' => esc_html__( 'Button Text Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the color of Privacy Consent Info Bar Button Text.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'=>'privacy_bar_button_bg_color',
						'type' => 'color',
						'title' => esc_html__( 'Button Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the background color of Privacy Consent Info Bar Button.', 'fildisi' ),
						'default'  => '#2bc137',
						'transparent' => false,
					),
					array(
						'id'=>'privacy_bar_button_bg_hover_color',
						'type' => 'color',
						'title' => esc_html__( 'Button Background Hover Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the background hover color of Privacy Consent Info Bar Button.', 'fildisi' ),
						'default'  => '#17a523',
						'transparent' => false,
					),
					array(
						'id'=>'privacy_modal_button_text_color',
						'type' => 'color',
						'title' => esc_html__( 'Modal Button Text Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the color of Modal Button Text.', 'fildisi' ),
						'default'  => '#ffffff',
						'transparent' => false,
					),
					array(
						'id'=>'privacy_modal_button_bg_color',
						'type' => 'color',
						'title' => esc_html__( 'Modal Button Background Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the background color of Modal Button.', 'fildisi' ),
						'default'  => '#2bc137',
						'transparent' => false,
					),
					array(
						'id'=>'privacy_modal_button_bg_hover_color',
						'type' => 'color',
						'title' => esc_html__( 'Modal Button Background Hover Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the background hover color of Modal Button.', 'fildisi' ),
						'default'  => '#17a523',
						'transparent' => false,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-cloud',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Social Media', 'fildisi' ),
				'id' => 'eut_redux_section_social_media',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'social_options',
						'type' => 'sortable',
						'title' => esc_html__( 'Social URLs', 'fildisi' ),
						'subtitle' => esc_html__( 'Define and reorder your social URLs. Clear the input field for any social link you do not wish to display.', 'fildisi' ),
						'desc' => '',
						'label' => true,
						'options' => $fildisi_eutf_social_options,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-map-marker',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Map Options', 'fildisi' ),
				'id' => 'eut_redux_section_map_options',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id'=>'map_api_mode',
						'type' => 'button_set',
						'title' => esc_html__( 'Map API', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select the map API', 'fildisi' ),
						'options' => array(
							'google-maps' => esc_html__( 'Google Maps', 'fildisi' ),
							'openstreetmap' => esc_html__( 'OpenStreetMap', 'fildisi' ),
						),
						'default' => 'google-maps',
					),
					array(
						'id'=>'map_tile_url',
						'type' => 'text',
						'title' => esc_html__( 'Tile Layer URL', 'fildisi' ),
						'subtitle' => esc_html__( 'Define the Tile Layer. Used to load and display tile layers on the map.', 'fildisi' ),
						'desc' => sprintf( '%1$s: <a href="//wiki.openstreetmap.org/wiki/Tile_servers" target="_blank" rel="noopener noreferrer"> %2$s </a>', esc_html__('See more tile servers', 'fildisi'), esc_html__('here', 'fildisi') ),
						"default" => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
						'required' => array( 'map_api_mode', 'equals', 'openstreetmap' ),
					),
					array(
						'id'=>'map_tile_url_subdomains',
						'type' => 'text',
						'title' => esc_html__( 'Tile Layer Subdomains', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define the Tile Layer subdomains.', 'fildisi' ),
						"default" => "abc",
						'required' => array( 'map_api_mode', 'equals', 'openstreetmap' ),
					),
					array(
						'id'=>'map_tile_attribution',
						'type' => 'text',
						'title' => esc_html__( 'Tile Layer Attribution', 'fildisi' ),
						'subtitle' => esc_html__( 'Enter the Tile Layer attribution', 'fildisi' ),
						"default" => '&copy; <a href="//www.openstreetmap.org/copyright">OpenStreetMap</a>',
						'required' => array( 'map_api_mode', 'equals', 'openstreetmap' ),
					),
					array(
						'id'       => 'gmap_api_key',
						'type'     => 'text',
						'title'    => esc_html__( 'Google API Key', 'fildisi' ),
						'subtitle' => $fildisi_gmap_api_key_link,
						'default'  => '',
						'required' => array( 'map_api_mode', 'equals', 'google-maps' ),
					),
					array(
						'id'=>'gmap_custom_enabled',
						'type' => 'button_set',
						'title' => esc_html__( 'Enable Custom Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to enable custom style for your map.', 'fildisi' ),
						'options' => array(
							'0' => esc_html__( 'No', 'fildisi' ),
							'1' => esc_html__( 'Custom', 'fildisi' ),
							'2' => esc_html__( 'Custom Code', 'fildisi' ),
						),
						'default' => '0',
						'required' => array( 'map_api_mode', 'equals', 'google-maps' ),
					),
					array(
						'id' => 'gmap_custom_code',
						'type' => 'textarea',
						'title' => esc_html__( 'Custom Code', 'fildisi' ),
						'subtitle' => esc_html__( 'Copy and paste the JSON here.', 'fildisi' ) . ' ' . $fildisi_gmap_style_link,
						'default' => '',
						'rows' => '30',
						'required' => array( 'gmap_custom_enabled', 'equals', '2' ),
					),
					array(
						'id'       => 'gmap_water_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Water color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for water', 'fildisi' ),
						'default'  => '#424242',
						'transparent' => false,
						'validate' => 'color',
						'required' => array( 'gmap_custom_enabled', 'equals', '1' ),
					),
					array(
						'id'       => 'gmap_landscape_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Landscape color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for landscape', 'fildisi' ),
						'default'  => '#232323',
						'transparent' => false,
						'validate' => 'color',
						'required' => array( 'gmap_custom_enabled', 'equals', '1' ),
					),
					array(
						'id'       => 'gmap_poi_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Point of interest color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for point of interest', 'fildisi' ),
						'default'  => '#232323',
						'transparent' => false,
						'validate' => 'color',
						'required' => array( 'gmap_custom_enabled', 'equals', '1' ),
					),
					array(
						'id'       => 'gmap_road_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Roads color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for roads', 'fildisi' ),
						'default'  => '#1a1a1a',
						'transparent' => false,
						'validate' => 'color',
						'required' => array( 'gmap_custom_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'gmap_label_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Enable Label', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to enable labels for your map.', 'fildisi' ),
						'default' => 1,
						'required' => array( 'gmap_custom_enabled', 'equals', '1' ),
					),
					array(
						'id'       => 'gmap_label_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Label color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for labels', 'fildisi' ),
						'default'  => '#777777',
						'transparent' => false,
						'validate' => 'color',
						'required' => array( 'gmap_label_enabled', 'equals', '1' ),
					),
					array(
						'id'       => 'gmap_label_stroke_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Label Stroke color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a stroke color for labels', 'fildisi' ),
						'default'  => '#1a1a1a',
						'transparent' => false,
						'validate' => 'color',
						'required' => array( 'gmap_label_enabled', 'equals', '1' ),
					),
					array(
						'id'       => 'gmap_country_color',
						'type'     => 'color',
						'title'    => esc_html__( 'Country Stroke color', 'fildisi' ),
						'subtitle' => esc_html__( 'Pick a color for country stroke.', 'fildisi' ),
						'default'  => '#000000',
						'transparent' => false,
						'validate' => 'color',
						'required' => array( 'gmap_custom_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'gmap_zoom_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Enable Zoom Control', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to enable zoom control for your map.', 'fildisi' ),
						'default' => 0,
						'required' => array( 'gmap_custom_enabled', 'not', '0' ),
					),
					array(
						'id' => 'gmap_type_control',
						'type' => 'switch',
						'title' => esc_html__( 'Enable Map Type Control', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to enable map type control for your map.', 'fildisi' ),
						'default' => 0,
						'required' => array( 'map_api_mode', 'equals', 'google-maps' ),
					),
					array(
						'id' => 'gmap_gesture_handling',
						'type' => 'select',
						'title' => esc_html__( 'Gesture Handling', 'fildisi' ),
						'subtitle'=> esc_html__( 'Control how the API handles gestures on the map. For mobile web in particular, users may swipe the touchscreen, intending to scroll the page, but change the scale of the map instead.', 'fildisi' ),
						'options' => array(
							'greedy' => esc_html__( 'Greedy', 'fildisi' ),
							'cooperative' => esc_html__( 'Cooperative', 'fildisi' ),
							'none' => esc_html__( 'None', 'fildisi' ),
							'auto' => esc_html__( 'Auto', 'fildisi' ),
						),
						'default' => 'auto',
						'required' => array( 'map_api_mode', 'equals', 'google-maps' ),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-error',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( '404 Page', 'fildisi'),
				'id' => 'eut_redux_section_404',
				'subtitle' => esc_html__( 'You can find the settings for the 404 page here.', 'fildisi' ),
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id' => 'page_404_header',
						'type' => 'switch',
						'title' => esc_html__( 'Show Header', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show header.', 'fildisi' ),
						'default' => 0,
					),
					array(
						'id' => 'page_404_header_style',
						'type' => 'select',
						'title' => esc_html__( 'Page 404 Header Style', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select your page header style.', 'fildisi' ),
						'options' => $fildisi_eutf_header_style,
						'default' => 'default',
						'required' => array( 'page_404_header', 'equals', 1 ),
					),
					array(
						'id' => 'page_404_header_overlapping',
						'type' => 'select',
						'title' => esc_html__( 'Page 404 Header Overlapping', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want the Page Header overlaps the content. Combine this option with the Light or Dark Header.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'no',
						'required' => array( 'page_404_header', 'equals', 1 ),
					),
					array(
						'id' => 'page_404_content',
						'type' => 'editor',
						'args' => array ( 'wpautop' => false ),
						'title' => esc_html__( 'Page 404 Content', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the content of your 404 page, you can also use shortcodes.', 'fildisi' ),
						'default' => '<h3><em>404 ERROR</em></h3><h1>Hey there mate!</h1><h2>Your lost treasure is not found here...</h2><p class="eut-leader-text">Sorry! The page you are looking for wasn\'t found!</p>',
					),
					array(
						'id' => 'page_404_search',
						'type' => 'checkbox',
						'title' => esc_html__( 'Show Search Box', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show a search box.', 'fildisi' ),
						'default' => 0,
					),
					array(
						'id' => 'page_404_home_button',
						'type' => 'checkbox',
						'title' => esc_html__( 'Show Back to home Button', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show a back to home button.', 'fildisi' ),
						'default' => 1,
					),
					array(
						'id' => 'page_404_footer',
						'type' => 'switch',
						'title' => esc_html__( 'Show Footer', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show footer.', 'fildisi' ),
						'default' => 0,
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-cog',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Miscellaneous', 'fildisi' ),
				'id' => 'eut_redux_section_miscellaneous',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id'       => 'feature_section_post_types',
						'type'     => 'select',
						'multi'    => true,
						'title'    => esc_html__( 'Feature Section Post Types', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the post types you wish to enable the Feature Section.', 'fildisi' ),
						'options' => $fildisi_eutf_feature_section_post_types_selection,
						'default' => array( 'page', 'portfolio' ),
					),
					array(
						'id'=>'wp_gallery_popup',
						'type' => 'switch',
						'title' => esc_html__( 'Lightbox for WordPress Gallery', 'fildisi' ),
						'subtitle'=> esc_html__( 'Toggle lightbox for native WordPress Gallery on or off.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__('On', 'fildisi' ),
						'off' => esc_html__('Off', 'fildisi' ),
					),
					array(
						'id'=>'wp_tagcloud',
						'type' => 'switch',
						'title' => esc_html__( 'Default WordPress Tag Cloud', 'fildisi' ),
						'subtitle'=> esc_html__( 'Toggle default tag cloud on or off.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__('On', 'fildisi' ),
						'off' => esc_html__('Off', 'fildisi' ),
					),
					array(
						'id'=>'logo_as_text_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Logo as text', 'fildisi' ),
						'subtitle'=> esc_html__( 'Toggle logo as text on or off. When on, all logo images will be replaced with site name.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__('On', 'fildisi' ),
						'off' => esc_html__('Off', 'fildisi' ),
					),
					array(
						'id'=>'logo_custom_link_url',
						'type' => 'text',
						'title' => esc_html__( 'Logo Custom Link URL', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define a custom URL link for your logo. If empty homepage will be used instead.', 'fildisi' ),
						"default" => '',
					),
					array(
						'id'=>'menu_header_integration',
						'type' => 'select',
						'title' => esc_html__( 'Main Menu Integration', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the main menu integration method. Selection available for custom menu integration.', 'fildisi' ),
						'options' => $fildisi_eutf_header_menu_selection,
						'default' => 'default',
						'validate' => 'not_empty',
					),
					array(
						'id'=>'sidebar_heading_tag',
						'type' => 'select',
						'title' => esc_html__( 'Sidebar Headings Tag', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the headings tag for your Sidebar Titles.', 'fildisi' ),
						'options' => $fildisi_eutf_headings_tag_selection,
						'default' => 'div',
						'validate' => 'not_empty',
					),
					array(
						'id'=>'footer_heading_tag',
						'type' => 'select',
						'title' => esc_html__( 'Footer Sidebar Headings Tag', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the headings tag for your Footer Sidebar Titles.', 'fildisi' ),
						'options' => $fildisi_eutf_headings_tag_selection,
						'default' => 'div',
						'validate' => 'not_empty',
					),
					array(
						'id' => 'disable_seo_page_analysis',
						'type' => 'checkbox',
						'title' => esc_html__( 'Disable WordPress SEO Page Analysis', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to disable WordPress SEO page analysis.', 'fildisi' ),
						'default' => 0,
					),
					array(
						'id' => 'responsive_sidearea_button_visibility',
						'type' => 'select',
						'title' => esc_html__( 'Responsive Sliding Area Button Visibility', 'fildisi' ),
						'subtitle' => esc_html__( 'Enable/Disable the Sliding Area Button on devices.', 'fildisi' ),
						'options' => $fildisi_eutf_enable_selection,
						'default' => 'yes',
					),
					array(
						'id' => 'spinner_image',
						'url' => true,
						'type' => 'media',
						'title' => esc_html__( 'Spinner Image', 'fildisi' ),
						'subtitle' => esc_html__( 'Upload a custom image if you want to replace the default graphic spinner.', 'fildisi' ),
					),
					array(
						'id'=>'placeholder_mode',
						'type' => 'select',
						'title' => esc_html__( 'Placeholder Mode', 'fildisi' ),
						'subtitle' => esc_html__( 'Select what images to display as placeholders.', 'fildisi' ),
						'options' => array(
							'dummy' => esc_html__( 'Theme Dummy Image', 'fildisi' ),
							'unsplash' => esc_html__( 'unsplash', 'fildisi' ),
							//'placehold' => esc_html__( 'placehold.it', 'fildisi' ),
						),
						'default' => 'dummy',
						'validate' => 'not_empty',
					),
					array(
						'id'=>'scroll_mode',
						'type' => 'select',
						'title' => esc_html__( 'Smooth Scroll Mode', 'fildisi' ),
						'subtitle' => esc_html__( 'Select your smooth scroll mode.', 'fildisi' ),
						'options' => array(
							'auto' => esc_html__( 'Browser defined', 'fildisi' ),
							'on' => esc_html__( 'Always On', 'fildisi' ),
							'off' => esc_html__( 'Always Off', 'fildisi' ),
						),
						'default' => 'auto',
						'validate' => 'not_empty',
					),
					array(
						'id'=>'device_animations',
						'type' => 'switch',
						'title' => esc_html__( 'Device Animations', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable/Disable animations for mobile devices.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__('On', 'fildisi' ),
						'off' => esc_html__('Off', 'fildisi' ),
					),
					array(
						'id'=>'device_hover_single_tap',
						'type' => 'switch',
						'title' => esc_html__( 'Device Single Tap', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable/Disable single tap for links/hovers on devices.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__('On', 'fildisi' ),
						'off' => esc_html__('Off', 'fildisi' ),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-cog',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Page Builder Addons', 'fildisi' ),
				'desc' => esc_html__( 'Enable/Disable default Page Builder functionality.', 'fildisi' ),
				'id' => 'eut_redux_section_page_builder',
				'submenu' => true,
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'=>'vc_grid_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Page Builder Grid Elements', 'fildisi' ),
						'subtitle'=> esc_html__( 'Toggle Page Builder Grid elements on or off.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__('On', 'fildisi' ),
						'off' => esc_html__('Off', 'fildisi' ),
					),
					array(
						'id'=>'vc_charts_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Page Builder Charts Elements', 'fildisi' ),
						'subtitle'=> esc_html__( 'Toggle Page Builder Charts elements on or off.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__('On', 'fildisi' ),
						'off' => esc_html__('Off', 'fildisi' ),
					),
					// array(
						// 'id'=>'vc_woo_visibility',
						// 'type' => 'switch',
						// 'title' => esc_html__( 'Page Builder WooCommerce Elements', 'fildisi' ),
						// 'subtitle'=> esc_html__( 'Toggle Page Builder WooCommerce elements on or off.', 'fildisi' ),
						// "default" => '0',
						// 'on' => esc_html__('On', 'fildisi' ),
						// 'off' => esc_html__('Off', 'fildisi' ),
					// ),
					array(
						'id'=>'vc_auto_updater',
						'type' => 'switch',
						'title' => esc_html__( 'Page Builder Auto Updater', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable/Disable Page Builder Auto Updater ( Activation Required ).', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__('On', 'fildisi' ),
						'off' => esc_html__('Off', 'fildisi' ),
					),
/* 					array(
						'id'=>'vc_content_manager_visibility',
						'type' => 'switch',
						'title' => esc_html__( 'Page Builder Theme Content Manager', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable/Disable Theme Content Manager.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__('On', 'fildisi' ),
						'off' => esc_html__('Off', 'fildisi' ),
					), */
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-cog',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Button Settings', 'fildisi' ),
				'desc' => esc_html__( 'Set the style for the buttons.', 'fildisi' ),
				'id' => 'eut_redux_section_buttons',
				'submenu' => true,
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'=>'button_type',
						'type' => 'select',
						'title' => esc_html__( 'Button Type', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the type of your Buttons.', 'fildisi' ),
						'options' => $fildisi_eutf_button_type_selection,
						'default' => 'outline',
						'validate' => 'not_empty',
					),
					array(
						'id'=>'button_shape',
						'type' => 'select',
						'title' => esc_html__( 'Button Shape', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the shape of your Buttons.', 'fildisi' ),
						'options' => $fildisi_eutf_button_shape_selection,
						'default' => 'square',
						'validate' => 'not_empty',
					),
					array(
						'id'=>'button_color',
						'type' => 'select',
						'title' => esc_html__( 'Button Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the color of your Buttons.', 'fildisi' ),
						'options' => $fildisi_eutf_button_color_selection,
						'default' => 'black',
						'validate' => 'not_empty',
					),
					array(
						'id'=>'button_hover_color',
						'type' => 'select',
						'title' => esc_html__( 'Button Hover Color', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the hover color of your Buttons.', 'fildisi' ),
						'options' => $fildisi_eutf_button_color_selection,
						'default' => 'black',
						'validate' => 'not_empty',
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-cog',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Back To Top', 'fildisi' ),
				'desc' => esc_html__( 'Set the style for the back to top button.', 'fildisi' ),
				'id' => 'eut_redux_section_back_top_top',
				'submenu' => true,
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'=>'back_to_top_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Back to Top', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable the Back to Top button.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'=>'back_to_top_shape',
						'type' => 'select',
						'title' => esc_html__( 'Back to Top Icon Shape', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the shape of Back to Top button.', 'fildisi' ),
						'options' => array(
							'none' => esc_html__( 'No Shape', 'fildisi' ),
							'square' => esc_html__( 'Square', 'fildisi' ),
							'round' => esc_html__( 'Round', 'fildisi' ),
							'circle' => esc_html__( 'Circle', 'fildisi' ),
						),
						'default' => 'circle',
						'validate' => 'not_empty',
						'required' => array( 'back_to_top_enabled', 'equals', '1' ),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-cog',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Meta Tags', 'fildisi' ),
				'desc' => esc_html__( 'Configure your site meta tags.', 'fildisi' ),
				'id' => 'eut_redux_section_meta_tags',
				'submenu' => true,
				'customizer' => false,
				'subsection' => true,
				'fields' => array(
					array(
						'id'=>'meta_viewport_responsive',
						'type' => 'switch',
						'title' => esc_html__( 'Responsive Viewport Meta', 'fildisi' ),
						'subtitle'=> esc_html__( 'Enable or Disable Responsive Viewport.', 'fildisi' ),
						"default" => '1',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'=>'meta_opengraph',
						'type' => 'switch',
						'title' => esc_html__( 'Open Graph Meta', 'fildisi' ),
						'subtitle'=> esc_html__( 'Generate open graph meta tags for social sharing ( e.g: Facebook, Google+, LinkedIn etc )', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'=>'meta_twitter',
						'type' => 'switch',
						'title' => esc_html__( 'Twitter Card Meta', 'fildisi' ),
						'subtitle'=> esc_html__( 'Generate meta tags for Twitter', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-wrench',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Coming Soon', 'fildisi' ),
				'id' => 'eut_redux_section_coming_soon',
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id'=>'coming_soon_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Enable Coming Soon', 'fildisi' ),
						'subtitle'=> esc_html__( 'Toggle Coming Soon Redirect on or off.', 'fildisi' ),
						"default" => '0',
						'on' => esc_html__( 'On', 'fildisi' ),
						'off' => esc_html__( 'Off', 'fildisi' ),
					),
					array(
						'id'=>'coming_soon_template',
						'type' => 'select',
						'title' => esc_html__( 'Coming Soon Template', 'fildisi' ),
						'subtitle' => esc_html__( 'Select content only or use the defined page template.', 'fildisi' ),
						'options' => array(
							'content' => esc_html__( 'Content Only', 'fildisi' ),
							'template' => esc_html__( 'Use Page Template', 'fildisi' ),
						),
						'default' => 'content',
						'validate' => 'not_empty',
						'required' => array( 'coming_soon_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'coming_soon_page',
						'type' => 'select',
						'title' => esc_html__( 'Coming Soon Page', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the page for your redirection.', 'fildisi' ),
						'data' => 'page',
						'default' => '',
						'required' => array( 'coming_soon_enabled', 'equals', '1' ),
					),

				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-resize-small',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Media Sizes', 'fildisi' ),
				'submenu' => true,
				'customizer' => false,
				'fields' => array(
					array(
						'id'=>'feature_section_bg_size',
						'type' => 'select',
						'title' => esc_html__( 'Feature Section Background Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select what background image to show for Feture Sections.', 'fildisi' ),
						'options' => array(
							'responsive' => esc_html__( 'Responsive', 'fildisi' ),
							'extra-extra-large' => esc_html__( 'Extra Extra Large', 'fildisi' ),
							'full' => esc_html__( 'Full', 'fildisi' ),
						),
						'default' => 'responsive',
						'validate' => 'not_empty',
					),
					array(
						'id'=>'row_section_bg_size',
						'type' => 'select',
						'title' => esc_html__( 'Row Section Background Image Size', 'fildisi' ),
						'subtitle' => esc_html__( 'Select what background image to show for Row Sections.', 'fildisi' ),
						'options' => array(
							'responsive' => esc_html__( 'Responsive', 'fildisi' ),
							'extra-extra-large' => esc_html__( 'Extra Extra Large', 'fildisi' ),
							'full' => esc_html__( 'Full', 'fildisi' ),
						),
						'default' => 'responsive',
						'validate' => 'not_empty',
					),
				)
			);

			$this->sections[] = array(
				'icon' => 'el-icon-lock',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Privacy / Cookies', 'fildisi' ),
				'desc' => esc_html__( 'The following shortcodes will allow your users to change certain behavior of your website regarding privacy.', 'fildisi' ) . '<br>' .
					'<ul>' .
					'<li><strong>fildisi_privacy_required</strong> - ' . esc_html__( 'displays a required content for your site e.g: Cloudlflare, CDN etc.', 'fildisi' ) . '</li>' .
					'<li><strong>fildisi_privacy_gtracking</strong> - ' . esc_html__( 'allows a user to enable/disable Google Analytics tracking code in the user\'s browser', 'fildisi' ) . '</li>' .
					'<li><strong>fildisi_privacy_gfonts</strong> - ' . esc_html__( 'allows a user to enable/disable the use of Google Fonts in the user\'s browser', 'fildisi' ) . '</li>' .
					'<li><strong>fildisi_privacy_gmaps</strong> - ' . esc_html__( 'allows a user to enable/disable the use of Google Maps in the user\'s browser', 'fildisi' ) . '</li>' .
					'<li><strong>fildisi_privacy_video_embeds</strong> - ' . esc_html__( 'allows a user to enable/disable video embeds in the user\'s browser', 'fildisi' ) . '</li>' .
					'<li><strong>fildisi_privacy_policy_page_link</strong> - ' . esc_html__( 'displays a link to the privacy policy page set from the WordPress admin panel', 'fildisi' ) . '</li>' .
					'<li><strong>fildisi_privacy_preferences_link</strong> - ' . esc_html__( 'displays a link to the privacy preferences as defined in the Privacy Consent Info Bar', 'fildisi' ) . '</li>' .
					'</ul>' .
					esc_html__( 'You can use any of them in your privacy policy or in any text area that allows shortcodes.', 'fildisi' ) . '<br><br>' .
					'<strong>[fildisi_privacy_required value="required"]For performance and security reasons we use Cloudflare[/fildisi_privacy_required]</strong><br>' .
					'<strong>[fildisi_privacy_gtracking]</strong><br>' .
					'<strong>[fildisi_privacy_gfonts]</strong><br>' .
					'<strong>[fildisi_privacy_gmaps]</strong><br>' .
					'<strong>[fildisi_privacy_video_embeds]</strong><br>' .
					'<strong>[fildisi_privacy_policy_page_link]</strong><br>' .
					'<strong>[fildisi_privacy_preferences_link]</strong><br><br>' .
					esc_html__( 'Note: To change the default text add your text inside the shortcode tags [shortcode]Your text[/shortcode]', 'fildisi' ),
				'id' => 'eut_redux_section_privacy_cookies',
				'submenu' => true,
				'customizer' => false,
				'class' => 'eut-redux-desc-full',
				'fields' => array(
					array(
						'id'   => 'info_style_blocking_content',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Privacy Content Blocking Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Configure the privacy content blocking settings.', 'fildisi' ) . '<br>' . esc_html__( 'Note: The usage of the Blocking content feature is not recommended if you have added caching plugins or page rules to cache static HTML content (aggressive cache). If you use this feature you might need to disable such type of caching.', 'fildisi' ),
					),
					array(
						'id' => 'privacy_content_blocking_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Privacy Content Blocking', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to enable/disable privacy content blocking.', 'fildisi' ) .'<br><br>' . esc_html__( 'Note: if you disable content blocking certain privacy shortcodes will be automatically disabled.', 'fildisi' ),
						'default' => 1,
					),
					array(
						'id'      => 'privacy_initial_state',
						'type'    => 'checkbox',
						'title'   => esc_html__( 'Privacy Initial Blocking State', 'fildisi' ),
						'subtitle'    => esc_html__( 'Check if you want to block the content when the page loads.', 'fildisi' ),
						'options'  => array(
							'gtracking'     => 'Google Tracking',
							'gfonts' => 'Google Fonts',
							'gmaps' => 'Google Maps',
							'video-embeds'   => 'Embed Videos'
						),
						'default' => array(
							'gtracking'     => '0',
							'gfonts' => '0',
							'gmaps' => '0',
							'video-embeds'   => '0'
						),
						'required' => array( 'privacy_content_blocking_enabled', 'equals', '1' ),
					),
					array(
						'id'   => 'info_style_fallback_content',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Privacy Fallback Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Configure the privacy fallback info box.', 'fildisi' ),
						'required' => array( 'privacy_content_blocking_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'privacy_fallback_content',
						'type' => 'textarea',
						'rows' => '2',
						'title' => esc_html__( 'Privacy Fallback Content', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the fallback text when the content is blocked.', 'fildisi' ),
						'default' => 'This content is blocked. Please review your Privacy Preferences.',
						'required' => array( 'privacy_content_blocking_enabled', 'equals', '1' ),
					),
					array(
						'id'       => 'privacy_fallback_preferences_link_visibility',
						'type'     => 'checkbox',
						'title'    => esc_html__('Display Privacy Preferences link', 'fildisi'),
						'subtitle' => esc_html__('Select if you want to display the Privacy Preferences link ( if defined in the privacy consent bar ).', 'fildisi'),
						'default'  => '1',
						'required' => array( 'privacy_content_blocking_enabled', 'equals', '1' ),
					),
					array(
						'id'       => 'privacy_fallback_content_link_visibility',
						'type'     => 'checkbox',
						'title'    => esc_html__('Display redirect link of the blocked content', 'fildisi'),
						'subtitle' => esc_html__('Select if you want to display a redirect link of the blocked content ( blocked content will open in a new tab e.g: YouTube website ).', 'fildisi'),
						'default'  => '1',
						'required' => array( 'privacy_content_blocking_enabled', 'equals', '1' ),
					),
					array(
						'id'   => 'info_style_consent_bar',
						'type' => 'info',
						'class' => 'eut-redux-sub-info',
						'title' => esc_html__( 'Privacy Consent Info Bar Settings', 'fildisi' ),
						'subtitle'=> esc_html__( 'Configure the consent info bar.', 'fildisi' ),
					),
					array(
						'id' => 'privacy_consent_bar_enabled',
						'type' => 'switch',
						'title' => esc_html__( 'Privacy Consent Info Bar', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to show a privacy consent info bar.', 'fildisi' ),
						'default' => 0,
					),
					array(
						'id' => 'privacy_consent_bar_position',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Privacy Consent Info Bar Position', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the position for Privacy Consent Info Bar', 'fildisi' ),
						'options' => array(
							'center' => esc_html__( 'Center', 'fildisi' ),
							'left' => esc_html__( 'Left', 'fildisi' ),
							'right' => esc_html__( 'Right', 'fildisi' ),
						),
						'default' => 'center',
						'required' => array( 'privacy_consent_bar_enabled', 'equals', '1' ),
					),
					array(
						'id' => 'privacy_consent_bar_content',
						'type' => 'textarea',
						'rows' => '2',
						'title' => esc_html__( 'Privacy Info Bar Content', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the content of your consent info bar.', 'fildisi' ),
						'default' => 'Our website uses cookies, mainly from 3rd party services. Define your Privacy Preferences and/or agree to our use of cookies.',
						'required' => array( 'privacy_consent_bar_enabled', 'equals', '1' ),
					),
					array(
						'id'=>'privacy_agreement_button_label',
						'type' => 'text',
						'title' => esc_html__( 'Privacy Agreement Button Label', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define the label for your privacy agreement button. Leave empty to remove.', 'fildisi' ),
						'required' => array( 'privacy_consent_bar_enabled', 'equals', '1' ),
						"default" => 'I Agree',
					),
					array(
						'id'=>'privacy_preferences_button_label',
						'type' => 'text',
						'title' => esc_html__( 'Privacy Preferences Button Label', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define the label for your privacy preferences button. Leave empty to remove.', 'fildisi' ),
						'required' => array( 'privacy_consent_bar_enabled', 'equals', '1' ),
						"default" => 'Privacy Preferences',
					),
					array(
						'id' => 'privacy_preferences_button_link',
						'type' => 'select',
						'compiler' => true,
						'title' => esc_html__( 'Privacy Preferences Button Link Mode', 'fildisi' ),
						'subtitle' => esc_html__( 'Select the preferences button link mode: modal, privacy policy page or custom url.', 'fildisi' ),
						'options' => array(
							'modal' => esc_html__( 'Modal', 'fildisi' ),
							'privacy_page' => esc_html__( 'Privacy Policy Page', 'fildisi' ),
							'custom_url' => esc_html__( 'Custom URL', 'fildisi' ),
						),
						'default' => 'modal',
						'validate' => 'not_empty',
						'required' => array( 'privacy_consent_bar_enabled', 'equals', '1' ),
					),
					array(
						'id'=>'privacy_preferences_link_url',
						'type' => 'text',
						'title' => esc_html__( 'Privacy Preferences Custom Link URL', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define a custom URL link for your privacy preferences button.', 'fildisi' ),
						'required' => array(
							array( 'privacy_consent_bar_enabled', 'equals', '1' ),
							array( 'privacy_preferences_button_link', 'equals', 'custom_url' ),
						),
						"default" => '',
					),
					array(
						'id'=>'privacy_preferences_link_target',
						'type' => 'select',
						'title' => esc_html__( 'Privacy Preferences Custom Link Target', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define the target for your custom url, same or new page.', 'fildisi' ),
						'options' => array(
							'_self' => esc_html__( 'Same Page', 'fildisi' ),
							'_blank' => esc_html__( 'New page', 'fildisi' ),
						),
						'required' => array(
							array( 'privacy_consent_bar_enabled', 'equals', '1' ),
							array( 'privacy_preferences_button_link', 'equals', 'custom_url' ),
						),
						"default" => '_self',
					),
					array(
						'id' => 'privacy_consent_modal_content',
						'type' => 'editor',
						'args' => array ( 'wpautop' => false ),
						'title' => esc_html__( 'Privacy Modal Content', 'fildisi' ),
						'subtitle' => esc_html__( 'Type the content of your modal consent dialog.', 'fildisi' ),
						'default' => '<h5>Privacy Preferences</h5><p>When you visit our website, it may store information through your browser from specific services, usually in the form of cookies. Here you can change your Privacy preferences. It is worth noting that blocking some types of cookies may impact your experience on our website and the services we are able to offer.</p><div>[fildisi_privacy_gtracking][fildisi_privacy_gfonts][fildisi_privacy_gmaps][fildisi_privacy_video_embeds]</div>',
						'required' => array(
							array( 'privacy_consent_bar_enabled', 'equals', '1' ),
							array( 'privacy_preferences_button_link', 'equals', 'modal' ),
						),
					),
					array(
						'id'=>'privacy_refresh_button_label',
						'type' => 'text',
						'title' => esc_html__( 'Privacy Refresh Button Label', 'fildisi' ),
						'subtitle'=> esc_html__( 'Define the label for your privacy refresh button. Leave empty to remove.', 'fildisi' ),
						'required' => array(
							array( 'privacy_consent_bar_enabled', 'equals', '1' ),
							array( 'privacy_preferences_button_link', 'equals', 'modal' ),
						),
						"default" => 'Save Preferences',
					),
				)
			);
			$this->sections[] = array(
				'icon' => 'el-icon-cloud',
				'icon_class' => 'el-icon-cog',
				'title' => esc_html__( 'Utilities', 'fildisi' ),
				'id' => 'eut_redux_section_utilities',
				'submenu' => true,
				'customizer' => false,
				'fields' => array()
			);
			$this->sections[] = array(
				'icon' => 'el-icon-cogs',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Theme Performance', 'fildisi' ),
				'id' => 'eut_redux_section_advanced',
				'submenu' => true,
				'subsection' => true,
				'customizer' => false,
				'fields' => array(
 					array(
						'id'=>'combine_js',
						'type' => 'select',
						'title' => esc_html__( 'Combine/Compress Theme Scripts', 'fildisi' ),
						'subtitle'=> esc_html__( 'Select if you want to combine/compress theme scripts.', 'fildisi' ),
						'options' => array(
							'1' => esc_html__( 'Partial merge/compression of theme javascript files', 'fildisi' ),
							'0' => esc_html__( 'No merge/compression of theme javascript files', 'fildisi' ),
						),
						"default" => '1',
					),
				)
			);

			if ( class_exists( 'Envato_Market' ) ) {
				$envato_market_link = 'admin.php?page=envato-market';
			} else {
				$envato_market_link = 'admin.php?page=fildisi-tgmpa-install-plugins';
			}
			$update_message = esc_html__( "Envato official solution is recommended for theme updates using the new Envato Market API.", 'fildisi' ) .
					"<br/>" . esc_html__( "You can now update the theme using the", 'fildisi' ) . " " . "<a href='" . esc_url( admin_url() . $envato_market_link ) . "'>" . esc_html__( "Envato Market", 'fildisi' ) . "</a>" . " ". esc_html__( "plugin", 'fildisi' ) . "." .
					" " . esc_html__( "For more information read the related article in our", 'fildisi' ) . " " . "<a href='//docs.euthemians.com/tutorials/envato-market-wordpress-plugin-for-theme-updates/' target='_blank'>" . esc_html__( "documentation", 'fildisi' ) . "</a>.";
			$this->sections[] = array(
				'icon' => 'el-icon-repeat',
				'icon_class' => 'el-icon-large',
				'title' => esc_html__( 'Theme Update', 'fildisi' ),
				'id' => 'eut_redux_section_theme_update',
				'desc' => $update_message,
				'submenu' => true,
				'customizer' => false,
				'fields' => array()
			);

			//Show Sections available only in customizer
			if ( ( defined( 'FILDISI_EUTF_THEME_REDUX_CUSTOM_PANEL' ) &&  true === FILDISI_EUTF_THEME_REDUX_CUSTOM_PANEL ) || $this->fildisi_eutf_redux_customizer_visibility() ) {
				foreach ( $this->sections as $k => $section ) {
					if ( isset( $section['eut_colors'] ) && isset( $section['panel'] ) ) {
						unset($this->sections[$k]['panel']);
					}
				}
			}

		}


		public function setArguments() {

			$theme = wp_get_theme();
			$theme_version = $theme->get('Version');
			if( is_child_theme() ) {
				$parent_theme = wp_get_theme( get_template() );
				$theme_version .= ' ( ' . $parent_theme->get('Name') .': ' . $parent_theme->get('Version') . ' )';
			}

			$this->args = array(
				// TYPICAL -> Change these values as you need/desire
				'opt_name' => 'fildisi_eutf_options', // This is where your data is stored in the database and also becomes your global variable name.
				'display_name' => ' ', // Name that appears at the top of your panel
				'display_version' => $theme_version, // Version that appears at the top of your panel
				'menu_type' => 'submenu', //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
				'allow_sub_menu' => false, // Show the sections below the admin menu item or not
				'menu_title' => esc_html__( 'Theme Options', 'fildisi' ),
				'page' => esc_html__( 'Theme Options', 'fildisi' ),
				'google_api_key' => '', // Must be defined to add google fonts to the typography module
				'admin_bar' => false, // Show the panel pages on the admin bar
				'global_variable' => 'fildisi_eutf_options', // Set a different name for your global variable other than the opt_name
				'dev_mode' => false, // Show the time the page took to load, etc
				'customizer' => true, // Enable basic customizer support
				'ajax_save' => true,
				'ajax_max_input' => true,
				'templates_path' => get_template_directory() . '/includes/admin/templates/panel', //Redux Template files

				// OPTIONAL -> Give you extra features
				'page_priority' => null, // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
				'page_parent' => 'themes.php', // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
				'page_permissions' => 'manage_options', // Permissions needed to access the options panel.
				'menu_icon' => get_template_directory_uri() .'/includes/images/adminmenu/theme.png', // Specify a custom URL to an icon
				'last_tab' => '', // Force your panel to always open to a specific tab (by id)
				'page_icon' => 'icon-themes', // Icon displayed in the admin panel next to your menu_title
				'page_slug' => 'fildisi_eutf_options', // Page slug used to denote the panel
				'save_defaults' => true, // On load save the defaults to DB before user clicks save or not
				'default_show' => false, // If true, shows the default value next to each field that is not the default value.
				'default_mark' => '', // What to print by the field's title if the value shown is default. Suggested: *

				// CAREFUL -> These options are for advanced use only
				'output_location' => array( 'frontend', 'admin' ),
				'use_cdn' => false,
				'transient_time' => 60 * MINUTE_IN_SECONDS,
				'output' => true, // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
				'output_tag' => true, // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head

				// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
				'database' => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
				'show_import_export' => true,
				'disable_tracking' => true,
				'allow_tracking' => false,
				'help_tabs' => array(),
				'help_sidebar' => '',
			);

			// Panel Intro text -> before the form
			$this->args['intro_text'] ='';

			// Add content after the form.
			$this->args['footer_text'] = '';
			$this->args['footer_credit'] = ' ';

		}

	}

	global $fildisi_eutf_redux_framework;
	$fildisi_eutf_redux_framework = new Fildisi_Eutf_Redux_Framework_config();
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
