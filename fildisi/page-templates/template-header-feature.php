<?php
/*
Template Name: Header and Feature Only
*/
?>
<?php get_header(); ?>

<?php the_post(); ?>

			<!-- SIDE AREA -->
			<?php
				$fildisi_eutf_sidearea_data = fildisi_eutf_get_sidearea_data();
				fildisi_eutf_print_side_area( $fildisi_eutf_sidearea_data );
			?>
			<!-- END SIDE AREA -->

			<!-- HIDDEN MENU -->
			<?php fildisi_eutf_print_hidden_menu(); ?>
			<!-- END HIDDEN MENU -->

			<?php fildisi_eutf_print_search_modal(); ?>
			<?php fildisi_eutf_print_form_modals(); ?>
			<?php fildisi_eutf_print_language_modal(); ?>
			<?php fildisi_eutf_print_login_modal(); ?>
			<?php fildisi_eutf_print_social_modal(); ?>

			</div> <!-- end #eut-theme-content -->
		</div> <!-- end #eut-theme-wrapper -->

		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>