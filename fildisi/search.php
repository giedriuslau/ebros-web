<?php get_header(); ?>


<?php
	fildisi_eutf_print_header_title('search_page');

	$search_mode = fildisi_eutf_option( 'search_page_mode', 'masonry' );
	$wrapper_attributes = array();

	if ( 'small' != $search_mode) {

		$columns_large_screen  = fildisi_eutf_option( 'search_page_columns_large_screen', '3' );
		$columns = fildisi_eutf_option( 'search_page_columns', '3' );
		$columns_tablet_landscape  = fildisi_eutf_option( 'search_page_columns_tablet_landscape', '2' );
		$columns_tablet_portrait  = fildisi_eutf_option( 'search_page_columns_tablet_portrait', '2' );
		$columns_mobile  = fildisi_eutf_option( 'search_page_columns_mobile', '1' );
		$search_shadow  = fildisi_eutf_option( 'search_page_shadow_style', 'shadow-mode' );
		if ( 'grid' == $search_mode) {
			$search_mode = 'fitRows';
		}

		$wrapper_attributes[] = 'data-columns-large-screen="' . esc_attr( $columns_large_screen ) . '"';
		$wrapper_attributes[] = 'data-columns="' . esc_attr( $columns ) . '"';
		$wrapper_attributes[] = 'data-columns-tablet-landscape="' . esc_attr( $columns_tablet_landscape ) . '"';
		$wrapper_attributes[] = 'data-columns-tablet-portrait="' . esc_attr( $columns_tablet_portrait ) . '"';
		$wrapper_attributes[] = 'data-columns-mobile="' . esc_attr( $columns_mobile ) . '"';
		$wrapper_attributes[] = 'data-layout="' . esc_attr( $search_mode ) . '"';
		$wrapper_attributes[] = 'data-gutter-size="30"';
		$wrapper_attributes[] = 'data-spinner="no"';

		$search_classes = array( 'eut-blog', 'eut-blog-columns', 'eut-isotope', 'eut-with-gap' );
		if( 'shadow-mode' == $search_shadow ){
			$search_classes[] = 'eut-with-shadow';
		}
	} else {
		$search_classes = array( 'eut-blog', 'eut-blog-small', 'eut-non-isotope' );
	}

	$search_class_string = implode( ' ', $search_classes );
	$wrapper_attributes[] = 'class="' . esc_attr( $search_class_string ) . '"';

?>

<!-- CONTENT -->
<div id="eut-content" class="clearfix <?php echo fildisi_eutf_sidebar_class( 'search_page' ); ?>">
	<div class="eut-content-wrapper">
		<!-- MAIN CONTENT -->
		<div id="eut-main-content">
			<div class="eut-main-content-wrapper clearfix">

				<div class="eut-section" style="margin-bottom: 0px;">

					<div class="eut-container">
						<!-- ROW -->
						<div class="eut-row">

							<!-- COLUMN 1 -->
							<div class="wpb_column eut-column-1">
							<?php
								if ( have_posts() ) :
							?>
								<div class="eut-column-wrapper">
									<div <?php echo implode( ' ', $wrapper_attributes ); ?>>
										<?php
											$fildisi_eutf_post_items = $fildisi_eutf_page_items = $fildisi_eutf_portfolio_items = $fildisi_eutf_other_post_items = 0;
											$fildisi_eutf_has_post_items = $fildisi_eutf_has_page_items = $fildisi_eutf_has_portfolio_items = 0;

											while ( have_posts() ) : the_post();
												$post_type = get_post_type();
												switch( $post_type ) {
													case 'post':
														 $fildisi_eutf_post_items++;
														 $fildisi_eutf_has_post_items = 1;
													break;
													case 'page':
														 $fildisi_eutf_page_items++;
														 $fildisi_eutf_has_page_items = 1;
													break;
													case 'portfolio':
														 $fildisi_eutf_portfolio_items++;
														 $fildisi_eutf_has_portfolio_items = 1;
													break;
													default:
														$fildisi_eutf_other_post_items++;
													break;
												}
											endwhile;
											$fildisi_eutf_item_types = $fildisi_eutf_has_post_items + $fildisi_eutf_has_page_items + $fildisi_eutf_has_portfolio_items;

											if ( $fildisi_eutf_item_types > 1 ) {
										?>
										<div class="eut-filter eut-link-text eut-list-divider eut-align-left">
											<ul>
												<li data-filter="*" class="selected"><?php esc_html_e( "All", 'fildisi' ); ?></li>
												<?php if ( $fildisi_eutf_has_post_items ) { ?>
												<li data-filter=".post"><?php esc_html_e( "Post", 'fildisi' ); ?></li>
												<?php } ?>
												<?php if ( $fildisi_eutf_has_page_items ) { ?>
												<li data-filter=".page"><?php esc_html_e( "Page", 'fildisi' ); ?></li>
												<?php } ?>
												<?php if ( $fildisi_eutf_has_portfolio_items ) { ?>
												<li data-filter=".portfolio"><?php esc_html_e( "Portfolio", 'fildisi' ); ?></li>
												<?php } ?>
											</ul>
										</div>
										<?php
											}
											if ( 'small' == $search_mode ) {
												echo '<div class="eut-standard-container">';
												while ( have_posts() ) : the_post();
												get_template_part( 'templates/search', 'small' );
												endwhile;
												echo '</div>';
											} else {
												echo '<div class="eut-isotope-container">';
												while ( have_posts() ) : the_post();
												get_template_part( 'templates/search', 'masonry' );
												endwhile;
												echo '</div>';
											}
											// Previous/next post navigation.
											fildisi_eutf_paginate_links();
										?>
									</div>
								</div>
								<?php
									else :
										// If no content, include the "No posts found" template.
										get_template_part( 'content', 'none' );
									endif;
								?>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
		<!-- END MAIN CONTENT -->

		<?php fildisi_eutf_set_current_view( 'search_page' ); ?>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer();

//Omit closing PHP tag to avoid accidental whitespace output errors.
