<?php
/**
 * Empty cart page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if ( version_compare( WC_VERSION, '3.5.0', '<' ) ) {
	wc_print_notices();
}

?>

<?php do_action( 'woocommerce_cart_is_empty' ); ?>

<div class="eut-empty-cart cart-empty">
	<div class="eut-h6"><?php esc_html_e( 'No products in the cart.', 'woocommerce' ); ?></div>
	<?php if ( wc_get_page_id( 'shop' ) > 0 ) : ?>
	<a class="eut-link-text eut-text-primary-1 eut-text-hover-black" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>"><?php esc_html_e( 'Return to shop', 'woocommerce' ) ?></a>
	<?php endif; ?>
</div>
