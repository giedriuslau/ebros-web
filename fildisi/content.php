<?php
/**
 * The default post template
 */
?>

<?php
if ( is_singular() ) {
	$fildisi_eutf_disable_media = fildisi_eutf_post_meta( '_fildisi_eutf_disable_media' );
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('eut-single-post'); ?> itemscope itemType="http://schema.org/BlogPosting">

		<?php
			if ( fildisi_eutf_visibility( 'post_feature_image_visibility', '1' ) && 'yes' != $fildisi_eutf_disable_media && has_post_thumbnail() ) {
				if( fildisi_eutf_option( 'has_sidebar' ) ) {
					$image_size = "medium_large";
				} else {
					$image_size = "large";
				}
		?>
		<div id="eut-single-media">
			<div class="eut-container">
				<div class="eut-media clearfix">
					<?php the_post_thumbnail( $image_size ); ?>
				</div>
			</div>
		</div>
		<?php
			}
		?>

		<div id="eut-single-content">
			<?php fildisi_eutf_print_post_simple_title(); ?>
			<?php fildisi_eutf_print_post_structured_data(); ?>
			<div itemprop="articleBody">
				<?php the_content(); ?>
			</div>
		</div>
	</article>

<?php
} else {

	$blog_mode = fildisi_eutf_option( 'blog_mode', 'large' );

	$post_style = fildisi_eutf_post_meta( '_fildisi_eutf_post_standard_style' );
	$bg_mode = false;

	if ( ( 'masonry' == $blog_mode || 'grid' == $blog_mode ) && 'fildisi' == $post_style ) {
		$bg_mode = true;
	}
	if ( $bg_mode ) {
		$fildisi_eutf_post_class = fildisi_eutf_get_post_class("eut-style-2");
		$bg_color = fildisi_eutf_post_meta( '_fildisi_eutf_post_standard_bg_color', 'black' );
		$bg_opacity = fildisi_eutf_post_meta( '_fildisi_eutf_post_standard_bg_opacity', '70' );
		$bg_options = array(
			'bg_color' => $bg_color,
			'bg_opacity' => $bg_opacity,
		);
	} else {
		$fildisi_eutf_post_class = fildisi_eutf_get_post_class();
	}

?>
	<!-- Article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class( $fildisi_eutf_post_class ); ?> itemscope itemType="http://schema.org/BlogPosting">
		<?php do_action( 'fildisi_eutf_inner_post_loop_item_before' ); ?>
		<?php if ( $bg_mode ) { ?>
		<?php fildisi_eutf_print_post_bg_image_container( $bg_options ); ?>
		<?php } else { ?>
		<?php fildisi_eutf_print_post_feature_media( 'image' ); ?>
		<?php } ?>
		<div class="eut-post-content-wrapper">
			<div class="eut-post-content">
				<?php fildisi_eutf_print_post_meta_top(); ?>
				<?php fildisi_eutf_print_post_structured_data(); ?>
				<div itemprop="articleBody">
					<?php fildisi_eutf_print_post_excerpt(); ?>
				</div>
			</div>
		</div>
		<?php do_action( 'fildisi_eutf_inner_post_loop_item_after' ); ?>
	</article>
	<!-- End Article -->

<?php

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
