<?php

/*
*	Admin screen functions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

function fildisi_eutf_admin_menu(){
	if ( current_user_can( 'edit_theme_options' ) ) {
		add_menu_page( 'Fildisi', 'Fildisi', 'edit_theme_options', 'fildisi', 'fildisi_eutf_admin_page_welcome', get_template_directory_uri() .'/includes/images/adminmenu/theme.png', 4 );
		add_submenu_page( 'fildisi', esc_html__('Welcome','fildisi'), esc_html__('Welcome','fildisi'), 'edit_theme_options', 'fildisi', 'fildisi_eutf_admin_page_welcome' );
		add_submenu_page( 'fildisi', esc_html__('Status','fildisi'), esc_html__('Status','fildisi'), 'edit_theme_options', 'fildisi-status', 'fildisi_eutf_admin_page_status' );
		add_submenu_page( 'fildisi', esc_html__( 'Custom Sidebars', 'fildisi' ), esc_html__( 'Custom Sidebars', 'fildisi' ), 'edit_theme_options','fildisi-sidebars','fildisi_eutf_admin_page_sidebars');
		add_submenu_page( 'fildisi', esc_html__( 'Import Demos', 'fildisi' ), esc_html__( 'Import Demos', 'fildisi' ), 'edit_theme_options','fildisi-import','fildisi_eutf_admin_page_import');
	}
}

add_action( 'admin_menu', 'fildisi_eutf_admin_menu' );


function fildisi_eutf_tgmpa_plugins_links(){
	fildisi_eutf_print_admin_links('plugins');
}
add_action( 'fildisi_eutf_before_tgmpa_plugins', 'fildisi_eutf_tgmpa_plugins_links' );

function fildisi_eutf_admin_page_welcome(){
	require_once get_template_directory() . '/includes/admin/pages/eut-admin-page-welcome.php';
}
function fildisi_eutf_admin_page_status(){
	require_once get_template_directory() . '/includes/admin/pages/eut-admin-page-status.php';
}
function fildisi_eutf_admin_page_sidebars(){
	require_once get_template_directory() . '/includes/admin/pages/eut-admin-page-sidebars.php';
}
function fildisi_eutf_admin_page_import(){
	require_once get_template_directory() . '/includes/admin/pages/eut-admin-page-import.php';
}

function fildisi_eutf_print_admin_links( $active_tab = 'status' ) {
?>
<h2 class="nav-tab-wrapper">
	<a href="?page=fildisi" class="nav-tab <?php echo 'welcome' == $active_tab ? 'nav-tab-active' : ''; ?>"><?php echo esc_html__('Welcome','fildisi'); ?></a>
	<a href="?page=fildisi-status" class="nav-tab <?php echo 'status' == $active_tab ? 'nav-tab-active' : ''; ?>"><?php echo esc_html__('Status','fildisi'); ?></a>
	<a href="?page=fildisi-sidebars" class="nav-tab <?php echo 'sidebars' == $active_tab ? 'nav-tab-active' : ''; ?>"><?php echo esc_html__('Custom Sidebars','fildisi'); ?></a>
	<a href="?page=fildisi-import" class="nav-tab <?php echo 'import' == $active_tab ? 'nav-tab-active' : ''; ?>"><?php echo esc_html__('Import Demos','fildisi'); ?></a>
	<a href="?page=fildisi-tgmpa-install-plugins" class="nav-tab <?php echo 'plugins' == $active_tab ? 'nav-tab-active' : ''; ?>"><?php echo esc_html__('Theme Plugins','fildisi'); ?></a>
	<?php do_action( 'fildisi_eutf_admin_links', $active_tab ); ?>
</h2>
<?php
}

//Omit closing PHP tag to avoid accidental whitespace output errors.