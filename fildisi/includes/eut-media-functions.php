<?php

/*
 *	Media functions
 *
 * 	@version	1.0
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
 */


 /**
 * Generic function that prints a slider/carousel navigation
 */
function fildisi_eutf_element_navigation( $navigation_type = 0, $navigation_color = 'dark' ) {

	$output = '';

	if ( 0 != $navigation_type ) {

		switch( $navigation_type ) {

			case '2':
				$icon_nav_prev = 'eut-icon-nav-left';
				$icon_nav_next = 'eut-icon-nav-right';
				break;
			case '3':
				$icon_nav_prev = 'eut-icon-nav-left-small';
				$icon_nav_next = 'eut-icon-nav-right-small';
				break;
			case '4':
				$icon_nav_prev = 'eut-icon-nav-left';
				$icon_nav_next = 'eut-icon-nav-right';
				break;
			default:
				$navigation_type = '1';
				$icon_nav_prev = 'eut-icon-nav-left-small';
				$icon_nav_next = 'eut-icon-nav-right-small';
				break;
		}

		$output .= '<div class="eut-carousel-navigation eut-' . esc_attr( $navigation_color ) . ' eut-navigation-' . esc_attr( $navigation_type ) . '">';
		$output .= '	<div class="eut-carousel-buttons">';
		$output .= '		<div class="eut-carousel-prev">';
		$output .= '			<i class="' . esc_attr( $icon_nav_prev ) . '"></i>';
		$output .= '		</div>';
		$output .= '		<div class="eut-carousel-next">';
		$output .= '			<i class="' . esc_attr( $icon_nav_next ) . '"></i>';
		$output .= '		</div>';
		$output .= '	</div>';
		$output .= '</div>';
	}

	return 	$output;

}

/**
 * Generic function that prints a slider or gallery
 */
function fildisi_eutf_print_gallery_slider( $gallery_mode, $slider_items , $image_size_slider = 'fildisi-eutf-large-rect-horizontal', $extra_class = "") {

	if ( empty( $slider_items ) ) {
		return;
	}

	$image_link_mode = "";

	$image_size_gallery_thumb = 'fildisi-eutf-small-rect-horizontal';
	if( 'gallery-vertical' == $gallery_mode ) {
		$image_size_gallery_thumb = $image_size_slider;
	}

	if ( 'gallery' == $gallery_mode || '' == $gallery_mode ) {

		$columns_large_screen = 3;
		$columns = 3;
		$columns_tablet_landscape  = 2;
		$columns_tablet_portrait  = 2;
		$columns_mobile  = 1;
		$gutter_size = 30;
		if ( is_singular( 'portfolio' ) ) {
			$portfolio_media_fullwidth = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_media_fullwidth' );
			if ( 'yes' == $portfolio_media_fullwidth ) {
				$columns_large_screen = 4;
				$columns = 4;
			}
			$image_link_mode = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_media_image_link_mode' );
		}

		$wrapper_attributes = array();

		$gallery_classes = array( 'eut-gallery' , 'eut-isotope', 'eut-with-gap' );
		if( empty( $image_link_mode ) ){
			$gallery_classes[] = 'eut-gallery-popup';
		}
		$gallery_class_string = implode( ' ', $gallery_classes );

		$wrapper_attributes[] = 'class="' . esc_attr( $gallery_class_string ) . '"';
		$wrapper_attributes[] = 'data-gutter-size="' . esc_attr( $gutter_size ) . '"';
		$wrapper_attributes[] = 'data-columns-large-screen="' . esc_attr( $columns_large_screen ) . '"';
		$wrapper_attributes[] = 'data-columns="' . esc_attr( $columns ) . '"';
		$wrapper_attributes[] = 'data-columns-tablet-landscape="' . esc_attr( $columns_tablet_landscape ) . '"';
		$wrapper_attributes[] = 'data-columns-tablet-portrait="' . esc_attr( $columns_tablet_portrait ) . '"';
		$wrapper_attributes[] = 'data-columns-mobile="' . esc_attr( $columns_mobile ) . '"';
		$wrapper_attributes[] = 'data-layout="fitRows"';
?>
		<div <?php echo implode( ' ', $wrapper_attributes ); ?>>
			<div class="eut-isotope-container">
<?php

		foreach ( $slider_items as $slider_item ) {

			$media_id = $slider_item['id'];
			$full_src = wp_get_attachment_image_src( $media_id, 'fildisi-eutf-fullscreen' );
			$image_full_url = $full_src[0];

			$caption = get_post_field( 'post_excerpt', $media_id );
			$figcaption = '';
			if	( !empty( $caption ) ) {
				$figcaption = wptexturize( $caption );
			}
?>
				<div class="eut-isotope-item eut-hover-item eut-hover-style-none">
					<div class="eut-isotope-item-inner">
					<?php if( empty( $image_link_mode ) ){ ?>
						<figure class="eut-image-hover eut-zoom-none">
							<a class="eut-item-url" data-title="<?php echo esc_attr( $figcaption ); ?>" href="<?php echo esc_url( $image_full_url ); ?>"></a>
							<div class="eut-hover-overlay eut-bg-light eut-opacity-30"></div>
							<div class="eut-media">
								<?php echo wp_get_attachment_image( $media_id, $image_size_gallery_thumb ); ?>
							</div>
						</figure>
					<?php } else { ?>
						<figure class="eut-zoom-none">
							<div class="eut-media">
								<?php echo wp_get_attachment_image( $media_id, $image_size_gallery_thumb ); ?>
							</div>
						</figure>
					<?php } ?>
					</div>
				</div>
<?php

		}
?>
			</div>
		</div>
<?php


	} elseif ( 'gallery-vertical' == $gallery_mode ) {
		if ( is_singular( 'portfolio' ) ) {
			$image_link_mode = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_media_image_link_mode' );
		}
?>
		<div class="eut-media">
			<ul class="eut-post-gallery eut-post-gallery-popup <?php echo esc_attr( $extra_class ); ?>">
<?php

		foreach ( $slider_items as $slider_item ) {

			$media_id = $slider_item['id'];
			$full_src = wp_get_attachment_image_src( $media_id, 'fildisi-eutf-fullscreen' );
			$image_full_url = $full_src[0];

			$caption = get_post_field( 'post_excerpt', $media_id );
			$figcaption = '';
			if	( !empty( $caption ) ) {
				$figcaption = wptexturize( $caption );
			}
			if( empty( $image_link_mode ) ){
				echo '<li class="eut-image-hover">';
				echo '<a data-title="' . esc_attr( $figcaption ) . '" href="' . esc_url( $image_full_url ) . '">';
				echo wp_get_attachment_image( $media_id, $image_size_gallery_thumb );
				echo '</a>';
				echo '</li>';
			} else {
				echo '<li>';
				echo wp_get_attachment_image( $media_id, $image_size_gallery_thumb );
				echo '</li>';
			}
		}
?>
			</ul>
		</div>
<?php

	} else {

		$slider_settings = array();
		if ( is_singular( 'post' ) || is_singular( 'portfolio' ) ) {
			if ( is_singular( 'post' ) ) {
				$slider_settings = fildisi_eutf_post_meta( '_fildisi_eutf_post_slider_settings' );
			} else {
				$slider_settings = fildisi_eutf_post_meta( '_fildisi_eutf_portfolio_slider_settings' );
			}
		}
		$slider_speed = fildisi_eutf_array_value( $slider_settings, 'slideshow_speed', '2500' );
		$slider_dir_nav = fildisi_eutf_array_value( $slider_settings, 'direction_nav', '1' );
		$slider_dir_nav_color = fildisi_eutf_array_value( $slider_settings, 'direction_nav_color', 'dark' );

		$image_atts = array();
		if( 'blog-slider' == $gallery_mode ) {
			$image_atts = fildisi_eutf_get_blog_image_atts();
		}

?>
		<div class="eut-media clearfix">
			<div class="eut-element eut-slider eut-layout-1">
				<div class="eut-carousel-wrapper eut-<?php echo esc_attr( $slider_dir_nav_color ); ?>">
					<?php echo fildisi_eutf_element_navigation( $slider_dir_nav, $slider_dir_nav_color ); ?>
					<div class="eut-slider-element " data-slider-speed="<?php echo esc_attr( $slider_speed ); ?>" data-slider-pause="yes" data-slider-autoheight="no">
<?php
						foreach ( $slider_items as $slider_item ) {
							$media_id = $slider_item['id'];
							echo '<div class="eut-slider-item">';
							echo wp_get_attachment_image( $media_id, $image_size_slider, '', $image_atts );
							echo '</div>';

						}
?>
					</div>
				</div>
			</div>
		</div>
<?php
	}
}

/**
 * Generic function that prints video settings ( HTML5 )
 */

if ( !function_exists( 'fildisi_eutf_print_media_video_settings' ) ) {
	function fildisi_eutf_print_media_video_settings( $video_settings ) {
		$video_attr = '';

		if ( !empty( $video_settings ) ) {

			$video_poster = fildisi_eutf_array_value( $video_settings, 'poster' );
			$video_preload = fildisi_eutf_array_value( $video_settings, 'preload', 'metadata' );

			if( 'yes' == fildisi_eutf_array_value( $video_settings, 'controls' ) ) {
				$video_attr .= ' controls';
			}
			if( 'yes' == fildisi_eutf_array_value( $video_settings, 'loop' ) ) {
				$video_attr .= ' loop="loop"';
			}
			if( 'yes' ==  fildisi_eutf_array_value( $video_settings, 'muted' ) ) {
				$video_attr .= ' muted="muted"';
			}
			if( 'yes' == fildisi_eutf_array_value( $video_settings, 'playsinline' ) ) {
				$video_attr .= ' playsinline';
			}
			if( 'yes' == fildisi_eutf_array_value( $video_settings, 'autoplay' ) ) {
				$video_attr .= ' autoplay="autoplay"';
			}
			if( !empty( $video_poster ) ) {
				$video_attr .= ' poster="' . esc_url( $video_poster ) . '"';
			}
			$video_attr .= ' preload="' . esc_attr( $video_preload ) . '"';

		}
		return $video_attr;
	}
}

/**
 * Generic function that prints a video ( Embed or HTML5 )
 */
function fildisi_eutf_print_media_video( $video_mode, $video_webm, $video_mp4, $video_ogv, $video_embed, $video_poster = '' ) {
	global $wp_embed;

	if( empty( $video_mode ) ) {
		if ( !empty( $video_embed ) ) {
			echo '<div class="eut-media">' . $wp_embed->run_shortcode( '[embed]' . $video_embed . '[/embed]' ) . '</div>';
		}
	} elseif( 'code' == $video_mode ) {
		if ( !empty( $video_embed ) ) {
			echo '<div class="eut-media">' . $video_embed . '</div>';
		}
	} else {
		if ( !empty( $video_webm ) || !empty( $video_mp4 ) || !empty( $video_ogv ) ) {

			$video_settings = array(
				'controls' => 'yes',
				'poster' => $video_poster,
			);
			$video_settings = apply_filters( 'fildisi_eutf_media_video_settings', $video_settings );

			echo '<div class="eut-media">';
			echo '<video ' . fildisi_eutf_print_media_video_settings( $video_settings ) . ' >';

			if ( !empty( $video_webm ) ) {
				echo '<source src="' . esc_url( $video_webm ) . '" type="video/webm">';
			}
			if ( !empty( $video_mp4 ) ) {
				echo '<source src="' . esc_url( $video_mp4 ) . '" type="video/mp4">';
			}
			if ( !empty( $video_ogv ) ) {
				echo '<source src="' . esc_url( $video_ogv ) . '" type="video/ogg">';
			}
			echo'</video>';
			echo '</div>';

		}
	}

}

function fildisi_eutf_get_image_size( $image_mode = 'large' ) {

	switch( $image_mode ) {
		case 'thumbnail':
			$image_size = 'thumbnail';
		break;
		case 'medium':
			$image_size = 'medium';
		break;
		case 'medium_large':
			$image_size = 'medium_large';
		break;
		case 'large':
			$image_size = 'large';
		break;
		case 'square':
			$image_size = 'fildisi-eutf-small-square';
		break;
		case 'landscape':
			$image_size = 'fildisi-eutf-small-rect-horizontal';
		break;
		case 'landscape-medium':
			$image_size = 'fildisi-eutf-medium-rect-horizontal';
		break;
		case 'portrait':
			$image_size = 'fildisi-eutf-small-rect-vertical';
		break;
		case 'portrait-medium':
			$image_size = 'fildisi-eutf-medium-rect-vertical';
		break;
		case 'landscape-large-wide':
			$image_size = 'fildisi-eutf-large-rect-horizontal';
		break;
		case 'fullscreen':
		case 'extra-extra-large':
			$image_size = 'fildisi-eutf-fullscreen';
		break;
		case 'full':
			$image_size = 'full';
		break;
		default:
			$image_size = 'large';
		break;
	}

	return $image_size;

}

function fildisi_eutf_get_fallback_image_attr( $size = 'fildisi-eutf-small-rect-horizontal' ) {

	$image_atts = array();

	switch( $size ) {
		case 'thumbnail':
			$image_atts['width'] = "150";
			$image_atts['height'] = "150";
		break;
		case 'medium':
			$image_atts['width'] = "300";
			$image_atts['height'] = "300";
		break;
		case 'large':
			$image_atts['width'] = "1024";
			$image_atts['height'] = "768";
		break;
		case 'fildisi-eutf-small-square':
			$image_atts['width'] = "560";
			$image_atts['height'] = "560";
		break;
		case 'fildisi-eutf-medium-square':
			$image_atts['width'] = "900";
			$image_atts['height'] = "900";
		break;
		case 'fildisi-eutf-small-rect-horizontal':
			$image_atts['width'] = "560";
			$image_atts['height'] = "420";
		break;
		case 'fildisi-eutf-medium-rect-horizontal':
			$image_atts['width'] = "900";
			$image_atts['height'] = "675";
		break;
		case 'fildisi-eutf-small-rect-vertical':
			$image_atts['width'] = "560";
			$image_atts['height'] = "745";
		break;
		case 'fildisi-eutf-medium-rect-vertical':
			$image_atts['width'] = "840";
			$image_atts['height'] = "1120";
		break;
		case 'fildisi-eutf-fullscreen':
		default:
			$size = 'full';
			$image_atts['width'] = "1920";
			$image_atts['height'] = "1080";
		break;
	}
	$placeholder_mode = fildisi_eutf_option( 'placeholder_mode', 'dummy' );
	$placeholder_mode =  apply_filters( 'fildisi_eutf_placeholder_mode', $placeholder_mode );
	switch( $placeholder_mode ) {
		case 'placehold':
			$image_atts['url'] = 'https://placehold.it/' . $image_atts['width'] . 'x' . $image_atts['height'];
		break;
		case 'unsplash':
			$image_atts['url'] = 'https://source.unsplash.com/category/people/' . $image_atts['width'] . 'x' . $image_atts['height'] . '?sig=' . uniqid();
		break;
		case 'dummy':
		default:
			$image_atts['url'] =  get_template_directory_uri() . '/images/empty/' . $size . '.jpg';
		break;
	}
	$image_atts['class'] = 'attachment-' . $size . ' size-' . $size ;
	$image_atts['alt'] = "Dummy Image";

	return $image_atts;

}

function fildisi_eutf_get_fallback_image( $size = 'fildisi-eutf-small-rect-horizontal', $mode = '' ) {
	$html = '';
	$image_atts = fildisi_eutf_get_fallback_image_attr( $size );
	if( 'url' == $mode ) {
		$html = $image_atts['url'];
	} else {
		$html = '<img class="' . esc_attr( $image_atts['class'] ) . '" alt="' . esc_attr( $image_atts['alt'] ) . '" src="' . esc_url( $image_atts['url'] ) . '" width="' . esc_attr( $image_atts['width'] ) . '" height="' . esc_attr( $image_atts['height'] ) . '">';
	}
	return $html;
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
