<?php

/*
*	Layout Helper functions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

/**
 * Function to fetch sidebar class
 */
function fildisi_eutf_sidebar_class( $sidebar_view = '' ) {

	$fildisi_eutf_sidebar_class = "";
	$fildisi_eutf_sidebar_extra_content = false;

	if ( 'search_page' == $sidebar_view ) {
		$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'search_page_sidebar' );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'search_page_layout', 'none' );
	} else if ( 'forum' == $sidebar_view ) {
		$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'forum_sidebar' );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'forum_layout', 'none' );
	} else if ( 'shop' == $sidebar_view ) {
		if ( is_search() ) {
			$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'product_search_sidebar' );
			$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'product_search_layout', 'none' );
		} else if ( is_shop() ) {
			$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta_shop( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'page_sidebar' ) );
			$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta_shop( '_fildisi_eutf_layout', fildisi_eutf_option( 'page_layout', 'none' ) );
		} else if( is_product() ) {
			$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'product_sidebar' ) );
			$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'product_layout', 'none' ) );
		} else {
			$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'product_tax_sidebar' );
			$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'product_tax_layout', 'none' );
		}
	} else if ( 'event' == $sidebar_view ) {
		if ( is_singular( 'tribe_events' ) ) {
			$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'event_sidebar' ) );
			$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'event_layout', 'none' ) );
		} else {
			$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'event_tax_sidebar' );
			$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'event_tax_layout', 'none' );
		}
	} else if ( is_singular() ) {
		if ( is_singular( 'post' ) ) {
			$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'post_sidebar' ) );
			$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'post_layout', 'none' ) );
		} else if ( is_singular( 'portfolio' ) ) {
			$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'portfolio_sidebar' ) );
			$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'portfolio_layout', 'none' ) );
			$fildisi_eutf_sidebar_extra_content = fildisi_eutf_check_portfolio_details();
			if( $fildisi_eutf_sidebar_extra_content && 'none' == $fildisi_eutf_sidebar_layout ) {
				$fildisi_eutf_sidebar_layout = 'right';
			}
		} else {
			$fildisi_eutf_sidebar_id = fildisi_eutf_post_meta( '_fildisi_eutf_sidebar', fildisi_eutf_option( 'page_sidebar' ) );
			$fildisi_eutf_sidebar_layout = fildisi_eutf_post_meta( '_fildisi_eutf_layout', fildisi_eutf_option( 'page_layout', 'none' ) );
		}
	} else {
		$fildisi_eutf_sidebar_id = fildisi_eutf_option( 'blog_sidebar' );
		$fildisi_eutf_sidebar_layout = fildisi_eutf_option( 'blog_layout', 'none' );
	}

	if ( 'none' != $fildisi_eutf_sidebar_layout && ( is_active_sidebar( $fildisi_eutf_sidebar_id ) || $fildisi_eutf_sidebar_extra_content ) ) {

		if ( 'right' == $fildisi_eutf_sidebar_layout ) {
			$fildisi_eutf_sidebar_class = 'eut-right-sidebar';
		} else if ( 'left' == $fildisi_eutf_sidebar_layout ) {
			$fildisi_eutf_sidebar_class = 'eut-left-sidebar';
		}

	}

	if( !empty( $fildisi_eutf_sidebar_class ) ) {
		global $fildisi_eutf_options;
		$fildisi_eutf_options['has_sidebar'] = "1";
	}

	$fildisi_eutf_sidebar_class = apply_filters( 'fildisi_eutf_sidebar_class', $fildisi_eutf_sidebar_class, $sidebar_view );

	return $fildisi_eutf_sidebar_class;

}


/**
 * Navigation Bar
 */

if ( !function_exists('fildisi_eutf_nav_bar') ) {

	function fildisi_eutf_nav_bar( $post_type = 'post', $mode = '') {

		global $post;

		$has_nav_section = false;

		if ( 'product' == $post_type ) {
			$eut_in_same_term = fildisi_eutf_option( 'product_nav_same_term' );
			if( $eut_in_same_term ) {
				$eut_in_same_term = true;
			} else {
				$eut_in_same_term = false;
			}
			$prev_post = get_adjacent_post( $eut_in_same_term, '', true, 'product_cat');
			$next_post = get_adjacent_post( $eut_in_same_term, '', false, 'product_cat' );

			if ( fildisi_eutf_visibility( 'product_nav_visibility', '1' )  && ( is_a( $prev_post, 'WP_Post' ) || is_a( $next_post, 'WP_Post' ) ) ) {
				$has_nav_section = true;
			}
		} elseif( 'portfolio' == $post_type ) {
			$eut_nav_term = fildisi_eutf_option( 'portfolio_nav_term', 'none' );
			if( 'none' != $eut_nav_term ) {
				$eut_in_same_term = true;
			} else {
				$eut_in_same_term = false;
				$eut_nav_term = 'portfolio_category';
			}
			$prev_post = get_adjacent_post( $eut_in_same_term, '', true, $eut_nav_term );
			$next_post = get_adjacent_post( $eut_in_same_term, '', false, $eut_nav_term );
			if ( fildisi_eutf_visibility( 'portfolio_nav_visibility', '1' )  && ( is_a( $prev_post, 'WP_Post' ) || is_a( $next_post, 'WP_Post' ) ) ) {
				$has_nav_section = true;
			}
		} elseif( 'event' == $post_type && fildisi_eutf_events_calendar_enabled() ) {
			$prev_post = Tribe__Events__Main::instance()->get_closest_event( $post, 'previous' ) ;
			$next_post = Tribe__Events__Main::instance()->get_closest_event( $post, 'next' ) ;
			if ( fildisi_eutf_visibility( 'event_nav_visibility', '1' )  && ( is_a( $prev_post, 'WP_Post' ) || is_a( $next_post, 'WP_Post' ) ) ) {
				$has_nav_section = true;
			}
		}  else {
			$eut_in_same_term = fildisi_eutf_visibility( 'post_nav_same_term', '0' );
			$prev_post = get_adjacent_post( $eut_in_same_term, '', true);
			$next_post = get_adjacent_post( $eut_in_same_term, '', false);
			if ( fildisi_eutf_visibility( 'post_nav_visibility', '1' )  && ( is_a( $prev_post, 'WP_Post' ) || is_a( $next_post, 'WP_Post' ) ) ) {
				$has_nav_section = true;
			}
		}


		if ( 'check' == $mode ) {
			return $has_nav_section;
		}

		$eut_backlink = $eut_backlink_url = $eut_backlink_title = '';

		if ( 'event' == $post_type && fildisi_eutf_events_calendar_enabled() ) {
			$eut_backlink_url = tribe_get_events_link();
			$eut_backlink_title = tribe_get_event_label_plural();
		} else {
			$eut_backlink = fildisi_eutf_post_meta( '_fildisi_eutf_backlink_id', fildisi_eutf_option( $post_type . '_backlink_id' ) );
			if( !empty( $eut_backlink ) ) {
				$eut_backlink = apply_filters( 'wpml_object_id', $eut_backlink, 'page', TRUE  );
				$eut_backlink_url = get_permalink( $eut_backlink );
			}
		}


		if ( $has_nav_section ) {
		?>
			<div id="eut-<?php echo esc_attr( $post_type ); ?>-bar" class="eut-navigation-bar eut-layout-1">
				<div class="eut-container">
					<div class="eut-bar-wrapper eut-align-center">
						<div class="eut-post-bar-item eut-post-navigation">
							<?php if ( is_a( $prev_post, 'WP_Post' ) ) { ?>
							<a class="eut-nav-item eut-prev" href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>">
								<div class="eut-nav-item-wrapper">
									<div class="eut-arrow eut-icon-nav-left"></div>
									<h6 class="eut-title"><?php echo get_the_title( $prev_post->ID ); ?></h6>
								</div>
							</a>
							<?php } ?>
							<?php if ( !empty( $eut_backlink_url ) ) { ?>
							<a class="eut-backlink" href="<?php echo esc_url( $eut_backlink_url ); ?>">
								<i class="eut-backlink-icon"></i>
							</a>
							<?php } ?>
							<?php if ( is_a( $next_post, 'WP_Post' ) ) { ?>
							<a class="eut-nav-item eut-next" href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">
								<div class="eut-nav-item-wrapper">
									<h6 class="eut-title"><?php echo get_the_title( $next_post->ID ); ?></h6>
									<div class="eut-arrow eut-icon-nav-right"></div>
								</div>
							</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
	<?php
		}
	}
}

/**
 * Social Like
 */
 if ( !function_exists('fildisi_eutf_social_like') ) {
	function fildisi_eutf_social_like( $post_type = 'post', $mode = '') {
		$post_likes = fildisi_eutf_option( $post_type . '_social', '', 'eut-likes' );
		if ( !empty( $post_likes  ) ) {
			global $post;
			$post_id = $post->ID;
			if ( 'icon' == $mode ) {
?>
			<div class="eut-like-counter eut-link-text"><i class="fa fa-heart-o"></i><span><?php echo fildisi_eutf_likes( $post_id, 'number' ); ?></span></div>
<?php
			} else {
?>
			<li class="eut-like-counter <?php echo fildisi_eutf_likes( $post_id, 'status' ); ?>"><span><?php echo fildisi_eutf_likes( $post_id ); ?></span></li>
<?php
			}
		}
	}
}

/**
 * Social Bar
 */

if ( !function_exists('fildisi_eutf_social_bar') ) {
	function fildisi_eutf_social_bar( $post_type = 'post', $mode = 'layout-1' ) {

		$has_nav_section = false;

		$eut_socials = fildisi_eutf_option( $post_type . '_social');
		if ( is_array( $eut_socials ) ) {
			$eut_socials = array_filter( $eut_socials );
		} else {
			$eut_socials = '';
		}

		if ( !empty( $eut_socials ) ) {
			$has_nav_section = true;
		}

		if ( 'check' == $mode ) {
			return $has_nav_section;
		}

		if ( $has_nav_section ) {
			global $post;
			$post_id = $post->ID;

			$eut_permalink = get_permalink( $post_id );
			$eut_title = get_the_title( $post_id );
			$eut_email = fildisi_eutf_option( $post_type . '_social', '', 'email' );
			$eut_facebook = fildisi_eutf_option( $post_type . '_social', '', 'facebook' );
			$eut_twitter = fildisi_eutf_option( $post_type . '_social', '', 'twitter' );
			$eut_linkedin = fildisi_eutf_option( $post_type . '_social', '', 'linkedin' );
			$eut_pinterest= fildisi_eutf_option( $post_type . '_social', '', 'pinterest' );
			$eut_reddit = fildisi_eutf_option( $post_type . '_social', '', 'reddit' );
			$eut_tumblr = fildisi_eutf_option( $post_type . '_social', '', 'tumblr' );
			$eut_likes = fildisi_eutf_option( $post_type . '_social', '', 'eut-likes' );
			$eut_email_string = 'mailto:?subject=' . $eut_title . '&body=' . $eut_title . ': ' . $eut_permalink;

		?>
			<div id="eut-<?php echo esc_attr( $post_type ); ?>-social-bar" class="eut-social-bar eut-padding-top-2x eut-padding-bottom-3x eut-border">
			<?php if( 'layout-1' == $mode ) { ?>
				<div class="eut-socials-bar-title eut-link-text eut-heading-color"><?php echo esc_html__( 'Share', 'fildisi' ); ?></div>
			<?php } ?>
				<ul class="eut-bar-socials">
					<?php if ( !empty( $eut_email  ) ) { ?>
					<li><a href="<?php echo esc_url( $eut_email_string ); ?>" title="<?php echo esc_attr( $eut_title ); ?>" class="eut-social-share-email"><i class="fa fa-envelope"></i></a></li>
					<?php } ?>
					<?php if ( !empty( $eut_facebook  ) ) { ?>
					<li><a href="<?php echo esc_url( $eut_permalink ); ?>" title="<?php echo esc_attr( $eut_title ); ?>" class="eut-social-share-facebook"><i class="fa fa-facebook"></i></a></li>
					<?php } ?>
					<?php if ( !empty( $eut_twitter  ) ) { ?>
					<li><a href="<?php echo esc_url( $eut_permalink ); ?>" title="<?php echo esc_attr( $eut_title ); ?>" class="eut-social-share-twitter"><i class="fa fa-twitter"></i></a></li>
					<?php } ?>
					<?php if ( !empty( $eut_linkedin  ) ) { ?>
					<li><a href="<?php echo esc_url( $eut_permalink ); ?>" title="<?php echo esc_attr( $eut_title ); ?>" class="eut-social-share-linkedin"><i class="fa fa-linkedin"></i></a></li>
					<?php } ?>
					<?php if ( !empty( $eut_pinterest  ) ) { ?>
					<li><a href="<?php echo esc_url( $eut_permalink ); ?>" title="<?php echo esc_attr( $eut_title ); ?>" data-pin-img="<?php echo esc_url( fildisi_eutf_get_thumbnail_url() ); ?>" class="eut-social-share-pinterest"><i class="fa fa-pinterest"></i></a></li>
					<?php } ?>
					<?php if ( !empty( $eut_reddit ) ) { ?>
					<li><a href="<?php echo esc_url( $eut_permalink ); ?>" title="<?php echo esc_attr( $eut_title ); ?>" class="eut-social-share-reddit"><i class="fa fa-reddit"></i></a></li>
					<?php } ?>
					<?php if ( !empty( $eut_tumblr ) ) { ?>
					<li><a href="<?php echo esc_url( $eut_permalink ); ?>" title="<?php echo esc_attr( $eut_title ); ?>" class="eut-social-share-tumblr"><i class="fa fa-tumblr"></i></a></li>
					<?php } ?>
					<?php if ( !empty( $eut_likes  ) ) { ?>
					<li><a href="#" class="eut-like-counter-link <?php echo fildisi_eutf_likes( $post_id, 'status' ); ?>" data-post-id="<?php echo esc_attr( $post_id ); ?>"><i class="fa fa-heart"></i><span class="eut-like-counter"><?php echo fildisi_eutf_likes( $post_id, 'number' ); ?></span></a></li>
					<?php } ?>
				</ul>
			</div>
			<!-- End Socials -->
<?php
		}
	}
}

/**
 * Get Thumbnail
 */

if ( !function_exists('fildisi_eutf_get_thumbnail_url') ) {
	function fildisi_eutf_get_thumbnail_url( $image_size = 'fildisi-eutf-small-square' ) {

		if ( has_post_thumbnail() ) {
			$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
			$attachment_src = wp_get_attachment_image_src( $post_thumbnail_id, $image_size );
			$image_src = $attachment_src[0];
		} else {
			$image_src = get_template_directory_uri() . '/images/empty/' . $image_size . '.jpg';
		}
		return $image_src ;
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
