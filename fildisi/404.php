<?php
if ( fildisi_eutf_visibility( 'page_404_header' ) ) {
	get_header();
} else {
	get_header( 'basic' );
}

$section_classes = "eut-section eut-custom-height eut-fullheight";
?>

			<div id="eut-content" class="eut-error-404 clearfix">
				<div class="eut-content-wrapper">
					<div id="eut-main-content">
						<div class="eut-main-content-wrapper eut-padding-none clearfix">

							<div class="<?php echo esc_attr( $section_classes); ?>">
								<div class="eut-container">
									<div class="eut-row">
										<div class="wpb_column eut-column-1">
											<div class="eut-column-wrapper">
												<div class="eut-align-center">

													<div id="eut-content-area">
													<?php
														$fildisi_eutf_404_search_box = fildisi_eutf_option('page_404_search');
														$fildisi_eutf_404_home_button = fildisi_eutf_option('page_404_home_button');
														echo do_shortcode( fildisi_eutf_option( 'page_404_content' ) );
													?>
													</div>

													<br/>

													<?php if ( $fildisi_eutf_404_search_box ) { ?>
													<div class="eut-widget">
														<?php get_search_form(); ?>
													</div>
													<br/>
													<?php } ?>

													<?php if ( $fildisi_eutf_404_home_button ) { ?>
													<div class="eut-element">
														<a class="eut-btn eut-btn-medium eut-round eut-bg-primary-1 eut-bg-hover-black" target="_self" href="<?php echo esc_url( home_url( '/' ) ); ?>">
															<span><?php echo esc_html( get_bloginfo( 'name' ) ); ?></span>
														</a>
													</div>
													<?php } ?>

												</div>

											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

<?php

if ( fildisi_eutf_visibility( 'page_404_footer' ) ) {
	get_footer();
} else {
	if ( fildisi_eutf_visibility( 'page_404_header' ) ) {
		get_footer( 'basic-header' );
	} else {
		get_footer( 'basic' );
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
